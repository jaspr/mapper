# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [5.5.0](https://gitlab.com/jaspr/mapper/compare/5.4.0...5.5.0) (2024-09-27)


### Added

* add Builder::empty to create empty Document ([3068c82](https://gitlab.com/jaspr/mapper/commit/3068c82e045d105c8079b7b469bb6191f6ddcd5e))


### Fixed

* access uninitialized Builder::data ([c26fe97](https://gitlab.com/jaspr/mapper/commit/c26fe9752073d708cefb6ab3e4f899de933c84b5))

## [5.4.0](https://gitlab.com/jaspr/mapper/compare/5.3.0...5.4.0) (2024-06-18)


### Added

* add new naming strategy KebabCaseURLNamingStrategy.php ([a231d1d](https://gitlab.com/jaspr/mapper/commit/a231d1dda159c7888bbaf1b7622c146f10cc169f))


### Fixed

* add missing type member ([9e5f8a6](https://gitlab.com/jaspr/mapper/commit/9e5f8a661146c6f03d7b0abdbcfa7393043f9c87))
* make type kebab-case too ([d299e8f](https://gitlab.com/jaspr/mapper/commit/d299e8fcc6a584871848a85c9970838118a4f689))
* union types ([6d29f8b](https://gitlab.com/jaspr/mapper/commit/6d29f8b2cb1311f4649d48aabea65099b8852660))
* use camelCase for IndexDocument.php keys ([6a95bd0](https://gitlab.com/jaspr/mapper/commit/6a95bd0b28170b1d985af7301ebc20250370ce9b))
* use camelCase for TypeScriptInterfaceBuilder.php export keys ([3a27d01](https://gitlab.com/jaspr/mapper/commit/3a27d0110463db6a10cf3a9ce0c67986a359dc10))

## [5.3.0](https://gitlab.com/jaspr/mapper/compare/5.2.0...5.3.0) (2024-05-12)


### Added

* Add HideLinksIfNoOperationIsSupported processor ([ebaecd6](https://gitlab.com/jaspr/mapper/commit/ebaecd63f5e7d7246fa1c9f336ffa158c464fa0a))

## [5.2.0](https://gitlab.com/jaspr/mapper/compare/5.1.3...5.2.0) (2024-03-06)


### Added

* add ::getRequest method to ParsedRequest.php ([3b14417](https://gitlab.com/jaspr/mapper/commit/3b144172d8177c4d0bc6197844ecb50443e5ddaf))

## [5.1.3](https://gitlab.com/jaspr/mapper/compare/5.1.2...5.1.3) (2024-03-04)


### Fixed

* Fix FieldsetResult bad URL key ([37032ec](https://gitlab.com/jaspr/mapper/commit/37032ec1ca193f9ad6277828082fd9690dfb9a73))

## [5.1.2](https://gitlab.com/jaspr/mapper/compare/5.1.1...5.1.2) (2024-01-15)


### Fixed

* bad url building for related relationship document links ([22e2c22](https://gitlab.com/jaspr/mapper/commit/22e2c22b49eda3e9076629fd016fab9354723528))
* stan errors ([cf9e972](https://gitlab.com/jaspr/mapper/commit/cf9e972ffbc60c0168a41d76c883bacf585720ca))
* wrong variable in condition ([9a72b0e](https://gitlab.com/jaspr/mapper/commit/9a72b0efbc56862b0a9631dbceab74554169bde7))

## [5.1.1](https://gitlab.com/jaspr/mapper/compare/5.1.0...5.1.1) (2023-11-30)


### Fixed

* Fix InclusionParser ([58b5320](https://gitlab.com/jaspr/mapper/commit/58b5320fb894af76950c55091b2037d90ffafddd)), closes [#10](https://gitlab.com/jaspr/mapper/issues/10)


### Changed

* add type docBlock ([65668bc](https://gitlab.com/jaspr/mapper/commit/65668bca0f686b8482684d76e38118aa933ed809))

## [5.1.0](https://gitlab.com/jaspr/mapper/compare/5.0.0...5.1.0) (2023-11-14)


### Fixed

* add query part separator arg ([9beae0a](https://gitlab.com/jaspr/mapper/commit/9beae0a72e970745b8e661a2112cadae0c9ae192)), closes [#7](https://gitlab.com/jaspr/mapper/issues/7)


### Added

* Add Document::addIncluded method ([3a8a5ef](https://gitlab.com/jaspr/mapper/commit/3a8a5ef8f0fce3f36434b20aa7c92e98448b3548)), closes [#9](https://gitlab.com/jaspr/mapper/issues/9)
* Add LinkProcessor::getBaseURL method ([3e92e21](https://gitlab.com/jaspr/mapper/commit/3e92e21739ee74a5cfbc1196c0155df52028140b))

## [5.0.0](https://gitlab.com/jaspr/mapper/compare/4.0.2...5.0.0)


### ⚠ BREAKING CHANGES
* Relative links
* Refactor Inclusion

### Fixed

* relation type in filter field parsing ([5f5370b](https://gitlab.com/jaspr/mapper/commit/5f5370b8479cb7bf20f7fa761db39e9e1ddfb99c))
* force string for param ([7188575](https://gitlab.com/jaspr/mapper/commit/718857580defbd161253fe1aebeef499b990698e))
* revert BC change ([4da2253](https://gitlab.com/jaspr/mapper/commit/4da2253e55a8e5e3711d91c909d1e5e47277000d))
* type converting in TS builder ([3a1233e](https://gitlab.com/jaspr/mapper/commit/3a1233e38fcdb4f8c295b10bfbd74f6b707e745c))
* parser relationship data only if present in parsed body ([375f385](https://gitlab.com/jaspr/mapper/commit/375f385d44b681cc3d97f179808e296382bd63cd))
* Relative links ([12c3243](https://gitlab.com/jaspr/mapper/commit/12c3243ccd910462035155b04e3bcec515136d94))

### Added

* Add export index interface to TS index file ([9361ee0](https://gitlab.com/jaspr/mapper/commit/9361ee032d31640b3328d56b35239a13174b6e52))
* IndexDocument should use URL builder ([4e54103](https://gitlab.com/jaspr/mapper/commit/4e541035b85674109c5c02b0efa24c28c4656701))
* relative base URL, TypeScript interfaces ([0f989e8](https://gitlab.com/jaspr/mapper/commit/0f989e8608450baaaa584d3becc0e4fe50ce9bc5))
* TypeScript interface builder ([2f66c0e](https://gitlab.com/jaspr/mapper/commit/2f66c0e0ad92fdd8b33d6b1d282486fec903079b))

### Changed

* Convertible interface cast as keyable object ([330f246](https://gitlab.com/jaspr/mapper/commit/330f246a1fc25ab578dbb2baced03b2410b1ca58))
* remove unnecessary cases ([16bead7](https://gitlab.com/jaspr/mapper/commit/16bead7f763981df5185dbd927cca95b170b3f5e))
* remove unnecessary meta.collection attribute ([a03feeb](https://gitlab.com/jaspr/mapper/commit/a03feeb867ba241f9bfd14950106a80c93e5dfb8))

## [4.0.2](https://gitlab.com/jaspr/mapper/compare/4.0.1...4.0.2) (2023-10-29)


### Fixed

* path prefix subtract ([04cfb27](https://gitlab.com/jaspr/mapper/commit/04cfb2751881644b3fe209cfa36d4d8b258e6166))

## [4.0.1](https://gitlab.com/jaspr/mapper/compare/4.0.0...4.0.1) (2023-10-25)


### Fixed

* Link can contain URI reference by RFC 3986 ([86d4343](https://gitlab.com/jaspr/mapper/commit/86d4343c022e5ca3aeeec11971fe10ecdd321b10))
* missing query part key in match ([0e6d5a0](https://gitlab.com/jaspr/mapper/commit/0e6d5a02f39b2f41faf2a94cab77a92575839b80))

## [4.0.0](https://gitlab.com/jaspr/mapper/compare/3.0.0...4.0.0) (2023-10-10)


### ⚠ BREAKING CHANGES

* * Pass PathInterface to FilterParserInterface::parse method
* Rename LimitOffsetPagination to OffsetStrategyParser
Rename PagePagination to PageStrategyParser
Change in PaginationInterface
* Remove possibility to make sort, pagination and inclusion optional
* Remove ErrorFactory
Remove DefaultErrorFactory
* JsonApiException removed
* * Remove class Configuration
* Rename namespace URI to Request
* Remove Builder.php, BuilderFactory.php, InclusionCollector.php from Document namespace
* Change OpenAPISpecificationBuilder.php constructor signature

### Added

* add ::setData method to Relationship to allow post-processing data assign ([d139cf2](https://gitlab.com/jaspr/mapper/commit/d139cf2714dfdd650df2a5b046937e497dceb61d))
* Add EncoderFactory ([265fcd2](https://gitlab.com/jaspr/mapper/commit/265fcd251d0ddf529d8b7e559b6284c63de7f17c))
* ErrorCollector ([dc58b82](https://gitlab.com/jaspr/mapper/commit/dc58b828649344fa8227d4a901c7f141a386109c))
* Implements enum type ([176b1d0](https://gitlab.com/jaspr/mapper/commit/176b1d0602ad01c65a7abefdd1c87f09d5c0114c))
* InclusionDocumentProcessor move data receiving to protected methods ::getData to allow custom data fetching ([74b1fc7](https://gitlab.com/jaspr/mapper/commit/74b1fc74ddbb6c703a0b7166df4bd0ce54f653ed))
* support for abstract classes ([1f3f56c](https://gitlab.com/jaspr/mapper/commit/1f3f56cc1618e06b6fa4f5051331c8b04c6ddfa9))


### Changed

* exception management ([90879ba](https://gitlab.com/jaspr/mapper/commit/90879bac70c817f835d4acf1bcb7ef2975db507d))
* Change error processing ([ebda8e6](https://gitlab.com/jaspr/mapper/commit/ebda8e6eff4a592c7cef845eed8301d0e2b8b5f3))
* move json_encode to encode method ([a2503b3](https://gitlab.com/jaspr/mapper/commit/a2503b37b4b7da031ce9f14bbe2a70364d9a6b74))
* Move parsers result to own class ([d70b030](https://gitlab.com/jaspr/mapper/commit/d70b030340bf36d3d9d4169cd2edaa7786eb30f1))
* reformat exception stacking ([23526c4](https://gitlab.com/jaspr/mapper/commit/23526c4224529fdee31d823944447793aba683d9))
* Relationships setData determines withData ([ae2f9f2](https://gitlab.com/jaspr/mapper/commit/ae2f9f2bc93d6a08104fc83630755a3573f22327))
* Relationships setData determines withData ([cdfb8d3](https://gitlab.com/jaspr/mapper/commit/cdfb8d368b5f0309a44ea4ec72ea2fc5378a6a65))
* remove unused exception ([c706a43](https://gitlab.com/jaspr/mapper/commit/c706a4395c583a05accbb2faf4db7c90f1d2c82e))
* Restructure of project ([791586c](https://gitlab.com/jaspr/mapper/commit/791586c86102e6993a310d08c83124648b886db5))
* several changes ([29714a8](https://gitlab.com/jaspr/mapper/commit/29714a89d628d1f3a437784731017a9bf551b7fe))
* use LoggerAwareInterface ([86b8cb0](https://gitlab.com/jaspr/mapper/commit/86b8cb0d0c161e4c3f5ed1a7abae3419f03205f4))
* Use of SPL exceptions ([9e0ada9](https://gitlab.com/jaspr/mapper/commit/9e0ada928760342f4f75ff201d5ac804c39fc22d))


### Fixed

* calling set data on Relationship must switch withData property to true ([88d5935](https://gitlab.com/jaspr/mapper/commit/88d5935a027f3166c67ce335a292a997a7cc50ec))
* fix ResourceSchema ([a497265](https://gitlab.com/jaspr/mapper/commit/a4972656830b058a0dff98b12347bbb6e1c5cdb6))
* fix rewriting meta ([a21a7e3](https://gitlab.com/jaspr/mapper/commit/a21a7e314f8132441845c5d7007e01a8c7d2600e))
* getLink remove string return type ([a361f73](https://gitlab.com/jaspr/mapper/commit/a361f733413033c3d53ca5915a8cd0d3f81ef9e9))
* Change exception type from generic Excetion to RuntimeException ([c77c1cd](https://gitlab.com/jaspr/mapper/commit/c77c1cd3572001369e6d9350ea5ff188e727c9f4))
* if not operation support don't register path ([92e4630](https://gitlab.com/jaspr/mapper/commit/92e4630941f9976f9ccafdb8a76d77d2b7af5a0b))
* make InclusionDocumentProcessor properties protected ([b06459d](https://gitlab.com/jaspr/mapper/commit/b06459d6db372b1745b4b164edd0e2ad5bf74983))
* MetadataExceptions cannot be handled as HTTP Not Found ([cad80bd](https://gitlab.com/jaspr/mapper/commit/cad80bdfd904bed9aacb1839206ac5f90e01455c))
* missing array type handling ([7405cf1](https://gitlab.com/jaspr/mapper/commit/7405cf1565d4c099ef018710328ebf3c34d5ba38))
* ObjectCollection::slice ([37e8fbe](https://gitlab.com/jaspr/mapper/commit/37e8fbe63879f90144cd0109dbeb40041a008304))
* pass artificial count to sliced collection ([baa27ab](https://gitlab.com/jaspr/mapper/commit/baa27ab1c0dda5255203ce96a8bd86a022679c38))
* Path parser bad primary type ([e2c34dc](https://gitlab.com/jaspr/mapper/commit/e2c34dc2db99c2eb4cc0999724ccf8f413e92965))
* RelationshipsResourceProcessor ::getData should be ::getValue ([9b76ef2](https://gitlab.com/jaspr/mapper/commit/9b76ef26f6917600f317c0ea478ee96e3fca200b))
* remove unused ctr param ([c399d38](https://gitlab.com/jaspr/mapper/commit/c399d38b97bd665d0c4c85ce4d73e4e73eb72b23))
* use proper type ([c78b08e](https://gitlab.com/jaspr/mapper/commit/c78b08ed37bf46d366e87026f45c62e37a6f5c4a))

## [3.0.0](https://gitlab.com/jaspr/mapper/compare/2.1.0...3.0.0) (2023-05-14)

### ⚠ BREAKING CHANGES
* NamingStrategy interface now contains only two methods

### Added

* add information if relationship is collection or not to meta ([c1b35d3](https://gitlab.com/jaspr/mapper/commit/c1b35d31e2f502c4181e930ddce65dee57e12542))
* RecommendedNamingStrategy class replace old naming strategies and contains recommended naming of resource type and resource fields
* IndexDocument class used as entrypoint of API

### Changed

* add debug info about attribute type ([c21cdae](https://gitlab.com/jaspr/mapper/commit/c21cdae35fcbe6548bb2d1966e45f1e517dad7ab))
* clearing ([9f86065](https://gitlab.com/jaspr/mapper/commit/9f86065b7e449420f062a41cd2316e5eb6a00807))
* Rename Index to IndexDocument ([1396e3b](https://gitlab.com/jaspr/mapper/commit/1396e3b8b3268a10ce1b766e35df42537170d7ce))


### Fixed

* change default data type of relation to mixed ([547e7c9](https://gitlab.com/jaspr/mapper/commit/547e7c963c123d5cbfea57b6d7e47723ed0cae69))
* return type to be ReflectionNamedType ([e4657ea](https://gitlab.com/jaspr/mapper/commit/e4657ead68e405f6ca9f83b1b89a7657b2881cd7))
* trim tailing slash ([12093fc](https://gitlab.com/jaspr/mapper/commit/12093fc212bdd9c0a9685d4bb87952582a58bde5))
* Union and Intersect reflection type handle ([71b320d](https://gitlab.com/jaspr/mapper/commit/71b320dccf0627f3414599bc6edea4da545f9ca5)), closes [#2](https://gitlab.com/jaspr/mapper/issues/2)

## [2.1.0](https://gitlab.com/jaspr/mapper/compare/2.0.0...2.1.0) (2023-02-01)


### Added

* Resource/Relationship allowed operations ([adf72cc](https://gitlab.com/jaspr/mapper/commit/adf72cc527186a2cbd6d9acb2383b52a1f5721a2))

## [2.0.0](https://gitlab.com/jaspr/mapper/compare/1.0.0...2.0.0) (2023-01-16)


### ⚠ BREAKING CHANGES

* Remove LinkComposer
* Rename URIParser -> Parser
* Rename ClassMetadata methods getAttribute(s) to getAttribute(s)Data, getRelationship(s) to getRelationship(s)Data
* ClassMetadata getAttributes now returns collection of attributes metadata, getRelationships too


### Fixed

* remove AnnotationDriver constructor ([e7e1dab](https://gitlab.com/jaspr/mapper/commit/e7e1dabfa63c09e4ce904684d3d65e86de0b1516))
* fix bug in CameFieldKebabUrl naming strategy ([f79bb33](https://gitlab.com/jaspr/mapper/commit/f79bb33a4e698fda0c6e56b2b2c75253e4e28d19))
* fix missing refactor changes ([d0ea2a5](https://gitlab.com/jaspr/mapper/commit/d0ea2a51880425895f2ca992b22b452e82afc516))


### Added

* add NamingStrategy interface ([15823bb](https://gitlab.com/jaspr/mapper/commit/15823bb7c7143d590d74067bbbc8c9d0cf6e74e8))
* add ExpressionBuilderInterface ([00c2a46](https://gitlab.com/jaspr/mapper/commit/00c2a4665b5c9d58f0f7a854d9ca562bdef29b59))
* Add link for all to LinkFactory response ([d51c5dc](https://gitlab.com/jaspr/mapper/commit/d51c5dc1a9a1eb85b23f2be3a0b0fa0ee0cadce4))
* Add LinkFactory ([05b979f](https://gitlab.com/jaspr/mapper/commit/05b979f97ce05cae2244509f1a478cb50598a5f8))
* add related link to document links if relationship ([0592b3c](https://gitlab.com/jaspr/mapper/commit/0592b3ca0d66fc5f5008fca08890ec07a128ed57))
* add support for dto in attribute ([4eb9087](https://gitlab.com/jaspr/mapper/commit/4eb9087182b7d2ca7f7a50a94411c70d2f0bc849))
* Relationship::withData ([e4fb0c6](https://gitlab.com/jaspr/mapper/commit/e4fb0c67af121ec170b3afd0c5dcb37fedf5fa07))

### Changed

* remove unused methods and properties ([8776a5e](https://gitlab.com/jaspr/mapper/commit/8776a5e8bd48937bc0f361bcd2a864f3a0249f5a))
* make possible use id field in filtering for resources ([5dc1f3e](https://gitlab.com/jaspr/mapper/commit/5dc1f3e555f6ef626c376b0dac1fe26e37e8e406))
* if withData is false, then data is not fetched at all ([1ad6e58](https://gitlab.com/jaspr/mapper/commit/1ad6e58b734a4d18036e6e70ee403b7e14e6def5))
* Implement changes from metadata ([71a0fd7](https://gitlab.com/jaspr/mapper/commit/71a0fd72d61c3a1d2fa448114db001d4c6a106cc))


### Fixed

* pass null if data not provided ([8f9006c](https://gitlab.com/jaspr/mapper/commit/8f9006c960b598d5b0145c3d0da58fe96ef0af5a))
* relationship name translation ([8718f4d](https://gitlab.com/jaspr/mapper/commit/8718f4d86d88680c222f8f7cae7ba146b4e09da5))
* return type of getRelationshipData can be null ([5e61fbb](https://gitlab.com/jaspr/mapper/commit/5e61fbb321247b9c3a3e4d9ec334feb756799944))
* translate id in related link ([3efd8a9](https://gitlab.com/jaspr/mapper/commit/3efd8a9613a8b2bf076d322ca6713417a9498071))

## 1.0.0 (2022-06-20)


### Changed

* moving project ([94e53dd](https://gitlab.com/jaspr/mapper/commit/94e53ddf2dc0124c77a4482930cbd391fb0307b0))


### Fixed

* fix iterable attribute type recognition ([9639fdc](https://gitlab.com/jaspr/mapper/commit/9639fdc61913c880b06f67b8bd2b7f7e901519c1))
* several fixes after moving project ([50c2233](https://gitlab.com/jaspr/mapper/commit/50c22337d7710a8778a727aeffe4e9b0149dfe76))
