# JSON API Mapper

Implementation of [JSON API Standard Specification](https://jsonapi.org/)

This project goal is to create easy-to-use library to implement JSON API specification.

Whole project is at the beginning of development. So don't hesitate to help. I'm open to some good ideas how make this
more customizable and friendly.

Library only provides wrappers to create valid JSON API document. Controllers and Response is on you.

## Issues

You can write [email](mailto://contact-project+jaspr-mapper-37048991-issue-@incoming.gitlab.com) or create issue
in [gitlab](https://gitlab.com/jaspr/mapper/issues)

## Installation

Install library via Composer

```
composer require jaspr/mapper
```

## Basic Usage

> For simplicity, we use `$container` as some dependency provider (Dependency Injection).

### Describing your objects

You can choose which way you want to describe your object metadata.

#### With Annotations

> Note: If you want to use annotations you have to use `AnnotationDriver` in `MetadataFactory`

[Example](tests/Resources/Valid/GettersExample.php)

How you can see, setting up resource object is quiet easy. Just annotate your getter with ``#[Attribute]``
or ``#[Relationship]`` annotation.

#### Schema

The important part is to implement Resource interface. Then fill up static method `getSchema`.

> Note: If you want to use schema you have to use `SchemaDriver` in `MetadataFactory`

[Example](tests/Resources/Valid/PropsExample.php)

### MetadataRepository

To create `MetadataRepository` we must use `MetadataFactory`.

#### Usage

```php
<?php
/** @var $container Psr\Container\ContainerInterface */
// This is cache instance implements PSR SimpleCache
$cache = $container->get( Psr\SimpleCache\CacheInterface::class);
// This is AnnotationDriver or SchemaDriver, depends on your preferences
$driver = $container->get(\JSONAPI\Driver\Driver::class);
// Paths to your object representing resources
$paths = ['paths/to/your/resources','another/path'];
// Factory returns instance of MetadataRepository
$repository = JSONAPI\Factory\MetadataFactory::create(
            $paths,
            $cache,
            $driver
        );

```

### Encoder

#### Options

| Param      | Default | Description                     |
|------------|---------|---------------------------------|
| repository |         | Instance of MetadataRepository. |

#### Usage

```php
<?php

// First we need DocumentBuilderFactory
// Let's get MetadataRepository from DI
/** @var $container Psr\Container\ContainerInterface */
$metadataRepository = $container->get(JSONAPI\Metadata\MetadataRepository::class);
$encoder = \JSONAPI\Mapper\Encoding\EncoderFactory::createDefaultEncoder($metadataRepository)
$data = new \JSONAPI\Mapper\Test\Resources\Valid\GettersExample('id');
/** @var \JSONAPI\Mapper\Document\ResourceObjectIdentifier $identifier */
$identifier = $encoder->identify($data);
/** @var \JSONAPI\Mapper\Document\ResourceObject $resource */
$resource = $encoder->encode($data);
/** @var \JSONAPI\Mapper\Document\Document $document */
$document = $encoder->compose($data);


```

### Request Parser

This object works with url, and parse required keywords as described at JSON API Standard

#### Options

| Param            | Default                | Description                                                                                                                                               |
|------------------|------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| baseUrl          |                        | URL where you API lays. Must end with `/` to work properly with relative links.                                                                           |
| repository       |                        | Instance of MetadataRepository.                                                                                                                           |
| pathParser       | PathParser             | Instance of PathParserInterface. Provides information about path, like resource type, resource ID, relation type, is it collection or is it relationship. |
| paginationParser | OffsetStrategyParser   | Instance of PaginationParserInterface. [Pagination](https://jsonapi.org/format/#fetching-pagination).                                                     |
| sortParser       | SortParser             | Instance of SortParserInterface. [Sort](https://jsonapi.org/format/#fetching-sorting).                                                                    |
| inclusionParser  | InclusionParser        | Instance of InclusionParserInterface. [Inclusion](https://jsonapi.org/format/#fetching-includes).                                                         |
| fieldsetParser   | FieldsetParser         | Instance of FieldsetParserInterface. [Sparse Fields](https://jsonapi.org/format/#fetching-sparse-fieldsets)                                               |
| filterParser     | ExpressionFilterParser | FilterParserInterface instance, which is responsible for parsing filter. [Filter](https://jsonapi.org/format/#fetching-filtering)                         |
| bodyParser       | BodyParser             | Instance of BodyParserInterface.                                                                                                                          |
| logger           | NullLogger             | LoggerInterface instance, PSR compliant logger instance.                                                                                                  |

#### Filter

As described, specification is agnostic about filter implementation. So I created, more like borrowed, expression filter
from OData. So now you can use something like this:

`?filter=stringProperty eq 'string' and contains(stringProperty,'asdf') and intProperty in (1,2,3) or boolProperty ne true and relation.property eq null`

Or if you have simpler use cases you can try QuatrodotFilter:

`?filter=stringProperty::contains::Bonus|boolProperty::eq::true`

#### Pagination

I implement two of three pagination technics

* LimitOffsetPagination
* PagePagination

#### Includes

https://jsonapi.org/format/#fetching-includes

#### Sort

https://jsonapi.org/format/#fetching-sorting

### Index Page

If you want use JASPR SDK to its full potential, consider expose index page.

```php
<?php
$doc      = new JSONAPI\Mapper\IndexDocument(self::$mr, self::$url);
$response = json_encode($doc);
```

which returns something like this:

```json
{
    "jsonapi": {
        "version": "1.0"
    },
    "links": {
        "relation": "https:\/\/unit.test.org\/relation",
        "getter": "https:\/\/unit.test.org\/getter",
        "meta": "https:\/\/unit.test.org\/meta",
        "prop": "https:\/\/unit.test.org\/prop",
        "third": "https:\/\/unit.test.org\/third"
    },
    "meta": {
        "title": "JSON:API Index Page",
        "baseUrl": "https:\/\/unit.test.org/"
    }
}
```

And if your front-end use `jaspr/client-js` library, then you can use `useJsonApiWithIndex` factory to enjoy RESTful
experience.

### Open API Schema

This library provides lightweight wrapper around OAS. It can generate OAS v3.0.3 schema in json,
so you can provide doc for your api easily.

#### Basic Example

```php
    $factory = new OpenAPISpecificationBuilder(
        $metadataRepository,
        'https://your.api.url'
    );

    $info = new Info('JSON:API OAS', '1.0.0');
    $info->setDescription('Test specification');
    $info->setContact(
        (new Contact())
            ->setName('Tomas Benedikt')
            ->setEmail('tomas.benedikt@gmail.com')
            ->setUrl('https://gitlab.com/jaspr')
    );
    $info->setLicense(
        (new License('MIT'))
            ->setUrl('https://gitlab.com/jaspr/mapper/-/blob/5.x/LICENSE')
    );
    $info->setTermsOfService('https://gitlab.com/jaspr/mapper/-/blob/5.x/CONTRIBUTING.md');

    $oas = $factory->create($info);
    $oas->setExternalDocs(new ExternalDocumentation('https://gitlab.com/jaspr/mapper/-/wikis/home'));

    $json = json_encode($oas);
```

> For more examples, try look at [tests](tests)