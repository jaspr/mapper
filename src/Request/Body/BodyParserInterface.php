<?php

/**
 * Created by tomas
 * at 26.05.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Body;

use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\RequestParserInterface;
use JsonException;
use Psr\Http\Message\ServerRequestInterface;

interface BodyParserInterface extends RequestParserInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param ParsedRequest|null $parsedRequest
     * @return Document|null
     * @throws JsonException
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): ?Document;
}
