<?php

/**
 * Created by tomas
 * at 26.05.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Body;

use BackedEnum;
use DateTimeImmutable;
use DateTimeInterface;
use JSONAPI\Mapper\Document\Attribute;
use JSONAPI\Mapper\Document\DefaultNamespace;
use JSONAPI\Mapper\Document\Deserializable;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\Id;
use JSONAPI\Mapper\Document\Relationship;
use JSONAPI\Mapper\Document\ResourceCollection;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\ResourceObjectIdentifier;
use JSONAPI\Mapper\Document\Type;
use JSONAPI\Mapper\ErrorFactory;
use JSONAPI\Mapper\Exception\Messages;
use JSONAPI\Mapper\Exception\MetadataNotFound;
use JSONAPI\Mapper\Metadata\ClassMetadata;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\Path\PathInterface;
use JSONAPI\Mapper\Request\Path\PathParserInterface;
use JSONAPI\Mapper\Request\RequestPart;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionClass;
use ReflectionException;
use stdClass;

/**
 * Class BodyParser
 *
 * @package JSONAPI\Mapper\Request\Body
 */
class BodyParser implements BodyParserInterface
{
    /**
     * @var MetadataRepository
     */
    private MetadataRepository $repository;
    /**
     * @var ClassMetadata
     */
    private ClassMetadata $metadata;
    /**
     * @var Document $document
     */
    private Document $document;
    /**
     * @var PathParserInterface $pathParser
     */
    private PathParserInterface $pathParser;

    /**
     * DocumentFactory constructor.
     *
     * @param MetadataRepository $repository
     * @param PathParserInterface $pathParser
     */
    public function __construct(MetadataRepository $repository, PathParserInterface $pathParser)
    {
        $this->repository = $repository;
        $this->pathParser = $pathParser;
    }

    /**
     * @inheritDoc
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): ?Document
    {
        $request->getBody()->rewind();
        $json = $request->getBody()->getContents();
        if (strlen($json) > 0) {
            $this->document = $parsedRequest?->getBody() ?? new Document();
            $json           = json_decode($json, false, 512, JSON_THROW_ON_ERROR);
            if (!property_exists($json, 'data')) {
                return $this->document;
            }
            $path = $this->pathParser->parse($request);
            $type = $path->getPrimaryResourceType();
            if ($this->metadata = $this->repository->getByType($type)) {
                if ($path->isCollection()) {
                    $data = $this->parseCollection($json->data, $path);
                } else {
                    $data = $this->parseResource($json->data, $path);
                }
                if ($this->document->getNamespaces()->has(DefaultNamespace::class)) {
                    $namespace = $this->document->getNamespaces()->get(DefaultNamespace::class);
                    $namespace->setData($data);
                }
            } else {
                throw new MetadataNotFound(Messages::classMetadataNotFound($type));
            }
            return $this->document;
        } else {
            return null;
        }
    }

    /**
     * @param object[] $collection
     * @param PathInterface $path
     *
     * @return ResourceCollection<ResourceObject|ResourceObjectIdentifier>
     */
    private function parseCollection(array $collection, PathInterface $path): ResourceCollection
    {
        $data = new ResourceCollection();
        if (!empty($collection)) {
            foreach ($collection as $object) {
                $resource = $this->parseResource($object, $path);
                $data->add($resource);
            }
        }
        return $data;
    }

    /**
     * @param object $object
     * @param PathInterface $path
     *
     * @return ResourceObject|ResourceObjectIdentifier
     */
    private function parseResource(object $object, PathInterface $path): ResourceObject|ResourceObjectIdentifier
    {
        if ($object->type !== $this->metadata->getType()) {
            $this->document->addError(ErrorFactory::resourceTypeMismatch($object->type, $this->metadata->getType()));
        }
        $type     = new Type($object->type);
        $id       = new Id(@$object->id);
        $resource = new ResourceObjectIdentifier($type, $id);
        if (!$path->isRelationship()) {
            $resource = new ResourceObject($type, $id);
            if (property_exists($object, 'attributes')) {
                $this->parseAttributes($object, $resource);
            }
            if (property_exists($object, 'relationships')) {
                $this->parseRelationships($object, $resource);
            }
        }
        return $resource;
    }

    /**
     * @param object $object
     * @param ResourceObject $resource
     *
     * @return void
     */
    private function parseAttributes(object $object, ResourceObject $resource): void
    {
        foreach ($this->metadata->getAttributes() as $attribute) {
            if (property_exists($object->attributes, $attribute->name)) {
                $value = $object->attributes->{$attribute->name};
                $type  = $attribute->type;
                if ($type) {
                    if (!is_null($value)) {
                        switch ($type) {
                            case 'int':
                                if (!is_int($value)) {
                                    $this->document->addError(
                                        ErrorFactory::unexpectedFieldDataType(
                                            $resource->getType(),
                                            $attribute->name,
                                            gettype($value),
                                            'int'
                                        )
                                    );
                                }
                                break;
                            case 'bool':
                                if (!is_bool($value)) {
                                    $this->document->addError(
                                        ErrorFactory::unexpectedFieldDataType(
                                            $resource->getType(),
                                            $attribute->name,
                                            gettype($value),
                                            'bool'
                                        )
                                    );
                                }
                                break;
                            case 'float':
                                if (!is_int($value) && !is_float($value)) {
                                    $this->document->addError(
                                        ErrorFactory::unexpectedFieldDataType(
                                            $resource->getType(),
                                            $attribute->name,
                                            gettype($value),
                                            'float'
                                        )
                                    );
                                }
                                $value = floatval($value);
                                break;
                            case 'string':
                                if (!is_string($value)) {
                                    $this->document->addError(
                                        ErrorFactory::unexpectedFieldDataType(
                                            $resource->getType(),
                                            $attribute->name,
                                            gettype($value),
                                            'string'
                                        )
                                    );
                                }
                                break;
                            case 'array':
                                if (!is_array($value) && !is_a($value, stdClass::class)) {
                                    $this->document->addError(
                                        ErrorFactory::unexpectedFieldDataType(
                                            $resource->getType(),
                                            $attribute->name,
                                            gettype($value),
                                            'array'
                                        )
                                    );
                                }
                                $value = (array)$value;
                                break;
                            default:
                                break;
                        }
                        try {
                            $ref = new ReflectionClass($type);
                            if ($ref->implementsInterface(Deserializable::class)) {
                                /** @var Deserializable $type */
                                $value = $type::jsonDeserialize($value);
                            } elseif ($ref->implementsInterface(DateTimeInterface::class)) {
                                $value = DateTimeImmutable::createFromFormat(DateTimeInterface::ATOM, $value);
                            } elseif ($ref->implementsInterface(BackedEnum::class)) {
                                /** @var BackedEnum $type */
                                $value = $type::from($value);
                            }
                        } catch (ReflectionException) {
                            //NOSONAR
                        }
                    } elseif ($attribute->nullable === false) {
                        $this->document->addError(
                            ErrorFactory::unexpectedFieldDataType(
                                $resource->getType(),
                                $attribute->name,
                                gettype($value),
                                'not null'
                            )
                        );
                    }
                }
                $resource->addAttribute(new Attribute($attribute->name, $value));
            }
        }
    }

    /**
     * @param object $object
     * @param ResourceObject $resource
     *
     * @return void
     */
    private function parseRelationships(object $object, ResourceObject $resource): void
    {
        foreach ($this->metadata->getRelationships() as $relationship) {
            if (
                property_exists($object->relationships, $relationship->name) &&
                property_exists($object->relationships->{$relationship->name}, 'data')
            ) {
                $value = $object->relationships->{$relationship->name}->data;
                if (!is_null($value)) {
                    if ($relationship->isCollection) {
                        $data = new ResourceCollection();
                        foreach ($value as $item) {
                            $data->add(new ResourceObjectIdentifier(new Type($item->type), new Id($item->id)));
                        }
                    } else {
                        $data = new ResourceObjectIdentifier(new Type($value->type), new Id($value->id));
                    }
                } else {
                    if ($relationship->nullable === false) {
                        $this->document->addError(
                            ErrorFactory::unexpectedFieldDataType(
                                $resource->getType(),
                                $relationship->name,
                                gettype($value),
                                'not null'
                            )
                        );
                    }
                    $data = $value;
                }
                $docRelationship = new Relationship($relationship->name, $resource);
                $docRelationship->setData($data);
                $resource->addRelationship($docRelationship);
            }
        }
    }

    public function getRequestPartKey(): string
    {
        return RequestPart::BODY_PART_KEY;
    }
}
