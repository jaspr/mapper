<?php

/**
 * Created by tomas
 * at 06.08.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Fieldset;

use JSONAPI\Mapper\Request\RequestPart;

/**
 * Class FieldsetResult
 * @package JSONAPI\Mapper\Request\Fieldset
 */
class FieldsetResult implements FieldsetInterface
{
    /**
     * @var string[][]
     */
    private array $fields = [];

    /**
     * @param string[][] $fields
     */
    public function __construct(array $fields)
    {
        $this->fields = $fields;
    }


    /**
     * @inheritDoc
     */
    public function showField(string $type, string $name): bool
    {
        if (!isset($this->fields[$type])) {
            return true;
        } elseif (in_array($name, $this->fields[$type])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        $str = '';
        foreach ($this->fields as $type => $fields) {
            $str .= (strlen($str) > 0 ? '&' : '') . rawurlencode('fields[' . $type . ']') . '='
                . implode(',', array_map('rawurlencode', $fields));
        }
        return $str;
    }

    public function toArray(): array
    {
        if (!empty($this->fields)) {
            $ret = [];
            foreach ($this->fields as $type => $fields) {
                $ret[RequestPart::FIELDS_PART_KEY . '[' . $type . ']'] = implode(',', $fields);
            }
            return $ret;
        }
        return [];
    }
}
