<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Fieldset;

use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\RequestPart;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class FieldsetParser
 *
 * @package JSONAPI\URI\Fieldset
 */
class FieldsetParser implements FieldsetParserInterface
{
    /**
     * @inheritDoc
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): ?FieldsetInterface
    {
        $data = $request->getQueryParams()[RequestPart::FIELDS_PART_KEY] ?? null;
        if ($data) {
            $fieldset = [];
            foreach ($data as $type => $fields) {
                $fieldset[$type] = array_map(
                    function ($item) {
                        return trim($item);
                    },
                    explode(',', $fields)
                );
            }
            return new FieldsetResult($fieldset);
        }
        return null;
    }

    public function getRequestPartKey(): string
    {
        return RequestPart::FIELDS_PART_KEY;
    }
}
