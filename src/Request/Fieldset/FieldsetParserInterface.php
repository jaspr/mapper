<?php

/**
 * Created by lasicka@logio.cz
 * at 06.10.2021 13:11
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Fieldset;

use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\RequestParserInterface;
use Psr\Http\Message\ServerRequestInterface;

interface FieldsetParserInterface extends RequestParserInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param ParsedRequest|null $parsedRequest
     * @return FieldsetInterface|null
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): ?FieldsetInterface;
}
