<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Fieldset;

use JSONAPI\Mapper\Request\RequestParserResultInterface;

/**
 * Interface FieldsetInterface
 *
 * @package JSONAPI\URI\Fieldset
 */
interface FieldsetInterface extends RequestParserResultInterface
{
    /**
     * @param string $type
     * @param string $name from Metadata\Attribute or Metadata\Relationship object
     *
     * @return bool
     */
    public function showField(string $type, string $name): bool;
}
