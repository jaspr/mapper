<?php

/**
 * Created by @bednic
 * at 13.08.2024 21:36
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request;

interface RequestParserResultInterface
{
    /**
     * @return string
     */
    public function __toString(): string;

    /**
     * @return string[]
     */
    public function toArray(): array;
}
