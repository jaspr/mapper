<?php

/**
 * Created by lasicka@logio.cz
 * at 06.10.2021 13:14
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Path;

use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\RequestParserInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface PathParserInterface
 *
 * @package JSONAPI\URI\Path
 */
interface PathParserInterface extends RequestParserInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param ParsedRequest|null $parsedRequest
     * @return PathInterface|null
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): ?PathInterface;
}
