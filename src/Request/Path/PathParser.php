<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Path;

use Fig\Http\Message\RequestMethodInterface;
use JSONAPI\Mapper\Driver\NamingStrategy;
use JSONAPI\Mapper\Driver\RecommendedNamingStrategy;
use JSONAPI\Mapper\Exception\Messages;
use JSONAPI\Mapper\Exception\MetadataNotFound;
use JSONAPI\Mapper\Exception\PatternDoesNotMatch;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Regex;
use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\RequestPart;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class PathParser
 *
 * @package JSONAPI\URI\Path
 */
class PathParser implements PathParserInterface
{
    /**
     * @var MetadataRepository
     */
    private MetadataRepository $metadataRepository;
    /**
     * @var string baseURL
     */
    private string $baseURL;

    /**
     * @var NamingStrategy $namingStrategy
     */
    private NamingStrategy $namingStrategy;


    /**
     * PathParser constructor.
     *
     * @param MetadataRepository $metadataRepository
     * @param string $baseURL
     * @param NamingStrategy|null $namingStrategy
     */
    public function __construct(
        MetadataRepository $metadataRepository,
        string $baseURL,
        ?NamingStrategy $namingStrategy = null
    ) {
        $this->metadataRepository = $metadataRepository;
        $this->baseURL            = $baseURL;
        $this->namingStrategy     = $namingStrategy ?? new RecommendedNamingStrategy();
    }

    /**
     * @param ServerRequestInterface $request
     * @param ParsedRequest|null $parsedRequest
     * @inheritDoc
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): ?PathInterface
    {
        $data     = $request->getUri()->getPath();
        $method   = $request->getMethod();
        $resource = '';
        /** @var string|null $id */
        $id                  = null;
        $relationship        = null;
        $isRelationship      = false;

        $prefix = parse_url($this->baseURL, PHP_URL_PATH) ?? '';
        $data   = preg_replace('~^' . $prefix . '~', '', $data);
        $data   = '/' . ltrim($data, '/');
        if (preg_match(Regex::PATH_PATTERN, $data, $matches)) {
            foreach (['resource', 'id', 'relationship', 'related'] as $key) {
                if (isset($matches[$key]) && strlen($matches[$key]) > 0) {
                    if ($key === 'relationship') {
                        $isRelationship = true;
                    }
                    if ($key === 'related') {
                        $relationship = $matches[$key];
                    } else {
                        $$key = $matches[$key];
                    }
                }
            }
        } else {
            return null;
        }
        if ($metadata = $this->metadataRepository->getByType($resource)) {
            if ($relationship) {
                $relationship = $this->namingStrategy->relationshipNameFromURI($relationship);
                if ($relMetadata = $metadata->getRelationship($relationship)) {
                    $primaryResourceType = $this->metadataRepository->getByClass($relMetadata->target)->getType();
                    $isCollection        = $relMetadata->isCollection;
                } else {
                    throw new MetadataNotFound(Messages::fieldMetadataNotFound($resource, $relationship));
                }
            } elseif ($id || (strtoupper($method) === RequestMethodInterface::METHOD_POST)) {
                $primaryResourceType = $metadata->getType();
                $isCollection        = false;
            } else {
                $primaryResourceType = $metadata->getType();
                $isCollection        = true;
            }
            return new PathResult($resource, $primaryResourceType, $id, $relationship, $isRelationship, $isCollection);
        } else {
            throw new MetadataNotFound(Messages::classMetadataNotFound($resource));
        }
    }
}
