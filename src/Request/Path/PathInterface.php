<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Path;

use JSONAPI\Mapper\Request\RequestParserResultInterface;

/**
 * Interface PathInterface
 *
 * @package JSONAPI\URI\Path
 */
interface PathInterface extends RequestParserResultInterface
{
    /**
     * @return string
     */
    public function getResourceType(): string;

    /**
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Returns field of relationship
     *
     * @return string|null
     */
    public function getRelationshipName(): ?string;

    /**
     * @return bool
     */
    public function isRelationship(): bool;

    /**
     * @return string
     */
    public function getPrimaryResourceType(): string;

    /**
     * @return bool
     */
    public function isCollection(): bool;
}
