<?php

/**
 * Created by tomas
 * at 06.08.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Path;

/**
 * Class PathResult
 * @package JSONAPI\Mapper\Request\Path
 */
readonly class PathResult implements PathInterface
{
    /**
     * @var string
     */
    private string $resource;

    /**
     * @var string
     */
    private string $primaryResourceType;

    /**
     * @var string|null
     */
    private ?string $id;

    /**
     * @var string|null
     */
    private ?string $relationship;

    /**
     * @var bool
     */
    private bool $isRelationship;

    /**
     * @var bool
     */
    private bool $isCollection;

    /**
     * @param string $resource
     * @param string|null $id
     * @param string|null $relationship
     * @param bool $isRelationship
     * @param string $primaryResourceType
     * @param bool $isCollection
     */
    public function __construct(
        string $resource,
        string $primaryResourceType,
        ?string $id = null,
        ?string $relationship = null,
        bool $isRelationship = false,
        bool $isCollection = false
    ) {
        $this->resource = $resource;
        $this->id = $id;
        $this->relationship = $relationship;
        $this->isRelationship = $isRelationship;
        $this->primaryResourceType = $primaryResourceType;
        $this->isCollection = $isCollection;
    }


    public function getResourceType(): string
    {
        return $this->resource;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getRelationshipName(): ?string
    {
        return $this->relationship;
    }

    public function isRelationship(): bool
    {
        return $this->isRelationship;
    }

    public function getPrimaryResourceType(): string
    {
        return $this->primaryResourceType;
    }

    public function isCollection(): bool
    {
        return $this->isCollection;
    }

    public function __toString(): string
    {
        $str = $this->resource;
        if ($this->id) {
            $str .= '/' . $this->id;
            if ($this->relationship) {
                if ($this->isRelationship) {
                    $str .= '/relationships';
                }
                $str .= '/' . $this->relationship;
            }
        }
        return $str;
    }

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        $path = [];
        $path[] = $this->resource;
        if ($this->id) {
            $path[] = '/' . $this->id;
            if ($this->relationship) {
                $path[] = '/relationships/' . $this->relationship;
            }
            $path[] = '/' . $this->relationship;
        }
        return $path;
    }
}
