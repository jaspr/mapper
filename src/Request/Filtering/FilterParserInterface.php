<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Filtering;

use JSONAPI\Mapper\Exception\FilterException;
use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\RequestParserInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface FilterParserInterface
 *
 * @package JSONAPI\URI\Filtering
 */
interface FilterParserInterface extends RequestParserInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param ParsedRequest|null $parsedRequest
     * @return FilterInterface|null
     * @throws FilterException
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): ?FilterInterface;
}
