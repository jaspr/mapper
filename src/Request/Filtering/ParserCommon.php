<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Filtering;

use JSONAPI\Mapper\Document\Convertible;
use JSONAPI\Mapper\Exception\FilterException;
use JSONAPI\Mapper\Exception\MetadataNotFound;
use JSONAPI\Mapper\Metadata\ClassMetadata;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Request\Path\PathParserInterface;
use JSONAPI\Mapper\Request\RequestPart;

/**
 * Class Parser
 *
 * @package JSONAPI\Mapper\Request\Filtering
 */
abstract class ParserCommon implements FilterParserInterface
{
    /**
     * @var MetadataRepository
     */
    protected MetadataRepository $repository;
    /**
     * @var ExpressionBuilderInterface
     */
    protected ExpressionBuilderInterface $builder;
    /**
     * @var ClassMetadata
     */
    protected ClassMetadata $metadata;
    /**
     * @var PathParserInterface $pathParser
     */
    protected PathParserInterface $pathParser;

    /**
     * @param MetadataRepository $repository
     * @param PathParserInterface $pathParser
     * @param ExpressionBuilderInterface|null $builder
     */
    public function __construct(
        MetadataRepository $repository,
        PathParserInterface $pathParser,
        ?ExpressionBuilderInterface $builder = null
    ) {
        $this->repository = $repository;
        $this->builder = $builder ?? new ExpressionBuilder();
        $this->pathParser = $pathParser;
    }

    /**
     * @param string $type
     * @return void
     */
    protected function loadMetadata(string $type): void
    {
        if ($metadata = $this->repository->getByType($type)) {
            $this->metadata = $metadata;
        } else {
            throw new MetadataNotFound("Metadata for resource type [$type] not found.");
        }
    }

    protected function ex(): ExpressionBuilderInterface
    {
        return $this->builder;
    }

    /**
     * @param string $identifier
     *
     * @return mixed
     * @throws FilterException
     */
    protected function parseField(string $identifier): mixed
    {
        $type = 'string';
        $classMetadata = $this->metadata;
        $parts = [...explode(".", $identifier)];
        while ($part = array_shift($parts)) {
            if ($classMetadata->hasRelationship($part)) {
                $relationship = $classMetadata->getRelationship($part);
                $classMetadata = $this->repository->getByClass(
                    $relationship->target
                );
                $type = $relationship->isCollection ? 'string[]' : 'string';
            } elseif ($classMetadata->hasAttribute($part)) {
                $type = $classMetadata->getAttribute($part)->type;
                if (is_a($type, Convertible::class, true)) {
                    // skip because it's DTO object
                    return $this->ex()->field($identifier, 'mixed');
                }
                if (!is_null($classMetadata->getAttribute($part)->of)) {
                    $type = $classMetadata->getAttribute($part)->of . '[]';
                }
            } elseif ($part === 'id') {
                $type = 'string';
            } else {
                throw new FilterException(Messages::failedToAccessProperty($part, $classMetadata->getClassName()));
            }
        }
        return $this->ex()->field($identifier, $type);
    }

    public function getRequestPartKey(): string
    {
        return RequestPart::FILTER_PART_KEY;
    }
}
