<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Filtering\QData;

use JSONAPI\Expression\Expression;
use JSONAPI\Expression\Type\TBoolean;
use JSONAPI\Mapper\Request\Filtering\FilterInterface;
use JSONAPI\Mapper\Request\RequestPart;

/**
 * Class QuatrodotResult
 *
 * @package JSONAPI\URI\Filtering\QData
 */
class QuatrodotResult implements FilterInterface
{
    /**
     * @var mixed
     */
    private mixed $condition = null;

    /**
     * @var mixed[]
     */
    private array $tree = [];

    /**
     * @var string|null
     */
    private ?string $origin = null;

    /**
     * @param Expression $condition
     */
    public function setCondition(mixed $condition): void
    {
        $this->condition = $condition;
    }

    /**
     * @param string $origin
     */
    public function setOrigin(string $origin): void
    {
        $this->origin = $origin;
    }

    /**
     * @return mixed|null
     */
    public function getCondition(): mixed
    {
        return $this->condition;
    }

    /**
     * @param string $identifier
     *
     * @return mixed[]
     */
    public function getConditionsFor(string $identifier): array
    {
        if (isset($this->tree[$identifier])) {
            return $this->tree[$identifier];
        }
        return [];
    }

    /**
     * @param string $identifier
     * @param TBoolean $expression
     *
     * @return void
     */
    public function addConditionFor(string $identifier, mixed $expression): void
    {
        $this->tree[$identifier][] = $expression;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->origin ? RequestPart::FILTER_PART_KEY . '=' . rawurlencode($this->origin) : '';
    }

    public function toArray(): array
    {
        if ($this->origin) {
            return [RequestPart::FILTER_PART_KEY => $this->origin];
        }
        return [];
    }
}
