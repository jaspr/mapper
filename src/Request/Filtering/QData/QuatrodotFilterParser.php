<?php

/**
 * Created by uzivatel
 * at 22.03.2022 15:03
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Filtering\QData;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use JSONAPI\Expression\Expression;
use JSONAPI\Expression\Literal\ArrayValue;
use JSONAPI\Mapper\Document\Members;
use JSONAPI\Mapper\Exception\FilterException;
use JSONAPI\Mapper\Metadata\Attribute;
use JSONAPI\Mapper\Request\Filtering\FilterInterface;
use JSONAPI\Mapper\Request\Filtering\FilterParserInterface;
use JSONAPI\Mapper\Request\Filtering\KeyWord;
use JSONAPI\Mapper\Request\Filtering\Messages;
use JSONAPI\Mapper\Request\Filtering\ParserCommon;
use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\RequestPart;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class FilterParser
 *
 * @package JSONAPI\URI\Filtering\Quatrodot
 */
class QuatrodotFilterParser extends ParserCommon implements FilterParserInterface
{
    /**
     * @var QuatrodotResult
     */
    private QuatrodotResult $result;
    private string $data;

    /**
     * @inheritDoc
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): ?FilterInterface
    {
        $data = $request->getQueryParams()[RequestPart::FILTER_PART_KEY] ?? null;
        if (is_string($data)) {
            $this->result = new QuatrodotResult();
            $this->data = $data;
            $path = $this->pathParser->parse($request);
            $this->loadMetadata($path->getPrimaryResourceType());
            $this->result->setOrigin($data);
            $phrases = explode(KeyWord::PHRASE_SEPARATOR->value, $data);
            $tree = [];
            foreach ($phrases as $phrase) {
                $tokens = explode(KeyWord::VALUE_SEPARATOR->value, $phrase);
                $field = array_shift($tokens);
                $op = array_shift($tokens);
                $args = $tokens;
                if ($field && $op && $args) {
                    $tree[$field][] = [$field, $op, $args];
                } else {
                    throw new FilterException(Messages::syntaxError());
                }
            }
            $condition = $this->parseExpression($tree);
            $this->result->setCondition($condition);
            return $this->result;
        }
        return null;
    }

    /**
     * @param array<string, array<array<array<string>|string>>> $expression
     *
     * @return Expression
     * @throws FilterException
     */
    private function parseExpression(array $expression): mixed
    {
        return $this->parseAnd($expression);
    }

    /**
     * @param string[][][] $expressionTree
     *
     * @return mixed
     * @throws FilterException
     */
    private function parseAnd(array $expressionTree): mixed
    {
        $left = null;
        foreach ($expressionTree as $expressions) {
            $right = $this->parseOr($expressions);
            if ($left) {
                $left = $this->ex()->and($left, $right);
            } else {
                $left = $right;
            }
        }
        return $left;
    }

    /**
     * @param string[][] $expressions
     *
     * @return mixed
     * @throws FilterException
     */
    private function parseOr(array $expressions): mixed
    {
        $left = null;
        foreach ($expressions as $expression) {
            $right = $this->parseComparison($expression);
            if ($left) {
                $left = $this->ex()->or($left, $right);
            } else {
                $left = $right;
            }
        }
        return $left;
    }

    /**
     * @param string[] $expression
     *
     * @return mixed
     * @throws FilterException
     */
    private function parseComparison(array $expression): mixed
    {
        $field = array_shift($expression);
        $op = array_shift($expression);
        /** @var string[]|string[][] $args */
        $args = array_shift($expression);
        $left = $this->parseField($field);
        $operand = KeyWord::tryFrom($op);
        $right = $this->parseArgs($args, $this->getAttributeForIdentifier($field));
        $ex = match ($operand) {
            KeyWord::LOGICAL_EQUAL => $this->ex()->eq($left, ...$right),
            KeyWord::LOGICAL_NOT_EQUAL => $this->ex()->ne($left, ...$right),
            KeyWord::LOGICAL_GREATER_THAN => $this->ex()->gt($left, ...$right),
            KeyWord::LOGICAL_GREATER_THAN_OR_EQUAL => $this->ex()->ge($left, ...$right),
            KeyWord::LOGICAL_LOWER_THAN => $this->ex()->lt($left, ...$right),
            KeyWord::LOGICAL_LOWER_THAN_OR_EQUAL => $this->ex()->le($left, ...$right),
            KeyWord::LOGICAL_IN => $this->ex()->in($left, new ArrayValue($right)),
            KeyWord::LOGICAL_BETWEEN => $this->ex()->be($left, ...$right),
            KeyWord::FUNCTION_CONTAINS => $this->ex()->contains($left, ...$right),
            KeyWord::FUNCTION_STARTS_WITH => $this->ex()->startsWith($left, ...$right),
            KeyWord::FUNCTION_ENDS_WITH => $this->ex()->endsWith($left, ...$right),
            default => throw new FilterException(Messages::operandOrFunctionNotImplemented($operand))
        };
        $this->result->addConditionFor($field, $ex);
        return $ex;
    }

    /**
     * @param string $identifier
     *
     * @return Attribute
     * @throws FilterException
     */
    private function getAttributeForIdentifier(string $identifier): Attribute
    {
        $attribute = null;
        $classMetadata = $this->metadata;
        $parts = [...explode(".", $identifier)];
        while ($part = array_shift($parts)) {
            if ($classMetadata->hasRelationship($part)) {
                $classMetadata = $this->repository->getByClass(
                    $classMetadata->getRelationship($part)->target
                );
            } elseif ($classMetadata->hasAttribute($part) || $part === Members::ID) {
                $attribute = $classMetadata->getAttribute($part);
            } else {
                throw new FilterException(Messages::failedToAccessProperty($part, $classMetadata->getClassName()));
            }
        }
        return $attribute;
    }

    /**
     * @param string[]|string[][] $args
     * @param Attribute|null $attribute
     *
     * @return mixed[]
     * @throws FilterException
     */
    private function parseArgs(mixed $args, ?Attribute $attribute): array
    {
        if (is_null($attribute)) {
            return [$this->ex()->literal($args)];
        } else {
            $type = $attribute->type;
            if ($attribute->type === 'array') {
                $type = $attribute->of;
            }
            $params = [];
            foreach ($args as $arg) {
                $params[] = match ($type) {
                    'int', 'integer' => $this->parseInt($arg),
                    'bool', 'boolean' => $this->parseBoolean($arg),
                    'float', 'double' => $this->parseFloat($arg),
                    DateTimeInterface::class,
                    DateTimeImmutable::class,
                    DateTime::class => $this->parseDate($arg),
                    default => $this->ex()->literal($arg)
                };
            }
            return $params;
        }
    }

    /**
     * @param string $arg
     *
     * @return mixed
     * @throws FilterException
     */
    private function parseInt(string $arg): mixed
    {
        if (($value = filter_var($arg, FILTER_VALIDATE_INT)) !== false) {
            return $this->ex()->literal($value);
        }
        throw new FilterException(Messages::expressionLexerDigitExpected(strpos($arg, $this->data)));
    }

    /**
     * @param string $arg
     *
     * @return mixed
     * @throws FilterException
     */
    private function parseBoolean(string $arg): mixed
    {
        if (($value = filter_var($arg, FILTER_VALIDATE_BOOL, FILTER_NULL_ON_FAILURE)) !== null) {
            return $this->ex()->literal($value);
        }
        throw new FilterException(Messages::expressionLexerBooleanExpected(strpos($arg, $this->data)));
    }

    /**
     * @param string $arg
     *
     * @return mixed
     * @throws FilterException
     */
    private function parseFloat(string $arg): mixed
    {
        if (($value = filter_var($arg, FILTER_VALIDATE_FLOAT)) !== false) {
            return $this->ex()->literal($value);
        }
        throw new FilterException(Messages::expressionLexerDigitExpected(strpos($arg, $this->data)));
    }

    /**
     * @param string $arg
     *
     * @return mixed
     * @throws FilterException
     */
    private function parseDate(string $arg): mixed
    {
        try {
            $data = new DateTimeImmutable($arg);
        } catch (Exception) {
            throw new FilterException(
                Messages::expressionParserUnrecognizedLiteral('datetime', $arg, strpos($arg, $this->data))
            );
        }
        return $this->ex()->literal($data);
    }
}
