<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Filtering\OData;

use JSONAPI\Mapper\Request\Filtering\FilterInterface;
use JSONAPI\Mapper\Request\RequestPart;

/**
 * Class ExpressionFilterResult
 *
 * @package JSONAPI\URI\Filtering\OData
 */
readonly class ExpressionFilterResult implements FilterInterface
{
    /**
     * @var string $origin
     */
    private string $origin;
    /**
     * @var mixed $condition
     */
    private mixed $condition;

    /**
     * @param string $origin
     * @param mixed $condition
     */
    public function __construct(string $origin, mixed $condition)
    {
        $this->origin = $origin;
        $this->condition = $condition;
    }

    /**
     * @return mixed|null
     */
    public function getCondition(): mixed
    {
        return $this->condition;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->origin ? RequestPart::FILTER_PART_KEY . '=' . rawurlencode($this->origin) : '';
    }

    public function toArray(): array
    {
        if ($this->origin) {
            return [RequestPart::FILTER_PART_KEY => $this->origin];
        }
        return [];
    }
}
