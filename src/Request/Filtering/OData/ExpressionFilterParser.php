<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Filtering\OData;

use DateTime;
use Exception;
use JSONAPI\Expression\Literal\ArrayValue;
use JSONAPI\Mapper\Exception\FilterException;
use JSONAPI\Mapper\Request\Filtering\FilterInterface;
use JSONAPI\Mapper\Request\Filtering\FilterParserInterface;
use JSONAPI\Mapper\Request\Filtering\KeyWord;
use JSONAPI\Mapper\Request\Filtering\Messages;
use JSONAPI\Mapper\Request\Filtering\ParserCommon;
use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\RequestPart;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class ExpressionFilterParser
 *
 * @package JSONAPI\URI\Filtering
 */
class ExpressionFilterParser extends ParserCommon implements FilterParserInterface
{
    /**
     * @var ExpressionLexer|null
     */
    private ?ExpressionLexer $lexer = null;

    /**
     * @inheritDoc
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): ?FilterInterface
    {
        $data = $request->getQueryParams()[RequestPart::FILTER_PART_KEY] ?? null;
        if (is_string($data)) {
            $this->lexer = null;
            $path = $this->pathParser->parse($request);
            $this->loadMetadata($path->getPrimaryResourceType());
            $this->lexer = new ExpressionLexer($data);
            $condition = $this->parseExpression();
            return new ExpressionFilterResult($data, $condition);
        }
        return null;
    }

    /**
     * @return mixed Expression
     * @throws FilterException
     */
    private function parseExpression(): mixed
    {
        return $this->parseLogicalOr();
    }

    /**
     * Parse logical or (or)
     *
     * @return mixed
     * @throws FilterException
     */
    private function parseLogicalOr(): mixed
    {
        $left = $this->parseLogicalAnd();
        while ($this->lexer->getCurrentToken()->identifierIs(KeyWord::LOGICAL_OR)) {
            $this->lexer->nextToken();
            $right = $this->parseLogicalAnd();
            $left = $this->ex()->or($left, $right);
        }

        return $left;
    }

    /**
     * Parse logical and (and)
     *
     * @return mixed
     * @throws FilterException
     */
    private function parseLogicalAnd(): mixed
    {
        $left = $this->parseComparison();
        while ($this->lexer->getCurrentToken()->identifierIs(KeyWord::LOGICAL_AND)) {
            $this->lexer->nextToken();
            $right = $this->parseComparison();
            $left = $this->ex()->and($left, $right);
        }
        return $left;
    }

    /**
     * Parse comparison operation (eq, ne, gt, gte, lt, lte, in, has, be)
     *
     * @return mixed
     * @throws FilterException
     */
    private function parseComparison(): mixed
    {
        $left = $this->parseAdditive();
        while ($this->lexer->getCurrentToken()->isComparisonOperator()) {
            $comparisonToken = clone $this->lexer->getCurrentToken();
            $this->lexer->nextToken();
            if ($comparisonToken->identifierIs(KeyWord::LOGICAL_IN)) {
                $right = new ArrayValue($this->parseArgumentList());
                $left = $this->ex()->in($left, $right);
            } elseif ($comparisonToken->identifierIs(KeyWord::LOGICAL_HAS)) {
                $right = $this->parsePrimary();
                $left = $this->ex()->has($left, $right);
            } elseif ($comparisonToken->identifierIs(KeyWord::LOGICAL_BETWEEN)) {
                $right = $this->parseArgumentList();
                $left = $this->ex()->be($left, ...$right);
            } else {
                $right = $this->parseAdditive();
                $left = $this->ex()->{$comparisonToken->text}($left, $right);
            }
        }
        return $left;
    }

    /**
     * Parse additive operation (add, sub).
     *
     * @return mixed
     * @throws FilterException
     */
    private function parseAdditive(): mixed
    {
        $left = $this->parseMultiplicative();
        while (
            $this->lexer->getCurrentToken()->identifierIs(KeyWord::ARITHMETIC_ADDITION) ||
            $this->lexer->getCurrentToken()->identifierIs(KeyWord::ARITHMETIC_SUBTRACTION)
        ) {
            $token = clone $this->lexer->getCurrentToken();
            $this->lexer->nextToken();
            $right = $this->parseMultiplicative();
            $left = match ($token->getIdentifier()) {
                KeyWord::ARITHMETIC_ADDITION => $this->ex()->add($left, $right),
                KeyWord::ARITHMETIC_SUBTRACTION => $this->ex()->sub($left, $right),
                default => throw new FilterException(Messages::operandOrFunctionNotImplemented($token->getIdentifier()))
            };
        }
        return $left;
    }

    /**
     * Parse multiplicative operators (mul, div, mod)
     *
     * @return mixed
     * @throws FilterException
     */
    private function parseMultiplicative(): mixed
    {
        $left = $this->parseUnary();
        while (
            $this->lexer->getCurrentToken()->identifierIs(KeyWord::ARITHMETIC_MULTIPLICATION) ||
            $this->lexer->getCurrentToken()->identifierIs(KeyWord::ARITHMETIC_DIVISION) ||
            $this->lexer->getCurrentToken()->identifierIs(KeyWord::ARITHMETIC_MODULO)
        ) {
            $token = clone $this->lexer->getCurrentToken();
            $this->lexer->nextToken();
            $right = $this->parseUnary();
            $left = match ($token->getIdentifier()) {
                KeyWord::ARITHMETIC_MULTIPLICATION => $this->ex()->mul($left, $right),
                KeyWord::ARITHMETIC_DIVISION => $this->ex()->div($left, $right),
                KeyWord::ARITHMETIC_MODULO => $this->ex()->mod($left, $right),
                default => throw new FilterException(Messages::operandOrFunctionNotImplemented($token->getIdentifier()))
            };
        }
        return $left;
    }

    /**
     * Parse unary operator (- ,not)
     *
     * @return mixed
     * @throws FilterException
     */
    private function parseUnary(): mixed
    {
        if (
            $this->lexer->getCurrentToken()->id == ExpressionTokenId::MINUS ||
            $this->lexer->getCurrentToken()->identifierIs(KeyWord::LOGICAL_NOT)
        ) {
            $op = clone $this->lexer->getCurrentToken();
            $this->lexer->nextToken();
            if (
                $op->id === ExpressionTokenId::MINUS &&
                ExpressionLexer::isNumeric($this->lexer->getCurrentToken()->id)
            ) {
                $numberLiteral = $this->lexer->getCurrentToken();
                $numberLiteral->text = '-' . $numberLiteral->text;
                $numberLiteral->position = $op->position;
                $this->lexer->setCurrentToken($numberLiteral);
                return $this->parsePrimary();
            }
            if ($op->identifierIs(KeyWord::LOGICAL_NOT)) {
                $expr = $this->parseLogicalOr();
                return $this->ex()->not($expr);
            }
        }
        return $this->parsePrimary();
    }

    /**
     * @return mixed
     * @throws FilterException
     */
    private function parsePrimary(): mixed
    {
        return match ($this->lexer->getCurrentToken()->id) {
            ExpressionTokenId::BOOLEAN_LITERAL => $this->parseBoolean(),
            ExpressionTokenId::DATETIME_LITERAL => $this->parseDatetime(),
            ExpressionTokenId::DECIMAL_LITERAL,
            ExpressionTokenId::DOUBLE_LITERAL,
            ExpressionTokenId::SINGLE_LITERAL => $this->parseFloat(),
            ExpressionTokenId::NULL_LITERAL => $this->parseNull(),
            ExpressionTokenId::IDENTIFIER => $this->parseIdentifier(),
            ExpressionTokenId::STRING_LITERAL => $this->parseString(),
            ExpressionTokenId::INT64_LITERAL,
            ExpressionTokenId::INTEGER_LITERAL => $this->parseInteger(),
            ExpressionTokenId::BINARY_LITERAL,
            ExpressionTokenId::GUID_LITERAL => throw new FilterException(
                Messages::operandOrFunctionNotImplemented($this->lexer->getCurrentToken()->getIdentifier())
            ),
            ExpressionTokenId::OPEN_PARAM => $this->parseParentExpression(),
            default => throw new FilterException(Messages::expressionLexerSyntaxError($this->lexer->getPosition()))
        };
    }

    /**
     * @return mixed
     * @throws FilterException
     */
    private function parseBoolean(): mixed
    {
        $data = KeyWord::tryFrom($this->lexer->getCurrentToken()->text) === KeyWord::RESERVED_TRUE;
        $value = $this->ex()->literal($data);
        $this->lexer->nextToken();
        return $value;
    }

    /**
     * @return mixed
     * @throws FilterException
     */
    private function parseDatetime(): mixed
    {
        $value = $this->lexer->getCurrentToken()->text;
        try {
            $value = new DateTime(trim($value, " \t\n\r\0\x0Bdatetime\'"));
        } catch (Exception) {
            throw new FilterException(
                Messages::expressionParserUnrecognizedLiteral('datetime', $value, $this->lexer->getPosition())
            );
        }
        $value = $this->ex()->literal($value);
        $this->lexer->nextToken();
        return $value;
    }

    /**
     * @return mixed
     * @throws FilterException
     */
    private function parseFloat(): mixed
    {
        $value = $this->lexer->getCurrentToken()->text;
        if (($value = filter_var($value, FILTER_VALIDATE_FLOAT)) !== false) {
            $value = $this->ex()->literal($value);
            $this->lexer->nextToken();
            return $value;
        }
        throw new FilterException(Messages::expressionLexerDigitExpected($this->lexer->getPosition()));
    }

    /**
     * @return mixed
     * @throws FilterException
     */
    private function parseNull(): mixed
    {
        $value = $this->ex()->literal(null);
        $this->lexer->nextToken();
        return $value;
    }

    /**
     * @return mixed
     * @throws FilterException
     */
    private function parseIdentifier(): mixed
    {
        if ($this->lexer->getCurrentToken()->id !== ExpressionTokenId::IDENTIFIER) {
            throw new FilterException(Messages::expressionLexerSyntaxError($this->lexer->getPosition()));
        }
        $isFunction = $this->lexer->peekNextToken()->id === ExpressionTokenId::OPEN_PARAM;
        if ($isFunction) {
            return $this->parseIdentifierAsFunction();
        } else {
            return $this->parsePropertyAccess();
        }
    }

    /**
     * @return mixed
     * @throws FilterException
     */
    private function parseIdentifierAsFunction(): mixed
    {
        $token = clone $this->lexer->getCurrentToken();
        $this->lexer->nextToken();
        $params = $this->parseArgumentList();
        return match ($token->getIdentifier()) {
            KeyWord::FUNCTION_STARTS_WITH => $this->ex()->startsWith(...$params),
            KeyWord::FUNCTION_ENDS_WITH => $this->ex()->endsWith(...$params),
            KeyWord::FUNCTION_CONTAINS => $this->ex()->contains(...$params),
            KeyWord::FUNCTION_CONCAT => $this->ex()->concat(...$params),
            KeyWord::FUNCTION_INDEX_OF => $this->ex()->indexOf(...$params),
            KeyWord::FUNCTION_LENGTH => $this->ex()->length(...$params),
            KeyWord::FUNCTION_SUBSTRING => $this->ex()->substring(...$params),
            KeyWord::FUNCTION_MATCHES_PATTERN => $this->ex()->matchesPattern(...$params),
            KeyWord::FUNCTION_TO_LOWER => $this->ex()->toLower(...$params),
            KeyWord::FUNCTION_TO_UPPER => $this->ex()->toUpper(...$params),
            KeyWord::FUNCTION_TRIM => $this->ex()->trim(...$params),
            KeyWord::FUNCTION_DATE => $this->ex()->date(...$params),
            KeyWord::FUNCTION_DAY => $this->ex()->day(...$params),
            KeyWord::FUNCTION_HOUR => $this->ex()->hour(...$params),
            KeyWord::FUNCTION_MINUTE => $this->ex()->minute(...$params),
            KeyWord::FUNCTION_MONTH => $this->ex()->month(...$params),
            KeyWord::FUNCTION_SECOND => $this->ex()->second(...$params),
            KeyWord::FUNCTION_TIME => $this->ex()->time(...$params),
            KeyWord::FUNCTION_YEAR => $this->ex()->year(...$params),
            KeyWord::FUNCTION_CEILING => $this->ex()->ceiling(...$params),
            KeyWord::FUNCTION_FLOOR => $this->ex()->floor(...$params),
            KeyWord::FUNCTION_ROUND => $this->ex()->round(...$params),
            default => throw new FilterException(
                Messages::operandOrFunctionNotImplemented($token->getIdentifier())
            )
        };
    }

    /**
     * @return mixed[]
     * @throws FilterException
     */
    private function parseArgumentList(): array
    {
        if ($this->lexer->getCurrentToken()->id !== ExpressionTokenId::OPEN_PARAM) {
            throw new FilterException(Messages::expressionLexerSyntaxError($this->lexer->getPosition()));
        }
        $this->lexer->nextToken();
        $args = $this->lexer->getCurrentToken()->id === ExpressionTokenId::CLOSE_PARAM ?
            [] : $this->parseArguments();
        if ($this->lexer->getCurrentToken()->id !== ExpressionTokenId::CLOSE_PARAM) {
            throw new FilterException(Messages::expressionLexerSyntaxError($this->lexer->getPosition()));
        }
        $this->lexer->nextToken();
        return $args;
    }

    # PARSERS

    /**
     * @return mixed[]
     * @throws FilterException
     */
    private function parseArguments(): array
    {
        $args = [];
        while (true) {
            $args[] = $this->parsePrimary();
            if ($this->lexer->getCurrentToken()->id !== ExpressionTokenId::COMMA) {
                break;
            }
            $this->lexer->nextToken();
        }
        return $args;
    }

    /**
     * @return mixed
     * @throws FilterException
     */
    private function parsePropertyAccess(): mixed
    {
        $identifier = $this->lexer->readDottedIdentifier();
        return $this->parseField($identifier);
    }

    /**
     * @return mixed
     * @throws FilterException
     */
    private function parseString(): mixed
    {
        $value = $this->lexer->getCurrentToken()->text;
        $value = str_replace("''", "'", substr($value, 1, strlen($value) - 2));
        $value = $this->ex()->literal((string)$value);
        $this->lexer->nextToken();
        return $value;
    }

    /**
     * @return mixed
     * @throws FilterException
     */
    private function parseInteger(): mixed
    {
        $value = $this->lexer->getCurrentToken()->text;

        if (($value = filter_var($value, FILTER_VALIDATE_INT)) !== false) {
            $value = $this->ex()->literal((int)$value);
            $this->lexer->nextToken();
            return $value;
        }

        throw new FilterException(Messages::expressionLexerDigitExpected($this->lexer->getPosition()));
    }

    /**
     * @return mixed
     * @throws FilterException
     */
    private function parseParentExpression(): mixed
    {
        if ($this->lexer->getCurrentToken()->id !== ExpressionTokenId::OPEN_PARAM) {
            throw new FilterException(Messages::expressionLexerSyntaxError($this->lexer->getPosition()));
        }
        $this->lexer->nextToken();
        $expr = $this->parseLogicalOr();
        if ($this->lexer->getCurrentToken()->id !== ExpressionTokenId::CLOSE_PARAM) {
            throw new FilterException(Messages::expressionLexerSyntaxError($this->lexer->getPosition()));
        }
        $this->lexer->nextToken();
        return $expr;
    }
}
