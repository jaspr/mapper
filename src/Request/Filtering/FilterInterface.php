<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Filtering;

use JSONAPI\Mapper\Request\RequestParserResultInterface;

/**
 * Interface FilterInterface
 *
 * @package JSONAPI\URI\Filtering
 */
interface FilterInterface extends RequestParserResultInterface
{
    /**
     * @return mixed
     */
    public function getCondition(): mixed;
}
