<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request;

/**
 * Interface QueryPartInterface
 *
 * @package JSONAPI\URI
 */
interface RequestPart
{
    public const FIELDS_PART_KEY = 'fields';
    public const FILTER_PART_KEY = 'filter';
    public const SORT_PART_KEY = 'sort';
    public const PAGINATION_PART_KEY = 'page';
    public const PAGINATION_PART_LIMIT_KEY = 'limit';
    public const PAGINATION_PART_OFFSET_KEY = 'offset';
    public const PAGINATION_PART_NUMBER_KEY = 'number';
    public const PAGINATION_PART_SIZE_KEY = 'size';
    public const INCLUSION_PART_KEY = 'include';
    public const PATH_PART_KEY = 'path';
    public const BODY_PART_KEY = 'body';
}
