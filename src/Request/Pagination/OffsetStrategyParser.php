<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Pagination;

use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\RequestPart;
use Psr\Http\Message\ServerRequestInterface;
use UnexpectedValueException;

/**
 * Class LimitOffsetPagination
 *
 * @package JSONAPI\URI\Pagination
 */
class OffsetStrategyParser extends PaginationParser
{
    /**
     * @var int
     */
    private int $defaultOffset;

    /**
     * @var int
     */
    private int $defaultLimit;

    /**
     * LimitOffsetPagination constructor.
     *
     * @param int $defaultOffset
     * @param int $defaultLimit
     */
    public function __construct(int $defaultOffset = 0, int $defaultLimit = 25)
    {
        $this->defaultOffset = $defaultOffset;
        $this->defaultLimit = $defaultLimit;
    }

    /**
     * @inheritDoc
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): ?LimitOffsetResult
    {
        $data = $request->getQueryParams()[RequestPart::PAGINATION_PART_KEY] ?? null;
        $limit = $this->defaultLimit;
        $offset = $this->defaultOffset;
        if ($data) {
            if (isset($data[RequestPart::PAGINATION_PART_LIMIT_KEY])) {
                /** @var int|false $limit */
                $limit = filter_var($data[RequestPart::PAGINATION_PART_LIMIT_KEY], FILTER_VALIDATE_INT);
                if ($limit === false) {
                    throw new UnexpectedValueException("Limit must be integer.");
                }
            }

            if (isset($data[RequestPart::PAGINATION_PART_OFFSET_KEY])) {
                /** @var int|false $offset */
                $offset = filter_var($data[RequestPart::PAGINATION_PART_OFFSET_KEY], FILTER_VALIDATE_INT);
                if ($offset === false) {
                    throw new UnexpectedValueException("Offset must be integer.");
                }
            }
        }
        return new LimitOffsetResult($limit, $offset);
    }
}
