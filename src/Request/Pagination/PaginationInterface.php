<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Pagination;

use JSONAPI\Mapper\Request\RequestParserResultInterface;

/**
 * Interface PaginationInterface
 *
 * @package JSONAPI\URI\Pagination
 */
interface PaginationInterface extends RequestParserResultInterface
{
    /**
     * @param int $itemsPerPage
     * @param int $startsFrom
     * @param int|null $totalItems
     */
    public function __construct(int $itemsPerPage, int $startsFrom = 0, ?int $totalItems = null);

    /**
     * Set total items count
     * It comes handy when we know total after request
     *
     * @param int $totalItems
     * @return void
     */
    public function setTotal(int $totalItems): void;

    /**
     * @return PaginationInterface|null
     */
    public function next(): ?PaginationInterface;

    /**
     * @return PaginationInterface|null
     */
    public function prev(): ?PaginationInterface;

    /**
     * @return PaginationInterface
     */
    public function first(): PaginationInterface;

    /**
     * @return PaginationInterface|null
     */
    public function last(): ?PaginationInterface;
}
