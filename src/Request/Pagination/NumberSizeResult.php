<?php

/**
 * Created by tomas
 * at 06.08.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Pagination;

use JSONAPI\Mapper\Request\RequestPart;

class NumberSizeResult extends PaginationResult
{
    protected int $startsFrom = 1;

    private function getPages(): ?int
    {
        if ($this->totalItems !== null) {
            return intval(ceil($this->totalItems / $this->itemsPerPage));
        }
        return null;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->itemsPerPage;
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return $this->startsFrom;
    }

    /**
     * @inheritDoc
     */
    public function next(): ?self
    {
        $static = null;
        if ($this->getPages() !== null) {
            if ($this->startsFrom + 1 <= $this->getPages()) {
                $static = new self($this->itemsPerPage, $this->startsFrom + 1, $this->totalItems);
            }
        } else {
            $static = new self($this->itemsPerPage, $this->startsFrom + 1);
        }
        return $static;
    }

    /**
     * @inheritDoc
     */
    public function prev(): ?self
    {
        if ($this->startsFrom - 1 > 0) {
            return new self($this->itemsPerPage, $this->startsFrom - 1, $this->totalItems);
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    public function first(): self
    {
        return new self($this->itemsPerPage, 1, $this->totalItems);
    }

    /**
     * @inheritDoc
     */
    public function last(): ?self
    {
        if ($this->getPages() !== null) {
            return new self($this->itemsPerPage, $this->getPages(), $this->totalItems);
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        return [
            RequestPart::PAGINATION_PART_KEY . '[' . RequestPart::PAGINATION_PART_SIZE_KEY . ']'
            => (string)$this->startsFrom,
            RequestPart::PAGINATION_PART_KEY . '[' . RequestPart::PAGINATION_PART_NUMBER_KEY . ']'
            => (string)$this->itemsPerPage
        ];
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return
            sprintf(
                '%s[%s]=%s',
                RequestPart::PAGINATION_PART_KEY,
                RequestPart::PAGINATION_PART_SIZE_KEY,
                $this->itemsPerPage
            ) . '&' .
            sprintf(
                '%s[%s]=%s',
                RequestPart::PAGINATION_PART_KEY,
                RequestPart::PAGINATION_PART_NUMBER_KEY,
                $this->startsFrom
            );
    }
}
