<?php

/**
 * Created by tomas
 * at 06.08.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Pagination;

abstract class PaginationResult implements PaginationInterface
{
    /**
     * @var int $itemsPerPage
     */
    protected int $itemsPerPage;
    /**
     * @var int $startsFrom
     */
    protected int $startsFrom;
    /**
     * @var int|null $totalItems
     */
    protected ?int $totalItems;

    /**
     * @param int $itemsPerPage
     * @param int $startsFrom
     * @param int|null $totalItems
     */
    public function __construct(int $itemsPerPage, int $startsFrom = 0, ?int $totalItems = null)
    {
        $this->itemsPerPage = $itemsPerPage;
        $this->startsFrom = $startsFrom;
        $this->totalItems = $totalItems;
    }

    public function setTotal(int $totalItems): void
    {
        $this->totalItems = $totalItems;
    }
}
