<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Pagination;

use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\RequestParserInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface PaginationParserInterface
 *
 * @package JSONAPI\URI\Pagination
 */
interface PaginationParserInterface extends RequestParserInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param ParsedRequest|null $parsedRequest
     * @return PaginationInterface|null
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): ?PaginationInterface;
}
