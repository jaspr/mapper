<?php

/**
 * Created by tomas
 * at 06.08.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Pagination;

use JSONAPI\Mapper\Request\RequestPart;

class LimitOffsetResult extends PaginationResult
{
    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->itemsPerPage;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->startsFrom;
    }

    /**
     * @inheritDoc
     */
    public function next(): ?self
    {
        if ($this->totalItems !== null) {
            if ($this->startsFrom + $this->itemsPerPage < $this->totalItems) {
                $static = new self($this->itemsPerPage, $this->startsFrom + $this->itemsPerPage, $this->totalItems);
            } else {
                return null;
            }
        } else {
            $static = new self($this->itemsPerPage, $this->startsFrom + $this->itemsPerPage, $this->totalItems);
        }
        return $static;
    }

    /**
     * @inheritDoc
     */
    public function prev(): ?self
    {
        if ($this->startsFrom - $this->itemsPerPage >= 0) {
            return new self($this->itemsPerPage, $this->startsFrom - $this->itemsPerPage, $this->totalItems);
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    public function first(): self
    {
        return new self($this->itemsPerPage, 0, $this->totalItems);
    }

    /**
     * @inheritDoc
     */
    public function last(): ?self
    {
        if ($this->totalItems !== null && (($this->totalItems - $this->itemsPerPage) >= $this->itemsPerPage)) {
            return new self($this->itemsPerPage, $this->totalItems - $this->itemsPerPage, $this->totalItems);
        }
        return null;
    }

    public function toArray(): array
    {
        return [
            RequestPart::PAGINATION_PART_KEY . '[' . RequestPart::PAGINATION_PART_OFFSET_KEY . ']'
            => (string)$this->startsFrom,
            RequestPart::PAGINATION_PART_KEY . '[' . RequestPart::PAGINATION_PART_LIMIT_KEY . ']'
            => (string)$this->itemsPerPage
        ];
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return
            sprintf(
                '%s[%s]=%s',
                RequestPart::PAGINATION_PART_KEY,
                RequestPart::PAGINATION_PART_OFFSET_KEY,
                $this->itemsPerPage
            ) . '&' .
            sprintf(
                '%s[%s]=%s',
                RequestPart::PAGINATION_PART_KEY,
                RequestPart::PAGINATION_PART_LIMIT_KEY,
                $this->startsFrom
            );
    }
}
