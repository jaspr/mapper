<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Pagination;

use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\RequestPart;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class PagePagination
 *
 * @package JSONAPI\URI\Pagination
 */
class PageStrategyParser extends PaginationParser
{
    /**
     * @var int
     */
    private int $defaultNumber;

    /**
     * @var int
     */
    private int $defaultSize;

    /**
     * @param int $defaultSize
     * @param int $defaultNumber
     */
    public function __construct(int $defaultSize = 25, int $defaultNumber = 1)
    {
        $this->defaultNumber = $defaultNumber;
        $this->defaultSize   = $defaultSize;
    }

    /**
     * @inheritDoc
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): ?NumberSizeResult
    {
        $data = $request->getQueryParams()[RequestPart::PAGINATION_PART_KEY] ?? null;
        $number = $this->defaultNumber;
        $size = $this->defaultSize;
        if ($data) {
            if (isset($data[RequestPart::PAGINATION_PART_NUMBER_KEY])) {
                $number = filter_var(
                    $data[RequestPart::PAGINATION_PART_NUMBER_KEY],
                    FILTER_VALIDATE_INT,
                    [
                        'options' => [
                            'default' => $this->defaultNumber
                        ]
                    ]
                );
            }

            if (isset($data[RequestPart::PAGINATION_PART_SIZE_KEY])) {
                $size = filter_var(
                    $data[RequestPart::PAGINATION_PART_SIZE_KEY],
                    FILTER_VALIDATE_INT,
                    [
                        'options' => [
                            'default' => $this->defaultSize
                        ]
                    ]
                );
            }
        }
        return new NumberSizeResult($size, $number);
    }
}
