<?php

/**
 * Created by lasicka@logio.cz
 * at 05.09.2024 22:28
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Pagination;

use JSONAPI\Mapper\Request\RequestPart;

abstract class PaginationParser implements PaginationParserInterface
{
    public function getRequestPartKey(): string
    {
        return RequestPart::PAGINATION_PART_KEY;
    }
}
