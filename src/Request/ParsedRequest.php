<?php

/**
 * Created by lasicka@logio.cz
 * at 06.10.2021 13:35
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request;

use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Request\Fieldset\FieldsetInterface;
use JSONAPI\Mapper\Request\Filtering\FilterInterface;
use JSONAPI\Mapper\Request\Inclusion\InclusionInterface;
use JSONAPI\Mapper\Request\Pagination\PaginationInterface;
use JSONAPI\Mapper\Request\Path\PathInterface;
use JSONAPI\Mapper\Request\Sorting\SortInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface ParsedRequest
 *
 * @package JSONAPI\URI
 */
class ParsedRequest
{
    private readonly string $baseUrl;

    private ?FilterInterface $filter = null;
    private ?PaginationInterface $pagination = null;
    private ?SortInterface $sort = null;
    private ?FieldsetInterface $fieldset = null;
    private ?InclusionInterface $inclusion = null;
    private ?PathInterface $path = null;
    private ?Document $body = null;

    public function __construct(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    public function getFilter(): ?FilterInterface
    {
        return $this->filter;
    }

    public function setFilter(FilterInterface $filter): void
    {
        $this->filter = $filter;
    }

    public function getPagination(): ?PaginationInterface
    {
        return $this->pagination;
    }

    public function setPagination(PaginationInterface $pagination): void
    {
        $this->pagination = $pagination;
    }

    public function getSort(): ?SortInterface
    {
        return $this->sort;
    }

    public function setSort(SortInterface $sort): void
    {
        $this->sort = $sort;
    }

    public function getFieldset(): ?FieldsetInterface
    {
        return $this->fieldset;
    }

    public function setFieldset(FieldsetInterface $fieldset): void
    {
        $this->fieldset = $fieldset;
    }

    public function getInclusion(): ?InclusionInterface
    {
        return $this->inclusion;
    }

    public function setInclusion(InclusionInterface $inclusion): void
    {
        $this->inclusion = $inclusion;
    }

    public function getPath(): ?PathInterface
    {
        return $this->path;
    }

    public function setPath(PathInterface $path): void
    {
        $this->path = $path;
    }

    public function getBody(): ?Document
    {
        return $this->body;
    }

    public function setBody(Document $body): void
    {
        $this->body = $body;
    }
}
