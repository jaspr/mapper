<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request;

use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Driver\NamingStrategy;
use JSONAPI\Mapper\Driver\RecommendedNamingStrategy;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Request\Body\BodyParser;
use JSONAPI\Mapper\Request\Body\BodyParserInterface;
use JSONAPI\Mapper\Request\Fieldset\FieldsetInterface;
use JSONAPI\Mapper\Request\Fieldset\FieldsetParser;
use JSONAPI\Mapper\Request\Fieldset\FieldsetParserInterface;
use JSONAPI\Mapper\Request\Filtering\FilterInterface;
use JSONAPI\Mapper\Request\Filtering\FilterParserInterface;
use JSONAPI\Mapper\Request\Filtering\OData\ExpressionFilterParser;
use JSONAPI\Mapper\Request\Inclusion\InclusionInterface;
use JSONAPI\Mapper\Request\Inclusion\InclusionParser;
use JSONAPI\Mapper\Request\Inclusion\InclusionParserInterface;
use JSONAPI\Mapper\Request\Pagination\OffsetStrategyParser;
use JSONAPI\Mapper\Request\Pagination\PaginationInterface;
use JSONAPI\Mapper\Request\Pagination\PaginationParserInterface;
use JSONAPI\Mapper\Request\Path\PathInterface;
use JSONAPI\Mapper\Request\Path\PathParser;
use JSONAPI\Mapper\Request\Path\PathParserInterface;
use JSONAPI\Mapper\Request\Sorting\SortInterface;
use JSONAPI\Mapper\Request\Sorting\SortParser;
use JSONAPI\Mapper\Request\Sorting\SortParserInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

/**
 * Class Parser
 * @package JSONAPI\Mapper\Request
 */
final class Parser implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var string $baseUrl
     */
    private string $baseUrl;

    /**
     * @var RequestParserInterface[] $parsers
     */
    private array $parsers = [];

    /**
     * @param string $baseUrl
     */
    public function __construct(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
        $this->logger  = new NullLogger();
    }

    /**
     *
     * @param ServerRequestInterface $request
     * @return ParsedRequest
     */
    public function parse(ServerRequestInterface $request): ParsedRequest
    {
        $parsedRequest = new ParsedRequest($this->baseUrl);
        $this->logger->debug('Begin parsing');
        foreach ($this->parsers as $parser) {
            $this->logger->debug(sprintf('Begin request parsing by [%s]', $parser::class));
            $result = $parser->parse($request, $parsedRequest);
            switch (true) {
                case is_a($result, PathInterface::class):
                    $parsedRequest->setPath($result);
                    break;
                case is_a($result, FilterInterface::class):
                    $parsedRequest->setFilter($result);
                    break;
                case is_a($result, SortInterface::class):
                    $parsedRequest->setSort($result);
                    break;
                case is_a($result, PaginationInterface::class):
                    $parsedRequest->setPagination($result);
                    break;
                case is_a($result, InclusionInterface::class):
                    $parsedRequest->setInclusion($result);
                    break;
                case is_a($result, FieldsetInterface::class):
                    $parsedRequest->setFieldset($result);
                    break;
                case is_a($result, Document::class):
                    $parsedRequest->setBody($result);
                    break;
            }
        }
        return $parsedRequest;
    }

    public function addParser(RequestParserInterface $parser): void
    {
        $this->parsers[] = $parser;
    }


    /**
     * @param string $baseUrl
     * @param MetadataRepository $repository
     * @return Parser
     */
    public static function createDefault(string $baseUrl, MetadataRepository $repository): Parser
    {
        $self           = new self($baseUrl);
        $namingStrategy = new RecommendedNamingStrategy();
        $pathParser     = new PathParser($repository, $baseUrl, $namingStrategy);
        $self->addParser($pathParser);
        $self->addParser(new OffsetStrategyParser());
        $self->addParser(new SortParser());
        $self->addParser(new InclusionParser());
        $self->addParser(new FieldsetParser());
        $self->addParser(new ExpressionFilterParser($repository, $pathParser));
        $self->addParser(new BodyParser($repository, $pathParser));
        return $self;
    }
}
