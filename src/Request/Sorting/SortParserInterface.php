<?php

/**
 * Created by lasicka@logio.cz
 * at 06.10.2021 13:15
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Sorting;

use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\RequestParserInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface SortParserInterface
 *
 * @package JSONAPI\URI\Sorting
 */
interface SortParserInterface extends RequestParserInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param ParsedRequest|null $parsedRequest
     * @return SortInterface|null
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): ?SortInterface;
}
