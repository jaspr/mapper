<?php

/**
 * Created by tomas
 * at 06.08.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Sorting;

use JSONAPI\Mapper\Request\RequestPart;

readonly class SortResult implements SortInterface
{
    /**
     * @var string[]
     */
    private array $sort;

    /**
     * @param string[] $sort
     */
    public function __construct(array $sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return string[] associative array contains field as key and order as value
     * @example [
     *      "fieldA" => "DESC",
     *      "fieldB" => "ASC"
     * ]
     */
    public function getOrder(): array
    {
        return $this->sort;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        $str = '';
        if (count($this->sort) > 0) {
            $str = 'sort=';
            $str .= implode(
                ',',
                array_map(
                    fn($field, $sort) => ($sort === SortInterface::DESC ? '-' : '') . $field,
                    array_keys($this->sort),
                    array_values($this->sort)
                )
            );
        }
        return $str;
    }

    /**
     * @return string[]
     */
    public function toArray(): array
    {
        if (!empty($this->sort)) {
            return [
                RequestPart::SORT_PART_KEY =>
                    implode(
                        ',',
                        array_map(
                            fn($field, $sort) => ($sort === SortInterface::DESC ? '-' : '') . $field,
                            array_keys($this->sort),
                            array_values($this->sort)
                        )
                    )
            ];
        }
        return [];
    }
}
