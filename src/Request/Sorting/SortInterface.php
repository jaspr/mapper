<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Sorting;

use JSONAPI\Mapper\Request\RequestParserResultInterface;

/**
 * Interface SortInterface
 *
 * @package JSONAPI\URI\Sorting
 */
interface SortInterface extends RequestParserResultInterface
{
    public const ASC = 'ASC';
    public const DESC = 'DESC';

    /**
     * @return string[] associative array contains field as key and order as value
     * @example [
     *      "fieldA" => "DESC",
     *      "fieldB" => "ASC"
     * ]
     */
    public function getOrder(): array;
}
