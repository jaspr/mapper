<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Sorting;

use JSONAPI\Mapper\Exception\PatternDoesNotMatch;
use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\RequestPart;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class SortParser
 *
 * @package JSONAPI\URI\Fieldset
 */
class SortParser implements SortParserInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param ParsedRequest|null $parsedRequest
     * @inheritDoc
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): ?SortInterface
    {
        $data = $request->getQueryParams()[RequestPart::SORT_PART_KEY] ?? null;
        if (!is_null($data) && strlen($data) > 0) {
            $sort  = [];
            $parts = explode(',', $data);
            $parts = array_map('trim', $parts);
            foreach ($parts as $part) {
                if (
                    preg_match(
                        '/^(?P<sort>(-|))(?P<field>((\.)?(?![-_])[a-zA-Z0-9][-a-zA-Z0-9_]*[a-zA-Z0-9](?![-_]))+)$/',
                        $part,
                        $matches
                    )
                ) {
                    $sort[$matches['field']] = strlen($matches['sort']) ? SortInterface::DESC
                        : SortInterface::ASC;
                } else {
                    throw new PatternDoesNotMatch("Sort does not match expected pattern.");
                }
            }
            return new SortResult($sort);
        }
        return null;
    }

    public function getRequestPartKey(): string
    {
        return RequestPart::SORT_PART_KEY;
    }
}
