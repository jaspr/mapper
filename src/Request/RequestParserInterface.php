<?php

/**
 * Created by @bednic
 * at 13.08.2024 21:36
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request;

use Psr\Http\Message\ServerRequestInterface;

interface RequestParserInterface
{
    /**
     * Return parsed result or null, if data for parser does not exit.
     *
     * @param ServerRequestInterface $request
     * @param ParsedRequest|null $parsedRequest
     * @return RequestParserResultInterface|null
     */
    public function parse(
        ServerRequestInterface $request,
        ?ParsedRequest $parsedRequest = null
    ): ?RequestParserResultInterface;
}
