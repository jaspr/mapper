<?php

/**
 * Created by lasicka@logio.cz
 * at 06.10.2021 13:13
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Inclusion;

use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\RequestParserInterface;
use Psr\Http\Message\ServerRequestInterface;

interface InclusionParserInterface extends RequestParserInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param ParsedRequest|null $parsedRequest
     * @return InclusionInterface|null
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): ?InclusionInterface;
}
