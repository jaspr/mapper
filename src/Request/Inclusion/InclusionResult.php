<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Inclusion;

use JSONAPI\Mapper\Request\RequestPart;

/**
 * Class Inclusion
 *
 * @package JSONAPI\URI\Inclusion
 */
class InclusionResult implements InclusionInterface
{
    /**
     * @var Inclusion[]
     */
    private array $inclusions = [];

    /**
     * @var string|null $data
     * @deprecated
     */
    private ?string $data;

    /**
     * Inclusion constructor.
     * todo: Remove data and build inclusion from inclusions
     */
    public function __construct(?string $data)
    {
        $this->data = $data;
    }

    /**
     * @return bool
     */
    public function hasInclusions(): bool
    {
        return count($this->inclusions) > 0;
    }

    /**
     * @param Inclusion $inclusion
     */
    public function addInclusion(Inclusion $inclusion): void
    {
        $this->inclusions[$inclusion->getRelationName()] = $inclusion;
    }

    /**
     * @return Inclusion[]
     */
    public function getInclusions(): array
    {
        return $this->inclusions;
    }

    /**
     * @param string $relation
     * @return bool
     */
    public function contains(string $relation): bool
    {
        return array_key_exists($relation, $this->inclusions);
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->data ? RequestPart::INCLUSION_PART_KEY . '=' . rawurlencode($this->data) : '';
    }

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        if ($this->data) {
            return [RequestPart::INCLUSION_PART_KEY => $this->data];
        }
        return [];
    }

    /**
     * @return array<string, mixed>
     * @example ['foo'=> ['bar'=>[]]]
     */
    public function toTree(): array
    {
        $tree = [];
        foreach ($this->inclusions as $inclusion) {
            if (!isset($tree[$inclusion->getRelationName()])) {
                $tree[$inclusion->getRelationName()] = [];
            }
            $root = &$tree[$inclusion->getRelationName()];
            $this->makeBranch($root, $inclusion);
        }
        return $tree;
    }

    /**
     * @param array<mixed> $root
     * @param Inclusion $inclusion
     * @return void
     */
    private function makeBranch(array &$root, Inclusion $inclusion): void
    {
        foreach ($inclusion->getInclusions() as $child) {
            if (!isset($root[$child->getRelationName()])) {
                $root[$child->getRelationName()] = [];
            }
            $parent = &$root[$child->getRelationName()];
            $this->makeBranch($parent, $child);
        }
    }
}
