<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Inclusion;

use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\RequestPart;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class InclusionParser
 *
 * @package JSONAPI\URI\Inclusion
 */
class InclusionParser implements InclusionParserInterface
{
    /**
     * @inheritDoc
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): ?InclusionInterface
    {
        $data = $request->getQueryParams()[RequestPart::INCLUSION_PART_KEY] ?? null;
        if ($data) {
            $inclusions = new InclusionResult($data);
            $t = explode(',', $data);
            $tree = [];
            foreach ($t as $i) {
                $branch = [];
                self::dot2tree($branch, $i, []);
                $tree = array_merge_recursive($tree, $branch);
            }
            foreach ($tree as $rel => $sub) {
                $parent = new Inclusion($rel);
                $inclusions->addInclusion($parent);
                if ($sub) {
                    $this->makeInclusionTree($parent, $sub);
                }
            }
            return $inclusions;
        }
        return null;
    }

    /**
     * @param array<string> $arr
     * @param string $path
     * @param string[] $value
     */
    private static function dot2tree(array &$arr, string $path, array $value): void
    {
        $keys = explode('.', $path);
        foreach ($keys as $key) {
            /* @phpstan-ignore parameterByRef.type */
            $arr = &$arr[$key];
        }
        $arr = $value;
    }

    /**
     * @param Inclusion $parent
     * @param string[]|string[][] $branch
     */
    private function makeInclusionTree(Inclusion $parent, array $branch): void
    {
        foreach ($branch as $name => $sub) {
            $child = new Inclusion($name);
            $parent->addInclusion($child);
            if ($sub) {
                $this->makeInclusionTree($child, $sub);
            }
        }
    }

    public function getRequestPartKey(): string
    {
        return RequestPart::INCLUSION_PART_KEY;
    }
}
