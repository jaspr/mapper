<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Request\Inclusion;

use JSONAPI\Mapper\Request\RequestParserResultInterface;

/**
 * Interface InclusionInterface
 *
 * @package JSONAPI\URI\Inclusion
 */
interface InclusionInterface extends RequestParserResultInterface
{
    /**
     * @return Inclusion[]
     */
    public function getInclusions(): array;

    /**
     * @return bool
     */
    public function hasInclusions(): bool;

    /**
     * @param string $relation
     *
     * @return bool
     */
    public function contains(string $relation): bool;
}
