<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Middleware;

use Fig\Http\Message\StatusCodeInterface;
use JSONAPI\Mapper\Compose\Builder;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Encoding\RequestDependentProcessor;
use JSONAPI\Mapper\ErrorFactory;
use JSONAPI\Mapper\Exception\FilterException;
use JSONAPI\Mapper\Exception\MetadataNotFound;
use JSONAPI\Mapper\Exception\RequiredMemberMissing;
use JSONAPI\Mapper\Exception\UnsupportedMediaType;
use JSONAPI\Mapper\Extension\Definition;
use JSONAPI\Mapper\Request\Parser;
use JSONAPI\Mapper\Request\RequestPart;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class PsrJsonApiMiddleware
 *
 * @package JSONAPI\Middleware
 */
class PsrJsonApiMiddleware implements MiddlewareInterface
{
    public const MEDIA_TYPE = 'application/vnd.api+json';

    public const BUILDER = '__jsonapi:builder__';
    /**
     * @var ResponseFactoryInterface
     */
    private ResponseFactoryInterface $responseFactory;
    /**
     * @var StreamFactoryInterface
     */
    private StreamFactoryInterface $streamFactory;
    /**
     * @var Parser $parser
     */
    private Parser $parser;
    /**
     * @var Encoder $encoder
     */
    private Encoder $encoder;
    /**
     * @var Definition[] $extensions
     */
    private array $extensions = [];

    /**
     * @var Definition[] $activeExtensions
     */
    private array $activeExtensions = [];

    /**
     * PsrJsonApiMiddleware constructor.
     *
     * @param ResponseFactoryInterface $responseFactory
     * @param StreamFactoryInterface $streamFactory
     * @param Parser $parser
     * @param Encoder $encoder
     */
    public function __construct(
        ResponseFactoryInterface $responseFactory,
        StreamFactoryInterface $streamFactory,
        Parser $parser,
        Encoder $encoder
    ) {
        $this->responseFactory = $responseFactory;
        $this->streamFactory   = $streamFactory;
        $this->parser          = $parser;
        $this->encoder         = $encoder;
    }

    public function addExtension(Definition $definition): void
    {
        $this->extensions[$definition->getURI()] = $definition;
    }

    /**
     * Process an incoming server request.
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     *
     * Every runtime exception which occurred before request handle is considered bad request.
     * Every exception which occurred after request handle is considered server error. Server errors must be handled by
     * implementation because can store sensitive data.
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            $this->processAccept($request);
            if ($request->getBody()->getSize() > 0) {
                $this->processContentType($request);
            }
            // Parse request
            $parsedRequest = $this->parser->parse($request);
            if (!empty($parsedRequest->getBody()?->getErrors())) {
                $document = $parsedRequest->getBody();
                return $this->responseFactory
                    ->createResponse(StatusCodeInterface::STATUS_BAD_REQUEST)
                    ->withHeader('Content-Type', self::MEDIA_TYPE)
                    ->withBody($this->streamFactory->createStream($this->encode($document)));
            }
        } catch (\RuntimeException $e) {
            $error    = match ($e::class) {
                MetadataNotFound::class => ErrorFactory::unknownResource($e->getMessage()),
                FilterException::class => ErrorFactory::malformedQueryParameter(
                    RequestPart::FILTER_PART_KEY,
                    $e->getMessage()
                ),
                UnsupportedMediaType::class => ErrorFactory::unsupportedMediaType(),
                RequiredMemberMissing::class => ErrorFactory::requiredMemberMissing($e),
                default => ErrorFactory::badRequest($e->getMessage())
            };
            $document = $this->encoder->failure([$error]);
            return $this->responseFactory
                ->createResponse((int)$error->getStatus())
                ->withHeader('Content-Type', self::MEDIA_TYPE)
                ->withBody($this->streamFactory->createStream($this->encode($document)));
        }

        // Assign request to processors demanding it
        foreach ($this->encoder->getProcessors() as $processor) {
            if ($processor instanceof RequestDependentProcessor) {
                $processor->setRequest($parsedRequest);
            }
        }

        // Proceed request
        $builder  = new Builder($parsedRequest, $this->encoder);
        $request  = $request->withAttribute(self::BUILDER, $builder);
        $request  = $request->withParsedBody($parsedRequest->getBody());
        $response = $handler->handle($request);
        return $response->withHeader('Content-Type', $this->getMediaType());
    }

    private function processContentType(ServerRequestInterface $request): void
    {
        $mediaTypes = $request->getHeader('Content-Type');
        if (!in_array(self::MEDIA_TYPE, $mediaTypes)) {
            throw new UnsupportedMediaType();
        }
        foreach ($mediaTypes as $mediaType) {
            if (str_contains($mediaType, 'ext=')) {
                $extensions = explode(" ", trim(str_replace('ext=', '', $mediaType), "\""));
                foreach ($extensions as $extension) {
                    if (!array_key_exists($extension, $this->extensions)) {
                        throw new UnsupportedMediaType();
                    }
                    $definition = $this->extensions[$extension];
                    foreach ($definition->getRequestParsers() as $requestParser) {
                        $this->parser->addParser($requestParser);
                    }
                }
            }
        }
    }

    private function processAccept(ServerRequestInterface $request): void
    {
        if ($request->hasHeader('Accept')) {
            $mediaTypes = $request->getHeader("Accept");
            if (!in_array(self::MEDIA_TYPE, $mediaTypes)) {
                throw new UnsupportedMediaType();
            }
            foreach ($mediaTypes as $mediaType) {
                if (str_contains($mediaType, 'ext=')) {
                    $extensions = explode(" ", trim(str_replace('ext=', '', $mediaType), "\""));
                    foreach ($extensions as $extension) {
                        if (!array_key_exists($extension, $this->extensions)) {
                            throw new UnsupportedMediaType();
                        }
                        $definition = $this->extensions[$extension];
                        foreach ($definition->getProcessors() as $processor) {
                            $this->encoder->addProcessor($processor);
                        }
                        $this->activeExtensions[] = $definition;
                    }
                }
            }
        }
    }

    /**
     * @return string[]
     */
    private function getMediaType(): array
    {
        $contentType = [Document::getMediaType()];
        $extensions  = [];
        foreach ($this->activeExtensions as $extension) {
            $ext = $extension->getURI();
            if (!empty($ext) && !in_array($ext, $extensions)) {
                $extensions[] = $ext;
            }
        }
        $contentType[] = 'ext="' . implode(' ', $extensions) . '"';
        return $contentType;
    }

    private function encode(Document $document): string
    {
        return json_encode($document, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_PRESERVE_ZERO_FRACTION);
    }
}
