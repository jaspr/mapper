<?php

/**
 * Created by tomas
 * at 20.03.2021 20:34
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding\Processor;

use JSONAPI\Mapper\Document\Relationship;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\ResourceObjectIdentifier;
use JSONAPI\Mapper\Encoding\RelationshipProcessor;
use JSONAPI\Mapper\Encoding\ResourceIdentifierProcessor;
use JSONAPI\Mapper\Encoding\ResourceProcessor;
use JSONAPI\Mapper\Metadata\MetadataRepository;

/**
 * Class MetaProcessor
 *
 * @package JSONAPI\Encoding
 */
class MetaProcessor implements ResourceIdentifierProcessor, ResourceProcessor, RelationshipProcessor
{
    /**
     * @var MetadataRepository
     */
    private MetadataRepository $repository;

    /**
     * MetaProcessor constructor.
     *
     * @param MetadataRepository $repository
     */
    public function __construct(MetadataRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @inheritDoc
     */
    public function processResource(ResourceObject $resource, object $object): void
    {
        $this->processResourceIdentifier($resource, $object);
    }

    /**
     * @inheritDoc
     */
    public function processRelationship(Relationship $resource, object $object): void
    {
        $metadata = $this->repository->getByType($resource->getOwner()->getType());
        if ($metadata->getRelationship($resource->getName())->meta) {
            $meta = call_user_func([$object, $metadata->getRelationship($resource->getName())->meta->getter]);
            $resource->setMeta($meta);
        }
    }

    /**
     * @inheritDoc
     */
    public function processResourceIdentifier(ResourceObjectIdentifier $resource, object $object): void
    {
        $metadata = $this->repository->getByType($resource->getType());
        if ($meta = $metadata->getMeta()) {
            $meta = call_user_func([$object, $meta->getter]);
            $resource->setMeta($meta);
        }
    }
}
