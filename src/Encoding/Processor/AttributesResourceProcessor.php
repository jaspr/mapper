<?php

/**
 * Created by tomas
 * at 20.03.2021 21:13
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding\Processor;

use BackedEnum;
use DateTimeInterface;
use JSONAPI\Mapper\Document\Attribute;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Encoding\ResourceProcessor;
use JSONAPI\Mapper\Exception\Messages;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use UnexpectedValueException;

/**
 * Class AttributesResourceProcessor
 *
 * @package JSONAPI\Encoding
 */
class AttributesResourceProcessor extends FieldsProcessor implements ResourceProcessor
{
    private MetadataRepository $repository;

    /**
     * AttributesResourceProcessor constructor.
     *
     * @param MetadataRepository $repository
     */
    public function __construct(MetadataRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @inheritDoc
     */
    public function processResource(ResourceObject $resource, object $object): void
    {
        if ($metadata = $this->repository->getByType($resource->getType())) {
            foreach ($metadata->getAttributes() as $attribute) {
                $value = $this->getValue($attribute, $object);
                if ($value instanceof DateTimeInterface) {
                    // ISO 8601
                    $value = $value->format(DATE_ATOM);
                }
                if ($value instanceof BackedEnum) {
                    $value = $value->value;
                }
                $resource->addAttribute(new Attribute($attribute->name, $value));
            }
        } else {
            throw new UnexpectedValueException(Messages::classMetadataNotFound($resource->getType()));
        }
    }
}
