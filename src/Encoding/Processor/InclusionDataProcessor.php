<?php

/**
 * Created by tomas
 * at 25.05.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding\Processor;

use JSONAPI\Mapper\Document\DefaultNamespace;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\ResourceCollection;
use JSONAPI\Mapper\Encoding\DocumentProcessor;
use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Encoding\RequestDependentProcessor;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Metadata\Relationship;
use JSONAPI\Mapper\Request\Inclusion\Inclusion;
use JSONAPI\Mapper\Request\Inclusion\InclusionInterface;
use JSONAPI\Mapper\Request\ParsedRequest;

/**
 * Class InclusionDocumentProcessor
 *
 * @package JSONAPI\Mapper\Encoding
 */
class InclusionDataProcessor implements DocumentProcessor, RequestDependentProcessor
{
    /**
     * @var MetadataRepository $repository
     */
    protected MetadataRepository $repository;
    /**
     * @var InclusionInterface|null $inclusion
     */
    protected InclusionInterface|null $inclusion = null;
    /**
     * @var Encoder $encoder
     */
    protected Encoder $encoder;
    /**
     * @var ResourceCollection $included
     */
    protected ResourceCollection $included;

    /**
     * @param MetadataRepository $repository
     * @param Encoder $encoder
     */
    public function __construct(MetadataRepository $repository, Encoder $encoder)
    {
        $this->repository = $repository;
        $this->encoder    = $encoder;
    }

    /**
     * @inheritDoc
     */
    public function setRequest(ParsedRequest $request): void
    {
        $this->inclusion = $request->getInclusion();
    }

    /**
     * @inheritDoc
     */
    public function processDocument(Document $document, object|iterable|null $data): void
    {
        if ($document->getNamespaces()->has(DefaultNamespace::class) && !is_null($this->inclusion)) {
            /** @var DefaultNamespace $namespace */
            $namespace = $document->getNamespaces()->get(DefaultNamespace::class);
            if ($this->inclusion->hasInclusions()) {
                $this->included = new ResourceCollection();
                if (is_iterable($data)) {
                    foreach ($data as $item) {
                        $this->fetchInclusions($item, $this->inclusion->getInclusions());
                    }
                } elseif (is_object($data)) {
                    $this->fetchInclusions($data, $this->inclusion->getInclusions());
                }
                $namespace->setIncludes($this->included);
            }
        }
    }

    /**
     * @param object $object
     * @param Inclusion[] $inclusions
     */
    private function fetchInclusions(object $object, array $inclusions): void
    {
        $classMetadata = $this->repository->getByClass(get_class($object));
        foreach ($inclusions as $sub) {
            $relationship = $classMetadata->getRelationship($sub->getRelationName());
            $data         = $this->getData($object, $relationship);
            if (!empty($data)) {
                if ($relationship->isCollection) {
                    foreach ($data as $item) {
                        $this->addInclusion($item);
                        if ($sub->hasInclusions()) {
                            $this->fetchInclusions($item, $sub->getInclusions());
                        }
                    }
                } else {
                    $this->addInclusion($data);
                    if ($sub->hasInclusions()) {
                        $this->fetchInclusions($data, $sub->getInclusions());
                    }
                }
            }
        }
    }

    /**
     * @param object $item
     *
     * @return void
     */
    private function addInclusion(object $item): void
    {
        $this->included->add($this->encoder->encode($item));
    }

    /**
     * @param object $object - instance of primary data object
     * @param Relationship $relationship
     * @return object|iterable<object>|null
     */
    protected function getData(object $object, Relationship $relationship): null|object|iterable
    {
        $data = null;
        if ($relationship->property) {
            $data = $object->{$relationship->property};
        } elseif ($relationship->getter) {
            $data = call_user_func([$object, $relationship->getter]);
        }
        return $data;
    }
}
