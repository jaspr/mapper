<?php

/**
 * Created by @bednic
 * at 06.08.2024 23:37
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding\Processor;

use JSONAPI\Mapper\Document\DefaultNamespace;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\ResourceCollection;
use JSONAPI\Mapper\Encoding\DocumentProcessor;
use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Encoding\RequestDependentProcessor;
use JSONAPI\Mapper\Request\ParsedRequest;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class PrimaryDataProcessor implements DocumentProcessor, RequestDependentProcessor, LoggerAwareInterface
{
    use LoggerAwareTrait;

    private Encoder $encoder;
    private ?ParsedRequest $request = null;

    public function __construct(Encoder $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @inheritDoc
     */
    public function setRequest(ParsedRequest $request): void
    {
        $this->request = $request;
    }

    /**
     * @inheritDoc
     */
    public function processDocument(Document $document, object|iterable|null $data): void
    {
        if ($document->getNamespaces()->has(DefaultNamespace::class)) {
            $namespace       = $document->getNamespaces()->get(DefaultNamespace::class);
            $onlyIdentifiers = $this->request?->getPath()?->isRelationship() ?? false;
            if (is_iterable($data)) {
                $collection = new ResourceCollection();
                foreach ($data as $item) {
                    try {
                        if ($onlyIdentifiers) {
                            $collection->add($this->encoder->identify($item));
                        } else {
                            $collection->add($this->encoder->encode($item));
                        }
                    } catch (\UnexpectedValueException $e) {
                        $this->logger?->warning($e->getMessage());
                    }
                }
                $namespace->setData($collection);
            } elseif (is_object($data)) {
                try {
                    if ($onlyIdentifiers) {
                        $namespace->setData($this->encoder->identify($data));
                    } else {
                        $namespace->setData($this->encoder->encode($data));
                    }
                } catch (\UnexpectedValueException $e) {
                    $this->logger?->warning($e->getMessage());
                }
            } else {
                $namespace->setData($data);
            }
        }
    }
}
