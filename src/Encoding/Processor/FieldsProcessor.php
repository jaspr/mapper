<?php

/**
 * Created by tomas
 * at 20.03.2021 16:56
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding\Processor;

use JSONAPI\Mapper\Metadata\Field;

/**
 * Class FieldsProcessor
 *
 * @package JSONAPI\Encoding
 */
abstract class FieldsProcessor
{
    /**
     * @param Field $field
     * @param object $object
     *
     * @return mixed
     */
    protected function getValue(Field $field, object $object): mixed
    {
        return $field->getter != null ? call_user_func([$object, $field->getter]) : $object->{$field->property};
    }
}
