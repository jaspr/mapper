<?php

/**
 * Created by tomas
 * at 20.03.2021 21:18
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding\Processor;

use InvalidArgumentException;
use JSONAPI\Mapper\Document;
use JSONAPI\Mapper\Document\ResourceCollection;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\ResourceObjectIdentifier;
use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Encoding\ResourceProcessor;
use JSONAPI\Mapper\Metadata;
use JSONAPI\Mapper\Metadata\Field;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\ObjectCollection;

/**
 * Class RelationshipsResourceProcessor
 *
 * @package JSONAPI\Encoding
 */
class RelationshipsResourceProcessor extends FieldsProcessor implements ResourceProcessor
{
    /**
     * @var MetadataRepository
     */
    private MetadataRepository $repository;
    /**
     * @var Encoder $encoder
     */
    private Encoder $encoder;

    /**
     * RelationshipsResourceProcessor constructor.
     *
     * @param MetadataRepository $repository
     * @param Encoder $encoder
     */
    public function __construct(MetadataRepository $repository, Encoder $encoder)
    {
        $this->repository = $repository;
        $this->encoder = $encoder;
    }

    /**
     * @inheritDoc
     */
    public function processResource(ResourceObject $resource, object $object): void
    {
        $metadata = $this->repository->getByType($resource->getType());
        foreach ($metadata->getRelationships() as $field) {
            $relationship = new Document\Relationship($field->name, $resource);
            if ($field->withData) {
                $relationship->setData($this->getValue($field, $object), $field->withData);
            }
            $resource->addRelationship($relationship);
        }
    }


    /**
     * @param Metadata\Relationship $field
     * @param mixed $object
     *
     * @return ResourceObjectIdentifier|ResourceCollection|null
     */
    protected function getValue(Field $field, mixed $object): ResourceObjectIdentifier|ResourceCollection|null
    {
        if (!is_a($field, Metadata\Relationship::class)) {
            throw new InvalidArgumentException("Unexpected class instance");
        }
        $value = $field->getter != null ? call_user_func([$object, $field->getter]) : $object->{$field->property};
        if ($field->isCollection) {
            if (!($value instanceof ObjectCollection)) {
                $value = new ObjectCollection($value);
            }
            $total = $value->count();
            if (is_int($field->withData)) {
                $value = $value->slice(0, $field->withData);
            }
            $data = new ResourceCollection([], $total);
            foreach ($value as $object) {
                $data->add($this->encoder->identify($object));
            }
        } elseif ($value) {
            $data = $this->encoder->identify($value);
        } else {
            $data = null;
        }
        return $data;
    }
}
