<?php

/**
 * Created by tomas
 * at 20.03.2021 21:05
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding\Processor;

use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\Link;
use JSONAPI\Mapper\Document\NamespaceCollection;
use JSONAPI\Mapper\Document\Relationship;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Driver\NamingStrategy;
use JSONAPI\Mapper\Encoding\DocumentProcessor;
use JSONAPI\Mapper\Encoding\LinkFactory;
use JSONAPI\Mapper\Encoding\RelationshipProcessor;
use JSONAPI\Mapper\Encoding\RequestDependentProcessor;
use JSONAPI\Mapper\Encoding\ResourceProcessor;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Request\Pagination\PaginationInterface;
use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\URL;

/**
 * Class LinksProcessor
 *
 * @package JSONAPI\Encoding
 */
class LinksProcessor implements
    ResourceProcessor,
    RelationshipProcessor,
    DocumentProcessor,
    RequestDependentProcessor
{
    /**
     * @var LinkFactory linkFactory
     */
    private LinkFactory $linkFactory;
    /**
     * @var ParsedRequest pagination
     */
    private ParsedRequest $request;

    /**
     * LinksProcessor constructor.
     *
     * @param MetadataRepository $repository
     * @param NamingStrategy|null $namingStrategy
     */
    public function __construct(MetadataRepository $repository, ?NamingStrategy $namingStrategy = null)
    {
        $this->linkFactory = new LinkFactory($repository, $namingStrategy);
    }

    /**
     * @inheritDoc
     */
    public function setRequest(ParsedRequest $request): void
    {
        $this->request = $request;
    }

    /**
     * @inheritDoc
     */
    public function processResource(ResourceObject $resource, object $object): void
    {
        $resLinks = $this->linkFactory->getResourceLinksByType($resource->getType());
        $self = URL::create($this->getBaseURL())
            ->path($resLinks[Link::SELF])
            ->replace('id', $resource->getId());
        $resource->getLinks()->create(Link::SELF, (string)$self);
    }

    /**
     * @inheritDoc
     */
    public function processRelationship(Relationship $resource, object $object): void
    {
        $relLinks = $this->linkFactory->getRelationshipLinksByType(
            $resource->getOwner()->getType(),
            $resource->getName()
        );
        $self = URL::create($this->getBaseURL())
            ->path($relLinks[Link::SELF])
            ->replace('id', $resource->getOwner()->getId());
        $resource->getLinks()->create(Link::SELF, (string)$self);
        $related = URL::create($this->getBaseURL())
            ->path($relLinks[Link::RELATED])
            ->replace('id', $resource->getOwner()->getId());
        $resource->getLinks()->create(Link::RELATED, (string)$related);
        if (is_int($resource->withData()) && !is_null($this->request->getPagination())) {
            $pagination = $this->request->getPagination()::class;
            /** @var PaginationInterface $result */
            $result = new $pagination(itemsPerPage: $resource->withData(), totalItems: $resource->getData()->count());
            $first = $self->clone()->query($result->toArray());
            $resource->getLinks()->create(Link::FIRST, (string)$first);
            if ($result->prev()) {
                $prev = $self->clone()->query($result->prev()->toArray());
                $resource->getLinks()->create(Link::PREV, (string)$prev);
            }
            if ($result->next()) {
                $next = $self->clone()->query($result->next()->toArray());
                $resource->getLinks()->create(Link::NEXT, (string)$next);
            }
            if ($result->last()) {
                $last = $self->clone()->query($result->last()->toArray());
                $resource->getLinks()->create(Link::LAST, (string)$last);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function processDocument(Document $document, object|iterable|null $data): void
    {
        if ($this->request->getPath()) {
            if ($this->request->getPath()->getRelationshipName()) {
                $links = $this->linkFactory->getRelationshipLinksByType(
                    $this->request->getPath()->getResourceType(),
                    $this->request->getPath()->getRelationshipName()
                );
                if ($this->request->getPath()->isRelationship()) {
                    $self    = URL::create($this->getBaseURL())
                        ->path($links[Link::SELF])
                        ->replace('id', $this->request->getPath()->getId());
                    $related = URL::create($this->getBaseURL())
                        ->path($links[Link::RELATED])
                        ->replace('id', $this->request->getPath()->getId());
                } else {
                    $self    = URL::create($this->getBaseURL())
                        ->path($links[Link::RELATED])
                        ->replace('id', $this->request->getPath()->getId());
                    $related = null;
                }
            } elseif ($this->request->getPath()->isCollection()) {
                $links   = $this->linkFactory->getResourceLinksByType(
                    $this->request->getPath()->getPrimaryResourceType()
                );
                $self    = URL::create($this->getBaseURL())
                    ->path($links[Link::COLLECTION]);
                $related = null;
            } else {
                $links   = $this->linkFactory->getResourceLinksByType(
                    $this->request->getPath()->getPrimaryResourceType()
                );
                $self    = URL::create($this->getBaseURL())
                    ->path($links[Link::SELF])
                    ->replace('id', (string)$this->request->getPath()->getId());
                $related = null;
            }

            if ($this->request->getPath()->isCollection()) {
                $self->query($this->request->getInclusion()?->toArray() ?? [])
                    ->query($this->request->getFieldset()?->toArray() ?? [])
                    ->query($this->request->getFilter()?->toArray() ?? [])
                    ->query($this->request->getPagination()?->toArray() ?? [])
                    ->query($this->request->getSort()?->toArray() ?? []);

                $document->getLinks()->create(Link::SELF, (string)$self);

                $this->request->getPagination()->first();
                $first = $self->clone()->query($this->request->getPagination()->first()->toArray());
                $document->getLinks()->create(Link::FIRST, (string)$first);

                if ($this->request->getPagination()->next()) {
                    $next = $self->clone()->query($this->request->getPagination()->next()->toArray());
                    $document->getLinks()->create(Link::NEXT, (string)$next);
                }
                if ($this->request->getPagination()->prev()) {
                    $prev = $self->clone()->query($this->request->getPagination()->prev()->toArray());
                    $document->getLinks()->create(Link::PREV, (string)$prev);
                }
                if ($this->request->getPagination()->last()) {
                    $last = $self->clone()->query($this->request->getPagination()->last()->toArray());
                    $document->getLinks()->create(Link::LAST, (string)$last);
                }
                if ($related) {
                    $related->query($this->request->getInclusion()->toArray())
                        ->query($this->request->getFieldset()->toArray())
                        ->query($this->request->getFilter()->toArray())
                        ->query($this->request->getPagination()->toArray())
                        ->query($this->request->getSort()->toArray());
                    $document->getLinks()->create(Link::RELATED, (string)$related);
                }
            } else {
                $self->query($this->request->getInclusion()->toArray())
                    ->query($this->request->getFieldset()->toArray());
                $document->getLinks()->create(Link::SELF, (string)$self);
                if ($related) {
                    $related->query($this->request->getInclusion()->toArray())
                        ->query($this->request->getFieldset()->toArray());
                    $document->getLinks()->create(Link::RELATED, (string)$related);
                }
            }
        }
    }

    /**
     * Returns base URL
     * Default is Base URL from Request
     *
     * @return string
     */
    protected function getBaseURL(): string
    {
        return $this->request->getBaseURL();
    }
}
