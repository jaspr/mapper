<?php

/**
 * Created by tomas
 * at 11.05.2024
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding\Processor;

use JSONAPI\Mapper\Document\Link;
use JSONAPI\Mapper\Document\Relationship;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Encoding\RelationshipProcessor;
use JSONAPI\Mapper\Encoding\ResourceProcessor;
use JSONAPI\Mapper\Metadata\MetadataRepository;

/**
 * Class HideLinksIfNoOperationIsSupported
 *
 * In case you want to hide self, related and pagination links when no operation is enabled on resource or relationship
 * use this processor.
 *
 * You have to add this processor after JSONAPI\Mapper\Encoding\Processor\LinksProcessor
 *
 * @package JSONAPI\Mapper\Encoding\Processor
 */
class HideLinksIfNoOperationIsSupported implements
    ResourceProcessor,
    RelationshipProcessor
{
    private MetadataRepository $repository;

    public function __construct(MetadataRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @inheritDoc
     */
    public function processResource(ResourceObject $resource, object $object): void
    {
        if (empty($this->repository->getByType($resource->getType())->getSupportedOperations())) {
            $resource->getLinks()->remove(Link::SELF);
        }
    }

    /**
     * @inheritDoc
     */
    public function processRelationship(Relationship $resource, object $object): void
    {
        $relMetadata = $this->repository
            ->getByType($resource->getOwner()->getType())
            ->getRelationship($resource->getName());
        if (empty($relMetadata->getSupportedOperations())) {
            $resource->getLinks()->remove(Link::SELF);
            $resource->getLinks()->remove(Link::RELATED);
            if (is_int($resource->withData())) {
                $resource->getLinks()->remove(Link::FIRST);
                $resource->getLinks()->remove(Link::PREV);
                $resource->getLinks()->remove(Link::NEXT);
                $resource->getLinks()->remove(Link::LAST);
            }
        }
    }
}
