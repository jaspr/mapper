<?php

/**
 * Created by tomas
 * at 26.05.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding\Processor;

use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Encoding\RequestDependentProcessor;
use JSONAPI\Mapper\Encoding\ResourceProcessor;
use JSONAPI\Mapper\Request\Fieldset\FieldsetInterface;
use JSONAPI\Mapper\Request\ParsedRequest;

/**
 * Class SparseFieldsetResourceProcessor
 *
 * @package JSONAPI\Mapper\Encoding
 */
class SparseFieldsetResourceProcessor implements ResourceProcessor, RequestDependentProcessor
{
    private FieldsetInterface|null $fieldset = null;

    /**
     * @inheritDoc
     */
    public function setRequest(ParsedRequest $request): void
    {
        $this->fieldset = $request->getFieldset();
    }

    /**
     * @inheritDoc
     */
    public function processResource(ResourceObject $resource, object $object): void
    {
        if (!is_null($this->fieldset)) {
            foreach ($resource->getAttributes() as $attribute) {
                if (!$this->fieldset->showField($resource->getType(), $attribute->getName())) {
                    $resource->removeAttribute($attribute);
                }
            }
            foreach ($resource->getRelationships() as $relationship) {
                if (!$this->fieldset->showField($resource->getType(), $relationship->getName())) {
                    $resource->removeRelationship($relationship);
                }
            }
        }
    }
}
