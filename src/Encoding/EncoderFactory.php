<?php

/**
 * Created by Lasicka <lasicka@logio.cz>
 * at 22.06.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JSONAPI\Mapper\Driver\NamingStrategy;
use JSONAPI\Mapper\Encoding\Processor\AttributesResourceProcessor;
use JSONAPI\Mapper\Encoding\Processor\InclusionDataProcessor;
use JSONAPI\Mapper\Encoding\Processor\LinksProcessor;
use JSONAPI\Mapper\Encoding\Processor\MetaProcessor;
use JSONAPI\Mapper\Encoding\Processor\PrimaryDataProcessor;
use JSONAPI\Mapper\Encoding\Processor\RelationshipsResourceProcessor;
use JSONAPI\Mapper\Encoding\Processor\SparseFieldsetResourceProcessor;
use JSONAPI\Mapper\Metadata\MetadataRepository;

/**
 * Class EncoderFactory
 *
 * @package JSONAPI\Mapper\Encoding
 */
class EncoderFactory
{
    /**
     * Create encoder with basic processors (Attributes, Relationships, Meta)
     *
     * @param MetadataRepository $repository
     *
     * @return Encoder
     */
    public static function createDefaultEncoder(MetadataRepository $repository): Encoder
    {
        $encoder = new Encoder($repository);
        $encoder->addProcessor(new PrimaryDataProcessor($encoder));
        $encoder->addProcessor(new AttributesResourceProcessor($repository));
        $encoder->addProcessor(new RelationshipsResourceProcessor($repository, $encoder));
        $encoder->addProcessor(new MetaProcessor($repository));
        return $encoder;
    }

    /**
     * Create encoder with basic processors (Attributes, Relationships, Meta) and request dependent
     * processors (Link, Fieldset, Inclusion), mainly for middleware.
     *
     * @param MetadataRepository $repository
     * @param NamingStrategy|null $namingStrategy
     *
     * @return Encoder
     */
    public static function createRequestDependentEncoder(
        MetadataRepository $repository,
        ?NamingStrategy $namingStrategy = null
    ): Encoder {
        $encoder = new Encoder($repository);
        $encoder->addProcessor(new PrimaryDataProcessor($encoder));
        $encoder->addProcessor(new AttributesResourceProcessor($repository));
        $encoder->addProcessor(new RelationshipsResourceProcessor($repository, $encoder));
        $encoder->addProcessor(new MetaProcessor($repository));
        $encoder->addProcessor(new SparseFieldsetResourceProcessor());
        $encoder->addProcessor(new InclusionDataProcessor($repository, $encoder));
        $encoder->addProcessor(new LinksProcessor($repository, $namingStrategy));
        return $encoder;
    }
}
