<?php

/**
 * Created by tomas
 * at 20.03.2021 15:32
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JetBrains\PhpStorm\ArrayShape;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\Error;
use JSONAPI\Mapper\Document\Id;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\ResourceObjectIdentifier;
use JSONAPI\Mapper\Document\Type;
use JSONAPI\Mapper\Exception\Messages;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\ObjectCollection;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use UnexpectedValueException;

/**
 * Class Encoder
 *
 * @package JSONAPI\Encoding
 */
class Encoder implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var MetadataRepository $repository
     */
    private MetadataRepository $repository;
    /**
     * @var ObjectCollection<Processor> $processors
     */
    private ObjectCollection $processors;

    /**
     * Encoder constructor.
     *
     * @param MetadataRepository $repository
     */
    public function __construct(MetadataRepository $repository)
    {
        $this->repository = $repository;
        $this->logger = new NullLogger();
        $this->processors = new ObjectCollection();
    }

    /**
     * @param object $object
     *
     * @return ResourceObjectIdentifier
     */
    public function identify(object $object): ResourceObjectIdentifier
    {
        $this->logger->debug('Identifying {object}.', ['object' => $object]);
        list($type, $id) = $this->getTypeAndId($object);
        $resource = new ResourceObjectIdentifier($type, $id);
        foreach ($this->processors as $processor) {
            if ($processor instanceof ResourceIdentifierProcessor) {
                $this->logger->debug('Applying processor: {processor}', [
                    'processor' => $processor::class,
                    'object' => $object
                ]);
                $processor->processResourceIdentifier($resource, $object);
            }
        }
        return $resource;
    }

    /**
     * @param object $object
     *
     * @return ResourceObject
     */
    public function encode(object $object): ResourceObject
    {
        $this->logger->debug('Encoding {object}.', ['object' => $object]);
        list($type, $id) = $this->getTypeAndId($object);
        $resource = new ResourceObject($type, $id);

        foreach ($this->processors as $processor) {
            if ($processor instanceof ResourceProcessor) {
                $this->logger->debug('Applying processor: {processor}', [
                    'processor' => $processor::class,
                    'object' => $object,
                    'resource' => $resource
                ]);
                $processor->processResource($resource, $object);
            }
        }

        foreach ($resource->getRelationships() as $relationship) {
            foreach ($this->processors as $processor) {
                if ($processor instanceof RelationshipProcessor) {
                    $this->logger->debug('Applying processor: {processor}', [
                        'processor' => $processor::class,
                        'object' => $object,
                        'relationship' => $relationship
                    ]);
                    $processor->processRelationship($relationship, $object);
                }
            }
        }
        return $resource;
    }


    /**
     * @param object|iterable<object>|null $data
     * @return Document
     */
    public function compose(object|iterable|null $data): Document
    {
        $this->logger->debug('Composing document with data.', ['data' => $data]);
        $document = new Document();
        foreach ($this->processors as $processor) {
            if ($processor instanceof DocumentProcessor) {
                $this->logger->debug('Applying processor: {processor}', [
                    'processor' => $processor::class,
                    'data' => $data,
                    'document' => $document
                ]);
                $processor->processDocument($document, $data);
            }
        }
        return $document;
    }

    /**
     * @param Error[] $errors
     * @return Document
     */
    public function failure(array $errors): Document
    {
        $this->logger->debug('Composing document with errors.', ['errors' => $errors]);
        $document = new Document();
        foreach ($errors as $error) {
            foreach ($this->processors as $processor) {
                if ($processor instanceof ErrorProcessor) {
                    $this->logger->debug('Applying processor: {processor}', [
                        'processor' => $processor::class,
                        'error' => $error
                    ]);
                    $processor->processError($error);
                }
            }
            $document->addError($error);
        }
        return $document;
    }

    /**
     * @return Processor[]
     */
    public function getProcessors(): array
    {
        return $this->processors->values();
    }

    /**
     * Only one processor per class can be added.
     *
     * @param Processor $processor
     *
     * @return bool Returns TRUE if processor added, FALSE otherwise
     */
    public function addProcessor(Processor $processor): bool
    {
        foreach ($this->processors as $registered) {
            if ($processor::class == $registered::class) {
                return false;
            }
        }
        $this->processors->add($processor);
        return true;
    }

    /**
     * @param string $processorClass class name of processor to remove
     *
     * @return bool Returns TRUE if processor removed, FALSE otherwise
     */
    public function removeProcessor(string $processorClass): bool
    {
        foreach ($this->processors as $processor) {
            if ($processor::class == $processorClass) {
                $this->processors->remove($processor);
                return true;
            }
        }
        return false;
    }

    /**
     * @return Document
     */
    public function empty(): Document
    {
        $this->logger->debug('Composing empty document.');
        $document = new Document();
        foreach ($this->processors as $processor) {
            if ($processor instanceof DocumentProcessor) {
                $this->logger->debug('Applying processor: {processor}', [
                    'processor' => $processor::class,
                    'document' => $document
                ]);
                $processor->processDocument($document, null);
            }
        }
        return $document;
    }

    /**
     * @param object $object
     *
     * @return array<Type|Id>
     */
    #[ArrayShape([Type::class, Id::class])]
    private function getTypeAndId(object $object): array
    {
        $className = get_class($object);
        if ($metadata = $this->repository->getByClass($className)) {
            $type = new Type($metadata->getType());
            if ($metadata->getId()->property != null) {
                $value = $object->{$metadata->getId()->property};
            } else {
                $value = (string)call_user_func([$object, $metadata->getId()->getter]);
            }
            $id = new Id($value);
            return [$type, $id];
        }
        throw new UnexpectedValueException(Messages::classMetadataNotFound($className));
    }
}
