<?php

/**
 * Created by Lasicka <lasicka@logio.cz>
 * at 12.07.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JSONAPI\Mapper\Document\Error;

interface ErrorProcessor extends Processor
{
    /**
     * @param Error $error
     * @return void
     */
    public function processError(Error $error): void;
}
