<?php

/**
 * Created by uzivatel
 * at 20.06.2022 13:06
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JetBrains\PhpStorm\ArrayShape;
use JSONAPI\Mapper\Document\Link;
use JSONAPI\Mapper\Driver\NamingStrategy;
use JSONAPI\Mapper\Driver\RecommendedNamingStrategy;
use JSONAPI\Mapper\Exception\Messages;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Request\Fieldset\FieldsetInterface;
use JSONAPI\Mapper\Request\Filtering\FilterInterface;
use JSONAPI\Mapper\Request\Inclusion\InclusionInterface;
use JSONAPI\Mapper\Request\Pagination\PaginationInterface;
use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\Path\PathInterface;
use JSONAPI\Mapper\Request\Sorting\SortInterface;
use UnexpectedValueException;

/**
 * Class LinkFactory
 *
 * @package JSONAPI\Mapper\URI
 */
class LinkFactory
{
    /**
     * @var MetadataRepository repository
     */
    private MetadataRepository $repository;
    /**
     * @var NamingStrategy $namingStrategy
     */
    private NamingStrategy $namingStrategy;

    /**
     * @param MetadataRepository $repository
     * @param NamingStrategy|null $namingStrategy
     */
    public function __construct(MetadataRepository $repository, ?NamingStrategy $namingStrategy = null)
    {
        $this->repository = $repository;
        $this->namingStrategy = $namingStrategy ?? new RecommendedNamingStrategy();
    }

    /**
     * @param string $type
     * @return string[]
     */
    #[ArrayShape([Link::COLLECTION => 'string', Link::SELF => 'string'])]
    public function getResourceLinksByType(string $type): array
    {
        if ($metadata = $this->repository->getByType($type)) {
            $type = rawurlencode($metadata->getType());
            $self = $type . '/{id}';
            return [
                Link::COLLECTION => $type,
                Link::SELF       => $self
            ];
        }
        throw new UnexpectedValueException(Messages::classMetadataNotFound($type));
    }

    /**
     * @param string $className
     * @return string[]
     */
    #[ArrayShape([Link::COLLECTION => 'string', Link::SELF => 'string'])]
    public function getResourceLinksByClass(string $className): array
    {
        if ($metadata = $this->repository->getByClass($className)) {
            $type = rawurlencode($metadata->getType());
            $self = $type . '/{id}';
            return [
                Link::COLLECTION => $type,
                Link::SELF       => $self
            ];
        }
        throw new UnexpectedValueException(Messages::classMetadataNotFound($className));
    }

    /**
     * @param string $type
     * @param string $relationshipName
     * @return string[]
     */
    #[ArrayShape([Link::SELF => 'string', Link::RELATED => 'string'])]
    public function getRelationshipLinksByType(string $type, string $relationshipName): array
    {
        if ($metadata = $this->repository->getByType($type)) {
            $resourceLinks = $this->getResourceLinksByType($type);
            if ($relationship = $metadata->getRelationship($relationshipName)) {
                return [
                    Link::SELF => $resourceLinks[Link::SELF] . '/relationships/' . rawurlencode(
                        $this->namingStrategy->relationshipNameToURI($relationship->name)
                    ),
                    Link::RELATED => $resourceLinks[Link::SELF] . '/' . rawurlencode(
                        $this->namingStrategy->relationshipNameToURI($relationship->name)
                    )
                ];
            }
            throw new UnexpectedValueException(Messages::fieldMetadataNotFound($type, $relationshipName));
        }
        throw new UnexpectedValueException(Messages::classMetadataNotFound($type));
    }

    /**
     * @param string $className
     * @param string $relationshipName
     * @return string[]
     */
    #[ArrayShape([Link::SELF => 'string', Link::RELATED => 'string'])]
    public function getRelationshipLinksByClass(string $className, string $relationshipName): array
    {
        if ($metadata = $this->repository->getByClass($className)) {
            $resourceLinks = $this->getResourceLinksByClass($className);
            if ($relationship = $metadata->getRelationship($relationshipName)) {
                return [
                    Link::SELF => $resourceLinks[Link::SELF] . '/relationships/' . rawurlencode(
                        $this->namingStrategy->relationshipNameToURI($relationship->name)
                    ),
                    Link::RELATED => $resourceLinks[Link::SELF] . '/' . rawurlencode(
                        $this->namingStrategy->relationshipNameToURI($relationship->name)
                    )
                ];
            }
            throw new UnexpectedValueException(Messages::fieldMetadataNotFound($className, $relationshipName));
        }
        throw new UnexpectedValueException(Messages::classMetadataNotFound($className));
    }

    /**
     * @param ParsedRequest $uri
     *
     * @return string[]
     */
    public function getDocumentLinks(ParsedRequest $uri): array
    {
        $links = [];
        $path = $uri->getPath();
        $filter = $uri->getFilter();
        $inclusion = $uri->getInclusion();
        $fieldset = $uri->getFieldset();
        $sort = $uri->getSort();
        $pagination = $uri->getPagination();
        if ($uri->getPath()->isCollection()) {
            $links[Link::SELF] = $this->createDocumentLink(
                $path,
                $filter,
                $inclusion,
                $fieldset,
                $pagination,
                $sort
            );
            $first = $pagination->first();
            $links[Link::FIRST] = $this->createDocumentLink(
                $path,
                $filter,
                $inclusion,
                $fieldset,
                $first,
                $sort
            );
            if ($last = $pagination->last()) {
                $links[Link::LAST] = $this->createDocumentLink(
                    $path,
                    $filter,
                    $inclusion,
                    $fieldset,
                    $last,
                    $sort
                );
            }
            if ($prev = $pagination->prev()) {
                $links[Link::PREV] = $this->createDocumentLink(
                    $path,
                    $filter,
                    $inclusion,
                    $fieldset,
                    $prev,
                    $sort
                );
            }
            if ($next = $pagination->next()) {
                $links[Link::NEXT] = $this->createDocumentLink(
                    $path,
                    $filter,
                    $inclusion,
                    $fieldset,
                    $next,
                    $sort
                );
            }
        } else {
            $links[Link::SELF] = $this->createDocumentLink(
                $path,
                null,
                $inclusion,
                $fieldset,
                null,
                null
            );
        }
        if ($path->isRelationship()) {
            $resourceLinks = $this->getRelationshipLinksByType(
                $path->getResourceType(),
                $path->getRelationshipName()
            );
            $related = $resourceLinks[Link::RELATED];
            $links[Link::RELATED] = str_replace('{id}', $path->getId(), $related);
        }
        return $links;
    }

    /**
     * @param PathInterface $path
     * @param FilterInterface|null $filter
     * @param InclusionInterface|null $inclusion
     * @param FieldsetInterface|null $fieldset
     * @param PaginationInterface|null $pagination
     * @param SortInterface|null $sort
     *
     * @return string
     */
    private function createDocumentLink(
        PathInterface $path,
        ?FilterInterface $filter,
        ?InclusionInterface $inclusion,
        ?FieldsetInterface $fieldset,
        ?PaginationInterface $pagination,
        ?SortInterface $sort
    ): string {
        $link = '' . $path;
        $mark = '?';
        if (strlen((string)$filter)) {
            $link .= $mark . $filter;
            $mark = '&';
        }
        if (strlen((string)$inclusion)) {
            $link .= $mark . $inclusion;
            $mark = '&';
        }
        if (strlen((string)$fieldset)) {
            $link .= $mark . $fieldset;
            $mark = '&';
        }
        if (strlen((string)$pagination)) {
            $link .= $mark . $pagination;
            $mark = '&';
        }
        if (strlen((string)$sort)) {
            $link .= $mark . $sort;
        }
        return $link;
    }
}
