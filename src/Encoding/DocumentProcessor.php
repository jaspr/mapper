<?php

/**
 * Created by tomas
 * at 22.05.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JSONAPI\Mapper\Document\Document;

interface DocumentProcessor extends Processor
{
    /**
     * @param Document $document
     * @param object|iterable<object>|null $data
     * @return void
     */
    public function processDocument(Document $document, object|iterable|null $data): void;
}
