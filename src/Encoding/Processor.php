<?php

/**
 * Created by tomas
 * at 20.03.2021 15:36
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

/**
 * Interface Processor
 *
 * @package JSONAPI\Encoding
 */
interface Processor
{
}
