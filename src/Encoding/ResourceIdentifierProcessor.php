<?php

/**
 * Created by uzivatel
 * at 22.06.2022 15:19
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JSONAPI\Mapper\Document\ResourceObjectIdentifier;

/**
 * Class ResourceIdentifierProcessor
 *
 * @package JSONAPI\Mapper\Encoding
 */
interface ResourceIdentifierProcessor extends Processor
{
    /**
     * @param ResourceObjectIdentifier $resource
     * @param object $object
     *
     * @return void
     */
    public function processResourceIdentifier(ResourceObjectIdentifier $resource, object $object): void;
}
