<?php

/**
 * Created by uzivatel
 * at 22.06.2022 15:21
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JSONAPI\Mapper\Document\Relationship;

interface RelationshipProcessor extends Processor
{
    /**
     * @param Relationship $resource
     * @param object $object
     *
     * @return void
     */
    public function processRelationship(Relationship $resource, object $object): void;
}
