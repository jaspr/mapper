<?php

/**
 * Created by tomas
 * at 29.05.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JSONAPI\Mapper\Request\ParsedRequest;

interface RequestDependentProcessor extends Processor
{
    /**
     * @param ParsedRequest $request
     *
     * @return void
     */
    public function setRequest(ParsedRequest $request): void;
}
