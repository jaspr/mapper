<?php

/**
 * Created by uzivatel
 * at 22.06.2022 15:19
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JSONAPI\Mapper\Document\ResourceObject;

/**
 * Class ResourceProcessor
 *
 * @package JSONAPI\Mapper\Encoding
 */
interface ResourceProcessor extends Processor
{
    /**
     * @param ResourceObject $resource
     * @param object $object
     *
     * @return void
     */
    public function processResource(ResourceObject $resource, object $object): void;
}
