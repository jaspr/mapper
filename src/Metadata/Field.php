<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Metadata;

/**
 * Class Common
 *
 * @package JSONAPI\Metadata
 */
abstract class Field
{
    /**
     * @var string|null getter
     */
    public ?string $getter;
    /**
     * @var string|null name
     */
    public ?string $name;
    /**
     * @var string|null property
     */
    public ?string $property;
    /**
     * @var bool|null nullable
     */
    public ?bool $nullable;

    /**
     * Field constructor.
     *
     * @param string|null $name
     * @param string|null $property
     * @param string|null $getter
     * @param bool|null $nullable
     */
    protected function __construct(
        ?string $name = null,
        ?string $property = null,
        ?string $getter = null,
        ?bool $nullable = null
    ) {
        $this->name = $name;
        $this->property = $property;
        $this->getter = $getter;
        $this->nullable = $nullable;
    }
}
