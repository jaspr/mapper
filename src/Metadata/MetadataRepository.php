<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Metadata;

use JSONAPI\Mapper\Helper\DoctrineProxyTrait;

/**
 * Class MetadataRepository
 *
 * @package JSONAPI\Metadata
 */
class MetadataRepository
{
    use DoctrineProxyTrait;

    /**
     * @var ClassMetadata[]
     */
    private array $collection;
    /**
     * @var string[] $classToTypeMap
     */
    private array $classToTypeMap;

    public function __construct()
    {
        $this->collection = [];
        $this->classToTypeMap = [];
    }

    /**
     * @param string $type - resource type
     *
     * @return ClassMetadata|null
     */
    public function getByType(string $type): ?ClassMetadata
    {
        if (array_key_exists($type, $this->collection)) {
            return $this->collection[$type];
        }
        return null;
    }

    /**
     * @return ClassMetadata[]
     */
    public function getAll(): array
    {
        return $this->collection;
    }

    /**
     * @param ClassMetadata $metadata
     */
    public function add(ClassMetadata $metadata): void
    {
        $this->collection[$metadata->getType()] = $metadata;
        $this->classToTypeMap[$metadata->getClassName()] = $metadata->getType();
    }

    /**
     * @param string $className
     * @return ClassMetadata|null
     */
    public function getByClass(string $className): ?ClassMetadata
    {
        $className = self::clearDoctrineProxyPrefix($className);
        if (array_key_exists($className, $this->classToTypeMap)) {
            $type = $this->classToTypeMap[$className];
            return $this->getByType($type);
        }
        return null;
    }
}
