<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Metadata;

enum Operation
{
    case CREATE; //POST
    case READ; //GET
    case UPDATE; //PATCH
    case DELETE; //DELETE
    case LIST; //GET

    /**
     * Returns $a without $b
     * @param Operation[] $a
     * @param Operation[] $b
     * @return Operation[]
     */
    public static function diff(array $a, array $b): array
    {
        $x = array_map(fn($operation) => $operation->name, $a);
        $y = array_map(fn($operation) => $operation->name, $b);
        $diff = array_diff($x, $y);
        $ret = [];
        foreach ($diff as $name) {
            $ret[] = constant("JSONAPI\Mapper\Metadata\Operation::$name");
        }
        return $ret;
    }
}
