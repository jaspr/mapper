<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Metadata;

use JSONAPI\Mapper\Document\Members;
use JSONAPI\Mapper\Metadata;

/**
 * Class ClassMetadata
 *
 * @package JSONAPI
 */
final class ClassMetadata
{
    /**
     * @var string
     */
    private string $className;
    /**
     * @var string
     */
    private string $type;

    /**
     * @var Metadata\Id
     */
    private Metadata\Id $id;

    /**
     * @var string[]
     */
    private array $fields = [];
    /**
     * @var Attribute[] $attributes
     */
    private array $attributes = [];
    /**
     * @var Relationship[] $relationships
     */
    private array $relationships = [];

    /**
     * @var Meta|null
     */
    private ?Meta $meta;
    /**
     * @var Operation[] $operations
     */
    private array $operations;

    /**
     * ClassMetadata constructor.
     *
     * @param string $className
     * @param string $type
     * @param Id $id
     * @param Attribute[] $attributes
     * @param Relationship[] $relationships
     * @param Meta|null $resourceMeta
     * @param Operation[] $operations
     */
    public function __construct(
        string $className,
        string $type,
        Metadata\Id $id,
        array $attributes,
        array $relationships,
        ?Meta $resourceMeta = null,
        array $operations = [
            Operation::CREATE,
            Operation::READ,
            Operation::UPDATE,
            Operation::DELETE,
            Operation::LIST
        ]
    ) {
        $this->className = $className;
        $this->id = $id;
        $this->type = $type;
        $this->meta = $resourceMeta;
        $this->fields[] = Members::ID;
        $this->fields[] = Members::TYPE;
        foreach ($attributes as $attribute) {
            if (!in_array($attribute->name, $this->fields)) {
                $this->fields[] = $attribute->name;
                $this->attributes[$attribute->name] = $attribute;
            }
        }
        foreach ($relationships as $relationship) {
            if (!in_array($relationship->name, $this->fields)) {
                $this->fields[] = $relationship->name;
                $this->relationships[$relationship->name] = $relationship;
            }
        }
        $this->operations = $operations;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return Meta|null
     */
    public function getMeta(): ?Meta
    {
        return $this->meta;
    }

    /**
     * @return Attribute[]
     */
    public function getAttributes(): array
    {
        return array_values($this->attributes);
    }

    /**
     * @param string $name
     *
     * @return Attribute|null
     */
    public function getAttribute(string $name): ?Attribute
    {
        if ($this->hasAttribute($name)) {
            return $this->attributes[$name];
        }
        return null;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasAttribute(string $name): bool
    {
        return array_key_exists($name, $this->attributes);
    }

    /**
     * @return Relationship[]
     */
    public function getRelationships(): array
    {
        return array_values($this->relationships);
    }

    /**
     * @param string $name
     *
     * @return Relationship|null
     */
    public function getRelationship(string $name): ?Relationship
    {
        if ($this->hasRelationship($name)) {
            return $this->relationships[$name];
        }
        return null;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasRelationship(string $name): bool
    {
        return array_key_exists($name, $this->relationships);
    }

    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasField(string $name): bool
    {
        return in_array($name, $this->fields);
    }

    /**
     * @return Operation[]
     */
    public function getSupportedOperations(): array
    {
        return $this->operations;
    }

    /**
     * @param Operation $operation
     *
     * @return bool
     */
    public function isOperationSupported(Operation $operation): bool
    {
        return in_array($operation, $this->operations);
    }
}
