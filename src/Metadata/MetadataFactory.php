<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Metadata;

use Composer\ClassMapGenerator\ClassMapGenerator;
use InvalidArgumentException;
use JSONAPI\Mapper\Driver\Driver;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException as CacheException;
use Symfony\Component\String\Slugger\AsciiSlugger;

/**
 * Class MetadataFactory
 *
 * @package JSONAPI\Metadata
 */
class MetadataFactory
{
    /**
     * @param string[] $paths
     * @param CacheInterface $cache
     * @param Driver $driver
     * @param LoggerInterface|null $logger
     *
     * @return MetadataRepository
     */
    public static function create(
        array $paths,
        CacheInterface $cache,
        Driver $driver,
        ?LoggerInterface $logger = null
    ): MetadataRepository {
        $logger = $logger ?: new NullLogger();
        $slugger = new AsciiSlugger();
        $cacheKey = $slugger->slug(self::class)->toString();
        try {
            if ($cache->has($cacheKey)) {
                $repository = $cache->get($cacheKey);
            } else {
                $repository = self::getRepository($paths, $driver);
                $cache->set($cacheKey, $repository);
            }
        } catch (CacheException $e) {
            $logger->warning($e);
            $repository = self::getRepository($paths, $driver);
        }
        return $repository;
    }

    /**
     * @param string[] $paths
     * @param Driver $driver
     * @return MetadataRepository
     */
    private static function getRepository(array $paths, Driver $driver): MetadataRepository
    {
        $repository = new MetadataRepository();
        foreach ($paths as $path) {
            if (!is_dir($path)) {
                throw new InvalidArgumentException("Path $path is not directory");
            }
            $map = ClassMapGenerator::createMap($path);
            foreach ($map as $className => $file) {
                if ($metadata = $driver->getClassMetadata($className)) {
                    $repository->add($metadata);
                }
            }
        }
        return $repository;
    }
}
