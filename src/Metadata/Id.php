<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Metadata;

/**
 * Class IdMetadata
 *
 * @package JSONAPI\Metadata
 */
class Id
{
    public ?string $property;
    public ?string $getter;

    /**
     * Id constructor.
     *
     * @param string|null $property
     * @param string|null $getter
     */
    private function __construct(
        ?string $property = null,
        ?string $getter = null
    ) {
        $this->property = $property;
        $this->getter = $getter;
    }

    /**
     * @param string $property
     *
     * @return Id
     */
    public static function createByProperty(string $property): Id
    {
        return new self($property);
    }

    /**
     * @param string $getter
     *
     * @return Id
     */
    public static function createByMethod(string $getter): Id
    {
        return new self(null, $getter);
    }
}
