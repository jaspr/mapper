<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Metadata;

/**
 * Class Attribute
 *
 * @package JSONAPI\Metadata
 */
class Attribute extends Field
{
    /**
     * @var string|null $of
     */
    public ?string $of;
    /**
     * @var string|null $type
     */
    public ?string $type;

    /**
     * Attribute constructor.
     *
     * @param string|null $name
     * @param string|null $property
     * @param string|null $getter
     * @param string|null $type
     * @param string|null $of
     * @param bool|null $nullable
     */
    protected function __construct(
        ?string $name = null,
        ?string $property = null,
        ?string $getter = null,
        ?string $type = null,
        ?string $of = null,
        ?bool $nullable = null
    ) {
        parent::__construct($name, $property, $getter, $nullable);
        $this->type = $type;
        $this->of = $of;
    }


    /**
     * @param string $property
     * @param string|null $of
     * @param string|null $name
     * @param string|null $type
     * @param bool|null $nullable
     *
     * @return self
     */
    public static function createByProperty(
        string $property,
        ?string $of = null,
        ?string $name = null,
        ?string $type = null,
        ?bool $nullable = null
    ): self {
        return new self($name, $property, null, $type, $of, $nullable);
    }

    /**
     * @param string $getter
     * @param string|null $name
     * @param string|null $type
     * @param string|null $of
     * @param bool|null $nullable
     *
     * @return self
     */
    public static function createByMethod(
        string $getter,
        ?string $name = null,
        ?string $type = null,
        ?string $of = null,
        ?bool $nullable = null
    ): self {
        return new self($name, null, $getter, $type, $of, $nullable);
    }
}
