<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Metadata;

/**
 * Class Relationship
 *
 * @package JSONAPI\Metadata
 */
class Relationship extends Field
{
    /**
     * @var bool|null $isCollection
     */
    public ?bool $isCollection;
    /**
     * @var string|null $target
     */
    public ?string $target;

    /**
     * @var Operation[] $operations
     */
    public array $operations;

    /**
     * @var Meta|null $meta
     */
    public ?Meta $meta;

    /**
     * @var int|bool $withData
     * @case TRUE - fetch data, do not limit them,
     * @case FALSE - do not fetch data,
     * @case INTEGER - fetch data, but limit them
     */
    public int|bool $withData;

    /**
     * Relationship constructor.
     *
     * @param string|null $target
     * @param string|null $name
     * @param string|null $property
     * @param string|null $getter
     * @param bool|null $isCollection
     * @param Meta|null $meta
     * @param bool|null $nullable
     * @param int|bool $withData
     * @param Operation[] $operations
     */
    protected function __construct(
        ?string $target,
        ?string $name = null,
        ?string $property = null,
        ?string $getter = null,
        ?bool $isCollection = null,
        ?Meta $meta = null,
        ?bool $nullable = null,
        int|bool $withData = 25,
        array $operations = [
            Operation::CREATE,
            Operation::READ,
            Operation::UPDATE,
            Operation::DELETE
        ]
    ) {
        parent::__construct($name, $property, $getter, $nullable);
        $this->target = $target;
        $this->meta = $meta;
        $this->isCollection = $isCollection;
        $this->withData = $withData;
        $this->operations = $operations;
    }

    /**
     * @param string $property
     * @param string $target
     * @param string|null $name
     * @param bool $isCollection
     * @param Meta|null $meta
     * @param bool|null $nullable
     * @param int|bool $withData
     * @param Operation[] $operations
     * @return Relationship
     */
    public static function createByProperty(
        string $property,
        string $target,
        ?string $name = null,
        ?bool $isCollection = null,
        ?Meta $meta = null,
        ?bool $nullable = null,
        int|bool $withData = 25,
        array $operations = [
            Operation::CREATE,
            Operation::READ,
            Operation::UPDATE,
            Operation::DELETE
        ]
    ): Relationship {
        return new self(
            $target,
            $name,
            $property,
            null,
            $isCollection,
            $meta,
            $nullable,
            $withData,
            $operations
        );
    }

    /**
     * @param string $getter
     * @param string $target
     * @param string|null $name
     * @param bool $isCollection
     * @param Meta|null $meta
     * @param bool|null $nullable
     * @param int|bool $withData
     * @param Operation[] $operations
     * @return Relationship
     */
    public static function createByMethod(
        string $getter,
        string $target,
        ?string $name = null,
        ?bool $isCollection = null,
        ?Meta $meta = null,
        ?bool $nullable = null,
        int|bool $withData = 25,
        array $operations = [
            Operation::CREATE,
            Operation::READ,
            Operation::UPDATE,
            Operation::DELETE
        ]
    ): Relationship {
        return new self(
            $target,
            $name,
            null,
            $getter,
            $isCollection,
            $meta,
            $nullable,
            $withData,
            $operations
        );
    }

    /**
     * @return Operation[]
     */
    public function getSupportedOperations(): array
    {
        return $this->operations;
    }

    /**
     * @param Operation $operation
     *
     * @return bool
     */
    public function isOperationSupported(Operation $operation): bool
    {
        return in_array($operation, $this->operations);
    }
}
