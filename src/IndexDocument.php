<?php

/**
 * Created by uzivatel
 * at 07.03.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper;

use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\Link;
use JSONAPI\Mapper\Document\Meta;
use JSONAPI\Mapper\Document\Serializable;
use JSONAPI\Mapper\Driver\NamingStrategy;
use JSONAPI\Mapper\Encoding\LinkFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;

use function Symfony\Component\String\u;

/**
 * Class IndexDocument
 * @package JSONAPI\Mapper
 */
class IndexDocument implements Serializable
{
    /**
     * @var MetadataRepository $repository
     */
    private MetadataRepository $repository;
    /**
     * @var string $baseUrl
     */
    private string $baseUrl;
    /**
     * @var LinkFactory $linkFactory
     */
    private LinkFactory $linkFactory;

    /**
     * @param MetadataRepository $repository
     * @param string $baseUrl
     * @param NamingStrategy|null $namingStrategy
     */
    public function __construct(MetadataRepository $repository, string $baseUrl, ?NamingStrategy $namingStrategy = null)
    {
        $this->repository = $repository;
        $this->baseUrl = $baseUrl;
        $this->linkFactory = new LinkFactory($this->repository, $namingStrategy);
    }

    /**
     * @return Document
     */
    public function jsonSerialize(): Document
    {
        $ret = new Document();
        $meta = new Meta([
            'title' => 'JSON:API Index Page',
            'baseUrl' => $this->baseUrl
        ]);
        $ret->setMeta($meta);
        foreach ($this->repository->getAll() as $metadata) {
            $key = u($metadata->getType())->camel()->toString();
            $links = $this->linkFactory->getResourceLinksByType($metadata->getType());
            $url = URL::create($this->baseUrl)
                ->path($links[Link::COLLECTION])
                ->build();
            $ret->getLinks()->create($key, $url);
        }
        return $ret;
    }
}
