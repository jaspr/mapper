<?php

/**
 * Created by tomas.benedikt@gmail.com
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Annotation;

use JSONAPI\Mapper\Metadata\Operation;

/**
 * Class Relationship
 *
 * @package JSONAPI\Annotation
 */
#[\Attribute(\Attribute::TARGET_METHOD | \Attribute::TARGET_PROPERTY)]
final readonly class Relationship
{
    /**
     * @param string $target
     * @param string|null $name
     * @param bool|null $isCollection
     * @param bool|null $nullable
     * @param int|bool $withData
     * @param Operation[] $operations
     */
    public function __construct(
        public string $target,
        public ?string $name = null,
        public ?bool $isCollection = null,
        public ?bool $nullable = null,
        public int|bool $withData = true,
        public array $operations = [
            Operation::CREATE,
            Operation::READ,
            Operation::UPDATE,
            Operation::DELETE
        ]
    ) {
    }
}
