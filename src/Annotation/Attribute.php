<?php

/**
 * Created by tomas.benedikt@gmail.com
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Annotation;

/**
 * Class Attribute
 *
 * @package JSONAPI\Annotation
 */
#[\Attribute(\Attribute::TARGET_METHOD | \Attribute::TARGET_PROPERTY)]
final readonly class Attribute
{
    public function __construct(
        public ?string $name = null,
        public ?string $type = null,
        public ?string $of = null,
        public ?bool $nullable = null
    ) {
    }
}
