<?php

/**
 * Created by tomas.benedikt@gmail.com
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Annotation;

use JSONAPI\Mapper\Metadata\Operation;

/**
 * Class Resource
 *
 * @package JSONAPI\Annotation
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class Resource
{
    /**
     * Resource constructor.
     *
     * @param string|null $type
     * @param Operation[] $operations
     */
    public function __construct(
        public ?string $type = null,
        public array $operations = [
            Operation::CREATE,
            Operation::READ,
            Operation::UPDATE,
            Operation::DELETE,
            Operation::LIST
        ]
    ) {
    }
}
