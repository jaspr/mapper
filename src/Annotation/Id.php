<?php

/**
 * Created by tomas.benedikt@gmail.com
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Annotation;

/**
 * Class Id
 *
 * @package JSONAPI\Annotation
 */
#[\Attribute(\Attribute::TARGET_METHOD | \Attribute::TARGET_PROPERTY)]
final readonly class Id
{
    public function __construct()
    {
    }
}
