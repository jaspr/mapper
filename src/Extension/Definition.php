<?php

/**
 * Created by @bednic
 * at 06.08.2024 23:14
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension;

use JSONAPI\Mapper\Document\DocumentNamespace;
use JSONAPI\Mapper\Encoding\Processor;
use JSONAPI\Mapper\Request\RequestParserInterface;

interface Definition
{
    public const URI = '';

    public function getNamespace(): string;
    /**
     * @return string
     */
    public function getURI(): string;

    /**
     * @return Processor[]
     */
    public function getProcessors(): array;

    /**
     * @return RequestParserInterface[]
     */
    public function getRequestParsers(): array;
}
