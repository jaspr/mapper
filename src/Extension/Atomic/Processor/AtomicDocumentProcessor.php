<?php

/**
 * Created by tomas.benedikt@gmail.com
 * at 28.08.2024 23:40
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Atomic\Processor;

use JSONAPI\Mapper\Document\DefaultNamespace;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Encoding\DocumentProcessor;
use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Extension\Atomic\AtomicDocumentNamespace;
use JSONAPI\Mapper\Extension\Atomic\Operations\Operation;
use JSONAPI\Mapper\Extension\Atomic\Operations\OperationCollection;
use JSONAPI\Mapper\Extension\Atomic\Operations\OperationWithResult;
use JSONAPI\Mapper\Extension\Atomic\Result;

class AtomicDocumentProcessor implements DocumentProcessor
{
    private string $extensionURI;

    public function __construct(string $extensionURI)
    {
        $this->extensionURI = $extensionURI;
    }

    /**
     * @inheritDoc
     */
    public function processDocument(Document $document, object|iterable|null $data): void
    {
        if ($data instanceof OperationCollection) {
            $namespace = new AtomicDocumentNamespace();
            if ($document->getNamespaces()->has(DefaultNamespace::class)) {
                $document->getNamespaces()->remove(DefaultNamespace::class);
            }
            $results = [];
            /** @var Operation $operation */
            foreach ($data as $operation) {
                if ($operation instanceof OperationWithResult) {
                    $results[] = $operation->getResult();
                } else {
                    $results[] = (object)[];
                }
            }
            $namespace->setResults($results);
            $document->getNamespaces()->add($namespace);
            $document->getJSONAPIObject()->addExtension($this->extensionURI);
        } else {
            throw new \InvalidArgumentException('Data must be instance of OperationCollection.');
        }
    }
}
