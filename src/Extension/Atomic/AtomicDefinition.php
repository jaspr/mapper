<?php

/**
 * Created by @bednic
 * at 16.08.2024 23:52
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Atomic;

use Fig\Http\Message\RequestMethodInterface;
use JSONAPI\Mapper\Document\DocumentNamespace;
use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Extension\Atomic\Parser\AtomicBodyParser;
use JSONAPI\Mapper\Extension\Atomic\Processor\AtomicDocumentProcessor;
use JSONAPI\Mapper\Extension\Definition;
use JSONAPI\Mapper\Metadata\MetadataRepository;

class AtomicDefinition implements Definition
{
    private MetadataRepository $repository;
    private Encoder $encoder;

    public function __construct(MetadataRepository $repository, Encoder $encoder)
    {
        $this->repository = $repository;
        $this->encoder    = $encoder;
    }

    /**
     * @inheritDoc
     */
    public function getURI(): string
    {
        return 'https://jsonapi.org/ext/atomic';
    }

    /**
     * @inheritDoc
     */
    public function getProcessors(): array
    {
        return [
            new AtomicDocumentProcessor($this->getURI())
        ];
    }

    /**
     * @inheritDoc
     */
    public function getRequestParsers(): array
    {
        return [
            new AtomicBodyParser($this->repository, $this->encoder)
        ];
    }

    public function getNamespace(): string
    {
        return 'atomic';
    }
}
