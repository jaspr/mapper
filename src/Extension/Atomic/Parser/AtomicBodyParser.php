<?php

/**
 * Created by @bednic
 * at 16.08.2024 22:16
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Atomic\Parser;

use BackedEnum;
use DateTimeImmutable;
use DateTimeInterface;
use JSONAPI\Mapper\Document\Attribute;
use JSONAPI\Mapper\Document\DefaultNamespace;
use JSONAPI\Mapper\Document\Deserializable;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\Id;
use JSONAPI\Mapper\Document\Lid;
use JSONAPI\Mapper\Document\Meta;
use JSONAPI\Mapper\Document\Relationship;
use JSONAPI\Mapper\Document\ResourceCollection;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\ResourceObjectIdentifier;
use JSONAPI\Mapper\Document\Type;
use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\ErrorFactory;
use JSONAPI\Mapper\Exception\MalformedDocument;
use JSONAPI\Mapper\Exception\RequiredMemberMissing;
use JSONAPI\Mapper\Extension\Atomic\AtomicDocumentNamespace;
use JSONAPI\Mapper\Extension\Atomic\Op;
use JSONAPI\Mapper\Extension\Atomic\Operations\Operation;
use JSONAPI\Mapper\Extension\Atomic\Operations\OperationCollection;
use JSONAPI\Mapper\Extension\Atomic\Operations\OperationFactory;
use JSONAPI\Mapper\Extension\Atomic\Ref;
use JSONAPI\Mapper\Metadata\ClassMetadata;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Request\Body\BodyParserInterface;
use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\RequestPart;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionClass;
use ReflectionException;
use stdClass;

/**
 * Class AtomicBodyParser
 * @package JSONAPI\Mapper\Extension\Atomic\Parser
 */
class AtomicBodyParser implements BodyParserInterface
{
    private ?Document $document = null;
    private MetadataRepository $repository;
    private ClassMetadata $metadata;
    private OperationFactory $operationFactory;

    public function __construct(MetadataRepository $repository, Encoder $encoder)
    {
        $this->repository = $repository;
        $this->operationFactory = new OperationFactory($encoder);
    }

    /**
     * @param ServerRequestInterface $request
     * @param ParsedRequest|null $parsedRequest
     * @inheritDoc
     */
    public function parse(ServerRequestInterface $request, ?ParsedRequest $parsedRequest = null): Document
    {
        $request->getBody()->rewind();
        $json = $request->getBody()->getContents();
        if (strlen($json) > 0) {
            if ($parsedRequest && $parsedRequest->getBody()) {
                $this->document = $parsedRequest->getBody();
            } else {
                $this->document = new Document();
            }
            $body       = json_decode($json, false, 512, JSON_THROW_ON_ERROR);
            $key        = 'atomic:operations';
            $data       = $body->$key;
            $operations = new OperationCollection();
            foreach ($data as $i => $item) {
                if (property_exists($item, 'op')) {
                    $op = Op::from($item->op);
                } else {
                    throw new RequiredMemberMissing('op', "#/atomic:operations/$i");
                }
                $ref    = null;
                $hasRef = property_exists($item, 'ref');
                if ($hasRef) {
                    if (!property_exists($item->ref, 'type')) {
                        throw new RequiredMemberMissing('type', "#/atomic:operations/$i/ref");
                    }
                    $type = new Type($item->ref->type);
                    $id   = null;
                    if (property_exists($item->ref, 'id')) {
                        $id = new Id($item->ref->id);
                    }
                    $lid = null;
                    if (property_exists($item->ref, 'lid')) {
                        $lid = new Lid($item->ref->lid);
                    }
                    $relationship = null;
                    if (property_exists($item->ref, 'relationship')) {
                        $relationship = $item->ref->relationship;
                    }
                    $ref = new Ref($type, $id, $lid, $relationship);
                }
                $href = null;
                if (property_exists($item, 'href')) {
                    if ($hasRef) {
                        throw new MalformedDocument(
                            "Members [ref] and [href] cannot appear simultaneously.",
                            "#/atomic:operations/$i"
                        );
                    }
                    $href = $item->href;
                }
                $meta = null;
                if (property_exists($item, 'meta')) {
                    $meta = new Meta((array)$item->meta);
                }
                $data = null;
                if (property_exists($item, 'data')) {
                    if (is_iterable($item->data)) {
                        $data = $this->parseCollection($item->data, $ref);
                    } elseif (!is_null($item->data)) {
                        $data = $this->parseResource($item->data, $ref);
                    }
                }
                $operation    = $this->operationFactory->create($op, $ref, $data, $meta, $href);
                $operations->add($operation);
            }
            $namespace = new AtomicDocumentNamespace();
            $namespace->set('operations', $operations);
            $this->document->getNamespaces()->add($namespace);
            $this->document->getNamespaces()->remove(DefaultNamespace::class);
        }
        return $this->document;
    }

    /**
     * @param array<object> $objects
     * @param Ref|null $ref
     * @return ResourceCollection
     */
    public function parseCollection(array $objects, ?Ref $ref): ResourceCollection
    {
        $collection = new ResourceCollection();
        foreach ($objects as $object) {
            $collection->add($this->parseResource($object, $ref));
        }
        return $collection;
    }

    /**
     * @param object $object
     * @param Ref|null $ref
     * @return ResourceObject|ResourceObjectIdentifier
     */
    private function parseResource(object $object, ?Ref $ref): ResourceObject|ResourceObjectIdentifier
    {
        $type           = new Type($object->type);
        $id             = new Id(@$object->id);
        $resource       = new ResourceObjectIdentifier($type, $id);
        $this->metadata = $this->repository->getByType($object->type);
        if (is_null($ref?->getRelationship())) {
            $resource = new ResourceObject($type, $id);
            if (property_exists($object, 'attributes')) {
                $this->parseAttributes($object, $resource);
            }
            if (property_exists($object, 'relationships')) {
                $this->parseRelationships($object, $resource);
            }
        }
        return $resource;
    }

    /**
     * @param object $object
     * @param ResourceObject $resource
     *
     * @return void
     */
    private function parseAttributes(object $object, ResourceObject $resource): void
    {
        foreach ($this->metadata->getAttributes() as $attribute) {
            if (property_exists($object->attributes, $attribute->name)) {
                $value = $object->attributes->{$attribute->name};
                $type  = $attribute->type;
                if ($type) {
                    if (!is_null($value)) {
                        switch ($type) {
                            case 'int':
                                if (!is_int($value)) {
                                    $this->document->addError(
                                        ErrorFactory::unexpectedFieldDataType(
                                            $resource->getType(),
                                            $attribute->name,
                                            gettype($value),
                                            'int'
                                        )
                                    );
                                }
                                break;
                            case 'bool':
                                if (!is_bool($value)) {
                                    $this->document->addError(
                                        ErrorFactory::unexpectedFieldDataType(
                                            $resource->getType(),
                                            $attribute->name,
                                            gettype($value),
                                            'bool'
                                        )
                                    );
                                }
                                break;
                            case 'float':
                                if (!is_int($value) && !is_float($value)) {
                                    $this->document->addError(
                                        ErrorFactory::unexpectedFieldDataType(
                                            $resource->getType(),
                                            $attribute->name,
                                            gettype($value),
                                            'float'
                                        )
                                    );
                                }
                                $value = floatval($value);
                                break;
                            case 'string':
                                if (!is_string($value)) {
                                    $this->document->addError(
                                        ErrorFactory::unexpectedFieldDataType(
                                            $resource->getType(),
                                            $attribute->name,
                                            gettype($value),
                                            'string'
                                        )
                                    );
                                }
                                break;
                            case 'array':
                                if (!is_array($value) && !is_a($value, stdClass::class)) {
                                    $this->document->addError(
                                        ErrorFactory::unexpectedFieldDataType(
                                            $resource->getType(),
                                            $attribute->name,
                                            gettype($value),
                                            'array'
                                        )
                                    );
                                }
                                $value = (array)$value;
                                break;
                            default:
                                break;
                        }
                        try {
                            $ref = new ReflectionClass($type);
                            if ($ref->implementsInterface(Deserializable::class)) {
                                /** @var Deserializable $type */
                                $value = $type::jsonDeserialize($value);
                            } elseif ($ref->implementsInterface(DateTimeInterface::class)) {
                                $value = DateTimeImmutable::createFromFormat(DateTimeInterface::ATOM, $value);
                            } elseif ($ref->implementsInterface(BackedEnum::class)) {
                                /** @var BackedEnum $type */
                                $value = $type::from($value);
                            }
                        } catch (ReflectionException) {
                            //NOSONAR
                        }
                    } elseif ($attribute->nullable === false) {
                        $this->document->addError(
                            ErrorFactory::unexpectedFieldDataType(
                                $resource->getType(),
                                $attribute->name,
                                gettype($value),
                                'not null'
                            )
                        );
                    }
                }
                $resource->addAttribute(new Attribute($attribute->name, $value));
            }
        }
    }

    /**
     * @param object $object
     * @param ResourceObject $resource
     *
     * @return void
     */
    private function parseRelationships(object $object, ResourceObject $resource): void
    {
        foreach ($this->metadata->getRelationships() as $relationship) {
            if (
                property_exists($object->relationships, $relationship->name) &&
                property_exists($object->relationships->{$relationship->name}, 'data')
            ) {
                $value = $object->relationships->{$relationship->name}->data;
                if (!is_null($value)) {
                    if ($relationship->isCollection) {
                        $data = new ResourceCollection();
                        foreach ($value as $item) {
                            $data->add(new ResourceObjectIdentifier(new Type($item->type), new Id($item->id)));
                        }
                    } else {
                        $data = new ResourceObjectIdentifier(new Type($value->type), new Id($value->id));
                    }
                } else {
                    if ($relationship->nullable === false) {
                        $this->document->addError(
                            ErrorFactory::unexpectedFieldDataType(
                                $resource->getType(),
                                $relationship->name,
                                gettype($value),
                                'not null'
                            )
                        );
                    }
                    $data = $value;
                }
                $docRelationship = new Relationship($relationship->name, $resource);
                $docRelationship->setData($data);
                $resource->addRelationship($docRelationship);
            }
        }
    }
}
