<?php

/**
 * Created by lasicka@logio.cz
 * at 07.09.2024 23:22
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Atomic;

use JSONAPI\Mapper\Document\HasMeta;
use JSONAPI\Mapper\Document\Meta;
use JSONAPI\Mapper\Document\MetaTrait;
use JSONAPI\Mapper\Document\PrimaryData;
use JSONAPI\Mapper\Document\Serializable;

class Result implements Serializable, HasMeta
{
    use MetaTrait;

    private PrimaryData $data;
    private bool $noData = true;

    public function __construct(?PrimaryData $data = null)
    {
        if ($data !== null) {
            $this->noData = false;
            $this->data   = $data;
        }
    }

    public function isEmpty(): bool
    {
        return $this->getMeta()->isEmpty() && $this->noData;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): object
    {
        $ret = [];
        if ($this->hasMeta()) {
            $ret['meta'] = $this->meta;
        }
        if (!$this->noData) {
            $ret['data'] = $this->data;
        }
        return (object)$ret;
    }
}
