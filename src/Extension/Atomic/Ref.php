<?php

/**
 * Created by Tomas Benedikt
 * at 06.08.2024 22:46
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Atomic;

use JSONAPI\Mapper\Document\Id;
use JSONAPI\Mapper\Document\Lid;
use JSONAPI\Mapper\Document\Type;

final class Ref
{
    public Type $type;
    public ?Id $id = null;
    public ?Lid $lid = null;
    public ?string $relationship = null;

    public function __construct(Type $type, ?Id $id = null, ?Lid $lid = null, ?string $relationship = null)
    {
        $this->type         = $type;
        $this->id           = $id;
        $this->lid          = $lid;
        $this->relationship = $relationship;
    }

    public function getType(): string
    {
        return $this->type->getData();
    }

    public function getId(): ?string
    {
        return $this->id?->getData();
    }

    public function getLid(): ?string
    {
        return $this->lid?->getData();
    }

    public function getRelationship(): ?string
    {
        return $this->relationship;
    }
}
