<?php

/**
 * Created by lasicka@logio.cz
 * at 27.09.2024 22:13
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Atomic\Operations;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use Traversable;

/**
 * Class OperationCollection
 * @package JSONAPI\Mapper\Extension\Atomic\Operations
 * @implements IteratorAggregate<int,Operation>
 */
class OperationCollection implements IteratorAggregate, Countable
{
    /**
     * @var Operation[] $operations
     */
    private array $operations = [];

    public function add(Operation $operation): void
    {
        $this->operations[] = $operation;
    }

    /**
     * @inheritDoc
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->operations);
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->operations);
    }

    /**
     * Returns TRUE/FALSE based on emptiness of operation results. If all operation results are empty, then return TRUE.
     * In other case return FALSE.
     *
     * @return bool
     *
     */
    public function isEmpty(): bool
    {
        foreach ($this->operations as $operation) {
            if ($operation instanceof OperationWithResult) {
                if (!$operation->getResult()->isEmpty()) {
                    return false;
                }
            }
        }
        return true;
    }
}
