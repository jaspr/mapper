<?php

/**
 * Created by lasicka@logio.cz
 * at 17.09.2024 22:34
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Atomic\Operations;

use JSONAPI\Mapper\Document\ResourceObject;

class RemoveResource extends Operation
{
    /**
     * @return ResourceObject
     */
    public function getData(): ResourceObject
    {
        $data = parent::getData();
        if ($data instanceof ResourceObject) {
            return $data;
        }
        throw new \DomainException('Resource is not a ResourceObject');
    }
}
