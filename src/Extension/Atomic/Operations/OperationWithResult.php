<?php

/**
 * Created by lasicka@logio.cz
 * at 27.09.2024 22:06
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Atomic\Operations;

use JSONAPI\Mapper\Document\Meta;
use JSONAPI\Mapper\Extension\Atomic\Result;

interface OperationWithResult
{
    /**
     * @param object $resultData instance of data source object for resource fulfilment
     * @param Meta|null $meta
     * @return void
     */
    public function setResult(object $resultData, ?Meta $meta = null): void;

    /**
     * @return Result
     */
    public function getResult(): Result;
}
