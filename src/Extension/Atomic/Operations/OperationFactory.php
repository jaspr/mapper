<?php

/**
 * Created by bednic
 * at 17.09.2024 22:46
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Atomic\Operations;

use InvalidArgumentException;
use JSONAPI\Mapper\Document\Meta;
use JSONAPI\Mapper\Document\PrimaryData;
use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Extension\Atomic\Op;
use JSONAPI\Mapper\Extension\Atomic\Ref;

class OperationFactory
{
    private Encoder $encoder;

    public function __construct(Encoder $encoder)
    {
        $this->encoder = $encoder;
    }

    public function create(Op $op, ?Ref $ref, ?PrimaryData $data, ?Meta $meta, ?string $href): Operation
    {
        if ($op === Op::ADD) {
            if ($ref && $ref->relationship) {
                return new CreateRelationship($op, $ref, $data, $meta, $href);
            }
            return new CreateResource($this->encoder, $op, $ref, $data, $meta, $href);
        } elseif ($op === Op::UPDATE) {
            if ($ref && $ref->relationship) {
                return new UpdateRelationship($op, $ref, $data, $meta, $href);
            }
            return new UpdateResource($this->encoder, $op, $ref, $data, $meta, $href);
        } elseif ($op === Op::REMOVE) {
            if ($ref && $ref->relationship) {
                return new RemoveRelationship($op, $ref, $data, $meta, $href);
            }
            return new RemoveResource($op, $ref, $data, $meta, $href);
        } else {
            throw new InvalidArgumentException("Unsupported operation type");
        }
    }
}
