<?php

/**
 * Created by lasicka@logio.cz
 * at 17.09.2024 22:33
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Atomic\Operations;

use JSONAPI\Mapper\Document\Meta;
use JSONAPI\Mapper\Document\PrimaryData;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Extension\Atomic\Op;
use JSONAPI\Mapper\Extension\Atomic\Ref;
use JSONAPI\Mapper\Extension\Atomic\Result;

/**
 * Class CreateResource
 * @package JSONAPI\Mapper\Extension\Atomic\Operations
 */
class CreateResource extends Operation implements OperationWithResult
{
    private Encoder $encoder;
    private Result $result;

    public function __construct(Encoder $encoder, Op $op, ?Ref $ref, ?PrimaryData $data, ?Meta $meta, ?string $href)
    {
        parent::__construct($op, $ref, $data, $meta, $href);
        $this->encoder = $encoder;
        $this->result  = new Result();
    }

    /**
     * @return ResourceObject
     */
    public function getData(): ResourceObject
    {
        $data = parent::getData();
        if ($data instanceof ResourceObject) {
            return $data;
        }
        throw new \DomainException('Data must be a ResourceObject');
    }

    /**
     * @inheritDoc
     */
    public function setResult(object $resultData, ?Meta $meta = null): void
    {
        $this->result = new Result($this->encoder->encode($resultData));
        if ($meta) {
            $this->result->getMeta()->merge($meta);
        }
    }

    /**
     * @inheritDoc
     */
    public function getResult(): Result
    {
        return $this->result;
    }
}
