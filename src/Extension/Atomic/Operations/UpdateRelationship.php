<?php

/**
 * Created by lasicka@logio.cz
 * at 17.09.2024 22:37
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Atomic\Operations;

use JSONAPI\Mapper\Document\ResourceCollection;
use JSONAPI\Mapper\Document\ResourceObjectIdentifier;

class UpdateRelationship extends Operation
{
    /**
     * @return bool
     */
    public function isCollection(): bool
    {
        $data = parent::getData();
        return is_iterable($data);
    }

    /**
     * @return ResourceObjectIdentifier|ResourceCollection<ResourceObjectIdentifier>|null
     */
    public function getData(): ResourceObjectIdentifier|ResourceCollection|null
    {
        $data = parent::getData();
        if ($data instanceof ResourceCollection) {
            return $data;
        } elseif ($data instanceof ResourceObjectIdentifier || is_null($data)) {
            return $data;
        } else {
            throw new \DomainException('Data must be a ResourceObject');
        }
    }
}
