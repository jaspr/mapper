<?php

/**
 * Created by lasicka@logio.cz
 * at 17.09.2024 22:38
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Atomic\Operations;

use JSONAPI\Mapper\Document\ResourceCollection;
use JSONAPI\Mapper\Document\ResourceObjectIdentifier;

class RemoveRelationship extends Operation
{
    /**
     * @return ResourceCollection<ResourceObjectIdentifier>
     */
    public function getData(): ResourceCollection
    {
        $data = parent::getData();
        if ($data instanceof ResourceCollection) {
            return $data;
        }
        throw new \DomainException('Data must be a ResourceCollection');
    }
}
