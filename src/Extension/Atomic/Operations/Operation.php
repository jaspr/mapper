<?php

/**
 * Created by @bednic
 * at 06.08.2024 22:37
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Atomic\Operations;

use JSONAPI\Mapper\Document\Meta;
use JSONAPI\Mapper\Document\PrimaryData;
use JSONAPI\Mapper\Extension\Atomic\Op;
use JSONAPI\Mapper\Extension\Atomic\Ref;
use JSONAPI\Mapper\Extension\Atomic\Result;

abstract class Operation
{
    private Op $op;
    private ?Ref $ref;
    private ?string $href;
    private ?PrimaryData $data;
    private ?Meta $meta;

    public function __construct(Op $op, ?Ref $ref, ?PrimaryData $data, ?Meta $meta, ?string $href)
    {
        $this->op = $op;
        $this->ref = $ref;
        $this->href = $href;
        $this->data = $data;
        $this->meta = $meta;
    }

    public function getOp(): Op
    {
        return $this->op;
    }

    public function getHref(): ?string
    {
        return $this->href;
    }

    public function getRef(): ?Ref
    {
        return $this->ref;
    }

    public function getData(): PrimaryData|null
    {
        return $this->data;
    }

    public function getMeta(): Meta
    {
        return $this->meta;
    }
}
