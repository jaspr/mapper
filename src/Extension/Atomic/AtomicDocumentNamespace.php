<?php

/**
 * Created by @bednic
 * at 12.08.2024 22:44
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Atomic;

use JSONAPI\Mapper\Document\DocumentNamespace;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\ResourceObjectIdentifier;
use JSONAPI\Mapper\Extension\Atomic\Operations\Operation;
use JSONAPI\Mapper\Extension\Atomic\Operations\OperationCollection;

class AtomicDocumentNamespace implements DocumentNamespace
{
    /**
     * @var array<string, mixed> $body
     */
    private array $body = [];

    public const MEMBER_OPERATIONS = 'operations';
    public const MEMBER_RESULTS = 'results';

    public function set(string $property, mixed $value): void
    {
        $this->body[$property] = $value;
    }

    public function get(string $property): mixed
    {
        return $this->body[$property];
    }

    public function propertyExists(string $property): bool
    {
        return array_key_exists($property, $this->body);
    }

    /**
     * @return OperationCollection
     */
    public function getOperations(): OperationCollection
    {
        return $this->get(self::MEMBER_OPERATIONS);
    }

    /**
     * @param array<Result> $results
     * @return void
     */
    public function setResults(array $results): void
    {
        $this->set(self::MEMBER_RESULTS, $results);
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): object
    {
        $data = [];
        foreach ($this->body as $property => $value) {
            $data["atomic:" . $property] = $value;
        }
        return (object)$data;
    }
}
