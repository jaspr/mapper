<?php

/**
 * Created by tomas.benedikt@gmail.com
 * at 06.08.2024 22:37
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Atomic;

enum Op: string
{
    case ADD = 'add';
    case UPDATE = 'update';
    case REMOVE = 'remove';
}
