<?php

/**
 * Created by lasicka@logio.cz
 * at 08.09.2024 21:30
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Version;

use JSONAPI\Mapper\Extension\Definition;
use JSONAPI\Mapper\Extension\Version\Processor\VersionDocumentProcessor;
use JSONAPI\Mapper\Extension\Version\Processor\VersionResourceProcessor;

class VersionDefinition implements Definition
{
    /**
     * @inheritDoc
     */
    public function getURI(): string
    {
        return 'https://jsonapi.org/ext/version';
    }

    /**
     * @inheritDoc
     */
    public function getProcessors(): array
    {
        return [
            new VersionResourceProcessor(),
            new VersionDocumentProcessor($this->getURI())
        ];
    }

    /**
     * @inheritDoc
     */
    public function getRequestParsers(): array
    {
        return [];
    }

    public function getNamespace(): string
    {
        return 'version';
    }
}
