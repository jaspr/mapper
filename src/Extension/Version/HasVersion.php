<?php

/**
 * Created by lasicka@logio.cz
 * at 08.09.2024 22:36
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Version;

interface HasVersion
{
    public function getVersion(): int;
}
