<?php

/**
 * Created by lasicka@logio.cz
 * at 08.09.2024 21:38
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Version;

use JSONAPI\Mapper\Document\Field;

class VersionId extends Field
{
    public function __construct(int $data)
    {
        parent::__construct('version:id');
        $this->data = $data;
    }
}
