<?php

/**
 * Created by lasicka@logio.cz
 * at 08.09.2024 22:33
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Version\Processor;

use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Encoding\DocumentProcessor;

class VersionDocumentProcessor implements DocumentProcessor
{
    private string $extensionURI;

    /**
     * @param string $extensionURI
     */
    public function __construct(string $extensionURI)
    {
        $this->extensionURI = $extensionURI;
    }

    /**
     * @inheritDoc
     */
    public function processDocument(Document $document, iterable|object|null $data): void
    {
        $document->getJSONAPIObject()->addExtension($this->extensionURI);
    }
}
