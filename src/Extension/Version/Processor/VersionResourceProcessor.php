<?php

/**
 * Created by lasicka@logio.cz
 * at 08.09.2024 21:33
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Extension\Version\Processor;

use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Encoding\ResourceProcessor;
use JSONAPI\Mapper\Extension\Version\HasVersion;
use JSONAPI\Mapper\Extension\Version\VersionId;

class VersionResourceProcessor implements ResourceProcessor
{
    /**
     * @inheritDoc
     */
    public function processResource(ResourceObject $resource, object $object): void
    {
        if ($object instanceof HasVersion) {
            $version = $object->getVersion();
            $id      = new VersionId($version);
            $resource->addField($id);
        }
    }
}
