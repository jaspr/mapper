<?php

/**
 * Created by tomas
 * at 26.05.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Compose;

use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\Error;
use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Request\ParsedRequest;

/**
 * Class Builder
 * @package JSONAPI\Mapper
 */
class Builder
{
    /**
     * @var Encoder $encoder
     */
    private Encoder $encoder;
    /**
     * @var object|iterable<object>|null|false $data
     */
    private null|object|iterable|false $data = false;
    /**
     * @var Error[] $errors
     */
    private array $errors = [];
    /**
     * @var ParsedRequest $request
     */
    private ParsedRequest $request;

    /**
     * @param ParsedRequest $request
     * @param Encoder $encoder
     */
    public function __construct(ParsedRequest $request, Encoder $encoder)
    {
        $this->request = $request;
        $this->encoder = $encoder;
    }

    /**
     * @return Encoder
     */
    public function getEncoder(): Encoder
    {
        return $this->encoder;
    }

    /**
     * @return ParsedRequest
     */
    public function getRequest(): ParsedRequest
    {
        return $this->request;
    }

    /**
     * @param object|iterable<object>|null $data
     * @return $this
     */
    public function setData(object|iterable|null $data): self
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param Error $error
     * @return $this
     * @deprecated This method should not longer be used. It stays only for backward compatibility reasons.
     *             Use $this->getNamespace('')->addError($error) instead.
     */
    public function addError(Error $error): self
    {
        $this->errors[] = $error;
        return $this;
    }

    /**
     * Shortcut method for setting pagination total.
     * @param int $total
     *
     * @return $this
     * @deprecated call $builder->getRequest()->getPagination()->setTotal()
     */
    public function setTotal(int $total): self
    {
        $this->request->getPagination()?->setTotal($total);
        return $this;
    }

    /**
     * This method returns new Document and clear data and errors to prevent unwanted misbehaving.
     *
     * @return Document
     */
    public function build(): Document
    {
        if (!empty($this->errors)) {
            return $this->encoder->failure($this->errors);
        } elseif ($this->data === false) {
            return $this->encoder->empty();
        } else {
            return $this->encoder->compose($this->data);
        }
    }
}
