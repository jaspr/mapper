<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Helper;

use Closure;
use Doctrine\Common\Collections\Collection;
use JSONAPI\Mapper\ObjectCollection;
use LogicException;
use Traversable;

/**
 * Class DoctrineCollectionAdapter
 * Adapter for Doctrine\Common\Collections\Collection. In case you want work with ArrayCollection or
 * PersistentCollection and don't want to work with array from collection, just use this adapter to get advantages of
 * Collection interface, like lazy load or partial load.
 *
 * @package JSONAPI\Helper
 * @extends ObjectCollection<object>
 * @codeCoverageIgnore
 */
class DoctrineObjectCollectionAdapter extends ObjectCollection
{
    /**
     * @var Collection<int|string, mixed>
     */
    private Collection $collection;

    /**
     * @param Collection<int|string, mixed> $collection
     */
    public function __construct(Collection $collection)
    {
        if (!interface_exists('Doctrine\Common\Collections\Collection')) {
            throw new LogicException(
                'For using ' . __CLASS__ . ' you need install [doctrine/orm] <i>composer require doctrine/orm</i>.'
            );
        }
        parent::__construct();
        $this->collection = $collection;
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return $this->collection->count();
    }

    /**
     * @param Closure $callback
     *
     * @return ObjectCollection<object>
     */
    public function filter(Closure $callback): ObjectCollection
    {
        return new DoctrineObjectCollectionAdapter($this->collection->filter($callback));
    }

    /**
     * @inheritDoc
     */
    public function values(): array
    {
        return $this->collection->getValues();
    }

    /**
     * @inheritDoc
     */
    public function has($item): bool
    {
        return $this->collection->contains($item);
    }

    /**
     * @inheritDoc
     */
    public function getIterator(): Traversable
    {
        return $this->collection->getIterator();
    }

    /**
     * @return Collection<int|string, mixed>
     */
    public function getCollection(): Collection
    {
        return $this->collection;
    }

    /**
     * @param int $offset
     * @param int|null $length
     *
     * @return ObjectCollection<object>
     */
    public function slice(int $offset, ?int $length = null): ObjectCollection
    {
        return new ObjectCollection($this->collection->slice($offset, $length));
    }

    /**
     * @param object $item
     *
     * @return void
     */
    public function add(object $item): void
    {
        $this->collection->add($item);
    }

    /**
     * @param object $item
     *
     * @return bool
     */
    public function remove(object $item): bool
    {
        return $this->collection->removeElement($item);
    }

    /**
     * @param string[] $order
     *
     * @return ObjectCollection<object>
     */
    public function sort(array $order): ObjectCollection
    {
        return (new ObjectCollection($this->collection->toArray()))->sort($order);
    }
}
