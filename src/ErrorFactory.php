<?php

/**
 * Created by tomas
 * at 10.07.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper;

use Fig\Http\Message\StatusCodeInterface;
use JSONAPI\Mapper\Document\Error;
use JSONAPI\Mapper\Document\Link;
use JSONAPI\Mapper\Document\Source;
use JSONAPI\Mapper\Exception\RequiredMemberMissing;
use JSONAPI\Mapper\Request\RequestPart;
use Swaggest\JsonSchema\InvalidValue;

class ErrorFactory
{
    /**
     * @param string $name
     * @return Error
     */
    public static function unsupportedQueryParameter(string $name): Error
    {
        $error = new Error();
        $error->setCode(4_001);
        $error->setStatus(StatusCodeInterface::STATUS_BAD_REQUEST);
        $error->setDetail(sprintf('Request cannot be processed due to unsupported query parameter [%s].', $name));
        $error->setSource(Source::parameter($name));
        $url = 'https://jsonapi.org/format/1.0/#fetching';
        $error->getLinks()->add(new Link(Link::ABOUT, $url));
        return $error;
    }


    /**
     * @param string $parameter
     * @param string $message
     * @return Error
     */
    public static function malformedQueryParameter(string $parameter, string $message = 'Syntax error.'): Error
    {
        $error = new Error();
        $error->setCode(4_002);
        $error->setStatus(StatusCodeInterface::STATUS_BAD_REQUEST);
        $error->setTitle(self::titleFromMethodName(__FUNCTION__));
        $error->setDetail($message);
        $error->setSource(Source::parameter($parameter));
        $url = match ($parameter) {
            RequestPart::SORT_PART_KEY => 'https://jsonapi.org/format/1.0/#fetching-sorting',
            RequestPart::PAGINATION_PART_KEY => 'https://jsonapi.org/format/1.0/#fetching-pagination',
            RequestPart::FIELDS_PART_KEY => 'https://jsonapi.org/format/1.0/#fetching-sparse-fieldsets',
            RequestPart::INCLUSION_PART_KEY => 'https://jsonapi.org/format/1.0/#fetching-includes',
            RequestPart::FILTER_PART_KEY => 'https://jsonapi.org/format/1.0/#fetching-filtering',
            default => null
        };
        if ($url) {
            $error->getLinks()->add(new Link(Link::ABOUT, $url));
        }
        return $error;
    }

    /**
     * @param string $name
     * @return string
     */
    private static function titleFromMethodName(string $name): string
    {
        $re = '/(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])/x';
        $words = preg_split($re, $name);
        return implode(' ', array_map(fn($word) => ucfirst($word), $words));
    }

    /**
     * @param InvalidValue $exception
     * @return Error
     */
    public static function invalidDocument(InvalidValue $exception): Error
    {
        $err = $exception->inspect();
        do {
            $err = $err->subErrors[0];
            $pointer = $err->dataPointer;
            $detail = $err->error;
        } while ($err->subErrors);
        $error = new Error();
        $error->setCode(4_003);
        $error->setStatus(StatusCodeInterface::STATUS_UNPROCESSABLE_ENTITY);
        $error->setTitle(self::titleFromMethodName(__FUNCTION__));
        $error->setSource(Source::pointer($pointer));
        $error->setDetail($detail);
        $error->getLinks()->add(new Link('schema', 'http://jsonapi.org/schema'));
        return $error;
    }

    /**
     * @param string $resource
     * @param string $field
     * @param string $provided
     * @param string $expected
     * @return Error
     */
    public static function unexpectedFieldDataType(
        string $resource,
        string $field,
        string $provided,
        string $expected
    ): Error {
        $error = new Error();
        $error->setCode(4_004);
        $error->setStatus(StatusCodeInterface::STATUS_UNPROCESSABLE_ENTITY);
        $error->setTitle(self::titleFromMethodName(__FUNCTION__));
        $error->setDetail(
            sprintf(
                'Field [%s] on Resource [%s] has unexpected value data type [%s], expected [%s].',
                $field,
                $resource,
                $provided,
                $expected
            )
        );
        $error->setSource(Source::pointer("/data/attributes/$field"));
        $url = 'https://jsonapi.org/format/1.0/#fetching';
        $error->getLinks()->add(new Link(Link::ABOUT, $url));
        return $error;
    }

    /**
     * @param string $provided
     * @param string $expected
     * @return Error
     */
    public static function resourceTypeMismatch(string $provided, string $expected): Error
    {
        $error = new Error();
        $error->setCode(4_005);
        $error->setStatus(StatusCodeInterface::STATUS_BAD_REQUEST);
        $error->setTitle(self::titleFromMethodName(__FUNCTION__));
        $error->setDetail(
            sprintf(
                'Provided resource type [%s] has different type than expected [%s].',
                $provided,
                $expected
            )
        );
        return $error;
    }

    public static function forbiddenCharacter(string $memberName): Error
    {
        $error = new Error();
        $error->setCode(4_006);
        $error->setStatus(StatusCodeInterface::STATUS_UNPROCESSABLE_ENTITY);
        $error->setTitle(self::titleFromMethodName(__FUNCTION__));
        $error->setDetail(sprintf("Member name [%s] contains forbidden characters.", $memberName));
        $url = 'https://jsonapi.org/format/1.0/#document-member-names-allowed-characters';
        $error->getLinks()->add(new Link(Link::ABOUT, $url));
        return $error;
    }

    public static function badRequest(string $message): Error
    {
        $error = new Error();
        $error->setCode(4_007);
        $error->setStatus(StatusCodeInterface::STATUS_BAD_REQUEST);
        $error->setTitle(self::titleFromMethodName(__FUNCTION__));
        $error->setDetail($message);
        return $error;
    }


    public static function unknownResource(string $message = "Cannot find metadata for requested resource."): Error
    {
        $error = new Error();
        $error->setCode(4_008);
        $error->setStatus(StatusCodeInterface::STATUS_BAD_REQUEST);
        $error->setTitle(self::titleFromMethodName(__FUNCTION__));
        $error->setDetail($message);
        return $error;
    }

    public static function unsupportedMediaType(string $message = "Unsupported media type"): Error
    {
        $error = new Error();
        $error->setCode(4_009);
        $error->setStatus(StatusCodeInterface::STATUS_UNSUPPORTED_MEDIA_TYPE);
        $error->setTitle(self::titleFromMethodName(__FUNCTION__));
        $error->setDetail($message);
        return $error;
    }
    public static function requiredMemberMissing(RequiredMemberMissing $e): Error
    {
        $error = new Error();
        $error->setCode(4_010);
        $error->setStatus(StatusCodeInterface::STATUS_BAD_REQUEST);
        $error->setTitle(self::titleFromMethodName(__FUNCTION__));
        $error->setDetail($e->getMessage());
        $error->setSource($e->getPointer());
        return $error;
    }
}
