<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Driver;

use BadMethodCallException;
use JSONAPI\Mapper\Exception\Messages;
use JSONAPI\Mapper\Metadata;
use JSONAPI\Mapper\Metadata\ClassMetadata;
use JSONAPI\Mapper\Schema\Resource;
use LogicException;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionProperty;

/**
 * Class SchemaDriver
 *
 * @package JSONAPI\Schema
 */
class SchemaDriver extends Driver
{
    /**
     * @inheritDoc
     */
    public function getClassMetadata(string $className): ?ClassMetadata
    {
        try {
            $res = new ReflectionClass($className);
            if ($res->implementsInterface(Resource::class)) {
                $this->logger->debug(sprintf('Found resource [%s]', $className));
                /** @var class-string<Resource> $className */
                $schema = $className::getSchema();
                $ref = new ReflectionClass($schema->getClassName());
                $attributes = $this->parseAttributes($ref, $schema->getAttributes());
                $relationships = $this->parseRelationships($ref, $schema->getRelationships());
                return new ClassMetadata(
                    $schema->getClassName(),
                    $schema->getType() ?? $this->namingStrategy->classNameToResourceType($ref->getShortName()),
                    $schema->getId(),
                    $attributes,
                    $relationships,
                    $schema->getMeta(),
                    $schema->getOperations()
                );
            }
            return null;
        } catch (ReflectionException) {
            return null;
        }
    }

    /**
     * @param ReflectionClass<object> $reflectionClass
     * @param Metadata\Attribute[] $metadata
     *
     * @return Metadata\Attribute[]
     */
    private function parseAttributes(ReflectionClass $reflectionClass, iterable $metadata): array
    {
        $attributes = [];
        foreach ($metadata as $attribute) {
            $reflection = $this->getReflection($attribute, $reflectionClass);
            $this->fillUpAttribute($attribute, $reflection);
            $attributes[] = $attribute;
        }
        return $attributes;
    }

    /**
     * @param Metadata\Field $metadata
     * @param ReflectionClass<Resource> $reflectionClass
     *
     * @return ReflectionMethod|ReflectionProperty
     */
    private function getReflection(
        Metadata\Field $metadata,
        ReflectionClass $reflectionClass
    ): ReflectionMethod|ReflectionProperty {
        if ($metadata->property) {
            try {
                $reflection = $reflectionClass->getProperty($metadata->property);
            } catch (ReflectionException) {
                throw new LogicException(
                    Messages::propertyDoesNotExist($metadata->property, $reflectionClass->getName())
                );
            }
        } else {
            try {
                $reflection = $reflectionClass->getMethod($metadata->getter);
            } catch (ReflectionException) {
                throw new BadMethodCallException(
                    Messages::methodDoesNotExist($metadata->getter, $reflectionClass->getName())
                );
            }
            $this->isGetter($reflection);
        }
        return $reflection;
    }

    /**
     * @param ReflectionClass<object> $reflectionClass
     * @param Metadata\Relationship[] $metadata
     *
     * @return Metadata\Relationship[]
     */
    private function parseRelationships(ReflectionClass $reflectionClass, iterable $metadata): array
    {
        $relationships = [];
        foreach ($metadata as $relationship) {
            $reflection = $this->getReflection($relationship, $reflectionClass);
            $this->fillUpRelationship($relationship, $reflection, $reflectionClass);
            $relationships[] = $relationship;
        }
        return $relationships;
    }
}
