<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Driver;

use BadMethodCallException;
use JSONAPI\Mapper\Annotation;
use JSONAPI\Mapper\Annotation\Resource;
use JSONAPI\Mapper\Exception\Messages;
use JSONAPI\Mapper\Metadata;
use JSONAPI\Mapper\Metadata\ClassMetadata;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionProperty;

/**
 * Class AnnotationDriver
 *
 * @package JSONAPI\AnnotationDriver
 */
class AnnotationDriver extends Driver
{
    /**
     * @inheritDoc
     */
    public function getClassMetadata(string $className): ?ClassMetadata
    {
        try {
            $ref = new ReflectionClass($className);
            $resource = $this->getResource($ref);
            if ($resource) {
                $this->logger->debug(sprintf('Found resource [%s]', $ref->getShortName()));
                if ($resource->type === null) {
                    $resource->type = $this->namingStrategy->classNameToResourceType($ref->getShortName());
                }
                $meta = $this->getMeta($ref);
                if ($meta && !$ref->hasMethod($meta->getter)) {
                    throw new BadMethodCallException(Messages::methodDoesNotExist($meta->getter, $ref->getName()));
                }
                $attributes = [];
                $relationships = [];
                $this->parseProperties($ref, $id, $attributes, $relationships);
                $this->parseMethods($ref, $id, $attributes, $relationships);
                return new ClassMetadata(
                    $ref->getName(),
                    $resource->type,
                    $id,
                    $attributes,
                    $relationships,
                    $meta ? Metadata\Meta::create($meta->getter) : null,
                    $resource->operations
                );
            }
            return null;
        } catch (ReflectionException) {
            return null;
        }
    }

    /**
     * @param ReflectionClass<object> $reflectionClass
     * @param Metadata\Id|null $id
     * @param Metadata\Attribute[] $attributes
     * @param Metadata\Relationship[] $relationships
     */
    private function parseProperties(
        ReflectionClass $reflectionClass,
        ?Metadata\Id &$id,
        array &$attributes,
        array &$relationships
    ): void {
        foreach ($reflectionClass->getProperties(ReflectionProperty::IS_PUBLIC) as $reflectionProperty) {
            if ($this->getId($reflectionProperty)) {
                $id = Metadata\Id::createByProperty($reflectionProperty->getName());
            }
            if ($annotation = $this->getAttribute($reflectionProperty)) {
                $attribute = Metadata\Attribute::createByProperty(
                    $reflectionProperty->getName(),
                    $annotation->of,
                    $annotation->name,
                    $annotation->type,
                    $annotation->nullable
                );
                $this->fillUpAttribute($attribute, $reflectionProperty);
                $attributes[] = $attribute;
            }
            if ($annotation = $this->getRelationship($reflectionProperty)) {
                $meta = null;
                if ($ma = $this->getMeta($reflectionProperty)) {
                    if (!$reflectionClass->hasMethod($ma->getter)) {
                        throw new BadMethodCallException(
                            Messages::methodDoesNotExist($ma->getter, $reflectionClass->getName())
                        );
                    }
                    $meta = Metadata\Meta::create($ma->getter);
                }
                $relationship = Metadata\Relationship::createByProperty(
                    $reflectionProperty->getName(),
                    $annotation->target,
                    $annotation->name,
                    $annotation->isCollection,
                    $meta,
                    $annotation->nullable,
                    $annotation->withData,
                    $annotation->operations
                );
                $this->fillUpRelationship($relationship, $reflectionProperty, $reflectionClass);
                $relationships[] = $relationship;
            }
        }
    }

    /**
     * @param ReflectionClass<object> $reflectionClass
     * @param Metadata\Id|null $id
     * @param Metadata\Attribute[] $attributes
     * @param Metadata\Relationship[] $relationships
     */
    private function parseMethods(
        ReflectionClass $reflectionClass,
        ?Metadata\Id &$id,
        array &$attributes,
        array &$relationships
    ): void {
        foreach ($reflectionClass->getMethods(ReflectionMethod::IS_PUBLIC) as $reflectionMethod) {
            if (!$reflectionMethod->isConstructor() && !$reflectionMethod->isDestructor()) {
                if ($this->getId($reflectionMethod)) {
                    $this->isGetter($reflectionMethod);
                    $id = Metadata\Id::createByMethod($reflectionMethod->getName());
                }
                if ($annotation = $this->getAttribute($reflectionMethod)) {
                    $this->isGetter($reflectionMethod);
                    $attribute = Metadata\Attribute::createByMethod(
                        $reflectionMethod->getName(),
                        $annotation->name,
                        $annotation->type,
                        $annotation->of,
                        $annotation->nullable
                    );
                    $this->fillUpAttribute($attribute, $reflectionMethod);
                    $attributes[] = $attribute;
                }

                if ($annotation = $this->getRelationship($reflectionMethod)) {
                    $this->isGetter($reflectionMethod);
                    $meta = null;
                    if ($ma = $this->getMeta($reflectionMethod)) {
                        if (!$reflectionClass->hasMethod($ma->getter)) {
                            throw new BadMethodCallException(
                                Messages::methodDoesNotExist($ma->getter, $reflectionClass->getName())
                            );
                        }
                        $meta = Metadata\Meta::create($ma->getter);
                    }
                    $relationship = Metadata\Relationship::createByMethod(
                        $reflectionMethod->getName(),
                        $annotation->target,
                        $annotation->name,
                        $annotation->isCollection,
                        $meta,
                        $annotation->nullable,
                        $annotation->withData,
                        $annotation->operations
                    );
                    $this->fillUpRelationship($relationship, $reflectionMethod, $reflectionClass);
                    $relationships[] = $relationship;
                }
            }
        }
    }

    /**
     * @param ReflectionClass<object> $ref
     *
     * @return Resource|null
     */
    private function getResource(ReflectionClass $ref): ?Resource
    {
        $attributes = $ref->getAttributes(Resource::class, ReflectionAttribute::IS_INSTANCEOF);
        if (count($attributes) < 1) {
            return null;
        }
        if (count($attributes) > 1) {
            $this->logger->warning("Expected 1 Resource annotation, got " . count($attributes));
        }
        return array_shift($attributes)->newInstance();
    }

    /**
     * @param ReflectionClass<object>|ReflectionProperty|ReflectionMethod $ref
     *
     * @return Annotation\Meta|null
     */
    private function getMeta(ReflectionClass|ReflectionProperty|ReflectionMethod $ref): ?Annotation\Meta
    {
        $attributes = $ref->getAttributes(Annotation\Meta::class, ReflectionAttribute::IS_INSTANCEOF);
        if (count($attributes) < 1) {
            return null;
        }
        if (count($attributes) > 1) {
            $this->logger->warning("Expected 1 Meta annotation, got " . count($attributes));
        }
        return array_shift($attributes)->newInstance();
    }

    /**
     * @param ReflectionProperty|ReflectionMethod $ref
     *
     * @return Annotation\Id|null
     */
    private function getId(ReflectionProperty|ReflectionMethod $ref): ?Annotation\Id
    {
        $attributes = $ref->getAttributes(Annotation\Id::class, ReflectionAttribute::IS_INSTANCEOF);
        if (count($attributes) < 1) {
            return null;
        }
        if (count($attributes) > 1) {
            $this->logger->warning("Expected 1 Id annotation, got " . count($attributes));
        }
        return array_shift($attributes)->newInstance();
    }

    /**
     * @param ReflectionProperty|ReflectionMethod $ref
     *
     * @return Annotation\Attribute|null
     */
    private function getAttribute(ReflectionProperty|ReflectionMethod $ref): ?Annotation\Attribute
    {
        $attributes = $ref->getAttributes(Annotation\Attribute::class, ReflectionAttribute::IS_INSTANCEOF);
        if (count($attributes) < 1) {
            return null;
        }
        if (count($attributes) > 1) {
            $this->logger->warning("Expected 1 Attribute annotation, got " . count($attributes));
        }
        return array_shift($attributes)->newInstance();
    }

    /**
     * @param ReflectionProperty|ReflectionMethod $ref
     *
     * @return Annotation\Relationship|null
     */
    private function getRelationship(ReflectionProperty|ReflectionMethod $ref): ?Annotation\Relationship
    {
        $attributes = $ref->getAttributes(Annotation\Relationship::class, ReflectionAttribute::IS_INSTANCEOF);
        if (count($attributes) < 1) {
            return null;
        }
        if (count($attributes) > 1) {
            $this->logger->warning("Expected 1 Relationship annotation, got " . count($attributes));
        }
        return array_shift($attributes)->newInstance();
    }
}
