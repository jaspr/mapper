<?php

/**
 * Created by uzivatel
 * at 20.09.2022 9:58
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Driver;

interface NamingStrategy
{
    /**
     * Converts short class name to resource type.
     * @link https://jsonapi.org/format/#document-resource-object-identification
     *
     * This type is used in url construction.
     * @link https://jsonapi.org/recommendations/#urls
     *
     * @param string $className
     * @return string
     */
    public function classNameToResourceType(string $className): string;

    /**
     * Converts object property name to member name
     * @link https://jsonapi.org/format/#document-member-names
     *
     * Member names of relationships are used in URL construction.
     * @link https://jsonapi.org/recommendations/#urls-relationships
     *
     * @param string $name
     * @return string
     */
    public function reflectionNameToMemberName(string $name): string;

    /**
     * @param string $name
     * @return string
     */
    public function relationshipNameToURI(string $name): string;

    /**
     * @param string $name
     * @return string
     */
    public function relationshipNameFromURI(string $name): string;
}
