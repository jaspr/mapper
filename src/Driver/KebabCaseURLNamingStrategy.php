<?php

/**
 * Created by uzivatel
 * at 20.09.2022 10:18
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Driver;

use Symfony\Component\String\Inflector\EnglishInflector;
use Symfony\Component\String\Inflector\InflectorInterface;

use function Symfony\Component\String\s;

/**
 * Class KebabCaseURLNamingStrategy
 * Only difference between this and recommended strategy is that
 * relationship name in URL is kebab-case instead camelCase
 *
 * @package JSONAPI\Mapper\Driver
 */
class KebabCaseURLNamingStrategy implements NamingStrategy
{
    /**
     * @var InflectorInterface
     */
    private InflectorInterface $inflector;

    public function __construct()
    {
        $this->inflector = new EnglishInflector();
    }

    /**
     * @inheritDoc
     */
    public function classNameToResourceType(string $className): string
    {
        return $this->inflector->pluralize(s($className)->snake()->replace('_', '-')->toString())[0];
    }

    /**
     * @inheritDoc
     */
    public function reflectionNameToMemberName(string $name): string
    {
        return s($name)->camel()->toString();
    }

    /**
     * @inheritDoc
     */
    public function relationshipNameToURI(string $name): string
    {
        return s($name)->snake()->replace('_', '-')->toString();
    }

    /**
     * @inheritDoc
     */
    public function relationshipNameFromURI(string $name): string
    {
        return s($name)->camel()->toString();
    }
}
