<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Driver;

use BadMethodCallException;
use DomainException;
use InvalidArgumentException;
use JSONAPI\Mapper\Exception\Messages;
use JSONAPI\Mapper\Metadata;
use JSONAPI\Mapper\Metadata\ClassMetadata;
use JSONAPI\Mapper\Metadata\Operation;
use JSONAPI\Mapper\Schema\Resource;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionProperty;

/**
 * Interface Driver
 *
 * @package JSONAPI\Metadata
 */
abstract class Driver implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var NamingStrategy namingStrategy
     */
    protected NamingStrategy $namingStrategy;

    /**
     * SchemaDriver constructor.
     *
     * @param NamingStrategy|null $namingStrategy
     */
    public function __construct(?NamingStrategy $namingStrategy = null)
    {
        $this->logger = new NullLogger();
        $this->namingStrategy = $namingStrategy ?? new RecommendedNamingStrategy();
    }

    /**
     * Regex patter for getters
     */
    private const GETTER = '/^(get|is|has)/';

    /**
     * @param string $className
     *
     * @phpstan-param class-string $className
     * @return ClassMetadata|null - If class is Resource, returns its metadata, null otherwise
     */
    abstract public function getClassMetadata(string $className): ?ClassMetadata;

    /**
     * This method check if annotated method is getter.
     *
     * @param ReflectionMethod $getter
     */
    protected function isGetter(ReflectionMethod $getter): void
    {
        $type = $this->getType($getter);
        if (!(($type->getName() !== 'void') || preg_match(self::GETTER, $getter->getName()))) {
            throw new InvalidArgumentException(Messages::notGetter($getter->getName(), $getter->class));
        }
    }

    /**
     * @param ReflectionMethod|ReflectionProperty|ReflectionParameter $reflection
     *
     * @return ReflectionNamedType
     */
    protected function getType(
        ReflectionMethod|ReflectionProperty|ReflectionParameter $reflection
    ): ReflectionNamedType {
        $type = $reflection instanceof ReflectionMethod ? $reflection->getReturnType() : $reflection->getType();
        if ($type instanceof ReflectionNamedType) {
            return $type;
        }
        throw new DomainException(
            Messages::returnTypeUnrecognized(
                $reflection->getName(),
                $reflection->getDeclaringClass()->getName()
            )
        );
    }

    /**
     * @param Metadata\Attribute $attribute
     * @param ReflectionProperty|ReflectionMethod $reflection
     */
    protected function fillUpAttribute(
        Metadata\Attribute $attribute,
        ReflectionProperty|ReflectionMethod $reflection
    ): void {
        if (!$attribute->name) {
            $attribute->name = $this->getName($reflection);
        }
        if ($attribute->type === null) {
            $attribute->type = $this->getType($reflection)->getName();
        }
        if ($attribute->nullable === null) {
            $attribute->nullable = $this->getType($reflection)->allowsNull();
        }
        if ($attribute->type === 'array' && $attribute->of === null) {
            $attribute->of = $this->tryGetArrayType($reflection);
        }
    }

    /**
     * @param ReflectionMethod|ReflectionProperty $reflection
     *
     * @return string
     */
    protected function getName(ReflectionMethod|ReflectionProperty $reflection): string
    {
        if ($reflection instanceof ReflectionProperty) {
            $name = $reflection->getName();
        } else {
            $name = lcfirst(preg_replace(self::GETTER, '', $reflection->getName()));
        }
        return $this->namingStrategy->reflectionNameToMemberName($name);
    }

    /**
     * @param ReflectionMethod|ReflectionProperty $reflection
     *
     * @return string|null
     */
    protected function tryGetArrayType(ReflectionProperty|ReflectionMethod $reflection): ?string
    {
        if (
            $reflection->getDocComment() !== false &&
            preg_match(
                '~(@return|@var) (null\|)?((array<)?(?P<type>\w+)(>)?(\[])*)(\|null)?~',
                $reflection->getDocComment(),
                $match
            )
        ) {
            return $match['type'];
        }
        return null;
    }

    /**
     * @param Metadata\Relationship $relationship
     * @param ReflectionProperty|ReflectionMethod $reflection
     * @param ReflectionClass<Resource|object> $reflectionClass
     */
    protected function fillUpRelationship(
        Metadata\Relationship $relationship,
        ReflectionProperty|ReflectionMethod $reflection,
        ReflectionClass $reflectionClass
    ): void {
        if (!$relationship->name) {
            $relationship->name = $this->getName($reflection);
        }
        if ($relationship->nullable === null) {
            $relationship->nullable = $this->getType($reflection)->allowsNull();
        }
        if ($relationship->isCollection === null) {
            $relationship->isCollection = $this->isCollection($reflection);
        }
        $excluded = [Operation::LIST];
        if (!$relationship->isCollection) {
            $excluded[] = Operation::DELETE;
            $excluded[] = Operation::CREATE;
        }
        $relationship->operations = Operation::diff($relationship->operations, $excluded);
        if ($relationship->meta && !$reflectionClass->hasMethod($relationship->meta->getter)) {
            throw new BadMethodCallException(
                Messages::methodDoesNotExist(
                    $relationship->meta->getter,
                    $reflectionClass->getName()
                )
            );
        }
    }

    /**
     * @param ReflectionProperty|ReflectionMethod $reflection
     *
     * @return bool
     */
    protected function isCollection(ReflectionMethod|ReflectionProperty $reflection): bool
    {
        $type = $this->getType($reflection);
        if ($type->isBuiltin()) {
            $ret = 'array' === $type->getName();
        } else {
            try {
                $ret = (new ReflectionClass($type->getName()))->isIterable();
            } catch (ReflectionException) {
                $ret = false;
            }
        }
        return $ret;
    }
}
