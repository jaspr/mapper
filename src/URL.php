<?php

/**
 * Created by tomas
 * at 30.10.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper;

use Stringable;

class URL implements Stringable
{
    private ?string $scheme;
    private ?string $host;
    private ?int $port;
    private ?string $user;
    private ?string $pass;
    private ?string $query;
    private ?string $path;
    private ?string $fragment;
    /**
     * @var string[] $substitutions
     */
    private array $substitutions = [];
    /**
     * @var string[] $args
     */
    private array $args = [];

    private function __construct(string $base)
    {
        $url            = parse_url($base);
        $this->scheme   = array_key_exists('scheme', $url) ? $url['scheme'] : null;
        $this->host     = array_key_exists('host', $url) ? $url['host'] : null;
        $this->port     = array_key_exists('port', $url) ? $url['port'] : null;
        $this->user     = array_key_exists('user', $url) ? $url['user'] : null;
        $this->pass     = array_key_exists('pass', $url) ? $url['pass'] : null;
        $this->query    = array_key_exists('query', $url) ? $url['query'] : null;
        $this->path     = array_key_exists('path', $url) ? $url['path'] : null;
        $this->fragment = array_key_exists('fragment', $url) ? $url['fragment'] : null;
    }

    /**
     * @param string $base
     * @return URL
     */
    public static function create(string $base): URL
    {
        return new URL($base);
    }

    /**
     * @param string $path
     * @return $this
     */
    public function path(string $path): static
    {
        if (str_starts_with($path, '/')) {
            $this->path = $path;
        } else {
            $prefix = explode('/', $this->path ?? '/');
            $suffix = explode('/', $path);
            for ($i = 0; $i < count($suffix); $i++) {
                $prefix[count($prefix) - 1 + $i] = $suffix[$i];
            }
            $this->path = implode('/', $prefix);
        }
        return $this;
    }

    /**
     * @param string[] $args
     * @return $this
     */
    public function query(array $args): static
    {
        $this->args = array_merge($this->args, $args);
        return $this;
    }

    /**
     * Placeholder have format as '{id}'
     * @param string $placeholder Just placeholder name e.g. 'id'
     * @param string $value
     * @return static
     */
    public function replace(string $placeholder, string $value): static
    {
        $this->substitutions['{' . $placeholder . '}'] = rawurlencode($value);
        return $this;
    }

    /**
     * @return string
     */
    public function build(): string
    {
        $query = $this->query;
        if ($this->args) {
            if (!empty($query)) {
                $query .= '&';
            }
            $query .= http_build_query($this->args, arg_separator: '&', encoding_type: PHP_QUERY_RFC3986);
        }
        $url = ($this->scheme ? $this->scheme . '://' : '') .
            ($this->user ? $this->user . ($this->pass ? ':' . $this->pass : '') . '@' : '') .
            ($this->host ?? '') .
            ($this->port ? ':' . $this->port : '') .
            ($this->path ?? '') .
            ($query ? '?' . $query : '') .
            ($this->fragment ? '#' . $this->fragment : '');
        return str_replace(array_keys($this->substitutions), array_values($this->substitutions), $url);
    }

    /**
     * @return URL
     */
    public function clone(): URL
    {
        return clone $this;
    }

    /**
     * @return string URL-save string by RFC3986
     */
    public function __toString(): string
    {
        return $this->build();
    }
}
