<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Schema;

use JSONAPI\Mapper\Metadata\Attribute;
use JSONAPI\Mapper\Metadata\Id;
use JSONAPI\Mapper\Metadata\Meta;
use JSONAPI\Mapper\Metadata\Operation;
use JSONAPI\Mapper\Metadata\Relationship;

/**
 * Class ResourceSchema
 * @package JSONAPI\Mapper\Schema
 */
final readonly class ResourceSchema
{
    /**
     * @var string
     */
    private string $className;
    /**
     * @var string|null
     */
    private ?string $type;
    /**
     * @var Id
     */
    private Id $id;
    /**
     * @var Attribute[]
     */
    private array $attributes;
    /**
     * @var Relationship[]
     */
    private array $relationships;
    /**
     * @var Meta|null
     */
    private ?Meta $meta;
    /**
     * @var Operation[] $operations
     */
    private array $operations;

    /**
     * ResourceSchema constructor.
     *
     * @param string $className
     * @param Id $id
     * @param string|null $type
     * @param Attribute[] $attributes
     * @param Relationship[] $relationships
     * @param Meta|null $resourceMeta
     * @param Operation[] $operations
     */
    public function __construct(
        string $className,
        Id $id,
        ?string $type = null,
        array $attributes = [],
        array $relationships = [],
        ?Meta $resourceMeta = null,
        array $operations = [
            Operation::CREATE,
            Operation::READ,
            Operation::UPDATE,
            Operation::DELETE,
            Operation::LIST
        ]
    ) {
        $this->className = $className;
        $this->type = $type;
        $this->id = $id;
        $this->attributes = $attributes;
        $this->relationships = $relationships;
        $this->meta = $resourceMeta;
        $this->operations = $operations;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return Attribute[]
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @return Relationship[]
     */
    public function getRelationships(): array
    {
        return $this->relationships;
    }

    /**
     * @return Meta|null
     */
    public function getMeta(): ?Meta
    {
        return $this->meta;
    }

    /**
     * @return Operation[]
     */
    public function getOperations(): array
    {
        return $this->operations;
    }
}
