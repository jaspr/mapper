<?php

/**
 * Created by lasicka@logio.cz
 * at 28.08.2024 23:01
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Exception;

class UnsupportedMediaType extends \RuntimeException
{
}
