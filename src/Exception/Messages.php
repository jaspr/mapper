<?php

/**
 * Created by tomas
 * at 14.07.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Exception;

class Messages
{
    /**
     * @param string $name
     * @param string $className
     * @return string
     */
    public static function methodDoesNotExist(string $name, string $className): string
    {
        return sprintf("Method [%s] does not exist on class [%s]", $name, $className);
    }

    /**
     * @param string $name
     * @param string $className
     * @return string
     */
    public static function notGetter(string $name, string $className): string
    {
        return sprintf(
            "Annotation on method MUST be on getter.
    It should start with 'get', 'is' or 'has' and have some return type.
    Method [%s] on resource [%s] doesn't seems like getter.",
            $name,
            $className
        );
    }

    /**
     * @param string $name
     * @param string $className
     * @return string
     */
    public static function returnTypeUnrecognized(string $name, string $className): string
    {
        return sprintf(
            "Cannot recognize return type of method/property [%s] on class [%s].
             Must by ReflectionNamedType. Be aware that union or intersect is not supported yet.",
            $name,
            $className
        );
    }

    /**
     * @param string $memberName
     * @return string
     */
    public static function forbiddenCharacter(string $memberName): string
    {
        return sprintf("Member name [%s] contains forbidden characters.", $memberName);
    }

    /**
     * @param string $name
     * @param string $className
     * @return string
     */
    public static function propertyDoesNotExist(string $name, string $className): string
    {
        return sprintf("Property [%s] does not exist on class [%s]", $name, $className);
    }

    /**
     * @param string $typeOrClass
     * @return string
     */
    public static function classMetadataNotFound(string $typeOrClass): string
    {
        return sprintf("Metadata for resource class/type [%s] not found.", $typeOrClass);
    }

    /**
     * @param string $typeOrClass
     * @param string $fieldName
     * @return string
     */
    public static function fieldMetadataNotFound(string $typeOrClass, string $fieldName): string
    {
        return sprintf("Metadata for field [%s] on class/type [%s] not found.", $fieldName, $typeOrClass);
    }

    public static function requiredMemberMissing(string $memberName): string
    {
        return sprintf("Member name [%s] missing.", $memberName);
    }
}
