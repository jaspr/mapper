<?php

/**
 * Created by tomas.benedikt@gmail.com
 * at 07.09.2024 23:31
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Exception;

class RequiredMemberMissing extends DocumentException
{
    public function __construct(string $member, string $pointer)
    {
        parent::__construct(Messages::requiredMemberMissing($member), $pointer);
    }
}
