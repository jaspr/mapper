<?php

/**
 * Created by tomas
 * at 15.07.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Exception;

use UnexpectedValueException;

class PatternDoesNotMatch extends UnexpectedValueException
{
}
