<?php

/**
 * Created by Lasicka <lasicka@logio.cz>
 * at 11.07.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Exception;

use RuntimeException;

class FilterException extends RuntimeException
{
}
