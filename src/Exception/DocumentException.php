<?php

/**
 * Created by lasicka@logio.cz
 * at 07.09.2024 23:58
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Exception;

use JSONAPI\Mapper\Document\Source;

abstract class DocumentException extends \RuntimeException
{
    private string $pointer;

    /**
     * @param string $message
     * @param string $pointer
     */
    public function __construct(string $message, string $pointer)
    {
        $this->pointer = $pointer;
        parent::__construct($message);
    }


    public function getPointer(): Source
    {
        return Source::pointer($this->pointer);
    }
}
