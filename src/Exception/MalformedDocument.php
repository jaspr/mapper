<?php

/**
 * Created by lasicka@logio.cz
 * at 07.09.2024 23:57
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Exception;

class MalformedDocument extends DocumentException
{
}
