<?php

/**
 * Created by uzivatel
 * at 11.07.2022 14:07
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

interface Members
{
    public const DATA = 'data';
    public const ERRORS = 'errors';
    public const JSONAPI = 'jsonapi';
    public const INCLUDED = 'included';
    public const ATTRIBUTES = 'attributes';
    public const RELATIONSHIPS = 'relationships';
    public const META = 'meta';
    public const LINKS = 'links';
    public const ID = 'id';
    public const LID = 'lid';
    public const TYPE = 'type';
    public const HREF = 'href';
    public const SELF = 'self';
    public const RELATED = 'related';
    public const FIRST = 'first';
    public const LAST = 'last';
    public const NEXT = 'next';
    public const PREV = 'prev';
    public const ABOUT = 'about';
    public const REL = 'rel';
    public const DESCRIBEDBY = 'describedby';
    public const TITLE = 'title';
    public const HREFLANG = 'hreflang';
    public const VERSION = 'version';
    public const EXT = 'ext';
    public const PROFILE = 'profile';
}
