<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

/**
 * Trait LinksTrait
 *
 * @package JSONAPI
 */
trait LinksTrait
{
    private ?Links $links = null;

    /**
     * @return Links
     */
    public function getLinks(): Links
    {
        if ($this->links === null) {
            $this->links = new Links();
        }
        return $this->links;
    }

    /**
     * @return bool
     */
    public function hasLinks(): bool
    {
        return isset($this->links) && !$this->links->isEmpty();
    }
}
