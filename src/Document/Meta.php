<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

use InvalidArgumentException;
use JSONAPI\Mapper\Exception\Messages;
use JSONAPI\Mapper\Regex;

/**
 * Class Meta
 *
 * @package JSONAPI\Document
 */
final class Meta implements Serializable
{
    /**
     * @var mixed[]
     */
    private array $properties = [];

    /**
     * Meta constructor.
     *
     * @param mixed[] $properties
     *
     * @example [
     *          'key' => 'value',
     *          ...
     * ]
     */
    public function __construct(array $properties = [])
    {
        foreach ($properties as $key => $value) {
            $this->setProperty($key, $value);
        }
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function setProperty(string $key, mixed $value): void
    {
        if (!preg_match(Regex::MEMBER_NAME, $key)) {
            throw new InvalidArgumentException(Messages::forbiddenCharacter($key));
        }
        $this->properties[$key] = $value;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getProperty(string $key): mixed
    {
        if (array_key_exists($key, $this->properties)) {
            return $this->properties[$key];
        }
        return null;
    }

    /**
     * @param string $key
     * @return void
     */
    public function removeProperty(string $key): void
    {
        if (array_key_exists($key, $this->properties)) {
            unset($this->properties[$key]);
        }
    }

    public function merge(Meta $meta): void
    {
        $this->properties = array_merge_recursive($this->properties, $meta->properties);
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return empty($this->properties);
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed[] data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): array
    {
        return $this->properties;
    }
}
