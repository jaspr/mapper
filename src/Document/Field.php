<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

use InvalidArgumentException;
use JSONAPI\Mapper\Exception\Messages;
use JSONAPI\Mapper\Regex;

/**
 * Class Field
 *
 * @package JSONAPI\Document
 */
abstract class Field implements Serializable
{
    /**
     * @var string
     */
    protected string $name;
    /**
     * @var mixed
     */
    protected mixed $data;

    /**
     * FieldMetadata constructor.
     *
     * @param string $name
     */
    protected function __construct(string $name)
    {
        if (!preg_match(Regex::MEMBER_NAME, $name)) {
            throw new InvalidArgumentException(Messages::forbiddenCharacter($name));
        }
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): mixed
    {
        return $this->getData();
    }

    /**
     * @return mixed
     */
    public function getData(): mixed
    {
        return $this->data;
    }
}
