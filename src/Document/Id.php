<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

/**
 * Class Id
 *
 * @package JSONAPI\Document
 */
final class Id extends Field
{
    /**
     * Id constructor.
     *
     * @param string|null $id
     */
    public function __construct(?string $id)
    {
        parent::__construct(Members::ID);
        $this->data = $id;
    }
}
