<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

/**
 * Interface PrimaryData
 *
 * @package JSONAPI\Document
 */
interface PrimaryData
{
}
