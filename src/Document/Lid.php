<?php

/**
 * Created by @bednic
 * at 13.08.2024 22:29
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

class Lid extends Field
{
    /**
     * Lid constructor.
     *
     * @param string|null $lid
     */
    public function __construct(?string $lid)
    {
        parent::__construct(Members::LID);
        $this->data = $lid;
    }
}
