<?php

/**
 * Created by lasicka@logio.cz
 * at 28.08.2024 22:21
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use Traversable;

/**
 * Class NamespaceCollection
 * @package JSONAPI\Mapper\Document
 * @implements IteratorAggregate<DocumentNamespace>
 */
class NamespaceCollection implements IteratorAggregate, Serializable, Countable
{
    /**
     * @var DocumentNamespace[] $namespaces
     */
    private array $namespaces = [];

    /**
     * @param DocumentNamespace $namespace
     * @return bool
     */
    public function add(DocumentNamespace $namespace): bool
    {
        if (!$this->has($namespace::class)) {
            $this->namespaces[$namespace::class] = $namespace;
            return true;
        }
        return false;
    }

    /**
     * @template T of DocumentNamespace
     * @param class-string<T> $namespace
     * @return T
     */
    public function get(string $namespace): DocumentNamespace
    {
        if (!$this->has($namespace)) {
            throw new \OutOfBoundsException("Namespace {$namespace} does not exist.");
        }
        return $this->namespaces[$namespace];
    }

    /**
     * @template T of DocumentNamespace
     * @param class-string<T> $namespace
     * @return bool
     */
    public function remove(string $namespace): bool
    {
        if ($this->has($namespace)) {
            unset($this->namespaces[$namespace]);
            return true;
        }
        return false;
    }

    /**
     * @template T of DocumentNamespace
     * @param class-string<T> $namespace
     * @return bool
     */
    public function has(string $namespace): bool
    {
        return array_key_exists($namespace, $this->namespaces);
    }

    /**
     * @return array<DocumentNamespace>
     */
    public function values(): array
    {
        return array_values($this->namespaces);
    }

    /**
     * @inheritDoc
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->namespaces);
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->namespaces);
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): object
    {
        $ret = [];
        foreach ($this->namespaces as $namespace) {
            $ret = array_merge($ret, $namespace->jsonSerialize());
        }
        return (object)$ret;
    }
}
