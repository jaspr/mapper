<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use Traversable;

use function count;

/**
 * Class ResourceCollection
 *
 * @package    JSONAPI\Document\PrimaryData
 * @implements IteratorAggregate<ResourceObjectIdentifier|ResourceObject>
 */
final class ResourceCollection implements IteratorAggregate, PrimaryData, Serializable, Countable
{
    /**
     * @var ResourceObjectIdentifier[]|ResourceObject[] data
     */
    private array $data = [];

    private ?int $total;

    /**
     * @param ResourceObjectIdentifier[]|ResourceObject[] $data
     * @param int|null $total
     */
    public function __construct(array $data = [], ?int $total = null)
    {
        $this->total = $total;
        foreach ($data as $item) {
            $this->add($item);
        }
    }


    /**
     * @param ResourceObjectIdentifier $resource
     *
     * @return string
     */
    private function key(ResourceObjectIdentifier $resource): string
    {
        return $resource->getType() . $resource->getId();
    }

    /**
     * @param ResourceObjectIdentifier $resource
     *
     * @return bool
     */
    public function add(ResourceObjectIdentifier $resource): bool
    {
        $key = $this->key($resource);
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $resource;
            return true;
        }
        return false;
    }

    /**
     * @param ResourceObjectIdentifier $resource
     *
     * @return bool
     */
    public function remove(ResourceObjectIdentifier $resource): bool
    {
        $key = $this->key($resource);
        if (array_key_exists($key, $this->data)) {
            unset($this->data[$key]);
            return true;
        }
        return false;
    }

    /**
     * @param ResourceObjectIdentifier $resource
     *
     * @return bool
     */
    public function has(ResourceObjectIdentifier $resource): bool
    {
        return array_key_exists($this->key($resource), $this->data);
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return $this->total ?? count($this->data);
    }

    /**
     * @inheritDoc
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->data);
    }

    /**
     * @return ResourceObjectIdentifier[]|ResourceObject[]
     */
    public function values(): array
    {
        return array_values($this->data);
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return ResourceObject[]|ResourceObjectIdentifier[] data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): array
    {
        return $this->values();
    }
}
