<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

/**
 * Class ResourceObject
 *
 * @package JSONAPI\Document
 */
final class ResourceObject extends ResourceObjectIdentifier implements HasLinks, PrimaryData
{
    use LinksTrait;

    /**
     * @param Attribute $attribute
     * @return bool
     */
    public function addAttribute(Attribute $attribute): bool
    {
        return $this->addField($attribute);
    }

    /**
     * @param Attribute $attribute
     *
     * @return bool
     */
    public function removeAttribute(Attribute $attribute): bool
    {
        return $this->removeField($attribute);
    }

    /**
     * @param Relationship $relationship
     * @return bool
     */
    public function addRelationship(Relationship $relationship): bool
    {
        return $this->addField($relationship);
    }

    /**
     * @param Relationship $attribute
     *
     * @return bool
     */
    public function removeRelationship(Relationship $attribute): bool
    {
        return $this->removeField($attribute);
    }

    /**
     * @param string $key
     * @return Attribute|null
     */
    public function getAttribute(string $key): ?Attribute
    {
        if ($this->hasAttribute($key)) {
            return $this->getAttributes()[$key];
        }
        return null;
    }

    /**
     * @return mixed[]
     */
    public function getAttributesData(): array
    {
        $ret = [];
        foreach ($this->getAttributes() as $attribute) {
            $ret[$attribute->getName()] = $attribute->getData();
        }
        return $ret;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function hasAttribute(string $key): bool
    {
        return array_key_exists($key, $this->getAttributes());
    }

    /**
     * @return Attribute[]
     */
    public function getAttributes(): array
    {
        return array_filter($this->fields, fn($element) => $element instanceof Attribute, ARRAY_FILTER_USE_BOTH);
    }

    /**
     * @param string $key
     * @return Relationship|null
     */
    public function getRelationship(string $key): ?Relationship
    {
        if ($this->hasRelationship($key)) {
            return $this->getRelationships()[$key];
        }
        return null;
    }

    /**
     * @return ResourceObjectIdentifier[]|ResourceObjectIdentifier[][]
     */
    public function getRelationshipsData(): array
    {
        $ret = [];
        foreach ($this->getRelationships() as $relationship) {
            $ret[$relationship->getName()] = $relationship->getData();
        }
        return $ret;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function hasRelationship(string $key): bool
    {
        return array_key_exists($key, $this->getRelationships());
    }

    /**
     * @return Relationship[]
     */
    public function getRelationships(): array
    {
        return array_filter($this->fields, fn($element) => $element instanceof Relationship, ARRAY_FILTER_USE_BOTH);
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return object data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): object
    {
        $ret = parent::jsonSerialize();
        if (count($this->getAttributes()) > 0) {
            $ret->attributes = (object)$this->getAttributes();
        }
        if (count($this->getRelationships()) > 0) {
            $ret->relationships = (object)$this->getRelationships();
        }
        if ($this->hasLinks()) {
            $ret->links = $this->getLinks();
        }
        return $ret;
    }
}
