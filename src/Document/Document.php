<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

use JSONAPI\Mapper\Request\RequestParserResultInterface;
use JsonException;
use ReflectionException;
use ReflectionProperty;

/**
 * Class Document
 *
 * @package JSONAPI\Document
 */
final class Document implements Serializable, HasLinks, HasMeta, RequestParserResultInterface
{
    use LinksTrait;
    use MetaTrait;

    /**
     * @var Error[]
     */
    private array $errors = [];

    /**
     * @var JSONAPIObject
     */
    private JSONAPIObject $jsonapi;

    /**
     * @var NamespaceCollection<DocumentNamespace> $namespaces
     */
    private NamespaceCollection $namespaces;

    /**
     * Document constructor.
     */
    public function __construct()
    {
        $this->jsonapi    = new JSONAPIObject();
        $this->namespaces = new NamespaceCollection();
        $this->namespaces->add(new DefaultNamespace());
    }

    /**
     * @return PrimaryData|null
     * @deprecated
     */
    public function getData(): ?PrimaryData
    {
        $namespace = $this->getNamespaces()->get(DefaultNamespace::class);
        return $namespace->getData();
    }

    /**
     * @return Error[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param PrimaryData|null $data
     * @deprecated
     */
    public function setData(?PrimaryData $data): void
    {
        if (count($this->errors) <= 0) {
            $namespace = $this->getNamespaces()->get(DefaultNamespace::class);
            $namespace->setData($data);
        }
    }

    /**
     * @param ResourceCollection $includes
     * @deprecated
     */
    public function setIncludes(ResourceCollection $includes): void
    {
        $namespace = $this->getNamespaces()->get(DefaultNamespace::class);
        $namespace->setIncludes($includes);
    }

    /**
     * @param ResourceObject ...$resources
     * @return void
     * @deprecated
     */
    public function addIncluded(ResourceObject ...$resources): void
    {
        $namespace = $this->getNamespaces()->get(DefaultNamespace::class);
        foreach ($resources as $resource) {
            $namespace->addIncluded($resource);
        }
    }

    /**
     * @param Error $error
     */
    public function addError(Error $error): void
    {
        $this->errors[] = $error;
    }

    /**
     * @return JSONAPIObject
     */
    public function getJSONAPIObject(): JSONAPIObject
    {
        return $this->jsonapi;
    }

    /**
     * @return object
     */
    public function jsonSerialize(): object
    {
        $ret                   = [];
        $ret[Members::JSONAPI] = $this->jsonapi;
        if (count($this->errors) > 0) {
            $ret[Members::ERRORS] = $this->errors;
        }
        if ($this->hasLinks()) {
            $ret[Members::LINKS] = $this->links;
        }
        if ($this->hasMeta()) {
            $ret[Members::META] = $this->meta;
        }
        if (!empty($this->namespaces)) {
            foreach ($this->namespaces as $namespace) {
                $ret = (object)array_merge((array)$ret, (array)$namespace->jsonSerialize());
            }
        }
        return (object)$ret;
    }

    /**
     * @return string
     * @throws JsonException
     */
    public function __toString(): string
    {
        return (string)json_encode($this, JSON_PRESERVE_ZERO_FRACTION | JSON_THROW_ON_ERROR);
    }

    /**
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        return (array)$this->jsonSerialize();
    }

    /**
     * @return string
     */
    public static function getMediaType(): string
    {
        return 'application/vnd.api+json';
    }

    /**
     * @return NamespaceCollection
     */
    public function getNamespaces(): NamespaceCollection
    {
        return $this->namespaces;
    }
}
