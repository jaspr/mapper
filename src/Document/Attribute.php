<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

/**
 * Class Attribute
 *
 * @package JSONAPI\Document
 */
final class Attribute extends Field
{
    /**
     * Attribute constructor.
     *
     * @param string $name
     * @param mixed $data
     */
    public function __construct(string $name, mixed $data)
    {
        parent::__construct($name);
        $this->data = $data;
    }
}
