<?php

/**
 * Created by lasicka@logio.cz
 * at 26.08.2024 23:20
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

/**
 * Class DefaultNamespace
 * @package JSONAPI\Mapper\Document
 */
class DefaultNamespace implements DocumentNamespace
{
    /**
     * @var array<string, mixed> $body
     */
    private array $body = [];

    public const MEMBER_DATA = 'data';
    public const MEMBER_INCLUDED = 'included';

    /**
     * @param string $property
     * @param mixed $value
     * @return void
     */
    public function set(string $property, mixed $value): void
    {
        $this->body[$property] = $value;
    }

    /**
     * @param string $property
     * @return mixed
     */
    public function get(string $property): mixed
    {
        if (!$this->propertyExists($property)) {
            throw new \OutOfBoundsException("Property $property does not exist");
        }
        return $this->body[$property];
    }

    /**
     * @param string $property
     * @return bool
     */
    public function propertyExists(string $property): bool
    {
        return array_key_exists($property, $this->body);
    }

    /**
     * @return PrimaryData|null
     */
    public function getData(): ?PrimaryData
    {
        return $this->get(self::MEMBER_DATA);
    }

    /**
     * @param PrimaryData|null $data
     * @return $this
     */
    public function setData(?PrimaryData $data): self
    {
        $this->set(self::MEMBER_DATA, $data);
        return $this;
    }

    /**
     * @param ResourceCollection $includes
     */
    public function setIncludes(ResourceCollection $includes): void
    {
        $this->set(self::MEMBER_INCLUDED, $includes);
    }

    /**
     * @param ResourceObject ...$resources
     * @return void
     */
    public function addIncluded(ResourceObject ...$resources): void
    {
        if (!$this->propertyExists(self::MEMBER_INCLUDED)) {
            $this->set(self::MEMBER_INCLUDED, new ResourceCollection());
        }
        foreach ($resources as $resource) {
            $this->get(self::MEMBER_INCLUDED)->add($resource);
        }
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): object
    {
        $data = [];
        foreach ($this->body as $property => $value) {
            $data[$property] = $value;
        }
        return (object)$data;
    }
}
