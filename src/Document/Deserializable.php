<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

use stdClass;

/**
 * Interface JsonDeserializable
 * Marks class that can be initialized from json data
 *
 * @package JSONAPI
 */
interface Deserializable
{
    /**
     * @param stdClass|mixed[] $json
     *
     * @return self
     */
    public static function jsonDeserialize(mixed $json): self;
}
