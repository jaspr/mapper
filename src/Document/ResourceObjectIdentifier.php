<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

/**
 * Class ResourceObjectIdentifier
 *
 * @package JSONAPI\Document
 */
class ResourceObjectIdentifier implements Serializable, HasMeta, PrimaryData
{
    use MetaTrait;

    /**
     * @var Field[]
     */
    protected array $fields;

    /**
     * ResourceObjectIdentifier constructor.
     *
     * @param Type $type
     * @param Id $id
     * @param Lid|null $lid
     */
    public function __construct(Type $type, Id $id, ?Lid $lid = null)
    {
        $this->fields = [
            Members::TYPE => $type,
            Members::ID => $id
        ];
        if ($lid) {
            $this->fields[Members::LID] = $lid;
        }
    }

    /**
     * @param Field $field
     *
     * @return bool
     */
    public function addField(Field $field): bool
    {
        if (!array_key_exists($field->getName(), $this->fields)) {
            $this->fields[$field->getName()] = $field;
            return true;
        }
        return false;
    }

    /**
     * @param Field $field
     *
     * @return bool
     */
    public function removeField(Field $field): bool
    {
        if (
            array_key_exists($field->getName(), $this->fields) && !in_array(
                $field->getName(),
                [Members::ID, Members::TYPE]
            )
        ) {
            unset($this->fields[$field->getName()]);
            return true;
        }
        return false;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->fields[Members::ID]->getData();
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->fields[Members::TYPE]->getData();
    }

    /**
     * Returns lid if exists, or null if doesn't
     * @return string|null
     */
    public function getLid(): ?string
    {
        return array_key_exists(Members::LID, $this->fields) ? $this->fields[Members::LID]->getData() : null;
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return object data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): object
    {
        $ret = [];
        $ret[Members::TYPE] = $this->fields[Members::TYPE];
        $ret[Members::ID] = $this->fields[Members::ID];
        if (array_key_exists(Members::LID, $this->fields)) {
            $ret[Members::LID] = $this->fields[Members::LID];
        }
        if ($this->hasMeta()) {
            $ret[Members::META] = $this->getMeta();
        }
        return (object)$ret;
    }
}
