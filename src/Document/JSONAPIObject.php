<?php

/**
 * Created by tomas
 * at 15.07.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

class JSONAPIObject implements Serializable, HasMeta
{
    use MetaTrait;

    public const VERSION = '1.1';
    /**
     * @var array<string> $extensions
     */
    private array $extensions = [];
    /**
     * @var array<string> $profiles
     */
    private array $profiles = [];

    /**
     * @param string $uri
     * @return void
     */
    public function addExtension(string $uri): void
    {
        $this->extensions[] = $uri;
    }

    /**
     * @param string $uri
     * @return void
     */
    public function addProfile(string $uri): void
    {
        $this->profiles[] = $uri;
    }

    /**
     * @return object
     */
    public function jsonSerialize(): object
    {
        $ret = [
            Members::VERSION => self::VERSION,
            Members::PROFILE => $this->profiles,
            Members::EXT     => $this->extensions,
        ];
        if ($this->hasMeta()) {
            $ret['meta'] = $this->meta;
        }
        return (object)$ret;
    }
}
