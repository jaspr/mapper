<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

use stdClass;

/**
 * Class Relationships
 *
 * @package JSONAPI\Document
 */
final class Relationship extends Field implements Serializable, HasLinks, HasMeta
{
    use LinksTrait;
    use MetaTrait;

    /**
     * @var ResourceObject owner
     */
    private ResourceObject $resource;
    /**
     * @var int|bool withData
     */
    private int|bool $withData = false;

    /**
     * @param string $name
     * @param ResourceObject $resource
     */
    public function __construct(string $name, ResourceObject $resource)
    {
        parent::__construct($name);
        $this->resource = $resource;
    }

    /**
     * @param ResourceObjectIdentifier|ResourceCollection|null $data
     * @param true|int $limit
     * @return void
     */
    public function setData(ResourceObjectIdentifier|ResourceCollection|null $data, true|int $limit = true): void
    {
        $this->withData = $limit;
        $this->data = $data;
    }

    /**
     * @return bool|int
     */
    public function withData(): bool|int
    {
        return $this->withData;
    }

    /**
     * @return ResourceObjectIdentifier|ResourceCollection|null
     */
    public function getData(): ResourceObjectIdentifier|ResourceCollection|null
    {
        return $this->data;
    }

    /**
     * @return ResourceObject
     */
    public function getOwner(): ResourceObject
    {
        return $this->resource;
    }

    /**
     * @return object
     */
    public function jsonSerialize(): object
    {
        $ret = new stdClass();
        if ($this->withData !== false) {
            $ret->data = $this->getData();
        }
        if ($this->hasLinks()) {
            $ret->links = $this->getLinks();
        }
        if ($this->hasMeta()) {
            $ret->meta = $this->getMeta();
        }
        return $ret;
    }
}
