<?php

/**
 * Created by @bednic
 * at 12.08.2024 22:24
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

/**
 * Class DocumentNamespace
 * @package JSONAPI\Mapper\Document
 */
interface DocumentNamespace extends Serializable
{
    public function set(string $property, mixed $value): void;

    public function get(string $property): mixed;

    public function propertyExists(string $property): bool;
}
