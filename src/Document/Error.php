<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

/**
 * Class Error
 *
 * @package JSONAPI\Document
 */
final class Error implements Serializable, HasLinks, HasMeta
{
    use LinksTrait;
    use MetaTrait;

    /**
     * A unique identifier for this particular occurrence of the problem.
     *
     * @var string|null
     */
    private ?string $id = null;
    /**
     * The HTTP status code applicable to this problem, expressed as a string value.
     *
     * @var string|null
     */
    private ?string $status = null;
    /**
     * An application-specific error code, expressed as a string value.
     *
     * @var string|null
     */
    private ?string $code = null;
    /**
     * A short, human-readable summary of the problem that SHOULD NOT change from occurrence to occurrence of the
     * problem, except for purposes of localization.
     *
     * @var string|null
     */
    private ?string $title = null;
    /**
     * A human-readable explanation specific to this occurrence of the problem. Like title, this field’s value can be
     * localized.
     *
     * @var string|null
     */
    private ?string $detail = null;
    /**
     * An object containing references to the source of the error.
     *
     * @var Source|null
     */
    private ?Source $source = null;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     */
    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|int|null $status
     */
    public function setStatus(null|string|int $status): void
    {
        $this->status = is_null($status) ? null : (string)$status;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string|int|null $code
     */
    public function setCode(string|int|null $code): void
    {
        $this->code = is_null($code) ? null : strval($code);
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getDetail(): ?string
    {
        return $this->detail;
    }

    /**
     * @param string|null $detail
     */
    public function setDetail(?string $detail): void
    {
        $this->detail = $detail;
    }

    /**
     * @return Source|null
     */
    public function getSource(): ?Source
    {
        return $this->source;
    }

    /**
     * @param Source|null $source
     */
    public function setSource(?Source $source): void
    {
        $this->source = $source;
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return object data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): object
    {
        $ret = [];
        if (!is_null($this->id)) {
            $ret['id'] = $this->id;
        }
        if (!is_null($this->status)) {
            $ret['status'] = $this->status;
        }
        if (!is_null($this->code)) {
            $ret['code'] = $this->code;
        }
        if (!is_null($this->title)) {
            $ret['title'] = $this->title;
        }
        if (!is_null($this->detail)) {
            $ret['detail'] = $this->detail;
        }
        if (!is_null($this->source)) {
            $ret['source'] = $this->source;
        }
        if ($this->hasMeta()) {
            $ret['meta'] = $this->meta;
        }
        if ($this->hasLinks()) {
            $ret['links'] = $this->links;
        }
        return (object)$ret;
    }
}
