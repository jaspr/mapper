<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

use InvalidArgumentException;
use JSONAPI\Mapper\Exception\Messages;
use JSONAPI\Mapper\Regex;
use JsonSerializable;

/**
 * Class Link
 * is an object that represents a web link
 *
 * @package JSONAPI\Document
 */
class Link implements HasMeta, JsonSerializable
{
    use MetaTrait;

    public const COLLECTION = 'collection';
    public const SELF = Members::SELF;
    public const RELATED = Members::RELATED;
    public const FIRST = Members::FIRST;
    public const LAST = Members::LAST;
    public const NEXT = Members::NEXT;
    public const PREV = Members::PREV;
    public const ABOUT = Members::ABOUT;
    public const TYPE = Members::TYPE;

    /**
     * Name of the link (e.g. self)
     * @var string name
     */
    private string $name;
    /**
     * String whose value is a URI-reference [RFC3986 Section 4.1](@see https://tools.ietf.org/html/rfc3986#section-4.1)
     * pointing to the link’s target
     * @var string url
     */
    private string $href;
    /**
     * A string indicating the link’s relation type.
     * The string MUST be a valid link relation type (@see https://tools.ietf.org/html/rfc8288#section-2.1)
     * @var string|null $rel
     */
    private ?string $rel;
    /**
     * A link to a description document (e.g. OpenAPI or JSON Schema) for the link target
     * @var Link|null $describedBy
     */
    private ?Link $describedBy;
    /**
     * A string which serves as a label for the destination of a link such that it can be used as a human-readable
     * identifier (e.g., a menu entry)
     * @var string|null $title
     */
    private ?string $title;
    /**
     * A string indicating the media type of the link’s target
     * @var string|null $type
     */
    private ?string $type;
    /**
     * a string or an array of strings indicating the language(s) of the link’s target. An array of strings indicates
     * that the link’s target is available in multiple languages. Each string MUST be a valid language tag [RFC5646]
     * @var string|string[]|null $hreflang
     */
    private string|array|null $hreflang;

    /**
     * Link constructor.
     *
     * @param string $key
     * @param string $url
     * @param Meta|null $meta
     * @param string|null $rel
     * @param Link|null $describedBy
     * @param string|null $title
     * @param string|null $type
     * @param string|array<string>|null $hreflang
     */
    public function __construct(
        string $key,
        string $url,
        ?Meta $meta = null,
        ?string $rel = null,
        ?Link $describedBy = null,
        ?string $title = null,
        ?string $type = null,
        string|array|null $hreflang = null
    ) {
        $this->name = $key;
        if (!preg_match(Regex::RFC3986, $url)) {
            throw new InvalidArgumentException("Expected valid URL. Got [{$url}].");
        }
        if (!preg_match(Regex::MEMBER_NAME, $key)) {
            throw new InvalidArgumentException(Messages::forbiddenCharacter($key));
        }
        $this->name = $key;
        $this->href = $url;
        if (!is_null($meta)) {
            $this->setMeta($meta);
        }
        $this->rel         = $rel;
        $this->describedBy = $describedBy;
        $this->title       = $title;
        $this->type        = $type;
        $this->hreflang    = $hreflang;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getHref(): string
    {
        return $this->href;
    }

    public function getRel(): ?string
    {
        return $this->rel;
    }

    public function getDescribedBy(): ?Link
    {
        return $this->describedBy;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return array<string>|string|null
     */
    public function getHreflang(): array|string|null
    {
        return $this->hreflang;
    }

    public function jsonSerialize(): string|object
    {
        $isEmpty = empty($this->rel)
            && empty($this->describedBy)
            && empty($this->title)
            && empty($this->type)
            && empty($this->hreflang)
            && empty($this->meta);

        if ($isEmpty) {
            return $this->href;
        }

        $ret = [
            Members::HREF => $this->href
        ];
        if ($this->hasMeta()) {
            $ret[Members::META] = $this->meta;
        }
        if ($this->rel) {
            $ret[Members::REL] = $this->rel;
        }
        if ($this->describedBy) {
            $ret[Members::DESCRIBEDBY] = $this->describedBy;
        }
        if ($this->title) {
            $ret[Members::TITLE] = $this->title;
        }
        if ($this->type) {
            $ret[Members::TYPE] = $this->type;
        }
        if ($this->hreflang) {
            $ret[Members::HREFLANG] = $this->hreflang;
        }
        return (object)$ret;
    }
}
