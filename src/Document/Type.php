<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

/**
 * Class Type
 *
 * @package JSONAPI\Document
 */
final class Type extends Field
{
    /**
     * Type constructor.
     *
     * @param string $type
     */
    public function __construct(string $type)
    {
        parent::__construct(Members::TYPE);
        $this->data = $type;
    }

    /**
     * @param string $data
     */
    protected function setData(mixed $data): void
    {
        $this->data = (string)$data;
    }
}
