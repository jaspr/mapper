<?php

declare(strict_types=1);

namespace JSONAPI\Mapper;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Fig\Http\Message\StatusCodeInterface;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\Link;
use JSONAPI\Mapper\Document\Members;
use JSONAPI\Mapper\Document\Meta;
use JSONAPI\Mapper\Document\JSONAPIObject;
use JSONAPI\Mapper\Driver\NamingStrategy;
use JSONAPI\Mapper\Driver\RecommendedNamingStrategy;
use JSONAPI\Mapper\Encoding\LinkFactory;
use JSONAPI\Mapper\Metadata\Attribute;
use JSONAPI\Mapper\Metadata\ClassMetadata;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Metadata\Operation as Op;
use JSONAPI\Mapper\Metadata\Relationship;
use JSONAPI\Mapper\Request\RequestPart;
use JSONAPI\OAS\Exception\DuplicationEntryException;
use JSONAPI\OAS\Exception\ExclusivityCheckException;
use JSONAPI\OAS\Exception\InvalidArgumentException;
use JSONAPI\OAS\Exception\InvalidFormatException;
use JSONAPI\OAS\Exception\OpenAPIException;
use JSONAPI\OAS\Exception\ReferencedObjectNotExistsException;
use JSONAPI\OAS\Header;
use JSONAPI\OAS\Info;
use JSONAPI\OAS\MediaType;
use JSONAPI\OAS\OpenAPISpecification;
use JSONAPI\OAS\Operation;
use JSONAPI\OAS\Parameter;
use JSONAPI\OAS\PathItem;
use JSONAPI\OAS\RequestBody;
use JSONAPI\OAS\Response;
use JSONAPI\OAS\Responses;
use JSONAPI\OAS\Schema;
use JSONAPI\OAS\Server;
use JSONAPI\OAS\Type\DataType;
use JSONAPI\OAS\Type\In;
use JSONAPI\OAS\Type\Style;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use ReflectionClass;
use ReflectionException;

use function Symfony\Component\String\s;

/**
 * Class OpenAPISpecificationBuilder
 * @package JSONAPI\Mapper
 */
class OpenAPISpecificationBuilder implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var MetadataRepository
     */
    private MetadataRepository $metadataRepository;
    /**
     * @var OpenAPISpecification
     */
    private OpenAPISpecification $oas;
    /**
     * @var bool supportInclusion
     */
    private bool $supportInclusion;
    /**
     * @var bool supportSort
     */
    private bool $supportSort;
    /**
     * @var bool supportPagination
     */
    private bool $supportPagination;
    /**
     * @var string baseUrl
     */
    private string $baseUrl;

    /**
     * @var NamingStrategy $namingStrategy
     */
    private NamingStrategy $namingStrategy;

    /**
     * OpenAPISpecificationBuilder constructor.
     *
     * @param MetadataRepository $repository
     * @param string $baseUrl
     * @param bool $supportInclusion
     * @param bool $supportSort
     * @param bool $supportPagination
     * @param NamingStrategy|null $namingStrategy
     */
    public function __construct(
        MetadataRepository $repository,
        string $baseUrl,
        bool $supportInclusion = true,
        bool $supportSort = true,
        bool $supportPagination = true,
        ?NamingStrategy $namingStrategy = null
    ) {
        $this->metadataRepository = $repository;
        $this->baseUrl = $baseUrl;
        $this->supportInclusion = $supportInclusion;
        $this->supportSort = $supportSort;
        $this->supportPagination = $supportPagination;
        $this->logger = new NullLogger();
        $this->namingStrategy = $namingStrategy ?? new RecommendedNamingStrategy();
    }

    /**
     * @param Info $info
     *
     * @return OpenAPISpecification
     * @throws DuplicationEntryException
     * @throws ExclusivityCheckException
     * @throws InvalidArgumentException
     * @throws InvalidFormatException
     * @throws OpenAPIException
     * @throws ReferencedObjectNotExistsException
     */
    public function create(Info $info): OpenAPISpecification
    {
        $this->oas = new OpenAPISpecification($info);
        $this->logger->debug("OAS instance created");
        $server = new Server(rtrim($this->baseUrl, '/'));
        $this->logger->debug('Default server added');
        $server->setDescription('API Server');
        $this->oas->addServer($server);

        $this->registerSchemas();
        $this->logger->debug("Schemas registered");
        $this->registerResponses();
        $this->logger->debug("Responses registered");
        $this->registerParameters();
        $this->logger->debug("Parameters registered");
        $this->registerPaths();
        $this->logger->debug("Paths registered");

        return $this->oas;
    }

    /**
     * @throws OpenAPIException
     * @throws ReferencedObjectNotExistsException
     */
    private function registerSchemas(): void
    {
        $this->oas->getComponents()->addSchema(self::shortName(Meta::class), $this->createMeta());
        $this->oas->getComponents()->addSchema(self::shortName(Link::class), $this->createLink());
        foreach ($this->metadataRepository->getAll() as $classMetadata) {
            $this->oas->getComponents()->addSchema(
                self::shortName($classMetadata->getClassName()),
                $this->createResource($classMetadata)
            );
        }
    }

    /**
     * @param string $className
     *
     * @return string
     */
    private static function shortName(string $className): string
    {
        return s($className)->afterLast('\\')->toString();
    }

    /**
     * @return Schema
     * @throws OpenAPIException
     */
    private function createLink(): Schema
    {
        return Schema::new()->setOneOf(
            [
                DataType::uri(),
                DataType::object()
                    ->addProperty('href', DataType::uri())
                    ->addProperty(
                        Members::META,
                        $this->oas->getComponents()->createSchemaReference(self::shortName(Meta::class))
                    )
            ]
        );
    }

    /**
     * @return Schema
     * @throws OpenAPIException
     * @throws ReferencedObjectNotExistsException
     */
    private function createLinks(): Schema
    {
        $ref = $this->oas->getComponents()->createSchemaReference(self::shortName(Link::class));
        return DataType::object()
            ->setReadOnly(true)
            ->addProperty(Link::SELF, $ref)
            ->setRequired([Link::SELF])
            ->setAdditionalProperties($ref);
    }

    /**
     * @return Schema
     */
    private function createMeta(): Schema
    {
        return DataType::object()
            ->setAdditionalProperties()
            ->setReadOnly(true)
            ->setDescription(
                'Where specified, a meta member can be used to include non-standard meta-information.'
            );
    }

    /**
     * @param ClassMetadata $metadata
     *
     * @return Schema
     * @throws OpenAPIException
     * @throws ReferencedObjectNotExistsException
     */
    private function createResource(ClassMetadata $metadata): Schema
    {
        $attributes = DataType::object();
        foreach ($metadata->getAttributes() as $attribute) {
            $attributes->addProperty($attribute->name, $this->attributeToSchema($attribute));
        }
        $relationships = DataType::object();
        foreach ($metadata->getRelationships() as $relationship) {
            $relationships->addProperty($relationship->name, $this->relationshipToSchema($relationship));
        }
        $schema = DataType::object()
            ->addProperty(Members::ATTRIBUTES, $attributes)
            ->addProperty(Members::RELATIONSHIPS, $relationships)
            ->addProperty(
                Members::LINKS,
                $this->createLinks()
            );
        return Schema::new()->setAllOf(
            [
                $this->createResourceIdentifier($metadata),
                $schema
            ]
        );
    }

    /**
     * @param Attribute $attribute
     *
     * @return Schema
     */
    private function attributeToSchema(Attribute $attribute): Schema
    {
        $schema = new Schema();
        $this->logger->debug(
            sprintf("Translating type %s of %s for %s ", $attribute->type, $attribute->of, $attribute->name)
        );
        $type = $this->translateType($attribute->type);
        $schema->setType($type);
        if ($type == 'integer') {
            $schema->setFormat(PHP_INT_SIZE === 4 ? 'int32' : 'int64');
        } elseif ($type == 'array') {
            if (is_null($attribute->of)) {
                $schema->setItems((new Schema())->setType('object'));
            } else {
                $schema->setItems((new Schema())->setType($this->translateType($attribute->of)));
            }
        } elseif ($type == 'number') {
            $schema->setFormat('float');
        } elseif (
            in_array(
                $attribute->type,
                [DateTimeInterface::class, DateTimeImmutable::class, DateTime::class]
            )
        ) {
            $schema->setFormat('date-time');
        } elseif ($type) {
            if ($attribute->nullable === true) {
                $schema->setNullable(true);
            }
        }
        return $schema;
    }

    /**
     * @param string|null $type
     *
     * @return string
     */
    private function translateType(?string $type): string
    {
        return match ($type) {
            DateTimeInterface::class,
            DateTimeImmutable::class,
            DateTime::class,
            'string' => 'string',
            'int' => 'integer',
            'bool' => 'boolean',
            'array' => 'array',
            'float' => 'number',
            default => 'object',
        };
    }

    /**
     * @param Relationship $relationship
     *
     * @return Schema
     * @throws OpenAPIException
     * @throws ReferencedObjectNotExistsException
     */
    private function relationshipToSchema(Relationship $relationship): Schema
    {
        $schema = DataType::object();
        $ref = $this->createResourceIdentifier($this->metadataRepository->getByClass($relationship->target));
        $schema->addProperty(
            Members::DATA,
            $relationship->isCollection ? DataType::array($ref) : $ref
        );

        $schema->addProperty(
            Members::META,
            $this->oas->getComponents()->createSchemaReference(self::shortName(Meta::class))
        );
        $schema->addProperty(
            Members::LINKS,
            $this->createLinks()->addProperty(
                Link::RELATED,
                $this->oas->getComponents()->createSchemaReference(self::shortName(Link::class))
            )
        );
        $schema->setRequired([Members::LINKS]);
        return $schema;
    }

    /**
     * @param ClassMetadata $metadata
     *
     * @return Schema
     * @throws OpenAPIException
     */
    private function createResourceIdentifier(ClassMetadata $metadata): Schema
    {
        $schema = DataType::object()->addProperty(Members::ID, DataType::string());
        $schema->addProperty(
            Members::TYPE,
            DataType::string()->setDefault($metadata->getType())->setEnum([$metadata->getType()])
        );
        try {
            $ref = new ReflectionClass($metadata->getClassName());
            if ($ref->isAbstract()) {
                $type = [];
                foreach ($this->metadataRepository->getAll() as $cm) {
                    if (is_subclass_of($cm->getClassName(), $metadata->getClassName())) {
                        $type[] = $cm->getType();
                    }
                }
                $schema->addProperty(Members::TYPE, DataType::string()->setEnum($type));
            }
        } catch (ReflectionException) {
            //ignored
        }
        $schema->addProperty(
            Members::META,
            $this->oas->getComponents()->createSchemaReference(self::shortName(Meta::class))
        );
        $schema->setRequired([Members::ID, Members::TYPE]);

        return $schema;
    }

    /**
     * @throws OpenAPIException
     */
    private function registerResponses(): void
    {
        $this->oas
            ->getComponents()
            ->addResponse(
                (string)StatusCodeInterface::STATUS_ACCEPTED,
                new Response('The request has been accepted for processing, but the processing has not been completed')
            )
            ->addResponse(
                (string)StatusCodeInterface::STATUS_NO_CONTENT,
                new Response('The request has been successfully processed, but is not returning any content')
            )
            ->addResponse(
                (string)StatusCodeInterface::STATUS_FORBIDDEN,
                new Response('The request was a legal request, but the server is refusing to respond to it')
            )
            ->addResponse(
                (string)StatusCodeInterface::STATUS_NOT_FOUND,
                new Response('The requested page could not be found but may be available again in the future')
            )
            ->addResponse(
                (string)StatusCodeInterface::STATUS_CONFLICT,
                new Response('The request could not be completed because of a conflict in the request')
            )
            ->addResponse(
                (string)StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR,
                $this->createErrorResponse()
            );
    }

    /**
     * @return Response
     * @throws OpenAPIException
     */
    private function createErrorResponse(): Response
    {
        $document = $this->createEmptyDocument();
        $document->addProperty(
            Members::ERRORS,
            DataType::array(
                DataType::object()
                    ->addProperty(
                        Members::ID,
                        DataType::string()
                            ->setDescription(
                                'A unique identifier for this particular occurrence of the problem'
                            )
                    )
                    ->addProperty(
                        Members::LINKS,
                        DataType::uri()
                            ->addProperty(
                                Link::ABOUT,
                                $this->createLink()
                                    ->setDescription(
                                        'A link that leads to further details about this particular
                                                  occurrence of the problem'
                                    )
                            )
                    )
                    ->addProperty(
                        'status',
                        DataType::string()
                            ->setDescription(
                                'The HTTP status code applicable to this problem, expressed as a string value'
                            )
                    )
                    ->addProperty('code', DataType::string())
                    ->addProperty('title', DataType::string())
                    ->addProperty('detail', DataType::string())
                    ->addProperty(
                        'source',
                        DataType::object()
                            ->addProperty('pointer', DataType::string()->setFormat('JSON Pointer'))
                            ->addProperty('parameter', DataType::string())
                    )
                    ->addProperty(
                        Members::META,
                        $this->oas->getComponents()->createSchemaReference(self::shortName(Meta::class))
                    )
            )
        );
        $document->setRequired([Members::ERRORS]);
        $response = new Response('A generic error message, given when no more specific message is suitable');
        $response->addContent(
            Document::getMediaType(),
            (new MediaType())->setSchema(
                $document
            )
        );
        return $response;
    }

    /**
     * @return Schema
     * @throws OpenAPIException
     */
    private function createEmptyDocument(): Schema
    {
        $document = DataType::object();
        $document->addProperty(Members::JSONAPI, $this->createJsonApiObject());
        $document->addProperty(
            Members::META,
            $this->oas->getComponents()->createSchemaReference(self::shortName(Meta::class))
        );
        $document->addProperty(Members::LINKS, $this->createLinks());
        return $document;
    }

    /**
     * @throws OpenAPIException
     */
    private function createJsonApiObject(): Schema
    {
        return DataType::object()
            ->addProperty('version', DataType::string()->setEnum([JSONAPIObject::VERSION]))
            ->addProperty(
                Members::META,
                $this->oas->getComponents()->createSchemaReference(self::shortName(Meta::class))
            );
    }

    /**
     * @throws InvalidArgumentException
     * @throws ExclusivityCheckException
     * @throws DuplicationEntryException
     */
    private function registerParameters(): void
    {
        // PARAMETERS
        $id = new Parameter(Members::ID, In::PATH);
        $id->setDescription('Id of resource');
        $id->setSchema(DataType::string());
        $this->oas->getComponents()->addParameter(Members::ID, $id);

        $inclusion = new Parameter(RequestPart::INCLUSION_PART_KEY, In::QUERY);
        $inclusion->setStyle(Style::FORM);
        $inclusion->setExplode(false);
        $inclusion->setDescription(
            'An endpoint MAY also support an **include** request parameter to allow the
        client to customize which related resources should be returned'
        );
        $inclusion->setSchema(DataType::array(DataType::string()));
        $inclusion->setExample(['resource.relationship']);
        $this->oas->getComponents()->addParameter(RequestPart::INCLUSION_PART_KEY, $inclusion);

        $fields = new Parameter(RequestPart::FIELDS_PART_KEY, In::QUERY);
        $fields->setStyle(Style::DEEP_OBJECT);
        $fields->setExplode(true);
        $fields->setDescription(
            'A client MAY request that an endpoint return only specific **fields** in the
        response on a per-type basis by including a fields[TYPE] parameter.'
        );
        $fields->setSchema(DataType::object()->setAdditionalProperties(DataType::string()));
        $fields->setExample(['resource' => 'attribute,relationship']);
        $this->oas->getComponents()->addParameter(RequestPart::FIELDS_PART_KEY, $fields);

        $sort = new Parameter(RequestPart::SORT_PART_KEY, In::QUERY);
        $sort->setStyle(Style::FORM);
        $sort->setExplode(false);
        $sort->setDescription(
            'An endpoint MAY support requests to sort the primary data with a **sort** query
        parameter. The value for sort MUST represent sort fields.'
        );
        $sort->setSchema(DataType::array(DataType::string()));
        $sort->setExample(['-attribute']);
        $this->oas->getComponents()->addParameter(RequestPart::SORT_PART_KEY, $sort);

        $pagination = new Parameter(RequestPart::PAGINATION_PART_KEY, In::QUERY);
        $pagination->setDescription(
            'The **page** query parameter is reserved for pagination. Servers and clients
        SHOULD use this key for pagination operations.'
        );
        $pagination->setStyle(Style::DEEP_OBJECT);
        $pagination->setExplode(true);
        $schema = Schema::new()->setOneOf([
            DataType::object()
                ->addProperty('offset', DataType::int32())
                ->addProperty('limit', DataType::int32()),
            DataType::object()
                ->addProperty('number', DataType::int32())
                ->addProperty('size', DataType::int32()),
            DataType::object()
                ->addProperty('cursor', DataType::string())
        ]);
        $pagination->setSchema($schema);
        $pagination->setExample(['offset' => 0, 'limit' => 25]);
        $this->oas->getComponents()->addParameter(RequestPart::PAGINATION_PART_KEY, $pagination);

        $filter = new Parameter(RequestPart::FILTER_PART_KEY, In::QUERY);
        $filter->setDescription(
            'The **filter** query parameter is reserved for filtering data. Servers and
        clients SHOULD use this key for filtering operations.'
        );
        $filter->setAllowReserved(true);
        $filter->setSchema(DataType::string());
        $filter->setExample('attribute gt 1');
        $this->oas->getComponents()->addParameter(RequestPart::FILTER_PART_KEY, $filter);
    }

    /**
     * @throws InvalidFormatException
     * @throws ReferencedObjectNotExistsException
     * @throws DuplicationEntryException
     * @throws OpenAPIException
     */
    private function registerPaths(): void
    {
        $linkFactory = new LinkFactory($this->metadataRepository, $this->namingStrategy);
        foreach ($this->metadataRepository->getAll() as $classMetadata) {
            if ($classMetadata->getSupportedOperations()) {
                $shortName = self::shortName($classMetadata->getClassName());
                $collectionPathItem = new PathItem();
                $singlePathItem = new PathItem();
                $links = $linkFactory->getResourceLinksByType($classMetadata->getType());
                $collection = '/' . $links[Link::COLLECTION];
                $single = '/' . $links[Link::SELF];
                $inclusion = Schema::new()->setOneOf($this->createInclusionSchemaSet($classMetadata));
                $ref = $this->oas->getComponents()->createSchemaReference($shortName);
                foreach ($classMetadata->getSupportedOperations() as $operation) {
                    if ($operation === Op::LIST) {
                        $getOperation = new Operation();
                        $getOperation->addParameter(
                            $this->oas->getComponents()->createParameterReference(
                                RequestPart::FILTER_PART_KEY
                            )
                        );
                        $getOperation->addParameter(
                            $this->oas->getComponents()->createParameterReference(
                                RequestPart::FIELDS_PART_KEY
                            )
                        );
                        if ($this->supportPagination) {
                            $getOperation->addParameter(
                                $this->oas->getComponents()->createParameterReference(
                                    RequestPart::PAGINATION_PART_KEY
                                )
                            );
                        }
                        if ($this->supportInclusion) {
                            $getOperation->addParameter(
                                $this->oas->getComponents()->createParameterReference(
                                    RequestPart::INCLUSION_PART_KEY
                                )
                            );
                        }
                        if ($this->supportSort) {
                            $getOperation->addParameter(
                                $this->oas->getComponents()->createParameterReference(RequestPart::SORT_PART_KEY)
                            );
                        }

                        $getOperation->setResponses(
                            $this->createReadResponses()->addResponse(
                                (string)StatusCodeInterface::STATUS_OK,
                                $this->createDocumentResponse(DataType::array($ref), $inclusion, true)
                            )
                        );
                        $collectionPathItem->setGet($getOperation);
                    } elseif ($operation === Op::READ) {
                        $singlePathItem->addParameter(
                            $this->oas->getComponents()->createParameterReference(Members::ID)
                        );
                        $getOperation = new Operation();
                        $getOperation->addParameter(
                            $this->oas->getComponents()->createParameterReference(
                                RequestPart::FIELDS_PART_KEY
                            )
                        );
                        $getOperation->setResponses(
                            $this->createReadResponses()->addResponse(
                                (string)StatusCodeInterface::STATUS_OK,
                                $this->createDocumentResponse($ref, $inclusion)
                            )
                        );
                        if ($this->supportInclusion) {
                            $getOperation->addParameter(
                                $this->oas->getComponents()->createParameterReference(
                                    RequestPart::INCLUSION_PART_KEY
                                )
                            );
                        }
                        $singlePathItem->setGet($getOperation);
                    } elseif ($operation === Op::CREATE) {
                        $postOperation = new Operation();
                        $postOperation->setRequestBody($this->createRequestBodyFor($shortName));
                        $header = new Header('Location');
                        $header->setDescription('New resource endpoint url.');
                        $header->setSchema(DataType::uri());
                        $postOperation->setResponses(
                            $this->createCreateResponses()->addResponse(
                                (string)StatusCodeInterface::STATUS_CREATED,
                                $this->createDocumentResponse($ref, $inclusion)->addHeader($header)
                            )
                        );
                        $collectionPathItem->setPost($postOperation);
                    } elseif ($operation === Op::UPDATE) {
                        $patchOperation = new Operation();
                        $patchOperation->addParameter(
                            $this->oas->getComponents()->createParameterReference(
                                RequestPart::FIELDS_PART_KEY
                            )
                        );
                        $patchOperation->setRequestBody($this->createRequestBodyFor($shortName));
                        $patchOperation->setResponses(
                            $this->createUpdateResponses()->addResponse(
                                (string)StatusCodeInterface::STATUS_OK,
                                $this->createDocumentResponse($ref, $inclusion)
                            )
                        );
                        $singlePathItem->setPatch($patchOperation);
                    } elseif ($operation === Op::DELETE) {
                        $singlePathItem->setDelete(Operation::new()->setResponses($this->createDeleteResponses()));
                    }
                }

                $this->oas->getPaths()->addPath($collection, $collectionPathItem);
                $this->oas->getPaths()->addPath($single, $singlePathItem);

                foreach ($classMetadata->getRelationships() as $relationship) {
                    $relationshipLinks = $linkFactory->getRelationshipLinksByType(
                        $classMetadata->getType(),
                        $relationship->name
                    );
                    $relationshipPathItem = new PathItem();
                    $relationshipPathItem->addParameter(
                        $this->oas->getComponents()->createParameterReference(Members::ID)
                    );
                    $relationshipUrl = '/' . $relationshipLinks[Link::SELF];
                    $relationPathItem = new PathItem();
                    $relationPathItem->addParameter($this->oas->getComponents()->createParameterReference(Members::ID));
                    $relationUrl = '/' . $relationshipLinks[Link::RELATED];
                    $relMetadata = $this->metadataRepository->getByClass($relationship->target);
                    $relRef = $relationship->isCollection ?
                        DataType::array($this->createResourceIdentifier($relMetadata)) :
                        $this->createResourceIdentifier($relMetadata);
                    $relInclusion = Schema::new()->setOneOf($this->createInclusionSchemaSet($relMetadata));
                    foreach ($relationship->operations as $operation) {
                        if ($operation === Op::READ) {
                            $relationshipPathItem->setGet(
                                Operation::new()
                                    ->setResponses(
                                        $this->createReadResponses()->addResponse(
                                            (string)StatusCodeInterface::STATUS_OK,
                                            $this->createDocumentResponse($relRef, $relInclusion)
                                        )
                                    )
                            );
                            $relationPathItem->setGet(
                                Operation::new()->setResponses(
                                    $this->createReadResponses()
                                        ->addResponse(
                                            (string)StatusCodeInterface::STATUS_OK,
                                            $this->createDocumentResponse(
                                                $relationship->isCollection ?
                                                    DataType::array($this->createResource($relMetadata)) :
                                                    $this->createResource($relMetadata),
                                                $relInclusion
                                            )
                                        )
                                )
                            );
                        } elseif ($operation === Op::CREATE) {
                            $relationshipPathItem->setPost(
                                Operation::new()
                                    ->setRequestBody(
                                        $this->createRequestBodyFor(
                                            self::shortName($relationship->target)
                                        )
                                    )
                                    ->setResponses(
                                        $this->createCreateResponses()->addResponse(
                                            (string)StatusCodeInterface::STATUS_OK,
                                            $this->createDocumentResponse($relRef, $relInclusion)
                                        )
                                    )
                            );
                        } elseif ($operation === Op::UPDATE) {
                            $relationshipPathItem->setPatch(
                                Operation::new()
                                    ->setRequestBody(
                                        $this->createRequestBodyFor(
                                            self::shortName($relationship->target)
                                        )
                                    )
                                    ->setResponses(
                                        $this->createCreateResponses()->addResponse(
                                            (string)StatusCodeInterface::STATUS_OK,
                                            $this->createDocumentResponse($relRef, $relInclusion)
                                        )
                                    )
                            );
                        } elseif ($operation === Op::DELETE) {
                            $relationshipPathItem->setDelete(
                                Operation::new()->setResponses($this->createDeleteResponses())
                            );
                        }
                    }

                    $this->oas->getPaths()->addPath($relationshipUrl, $relationshipPathItem);
                    $this->oas->getPaths()->addPath($relationUrl, $relationPathItem);
                }
            }
        }
    }

    /**
     * @param ClassMetadata $metadata
     * @param Schema[] $schemas
     *
     * @return Schema[]
     * @throws OpenAPIException
     * @throws ReferencedObjectNotExistsException
     */
    private function createInclusionSchemaSet(ClassMetadata $metadata, array &$schemas = []): array
    {
        foreach ($metadata->getRelationships() as $relationship) {
            if (!isset($schemas[$relationship->target])) {
                $schemas[$relationship->target] = $this->oas->getComponents()->createSchemaReference(
                    self::shortName($relationship->target)
                );
                $relMetadata = $this->metadataRepository->getByClass($relationship->target);
                $this->createInclusionSchemaSet($relMetadata, $schemas);
            }
        }
        return array_values($schemas);
    }

    /**
     * 200, 404, 500
     * @return Responses
     * @throws OpenAPIException
     * @throws ReferencedObjectNotExistsException
     */
    private function createReadResponses(): Responses
    {
        $responses = new Responses();
        $responses->addResponse(
            (string)StatusCodeInterface::STATUS_NOT_FOUND,
            $this->oas->getComponents()
                ->createResponseReference((string)StatusCodeInterface::STATUS_NOT_FOUND)
        );
        $responses->addResponse(
            (string)StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR,
            $this->oas->getComponents()
                ->createResponseReference((string)StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR)
        );
        return $responses;
    }

    /**
     * @param Schema $data
     * @param Schema $inclusion
     * @param bool $collection
     *
     * @return Response
     * @throws OpenAPIException
     * @throws ReferencedObjectNotExistsException
     */
    private function createDocumentResponse(Schema $data, Schema $inclusion, bool $collection = false): Response
    {
        $document = $this->createEmptyDocument();
        $document->addProperty(Members::DATA, $data);
        $document->addProperty(Members::INCLUDED, $inclusion);
        if ($collection) {
            $link = $this->oas->getComponents()->createSchemaReference(self::shortName(Link::class));
            $document->addProperty(
                Members::LINKS,
                $this->createLinks()
                    ->addProperty(Link::PREV, $link)
                    ->addProperty(Link::NEXT, $link)
                    ->addProperty(Link::FIRST, $link)
                    ->addProperty(Link::LAST, $link)
            );
        }
        $document->setRequired([Members::DATA]);
        $response = new Response('Returns data of successful request');
        $response->addContent(
            Document::getMediaType(),
            (new MediaType())->setSchema(
                $document
            )
        );
        return $response;
    }

    /**
     * @param string $shortClassName
     *
     * @return RequestBody
     * @throws OpenAPIException
     * @throws ReferencedObjectNotExistsException
     */
    private function createRequestBodyFor(string $shortClassName): RequestBody
    {
        $content = new MediaType();
        $content->setSchema(
            DataType::object()
                ->addProperty(Members::JSONAPI, $this->createJsonApiObject())
                ->addProperty(Members::DATA, $this->oas->getComponents()->createSchemaReference($shortClassName))
                ->setRequired([Members::DATA])
        );
        return new RequestBody(Document::getMediaType(), $content);
    }

    /**
     * 201,202,204,403,404,409,500
     * @return Responses
     * @throws OpenAPIException
     * @throws ReferencedObjectNotExistsException
     */
    private function createCreateResponses(): Responses
    {
        $responses = $this->createReadResponses();
        $responses->addResponse(
            (string)StatusCodeInterface::STATUS_ACCEPTED,
            $this->oas->getComponents()->createResponseReference((string)StatusCodeInterface::STATUS_ACCEPTED)
        );
        $responses->addResponse(
            (string)StatusCodeInterface::STATUS_NO_CONTENT,
            $this->oas->getComponents()->createResponseReference((string)StatusCodeInterface::STATUS_NO_CONTENT)
        );
        $responses->addResponse(
            (string)StatusCodeInterface::STATUS_FORBIDDEN,
            $this->oas->getComponents()->createResponseReference((string)StatusCodeInterface::STATUS_FORBIDDEN)
        );
        $responses->addResponse(
            (string)StatusCodeInterface::STATUS_NOT_FOUND,
            $this->oas->getComponents()->createResponseReference((string)StatusCodeInterface::STATUS_NOT_FOUND)
        );
        $responses->addResponse(
            (string)StatusCodeInterface::STATUS_CONFLICT,
            $this->oas->getComponents()->createResponseReference((string)StatusCodeInterface::STATUS_CONFLICT)
        );
        return $responses;
    }

    /**
     * 200,202,204,403,404,409,500
     * @return Responses
     * @throws OpenAPIException
     * @throws ReferencedObjectNotExistsException
     */
    private function createUpdateResponses(): Responses
    {
        return $this->createCreateResponses();
    }

    /**
     * 202,204,200,404,500
     * @return Responses
     * @throws OpenAPIException
     * @throws ReferencedObjectNotExistsException
     */
    private function createDeleteResponses(): Responses
    {
        $responses = $this->createReadResponses();
        $responses->addResponse(
            (string)StatusCodeInterface::STATUS_OK,
            $this->createEmptyResponse()
        );
        $responses->addResponse(
            (string)StatusCodeInterface::STATUS_ACCEPTED,
            $this->oas->getComponents()->createResponseReference((string)StatusCodeInterface::STATUS_ACCEPTED)
        );
        $responses->addResponse(
            (string)StatusCodeInterface::STATUS_NO_CONTENT,
            $this->oas->getComponents()->createResponseReference((string)StatusCodeInterface::STATUS_NO_CONTENT)
        );
        return $responses;
    }

    /**
     * @return Response
     * @throws OpenAPIException
     */
    private function createEmptyResponse(): Response
    {
        $document = $this->createEmptyDocument();
        $response = new Response('Response when request was successful but there is no resource data to return');
        $response->addContent(
            Document::getMediaType(),
            (new MediaType())->setSchema(
                $document
            )
        );
        return $response;
    }
}
