<?php

/**
 * Created by Lasicka <lasicka@logio.cz>
 * at 30.10.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper;

use BackedEnum;
use DateTime;
use DateTimeInterface;
use JSONAPI\Mapper\Document\Convertible;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use ReflectionClass;
use ReflectionException;
use stdClass;

use function Symfony\Component\String\u;

/**
 * Class TypeScriptInterfaceBuilder
 *
 * @package JSONAPI\Mapper
 */
class TypeScriptInterfaceBuilder
{
    /**
     * @var MetadataRepository $repository
     */
    private MetadataRepository $repository;

    /**
     * @param MetadataRepository $repository
     */
    public function __construct(MetadataRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param class-string<BackedEnum> $type
     * @param string|null $of
     * @return string
     */
    private function convertType(string $type, ?string $of): string
    {
        if (enum_exists($type)) {
            return implode(' | ', array_map(fn($case) => "'" . $case->value . "'", $type::cases()));
        }
        if (is_subclass_of($type, Convertible::class)) {
            $type = 'object';
        }

        return match ($type) {
            'string' => 'string',
            'integer', 'int', 'float', 'double' => 'number',
            'boolean', 'bool' => 'boolean',
            'datetime', DateTimeInterface::class, DateTime::class => 'Date',
            'array' => $this->convertType((string)$of, null) . '[]',
            'object', stdClass::class => '{ [key: string]: any }',
            default => 'any'
        };
    }

    private function memberNameToJson(string $memberName): string
    {
        if (str_contains($memberName, '-')) {
            return "'$memberName'";
        }
        return $memberName;
    }

    /**
     * @return array<string, string>
     * @throws ReflectionException
     */
    private function parentChildrenMap(): array
    {
        $typeMap = [];
        foreach ($this->repository->getAll() as $metadata) {
            $rf = new ReflectionClass($metadata->getClassName());
            $parent = $rf->getParentClass();
            $type = $metadata->getType();
            $typeMap[$type][] = $type;
            if ($parent && $parentMeta = $this->repository->getByClass($parent->getName())) {
                $parentType = $parentMeta->getType();
                $typeMap[$parentType][] = $type;
            }
        }
        foreach ($typeMap as $key => $values) {
            $typeMap[$key] = implode('|', array_map(fn($v) => "'$v'", $values));
        }
        return $typeMap;
    }

    /**
     * @return string
     */
    public function build(): string
    {
        $content = 'import type {Resource, ToOneRelationship, ToManyRelationship, Index} from "@jaspr/client-js";';
        $appendix = 'export default interface ResourceIndex extends Index {' . PHP_EOL;
        $content .= PHP_EOL . PHP_EOL;
        $tab = '    ';
        try {
            $typeMap = $this->parentChildrenMap();
            foreach ($this->repository->getAll() as $metadata) {
                $rf = new ReflectionClass($metadata->getClassName());
                $extends = 'Resource';
                $parent = $rf->getParentClass();
                if ($parent && $this->repository->getByClass($parent->getName())) {
                    $extends = $parent->getShortName();
                }
                $content .= 'export interface ' . $rf->getShortName() . ' extends ' . $extends . ' {';
                $content .= PHP_EOL;
                $content .= $tab . 'type: ' . $typeMap[$metadata->getType()] . ';';
                $content .= PHP_EOL;
                $key = u($metadata->getType())->camel()->toString();
                $appendix .= $tab . $key . ': ' . $rf->getShortName() . ',' . PHP_EOL;
                if (count($metadata->getAttributes()) > 0) {
                    $content .= $tab . 'attributes: {' . PHP_EOL;
                    foreach ($metadata->getAttributes() as $attribute) {
                        $content .= $tab . $tab . $this->memberNameToJson($attribute->name) . ': ';
                        $content .= $this->convertType($attribute->type, $attribute->of) . ';' . PHP_EOL;
                    }
                    $content .= $tab . '}' . PHP_EOL;
                }
                if (count($metadata->getRelationships()) > 0) {
                    $content .= $tab . 'relationships: {' . PHP_EOL;
                    foreach ($metadata->getRelationships() as $relationship) {
                        $rf = new ReflectionClass($relationship->target);
                        $content .= $tab . $tab . $this->memberNameToJson($relationship->name) . ': ';
                        if ($relationship->isCollection) {
                            $content .= 'ToManyRelationship<' . $rf->getShortName() . '>;' . PHP_EOL;
                        } else {
                            $content .= 'ToOneRelationship<' . $rf->getShortName() . '>;' . PHP_EOL;
                        }
                    }
                    $content .= $tab . '}' . PHP_EOL;
                }
                $content .= '}' . PHP_EOL;
                $content .= PHP_EOL;
            }
        } catch (ReflectionException) {
            // Ignored. If class was mapped, it cannot be undefined.
        }
        $appendix .= '}' . PHP_EOL;
        $content .= $appendix;
        return $content;
    }
}
