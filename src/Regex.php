<?php

/**
 * Created by Lasicka <lasicka@logio.cz>
 * at 25.10.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper;

class Regex
{
    public const RFC3986 = <<<REGEXP
/
# protocol user host-ip port path path path querystring fragment
^
#protocol
(?:(?<scheme>[a-zA-Z][a-zA-Z\d+-.]*):)?
(?:
  (?:
    (?:
        \/\/
        (?:
            #userinfo
            (?:((?:[a-zA-Z\d\-._~\!$&'()*+,;=%]*)(?::(?:[a-zA-Z\d\-._~\!$&'()*+,;=:%]*))?)@)?
            #host-ip
            ((?:[a-zA-Z\d\-.%]+)|(?:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(?:\[(?:[a-fA-F\d.:]+)\]))?
            #port
            (?::(\d*))?
        )
    )
    #slash-path
    (
        (?:\/[a-zA-Z\d\-._~\!$&'()*+,;=:@%]*)*
    )
  )
 #slash-path
 |(\/(?:(?:[a-zA-Z\d\-._~\!$&'()*+,;=:@%]+(?:\/[a-zA-Z\d\-._~\!$&'()*+,;=:@%]*)*))?)
 #path
 |([a-zA-Z\d\-._~\!$&'()*+,;=:@%]+(?:\/[a-zA-Z\d\-._~\!$&'()*+,;=:@%]*)*)
)?
#querystring
(?:\?([a-zA-Z\d\-._~\!$&'()*+,;=:@%\/?]*))?
#fragment
(?:\#([a-zA-Z\d\-._~\!$&'()*+,;=:@%\/?]*))?
$/x
REGEXP;
    public const MEMBER_NAME = <<<REGEXP
/^(?![-_])[a-zA-Z0-9][-a-zA-Z0-9_]*[a-zA-Z0-9](?![-_])$/
REGEXP;
    public const PATH_PATTERN = <<<REGEXP
~
^
/(?P<resource>[a-zA-Z0-9-_]+)
(/(?P<id>[^/]+)?
((/relationships/(?P<relationship>[a-zA-Z0-9-_]+))|(/(?P<related>[a-zA-Z0-9-_]+)))?)?
$~x
REGEXP;
}
