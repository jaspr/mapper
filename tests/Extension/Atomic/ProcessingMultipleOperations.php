<?php

/**
 * Created by lasicka@logio.cz
 * at 25.11.2024 21:48
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Extension\Atomic;

use Fig\Http\Message\RequestMethodInterface;
use Fig\Http\Message\StatusCodeInterface;
use JSONAPI\Mapper\Compose\Builder;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Extension\Atomic\AtomicDocumentNamespace;
use JSONAPI\Mapper\Extension\Atomic\Op;
use JSONAPI\Mapper\Extension\Atomic\Operations\CreateResource;
use JSONAPI\Mapper\Middleware\PsrJsonApiMiddleware;
use JSONAPI\Mapper\Test\Resources\Valid\DummyRelation;
use JSONAPI\Mapper\Test\Resources\Valid\PropsExample;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Factory\ServerRequestFactory;

class ProcessingMultipleOperations extends OperationsTestCase
{
    /**
     * @inheritDoc
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var Document $body */
        $body = $request->getParsedBody();
        $this->assertInstanceOf(Document::class, $body);
        /** @var AtomicDocumentNamespace $namespace */
        $namespace = $body->getNamespaces()->get(AtomicDocumentNamespace::class);
        $this->assertInstanceOf(AtomicDocumentNamespace::class, $namespace);
        $operations = $namespace->getOperations();
        $this->assertIsIterable($operations);
        $kvStore = [];
        foreach ($operations as $operation) {
            $this->assertInstanceOf(CreateResource::class, $operation);
            $this->assertNotEmpty($operation->getOp());
            $this->assertEquals(Op::ADD, $operation->getOp());
            $data = $operation->getData();
            if ($data !== null) {
                $this->assertInstanceOf(ResourceObject::class, $data);
                $this->assertNotEmpty($data->getId());
                $this->assertNotEmpty($data->getType());
            }
            $entity = null;
            if ($data->getType() === 'prop') {
                $entity                 = new PropsExample($data->getId());
                $entity->stringProperty = $data->getAttribute('stringProperty')->getData();
                $entity->relation       = $kvStore[$data->getRelationship('relation')->getData()->getId()];
            } elseif ($data->getType() === 'relation') {
                $entity           = new DummyRelation($data->getId());
                $entity->property = $data->getAttribute('property')->getData();
            }
            $kvStore[$entity->id] = $entity;
            $operation->setResult($entity);
        }
        $this->assertInstanceOf(Document::class, $request->getParsedBody());
        $builder = $request->getAttribute(PsrJsonApiMiddleware::BUILDER);
        $this->assertInstanceOf(Builder::class, $builder);
        $document = $builder->setData($operations)->build();
        $factory  = new ResponseFactory();
        $out      = json_encode($document);
        if ($operations->isEmpty()) {
            return $factory->createResponse(StatusCodeInterface::STATUS_NO_CONTENT);
        }
        return $factory->createResponse()->withBody(self::$sf->createStream($out));
    }

    public function testCreateTwoResourcesAndAddRelationshipBetweenThem(): void
    {
        $req      = <<<JSON
{
  "atomic:operations": [{
    "op": "add",
    "data": {
      "type": "relation",
      "id": "acb2ebd6-ed30-4877-80ce-52a14d77d470",
      "attributes": {
        "property": "dgeb"
      }
    }
  }, {
    "op": "add",
    "data": {
      "type": "prop",
      "id": "bb3ad581-806f-4237-b748-f2ea0261845c",
      "attributes": {
        "stringProperty": "JSON API paints my bikeshed!"
      },
      "relationships": {
        "relation": {
          "data": {
            "type": "relation",
            "id": "acb2ebd6-ed30-4877-80ce-52a14d77d470"
          }
        }
      }
    }
  }]
}
JSON;
        $res      = <<<JSON
{
    "jsonapi": {
        "version": "1.1",
        "profile": [],
        "ext": [
            "https:\/\/jsonapi.org\/ext\/atomic"
        ]
    },
    "atomic:results": [
        {
            "data": {
                "type": "relation",
                "id": "acb2ebd6-ed30-4877-80ce-52a14d77d470",
                "attributes": {
                    "property": "dgeb"
                },
                "relationships": {
                    "example": {
                        "data": {
                            "type": "third",
                            "id": "ex1"
                        },
                        "links": {
                            "self": "https:\/\/unit.test.org\/relation\/acb2ebd6-ed30-4877-80ce-52a14d77d470\/relationships\/example",
                            "related": "https:\/\/unit.test.org\/relation\/acb2ebd6-ed30-4877-80ce-52a14d77d470\/example"
                        }
                    }
                },
                "links": {
                    "self": "https:\/\/unit.test.org\/relation\/acb2ebd6-ed30-4877-80ce-52a14d77d470"
                }
            }
        },
        {
            "data": {
                "type": "prop",
                "id": "bb3ad581-806f-4237-b748-f2ea0261845c",
                "attributes": {
                    "stringProperty": "JSON API paints my bikeshed!",
                    "intProperty": 1,
                    "arrayProperty": [
                        1,
                        2,
                        3
                    ],
                    "boolProperty": true,
                    "dtoProperty": {
                        "word": "string-value",
                        "number": 1234,
                        "boolean": true
                    }
                },
                "relationships": {
                    "relation": {
                        "data": {
                            "type": "relation",
                            "id": "acb2ebd6-ed30-4877-80ce-52a14d77d470"
                        },
                        "links": {
                            "self": "https:\/\/unit.test.org\/prop\/bb3ad581-806f-4237-b748-f2ea0261845c\/relationships\/relation",
                            "related": "https:\/\/unit.test.org\/prop\/bb3ad581-806f-4237-b748-f2ea0261845c\/relation"
                        }
                    },
                    "collection": {
                        "data": [
                            {
                                "type": "relation",
                                "id": "relation2"
                            },
                            {
                                "type": "relation",
                                "id": "relation3"
                            }
                        ],
                        "links": {
                            "self": "https:\/\/unit.test.org\/prop\/bb3ad581-806f-4237-b748-f2ea0261845c\/relationships\/collection",
                            "related": "https:\/\/unit.test.org\/prop\/bb3ad581-806f-4237-b748-f2ea0261845c\/collection"
                        }
                    }
                },
                "links": {
                    "self": "https:\/\/unit.test.org\/prop\/bb3ad581-806f-4237-b748-f2ea0261845c"
                }
            }
        }
    ]
}
JSON;
        $rf       = new ServerRequestFactory();
        $request  = $rf->createServerRequest(RequestMethodInterface::METHOD_POST, 'https://unit.test.org/')
            ->withHeader('Content-Type', self::$mt)
            ->withHeader('Accept', self::$mt)
            ->withBody(self::$sf->createStream($req));
        $response = self::$mw->process($request, $this);
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $body   = $response->getBody()->getContents();
        $status = $response->getStatusCode();
        $this->assertEquals(StatusCodeInterface::STATUS_OK, $status);
//        $this->assertEquals($res, $body);
        $contentTypes = $response->getHeader('Content-Type');
        $this->assertEquals(self::$mt, $contentTypes);
    }
}
