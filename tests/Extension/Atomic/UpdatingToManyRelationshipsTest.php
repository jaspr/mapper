<?php

/**
 * Created by lasicka@logio.cz
 * at 08.11.2024 11:13
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Extension\Atomic;

use Fig\Http\Message\RequestMethodInterface;
use Fig\Http\Message\StatusCodeInterface;
use JSONAPI\Mapper\Compose\Builder;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\ResourceObjectIdentifier;
use JSONAPI\Mapper\Extension\Atomic\AtomicDocumentNamespace;
use JSONAPI\Mapper\Extension\Atomic\Op;
use JSONAPI\Mapper\Extension\Atomic\Operations\CreateRelationship;
use JSONAPI\Mapper\Extension\Atomic\Operations\RemoveRelationship;
use JSONAPI\Mapper\Extension\Atomic\Operations\UpdateRelationship;
use JSONAPI\Mapper\Middleware\PsrJsonApiMiddleware;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Factory\ServerRequestFactory;

class UpdatingToManyRelationshipsTest extends OperationsTestCase
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var Document $body */
        $body = $request->getParsedBody();
        $this->assertInstanceOf(Document::class, $body);
        /** @var AtomicDocumentNamespace $namespace */
        $namespace = $body->getNamespaces()->get(AtomicDocumentNamespace::class);
        $this->assertInstanceOf(AtomicDocumentNamespace::class, $namespace);
        $operations = $namespace->getOperations();
        $this->assertIsIterable($operations);
        $this->assertCount(1, $operations);
        foreach ($operations as $operation) {
            $this->assertTrue(
                is_a($operation, UpdateRelationship::class)
                || is_a($operation, CreateRelationship::class)
                || is_a($operation, RemoveRelationship::class)
            );
            $this->assertNotEmpty($operation->getOp());
            $this->assertNotEmpty($operation->getRef());
            $this->assertNotEmpty($operation->getRef()->getId());
            $this->assertNotEmpty($operation->getRef()->getType());
            $this->assertNotEmpty($operation->getData());
            $this->assertTrue(is_iterable($operation->getData()));
            if ($operation instanceof UpdateRelationship) {
                $this->assertEquals(Op::UPDATE, $operation->getOp());
            }
            if ($operation instanceof CreateRelationship) {
                $this->assertEquals(Op::ADD, $operation->getOp());
            }
            if ($operation instanceof RemoveRelationship) {
                $this->assertEquals(Op::REMOVE, $operation->getOp());
            }
            $data = $operation->getData();
            foreach ($data as $item) {
                $this->assertInstanceOf(ResourceObjectIdentifier::class, $item);
            }
        }
        $this->assertInstanceOf(Document::class, $request->getParsedBody());
        $builder = $request->getAttribute(PsrJsonApiMiddleware::BUILDER);
        $this->assertInstanceOf(Builder::class, $builder);
        $document = $builder->setData($operations)->build();
        $factory  = new ResponseFactory();
        $out      = json_encode($document);
        if ($operations->isEmpty()) {
            return $factory->createResponse(StatusCodeInterface::STATUS_NO_CONTENT);
        }
        return $factory->createResponse()->withBody(self::$sf->createStream($out));
    }

    public function testReplaceRelationships()
    {
        $req = <<<JSON
{
  "atomic:operations": [{
    "op": "update",
    "ref": {
      "type": "prop",
      "id": "1",
      "relationship": "collection"
    },
    "data": [
      { "type": "relation", "id": "2" },
      { "type": "relation", "id": "3" }
    ]
  }]
}
JSON;

        $rf       = new ServerRequestFactory();
        $request  = $rf->createServerRequest(RequestMethodInterface::METHOD_POST, 'https://unit.test.org/')
            ->withHeader('Content-Type', self::$mt)
            ->withHeader('Accept', self::$mt)
            ->withBody(self::$sf->createStream($req));
        $response = self::$mw->process($request, $this);
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $body   = json_decode($response->getBody()->getContents());
        $status = $response->getStatusCode();
        $this->assertEquals(StatusCodeInterface::STATUS_NO_CONTENT, $status);
        $this->assertEmpty($body);
        $contentTypes = $response->getHeader('Content-Type');
        $this->assertEquals(self::$mt, $contentTypes);
    }

    public function testAddRelationships()
    {
        $req = <<<JSON
{
  "atomic:operations": [{
    "op": "add",
    "ref": {
      "type": "prop",
      "id": "1",
      "relationship": "collection"
    },
    "data": [
      { "type": "relation", "id": "123" }
    ]
  }]
}
JSON;

        $rf       = new ServerRequestFactory();
        $request  = $rf->createServerRequest(RequestMethodInterface::METHOD_POST, 'https://unit.test.org/')
            ->withHeader('Content-Type', self::$mt)
            ->withHeader('Accept', self::$mt)
            ->withBody(self::$sf->createStream($req));
        $response = self::$mw->process($request, $this);
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $body   = json_decode($response->getBody()->getContents());
        $status = $response->getStatusCode();
        $this->assertEquals(StatusCodeInterface::STATUS_NO_CONTENT, $status);
        $this->assertEmpty($body);
        $contentTypes = $response->getHeader('Content-Type');
        $this->assertEquals(self::$mt, $contentTypes);
    }

    public function testRemoveRelationships()
    {
        $req = <<<JSON
{
  "atomic:operations": [{
    "op": "remove",
    "ref": {
      "type": "prop",
      "id": "1",
      "relationship": "collection"
    },
    "data": [
      { "type": "relation", "id": "12" },
      { "type": "relation", "id": "13" }
    ]
  }]
}
JSON;

        $rf       = new ServerRequestFactory();
        $request  = $rf->createServerRequest(RequestMethodInterface::METHOD_POST, 'https://unit.test.org/')
            ->withHeader('Content-Type', self::$mt)
            ->withHeader('Accept', self::$mt)
            ->withBody(self::$sf->createStream($req));
        $response = self::$mw->process($request, $this);
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $body   = json_decode($response->getBody()->getContents());
        $status = $response->getStatusCode();
        $this->assertEquals(StatusCodeInterface::STATUS_NO_CONTENT, $status);
        $this->assertEmpty($body);
        $contentTypes = $response->getHeader('Content-Type');
        $this->assertEquals(self::$mt, $contentTypes);
    }
}
