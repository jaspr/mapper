<?php

/**
 * Created by lasicka@logio.cz
 * at 08.09.2024 22:01
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Extension\Atomic;

use Fig\Http\Message\RequestMethodInterface;
use Fig\Http\Message\StatusCodeInterface;
use JSONAPI\Mapper\Compose\Builder;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Encoding\EncoderFactory;
use JSONAPI\Mapper\Extension\Atomic\AtomicDefinition;
use JSONAPI\Mapper\Extension\Atomic\AtomicDocumentNamespace;
use JSONAPI\Mapper\Extension\Atomic\Operations\Operation;
use JSONAPI\Mapper\Extension\Atomic\Operations\OperationWithResult;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Middleware\PsrJsonApiMiddleware;
use JSONAPI\Mapper\Request\Parser;
use JSONAPI\Mapper\Test\Resources\Valid\PropsExample;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Factory\ServerRequestFactory;
use Slim\Psr7\Factory\StreamFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class MiddlewareTest extends TestCase implements RequestHandlerInterface
{
    private static PsrJsonApiMiddleware $mw;
    private static StreamFactory $sf;
    private static array $mt;

    public static function setUpBeforeClass(): void
    {
        $rf       = new ResponseFactory();
        $sf       = new StreamFactory();
        $mr       = MetadataFactory::create(
            [__DIR__ . '/../../Resources/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        $rp       = Parser::createDefault('https://unit.test.org', $mr);
        $encoder  = EncoderFactory::createRequestDependentEncoder($mr);
        self::$mw = new PsrJsonApiMiddleware($rf, $sf, $rp, $encoder);
        self::$mw->addExtension(new AtomicDefinition($mr, $encoder));
        self::$sf = new StreamFactory();
        self::$mt = ['application/vnd.api+json', 'ext="https://jsonapi.org/ext/atomic"'];
    }


    public function testCaseUpdateResource()
    {
        $req      = <<<JSON
{
    "atomic:operations": [{
        "op": "update",
        "data": {
            "type": "prop",
            "id": "a1b2c3",
            "attributes": {
                "stringProperty": "JSON API paints my bikeshed!"
            }
        }
    }]
}
JSON;
        $rf       = new ServerRequestFactory();
        $request  = $rf->createServerRequest(RequestMethodInterface::METHOD_POST, 'https://unit.test.org/')
            ->withHeader('Content-Type', self::$mt)
            ->withHeader('Accept', self::$mt)
            ->withBody(self::$sf->createStream($req));
        $response = self::$mw->process($request, $this);
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $body   = json_decode($response->getBody()->getContents());
        $status = $response->getStatusCode();
        $this->assertEquals(StatusCodeInterface::STATUS_OK, $status);
        $this->assertIsObject($body);
        $key = 'atomic:results';
        $this->assertObjectHasProperty($key, $body);
        $results = $body->$key;
        $this->assertIsArray($results);
        $this->assertCount(1, $results);
        $result = $results[0];
        $this->assertObjectHasProperty('data', $result);
        $data = $result->data;
        $this->assertObjectHasProperty('type', $data);
        $this->assertObjectHasProperty('id', $data);
        $this->assertEquals("JSON API paints my bikeshed!", $data->attributes->stringProperty);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var Document $body */
        $body = $request->getParsedBody();
        $this->assertInstanceOf(Document::class, $body);
        /** @var AtomicDocumentNamespace $namespace */
        $namespace = $body->getNamespaces()->get(AtomicDocumentNamespace::class);
        $this->assertInstanceOf(AtomicDocumentNamespace::class, $namespace);
        $operations = $namespace->getOperations();
        $this->assertIsIterable($operations);
        $this->assertCount(1, $operations);
        foreach ($operations as $operation) {
            $this->assertInstanceOf(Operation::class, $operation);
            $this->assertNotEmpty($operation->getOp());
            if ($operation instanceof OperationWithResult) {
                $result = new PropsExample('a1b2c3');
                /** @var ResourceObject $resourceObject */
                $resourceObject         = $operation->getData();
                $result->stringProperty = $resourceObject->getAttribute('stringProperty')->getData();
                $operation->setResult($result);
            }
        }
        $this->assertInstanceOf(Document::class, $request->getParsedBody());
        $builder = $request->getAttribute(PsrJsonApiMiddleware::BUILDER);
        $this->assertInstanceOf(Builder::class, $builder);
        $document = $builder->setData($operations)->build();
        $factory  = new ResponseFactory();
        $out      = json_encode($document);
        return $factory->createResponse()->withBody(self::$sf->createStream($out));
    }
}
