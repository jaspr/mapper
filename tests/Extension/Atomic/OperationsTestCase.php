<?php

/**
 * Created by lasicka@logio.cz
 * at 08.11.2024 11:13
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Extension\Atomic;

use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Encoding\EncoderFactory;
use JSONAPI\Mapper\Extension\Atomic\AtomicDefinition;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Middleware\PsrJsonApiMiddleware;
use JSONAPI\Mapper\Request\Parser;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Factory\StreamFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

abstract class OperationsTestCase extends TestCase implements RequestHandlerInterface
{
    protected static PsrJsonApiMiddleware $mw;
    protected static StreamFactory $sf;
    protected static array $mt;

    public static function setUpBeforeClass(): void
    {
        $rf       = new ResponseFactory();
        $sf       = new StreamFactory();
        $mr       = MetadataFactory::create(
            [__DIR__ . '/../../Resources/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        $rp       = Parser::createDefault('https://unit.test.org', $mr);
        $encoder  = EncoderFactory::createRequestDependentEncoder($mr);
        self::$mw = new PsrJsonApiMiddleware($rf, $sf, $rp, $encoder);
        self::$mw->addExtension(new AtomicDefinition($mr, $encoder));
        self::$sf = new StreamFactory();
        self::$mt = ['application/vnd.api+json', 'ext="https://jsonapi.org/ext/atomic"'];
    }

    /**
     * @inheritDoc
     */
    abstract public function handle(ServerRequestInterface $request): ResponseInterface;
}
