<?php

/**
 * Created by @bednic
 * at 08.08.2024 23:13
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Extension\Atomic;

use Fig\Http\Message\RequestMethodInterface;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Encoding\EncoderFactory;
use JSONAPI\Mapper\Extension\Atomic\AtomicDocumentNamespace;
use JSONAPI\Mapper\Extension\Atomic\Op;
use JSONAPI\Mapper\Extension\Atomic\Operations\Operation;
use JSONAPI\Mapper\Extension\Atomic\Parser\AtomicBodyParser;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;
use Slim\Psr7\Factory\StreamFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class BodyParserTest extends TestCase
{
    private static MetadataRepository $mr;

    public static function setUpBeforeClass(): void
    {
        self::$mr = MetadataFactory::create(
            [__DIR__ . '/../../Resources/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
    }

    public function testParse()
    {
        $body = <<<JSON
{
  "atomic:operations": [{
    "op": "add",
    "data": {
      "type": "relation",
      "id": "acb2ebd6-ed30-4877-80ce-52a14d77d470",
      "attributes": {
        "property": "dgeb"
      }
    }
  }, {
    "op": "add",
    "data": {
      "type": "getter",
      "id": "bb3ad581-806f-4237-b748-f2ea0261845c",
      "attributes": {
        "stringProperty": "JSON API paints my bikeshed!"
      },
      "relationships": {
        "relation": {
          "data": {
            "type": "relation",
            "id": "acb2ebd6-ed30-4877-80ce-52a14d77d470"
          }
        }
      }
    }
  }]
}
JSON;


        $sf        = new StreamFactory();
        $stream    = $sf->createStream($body);
        $rf        = new ServerRequestFactory();
        $mediaType = 'application/vnd.api+json;ext="https://jsonapi.org/ext/atomic"';
        $request   = $rf
            ->createServerRequest(RequestMethodInterface::METHOD_POST, 'https://unit.test.org')
            ->withAddedHeader('Content-Type', $mediaType)
            ->withAddedHeader('Accept', $mediaType)
            ->withBody($stream);
        $encoder   = EncoderFactory::createRequestDependentEncoder(self::$mr);
        $parser    = new AtomicBodyParser(self::$mr, $encoder);
        $document  = $parser->parse($request);
        /** @var AtomicDocumentNamespace $namespace */
        $namespace  = $document->getNamespaces()->get(AtomicDocumentNamespace::class);
        $operations = $namespace->getOperations();
        $this->assertCount(2, $operations);
        foreach ($operations as $operation) {
            $this->assertInstanceOf(Operation::class, $operation);
            $this->assertInstanceOf(Op::class, $operation->getOp());
            $this->assertInstanceOf(ResourceObject::class, $operation->getData());
        }
    }
}
