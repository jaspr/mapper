<?php

/**
 * Created by lasicka@logio.cz
 * at 08.11.2024 11:13
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Extension\Atomic;

use Fig\Http\Message\RequestMethodInterface;
use Fig\Http\Message\StatusCodeInterface;
use JSONAPI\Mapper\Compose\Builder;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Extension\Atomic\AtomicDocumentNamespace;
use JSONAPI\Mapper\Extension\Atomic\Op;
use JSONAPI\Mapper\Extension\Atomic\Operations\UpdateResource;
use JSONAPI\Mapper\Middleware\PsrJsonApiMiddleware;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Factory\ServerRequestFactory;

class UpdatingResourcesTest extends OperationsTestCase
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var Document $body */
        $body = $request->getParsedBody();
        $this->assertInstanceOf(Document::class, $body);
        /** @var AtomicDocumentNamespace $namespace */
        $namespace = $body->getNamespaces()->get(AtomicDocumentNamespace::class);
        $this->assertInstanceOf(AtomicDocumentNamespace::class, $namespace);
        $operations = $namespace->getOperations();
        $this->assertIsIterable($operations);
        $this->assertCount(1, $operations);
        foreach ($operations as $operation) {
            $this->assertInstanceOf(UpdateResource::class, $operation);
            $this->assertNotEmpty($operation->getOp());
            $this->assertEquals(Op::UPDATE, $operation->getOp());
            $resourceObject = $operation->getData();
            $this->assertEquals('a1b2c3', $resourceObject->getId());
        }
        $this->assertInstanceOf(Document::class, $request->getParsedBody());
        $builder = $request->getAttribute(PsrJsonApiMiddleware::BUILDER);
        $this->assertInstanceOf(Builder::class, $builder);
        $document = $builder->setData($operations)->build();
        $factory  = new ResponseFactory();
        $out      = json_encode($document);
        if ($operations->isEmpty()) {
            return $factory->createResponse(StatusCodeInterface::STATUS_NO_CONTENT);
        }
        return $factory->createResponse()->withBody(self::$sf->createStream($out));
    }

    public function testUpdateResource()
    {
        $req = <<<JSON
{
  "atomic:operations": [{
    "op": "update",
    "data": {
      "type": "prop",
      "id": "a1b2c3",
      "attributes": {
        "stringProperty": "To TDD or Not"
      }
    }
  }]
}
JSON;

        $rf       = new ServerRequestFactory();
        $request  = $rf->createServerRequest(RequestMethodInterface::METHOD_POST, 'https://unit.test.org/')
            ->withHeader('Content-Type', self::$mt)
            ->withHeader('Accept', self::$mt)
            ->withBody(self::$sf->createStream($req));
        $response = self::$mw->process($request, $this);
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $body   = json_decode($response->getBody()->getContents());
        $status = $response->getStatusCode();
        $this->assertEquals(StatusCodeInterface::STATUS_NO_CONTENT, $status);
        $this->assertEmpty($body);
//        $key = 'atomic:results';
//        $this->assertObjectHasProperty($key, $body);
//        $results = $body->$key;
//        $this->assertIsArray($results);
//        $this->assertCount(1, $results);
//        $result = $results[0];
//        $this->assertObjectNotHasProperty('data', $result);
//        $data = $result->data;
//        $this->assertObjectHasProperty('type', $data);
//        $this->assertObjectHasProperty('id', $data);
//        $this->assertObjectHasProperty('attributes', $data);
        $contentTypes = $response->getHeader('Content-Type');
        $this->assertEquals(self::$mt, $contentTypes);
    }
}
