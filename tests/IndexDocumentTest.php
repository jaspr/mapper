<?php

/**
 * Created by tomas
 * at 20.03.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test;

use JSONAPI\Mapper\Driver\SchemaDriver;
use JSONAPI\Mapper\IndexDocument;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class IndexDocumentTest extends TestCase
{
    private static MetadataRepository $mr;
    private static string $url;

    public static function setupBeforeClass(): void
    {
        self::$mr  = MetadataFactory::create(
            [__DIR__ . '/Resources/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new SchemaDriver()
        );
        self::$url = 'https://unit.test.org/';
    }

    public function testResponse()
    {
        $doc      = new IndexDocument(self::$mr, self::$url);
        $response = json_encode($doc);
        $this->assertJsonStringEqualsJsonFile(__DIR__ . '/index.json', $response);
    }
}
