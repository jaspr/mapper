<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test;

use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\OpenAPISpecificationBuilder;
use JSONAPI\OAS\Contact;
use JSONAPI\OAS\ExternalDocumentation;
use JSONAPI\OAS\Info;
use JSONAPI\OAS\License;
use JSONAPI\OAS\OpenAPISpecification;
use PHPUnit\Framework\TestCase;
use Swaggest\JsonSchema\Schema;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

/**
 * Class OpenAPISpecificationBuilderTest
 *
 * @package JSONAPI\Test\OAS\Factory
 */
class OpenAPISpecificationBuilderTest extends TestCase
{
    public function testCreate()
    {
        $validator     = Schema::import(json_decode(file_get_contents(__DIR__ . '/openapi-v3.0.json')));
        $mr                  = MetadataFactory::create(
            [RESOURCES . '/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        $baseUrl             = 'https://unit.test.org';
        $factory = new OpenAPISpecificationBuilder($mr, $baseUrl);

        $info = new Info('JSON:API OAS', '1.0.0');
        $info->setDescription('Test specification');
        $info->setContact(
            (new Contact())
                ->setName('Tomas Benedikt')
                ->setEmail('tomas.benedikt@gmail.com')
                ->setUrl('https://gitlab.com/bednic')
        );
        $info->setLicense(
            (new License('MIT'))
                ->setUrl('https://gitlab.com/bednic/json-api/-/blob/5.x/LICENSE')
        );
        $info->setTermsOfService('https://gitlab.com/bednic/json-api/-/blob/5.x/CONTRIBUTING.md');

        $oas = $factory->create($info);
        $oas->setExternalDocs(new ExternalDocumentation('https://gitlab.com/bednic/json-api/-/wikis/home'));

        $json = json_encode($oas);

        $this->assertIsString($json);

        $validator->in(json_decode($json));

        $this->assertInstanceOf(OpenAPISpecification::class, $oas);
    }
}
