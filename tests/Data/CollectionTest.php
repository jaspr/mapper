<?php

/**
 * Created by tomas
 * at 23.01.2021 22:28
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Data;

use JSONAPI\Mapper\ObjectCollection;
use PHPUnit\Framework\TestCase;
use stdClass;
use Traversable;

class CollectionTest extends TestCase
{
    public static function dummyData()
    {
        $stds = [];
        $props = [];
        $getters = [];
        for ($i = 0; $i < 10; $i++) {
            $item = new stdClass();
            $item->id = $i;
            $item->value = "value" . ($i % 2);
            $stds[] = $item;
            $props[] = new class ($i, "value" . ($i % 2)) {
                public function __construct(public int $id, public string $value)
                {
                }
            };
            $getters[] = new class ($i, "value" . ($i % 2)) {
                public function __construct(private readonly int $id, private readonly string $value)
                {
                }

                /**
                 * @return int
                 */
                public function getId(): int
                {
                    return $this->id;
                }

                /**
                 * @return string
                 */
                public function getValue(): string
                {
                    return $this->value;
                }
            };
        }
        return [
            [$stds, $props, $getters]
        ];
    }

    public function testGetIterator()
    {
        $collection = new ObjectCollection();
        $this->assertInstanceOf(Traversable::class, $collection->getIterator());
    }

    /**
     * @dataProvider dummyData
     */
    public function testSlice($items)
    {
        $collection = new ObjectCollection($items);
        $this->assertEquals(5, $collection->slice(0, 5)->count());
        $this->assertEquals(0, $collection->slice(99, 1)->count());
        $abstract = new ObjectCollection($items, 100);
        $this->assertCount(100, $abstract);
        $this->assertEquals(20, $abstract->slice(0, 20)->count());
        $this->assertEquals(2, $abstract->slice(0, 2)->count());
    }

    /**
     * @dataProvider dummyData
     */
    public function testValues($items)
    {
        $collection = new ObjectCollection($items);
        $this->assertIsArray($collection->values());
        $this->assertCount(10, $collection->values());
    }

    /**
     * @dataProvider dummyData
     */
    public function testHas($items)
    {
        $item = new stdClass();
        $item->id = 0;
        $item->value = 'value0';
        $collection = new ObjectCollection($items);
        $this->assertFalse($collection->has($item));
    }

    /**
     * @dataProvider dummyData
     */
    public function testFilter($items)
    {
        $collection = new ObjectCollection($items);
        $filter = function ($item) {
            return $item->id < 5;
        };
        $this->assertCount(5, $collection->filter($filter));
    }

    /**
     * @dataProvider dummyData
     */
    public function testCount($items)
    {
        $collection = new ObjectCollection($items);
        $this->assertEquals(10, $collection->count());
    }

    public function testConstruct()
    {
        $collection = new ObjectCollection();
        $this->assertInstanceOf(ObjectCollection::class, $collection);
        $collection = new ObjectCollection([]);
        $this->assertInstanceOf(ObjectCollection::class, $collection);
    }

    /**
     * @dataProvider dummyData
     */
    public function testSort($items, $props, $getters)
    {
        $collection = new ObjectCollection($items);
        $collection->sort(
            [
                'value' => ObjectCollection::SORT_ASC,
                'id' => ObjectCollection::SORT_DESC
            ]
        );
        $objects = $collection->values();

        $first = array_shift($objects);
        $this->assertTrue($first->id === 8);
        $this->assertTrue($first->value === 'value0');
        $last = array_pop($objects);
        $this->assertTrue($last->id === 1);
        $this->assertTrue($last->value === 'value1');


        $collection = new ObjectCollection($props);
        $collection->sort(
            [
                'value' => ObjectCollection::SORT_ASC,
                'id' => ObjectCollection::SORT_DESC
            ]
        );
        $objects = $collection->values();

        $first = array_shift($objects);
        $this->assertTrue($first->id === 8);
        $this->assertTrue($first->value === 'value0');
        $last = array_pop($objects);
        $this->assertTrue($last->id === 1);
        $this->assertTrue($last->value === 'value1');

        $collection = new ObjectCollection($getters);
        $collection->sort(
            [
                'value' => ObjectCollection::SORT_ASC,
                'id' => ObjectCollection::SORT_DESC
            ]
        );
        $objects = $collection->values();

        $first = array_shift($objects);
        $this->assertTrue($first->getId() === 8);
        $this->assertTrue($first->getValue() === 'value0');
        $last = array_pop($objects);
        $this->assertTrue($last->getId() === 1);
        $this->assertTrue($last->getValue() === 'value1');
    }
}
