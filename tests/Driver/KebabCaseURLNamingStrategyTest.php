<?php

/**
 * Created by tomas
 * at 16.06.2024
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Driver;

use JSONAPI\Mapper\Driver\KebabCaseURLNamingStrategy;
use PHPUnit\Framework\TestCase;

class KebabCaseURLNamingStrategyTest extends TestCase
{
    public function testRelationshipNameFromURI()
    {
        $s = new KebabCaseURLNamingStrategy();
        $this->assertEquals('someRelation', $s->relationshipNameFromURI('some-relation'));
    }

    public function testClassNameToResourceType()
    {
        $s = new KebabCaseURLNamingStrategy();
        $this->assertEquals('test-resources', $s->classNameToResourceType('TestResource'));
    }

    public function testRelationshipNameToURI()
    {
        $s = new KebabCaseURLNamingStrategy();
        $this->assertEquals('some-relation', $s->relationshipNameToURI('someRelation'));
    }

    public function testReflectionNameToMemberName()
    {
        $s = new KebabCaseURLNamingStrategy();
        $this->assertEquals('someRelation', $s->reflectionNameToMemberName('some_relation'));
    }
}
