<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Driver;

use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Driver\RecommendedNamingStrategy;
use JSONAPI\Mapper\Metadata\Attribute;
use JSONAPI\Mapper\Metadata\Id;
use JSONAPI\Mapper\Metadata\Meta;
use JSONAPI\Mapper\Metadata\Relationship;
use JSONAPI\Mapper\Test\Resources\Invalid\BadAnnotationPlacement;
use JSONAPI\Mapper\Test\Resources\Invalid\BadRelationshipGetter;
use JSONAPI\Mapper\Test\Resources\Invalid\MethodDoesNotExist;
use JSONAPI\Mapper\Test\Resources\Invalid\NotResource;
use JSONAPI\Mapper\Test\Resources\Invalid\WithBadMethodSignature;
use JSONAPI\Mapper\Test\Resources\Valid\DtoValue;
use JSONAPI\Mapper\Test\Resources\Valid\GettersExample;
use JSONAPI\Mapper\Test\Resources\Valid\MetaExample;
use JSONAPI\Mapper\Test\Resources\Valid\PropsExample;
use PHPUnit\Framework\TestCase;

/**
 * Class AnnotationDriverTest
 *
 * @package JSONAPI\Test
 */
class AnnotationDriverTest extends TestCase
{
    public function testConstruct()
    {
        $driver = new AnnotationDriver();
        $this->assertInstanceOf(AnnotationDriver::class, $driver);
        $driver = new AnnotationDriver(new RecommendedNamingStrategy());
        $this->assertInstanceOf(AnnotationDriver::class, $driver);
        return $driver;
    }

    public function testMetaAnnotation()
    {
        $driver = new AnnotationDriver();
        $resource = new MetaExample('test');
        $metadata = $driver->getClassMetadata(get_class($resource));
        $this->assertInstanceOf(Meta::class, $metadata->getMeta());
        $this->assertEquals('getMeta', $metadata->getMeta()->getter);
        $this->assertInstanceOf(Meta::class, $metadata->getRelationship('relation')->meta);
        $this->assertEquals('getRelationMeta', $metadata->getRelationship('relation')->meta->getter);
    }

    /**
     * @dataProvider classProvider
     */
    public function testGetClassMetadata($instance)
    {
        $driver = new AnnotationDriver();
        $metadata = $driver->getClassMetadata(get_class($instance));
        $this->assertMatchesRegularExpression('/[a-zA-Z0-9]+/', $metadata->getType());
        $this->assertNotEmpty($metadata->getClassName());
        $this->assertInstanceOf(Id::class, $metadata->getId());
        $this->assertIsString($metadata->getType());
        $this->assertInstanceOf(Attribute::class, $metadata->getAttribute('stringProperty'));
        $this->assertEquals('string', $metadata->getAttribute('stringProperty')->type);
        $this->assertInstanceOf(Attribute::class, $metadata->getAttribute('intProperty'));
        $this->assertEquals('int', $metadata->getAttribute('intProperty')->type);
        $this->assertInstanceOf(Attribute::class, $metadata->getAttribute('arrayProperty'));
        $this->assertEquals('array', $metadata->getAttribute('arrayProperty')->type);
        $this->assertEquals('int', $metadata->getAttribute('arrayProperty')->of);
        $this->assertInstanceOf(Attribute::class, $metadata->getAttribute('boolProperty'));
        $this->assertEquals('bool', $metadata->getAttribute('boolProperty')->type);
        $this->assertInstanceOf(Attribute::class, $metadata->getAttribute('dtoProperty'));
        $this->assertEquals(DtoValue::class, $metadata->getAttribute('dtoProperty')->type);
        $this->assertInstanceOf(Relationship::class, $metadata->getRelationship('relation'));
        $this->assertInstanceOf(Relationship::class, $metadata->getRelationship('collection'));
    }

    public function testDoctrineCollectionAdapter()
    {
        $driver = new AnnotationDriver();
        $metadata = $driver->getClassMetadata(GettersExample::class);
        $this->assertTrue($metadata->getRelationship('doctrineCollection')->isCollection);
    }

    public static function classProvider()
    {
        return [
            [new PropsExample('test')],
            [new GettersExample('test')]
        ];
    }

    public function testBadAnnotationPlacement()
    {
        $this->expectException(\InvalidArgumentException::class);
        $driver = new AnnotationDriver();
        $driver->getClassMetadata(get_class(new BadAnnotationPlacement()));
    }

    public function testMethodBadSignature()
    {
        $this->expectException(\DomainException::class);
        $driver = new AnnotationDriver();
        $driver->getClassMetadata(WithBadMethodSignature::class);
    }

    public function testBadRelationshipGetter()
    {
        $this->expectException(\DomainException::class);
        $driver = new AnnotationDriver();
        $driver->getClassMetadata(BadRelationshipGetter::class);
    }

    public function testClassNotExists()
    {
        $driver = new AnnotationDriver();
        $this->assertNull($driver->getClassMetadata('NonExistingClass'));
    }

    public function testClassNotResource()
    {
        $driver = new AnnotationDriver();
        $this->assertNull($driver->getClassMetadata(get_class(new NotResource())));
    }

    public function testMethodDoesNotExist()
    {
        $this->expectException(\LogicException::class);
        $driver = new AnnotationDriver();
        $driver->getClassMetadata(MethodDoesNotExist::class);
    }
}
