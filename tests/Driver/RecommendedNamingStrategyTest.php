<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Driver;

use JSONAPI\Mapper\Driver\RecommendedNamingStrategy;
use PHPUnit\Framework\TestCase;

class RecommendedNamingStrategyTest extends TestCase
{
    private string $className = 'TestResource';
    private string $typeField = 'testResources';
    private string $relationshipName = 'some_Relation';
    private string $relationshipField = 'someRelation';

    public function testClassNameToResourceType()
    {
        $strategy = new RecommendedNamingStrategy();
        $this->assertEquals($this->typeField, $strategy->classNameToResourceType($this->className));
    }

    public function testClassPropertyToMemberName()
    {
        $strategy = new RecommendedNamingStrategy();
        $this->assertEquals($this->relationshipField, $strategy->reflectionNameToMemberName($this->relationshipName));
    }
}
