<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Document;

use ArrayIterator;
use JSONAPI\Mapper\Annotation\Resource;
use JSONAPI\Mapper\Document\Id;
use JSONAPI\Mapper\Document\ResourceCollection;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\Type;
use PHPUnit\Framework\TestCase;

class ResourceCollectionTest extends TestCase
{
    public function testConstruct()
    {
        $collection = new ResourceCollection();
        $this->assertInstanceOf(ResourceCollection::class, $collection);
        $collection = new ResourceCollection([]);
        $this->assertInstanceOf(ResourceCollection::class, $collection);
        $collection = new ResourceCollection(
            [
                new ResourceObject(new Type('resources'), new Id('id1'))
            ]
        );
        $this->assertInstanceOf(ResourceCollection::class, $collection);
    }


    public function testRemove()
    {
        $resource = new ResourceObject(new Type('type'), new Id('1'));
        $collection = new ResourceCollection();
        $collection->add($resource);
        $this->assertTrue($collection->has($resource));
        $this->assertTrue($collection->remove($resource));
        $this->assertFalse($collection->remove($resource));
    }

    public function testHas()
    {
        $resource = new ResourceObject(new Type('type'), new Id('1'));
        $nonexist = new ResourceObject(new Type('type'), new Id('2'));
        $collection = new ResourceCollection();
        $collection->add($resource);
        $this->assertTrue($collection->has($resource));
        $this->assertFalse($collection->has($nonexist));
    }

    public function testGetIterator()
    {
        $collection = new ResourceCollection();
        $this->assertInstanceOf(ArrayIterator::class, $collection->getIterator());
    }
}
