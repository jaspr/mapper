<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Document;

use JSONAPI\Mapper\Document\DefaultNamespace;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\Error;
use JSONAPI\Mapper\Document\Id;
use JSONAPI\Mapper\Document\Meta;
use JSONAPI\Mapper\Document\PrimaryData;
use JSONAPI\Mapper\Document\ResourceCollection;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\Type;
use JsonSerializable;
use PHPUnit\Framework\TestCase;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\SchemaContract;

class DocumentTest extends TestCase
{
    private static SchemaContract $schema;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$schema = Schema::import(json_decode(file_get_contents(__DIR__ . '/../../src/Middleware/out.json')));
    }

    public function testToString()
    {
        $document = new Document();
        $this->assertIsString($document->__toString());
    }

    public function testSetIncludes()
    {
        $document = new Document();
        $document->setData(null);
        $included = new ResourceCollection();
        $included->add(new ResourceObject(new Type('test'), new Id('1')));
        $document->setIncludes($included);
        $this->assertTrue($this->isValid($document));
    }

    public function testGetData()
    {
        $document = new Document();
        $document->setData(null);
        $this->assertNull($document->getData());
        $document->setData(new ResourceCollection());
        $this->assertInstanceOf(PrimaryData::class, $document->getData());
        $this->assertTrue($this->isValid($document));
    }

    public function testJsonSerialize()
    {
        $document = new Document();
        $this->assertInstanceOf(JsonSerializable::class, $document);
        $this->assertIsString(json_encode($document));
    }

    public function testConstruct()
    {
        $document = new Document();
        $document->setData(null);
        $this->assertInstanceOf(Document::class, $document);
        $this->assertTrue($this->isValid($document));
    }

    public function testSetData()
    {
        $document = new Document();
        $data = new ResourceCollection();
        $document->setData($data);
        $this->assertTrue($this->isValid($document));
    }

    public function testAddError()
    {
        $document = new Document();
        $error = new Error();
        $document->addError($error);
        $this->assertTrue($this->isValid($document));
    }

    private function isValid(Document $document): bool
    {
        self::$schema->in(json_decode(json_encode($document)));
        return true;
    }

    public function testSetJSONAPIObjectMeta()
    {
        $document = new Document();
        $document->getJSONAPIObject()->setMeta(new Meta(['prop' => 'value']));
        $json = $document->jsonSerialize();
        $this->assertObjectHasProperty('meta', $json->jsonapi);
    }

    public function testAddIncluded()
    {
        $document = new Document();
        $document->setData(null);
        $document->addIncluded(
            new ResourceObject(new Type('test'), new Id('1'))
        );
        $document->addIncluded(
            new ResourceObject(new Type('test'), new Id('2')),
            new ResourceObject(new Type('test'), new Id('3'))
        );
        $data = json_decode(json_encode($document));
        $this->assertIsArray($data->included);
        $this->assertCount(3, $data->included);
        $included = new ResourceCollection();
        $included->add(new ResourceObject(new Type('test'), new Id('1')));
        $document->setIncludes($included);
        $data = json_decode(json_encode($document));
        $this->assertIsArray($data->included);
        $this->assertCount(1, $data->included);
        $document->addIncluded(
            new ResourceObject(new Type('test'), new Id('2')),
            new ResourceObject(new Type('test'), new Id('3'))
        );
        $data = json_decode(json_encode($document));
        $this->assertIsArray($data->included);
        $this->assertCount(3, $data->included);
    }
}
