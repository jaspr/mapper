<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Document;

use Fig\Http\Message\StatusCodeInterface;
use JSONAPI\Mapper\Document\Error;
use JSONAPI\Mapper\Document\Link;
use JSONAPI\Mapper\Document\Meta;
use JSONAPI\Mapper\Document\Source;
use JSONAPI\Mapper\Request\RequestPart;
use PHPUnit\Framework\TestCase;

/**
 * Class ErrorTest
 *
 * @package JSONAPI\Test\Document
 */
class ErrorTest extends TestCase
{
    public function testSetters()
    {
        $source = Source::parameter(RequestPart::SORT_PART_KEY);
        $error = new Error();
        $error->setMeta(new Meta(['custom' => 'property']));
        $error->setId('id');
        $error->setStatus((string)StatusCodeInterface::STATUS_OK);
        $error->setDetail('detail');
        $error->setTitle('title');
        $error->setCode('code');
        $error->setSource($source);
        $error->getLinks()->add(new Link('about', 'http://about.error.com'));
        $json = $error->jsonSerialize();

        $this->assertObjectHasProperty('id', $json);
        $this->assertEquals('id', $json->id);
        $this->assertObjectHasProperty('status', $json);
        $this->assertEquals(200, $json->status);
        $this->assertObjectHasProperty('detail', $json);
        $this->assertEquals('detail', $json->detail);
        $this->assertObjectHasProperty('title', $json);
        $this->assertEquals('title', $json->title);
        $this->assertObjectHasProperty('code', $json);
        $this->assertEquals('code', $json->code);
        $this->assertObjectHasProperty('source', $json);
        $this->assertObjectHasProperty('meta', $json);
        $this->assertObjectHasProperty('links', $json);
    }
}
