<?php

/**
 * Created by uzivatel
 * at 28.03.2022 11:31
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Request\Filtering\OData;

use JSONAPI\Expression\Dispatcher\PostgresSQLResolver;
use JSONAPI\Mapper\Driver\SchemaDriver;
use JSONAPI\Mapper\Exception\FilterException;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Request\Filtering\OData\ExpressionFilterParser;
use JSONAPI\Mapper\Request\Path\PathParser;
use JSONAPI\Mapper\Request\Path\PathResult;
use JSONAPI\Mapper\Test\Resources\Valid\GettersExample;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class ExpressionFilterParserTest extends TestCase
{
    /**
     * @var MetadataRepository mr
     */
    private static MetadataRepository $mr;

    public static function setUpBeforeClass(): void
    {
        self::$mr = MetadataFactory::create(
            [RESOURCES . '/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new SchemaDriver()
        );
    }

    public function testParse()
    {
        $filter = "/getter?filter=";
        $filter .= "(contains(stringProperty,'Bonus') or ";
        $filter .= "stringProperty eq 'mortgages') and ";
        $filter .= "boolProperty eq true and ";
        $filter .= "intProperty in (1,2,3) and ";
        $filter .= "arrayProperty has 1 and ";
        $filter .= "dateProperty be (datetime'2015-01-13T02:13:40Z',datetime'2015-01-13T02:13:40Z') and ";
        $filter .= "dtoProperty.number eq 1234";

        $_SERVER["REQUEST_URI"] = $filter;

        $pp         = new PathParser(self::$mr, 'http://test.unit.org/');
        $parser     = new ExpressionFilterParser(self::$mr, $pp);
        $request    = ServerRequestFactory::createFromGlobals();
        $result     = $parser->parse($request);
        $expression = $result->getCondition();
        $dispatcher = new PostgresSQLResolver();
        $where      = $dispatcher->dispatch($expression);
        $this->assertEquals(
            "((((((stringProperty LIKE :0 OR stringProperty = :1) AND boolProperty = :2) AND intProperty IN (:3,:4,:5)) AND :6 = ANY(arrayProperty)) AND (dateProperty BETWEEN :7 AND :8)) AND dtoProperty.number = :9)",
            //@phpcs:ignore
            $where
        );
        $this->assertEquals(
            ["%Bonus%", "mortgages", true, 1, 2, 3, 1, "2015-01-13T02:13:40+00:00", "2015-01-13T02:13:40+00:00", 1234],
            $dispatcher->getParams()
        );
    }

    public function testBadFilter()
    {
        $this->expectException(FilterException::class);
        $filter                 = "/getter?filter=stringProperty |eq 1";
        $_SERVER["REQUEST_URI"] = $filter;
        $request                = ServerRequestFactory::createFromGlobals();
        $parser                 = new ExpressionFilterParser(
            self::$mr,
            new PathParser(self::$mr, 'http://test.unit.org/')
        );
        $parser->parse($request);
    }

    public function testIdInOperator()
    {
        $filter                 = "/getter?filter=id in ('1a','2b')";
        $_SERVER["REQUEST_URI"] = $filter;
        $request                = ServerRequestFactory::createFromGlobals();
        $parser                 = new ExpressionFilterParser(
            self::$mr,
            new PathParser(self::$mr, 'http://test.unit.org/')
        );
        $result                 = $parser->parse($request);
        $expression             = $result->getCondition();
        $dispatcher             = new PostgresSQLResolver();
        $where                  = $dispatcher->dispatch($expression);
        $this->assertEquals(
            "id IN (:0,:1)",
            $where
        );
        $this->assertEquals(
            ['1a', '2b'],
            $dispatcher->getParams()
        );
    }
}
