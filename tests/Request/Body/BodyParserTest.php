<?php

/**
 * Created by Lasicka <lasicka@logio.cz>
 * at 21.06.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Request\Body;

use Fig\Http\Message\RequestMethodInterface;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Request\Body\BodyParser;
use JSONAPI\Mapper\Request\Path\PathParser;
use JSONAPI\Mapper\Test\Resources\CustomEnum;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;
use Slim\Psr7\Factory\StreamFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class BodyParserTest extends TestCase
{
    private static MetadataRepository $mr;
    private static StreamFactory $sf;
    private static PathParser $pp;
    private static ServerRequestFactory $srf;

    public static function setUpBeforeClass(): void
    {
        self::$mr  = MetadataFactory::create(
            [__DIR__ . '/../../Resources/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        self::$sf  = new StreamFactory();
        self::$pp  = (new PathParser(self::$mr, 'http://unit.test.org/'));
        self::$srf = new ServerRequestFactory(self::$sf);
    }

    public function testParse()
    {
        $body = json_encode([
            'data' => [
                'id'         => 'test',
                'type'       => 'builtin',
                'attributes' => [
                    'dateTime' => (new \DateTime())->format(\DateTimeInterface::ATOM),
                    'enum'     => 'bar',
                    'assoc'    => ['foo' => 'bar']
                ]
            ]
        ]);

        $parser  = new BodyParser(self::$mr, self::$pp);
        $request = self::$srf
            ->createServerRequest(RequestMethodInterface::METHOD_GET, '/builtin/test')
            ->withBody(self::$sf->createStream($body));

        $doc = $parser->parse($request);
        /** @var ResourceObject $resource */
        $resource = $doc->getData();
        /** @var CustomEnum $enum */
        $enum = $resource->getAttribute('enum')->getData();
        $this->assertEquals('bar', $enum->value);
        /** @var \DateTimeImmutable $dateTime */
        $dateTime = $resource->getAttribute('dateTime')->getData();
        $this->assertInstanceOf(\DateTimeInterface::class, $dateTime);
        $this->assertInstanceOf(\DateTimeImmutable::class, $dateTime);
        $array = $resource->getAttribute('assoc')->getData();
        $this->assertIsArray($array);
        $this->assertIsNotObject($array);
        $this->assertArrayHasKey('foo', $array);
    }
}
