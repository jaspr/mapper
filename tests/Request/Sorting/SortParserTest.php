<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Request\Sorting;

use JSONAPI\Mapper\Exception\PatternDoesNotMatch;
use JSONAPI\Mapper\Request\Sorting\SortInterface;
use JSONAPI\Mapper\Request\Sorting\SortParser;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;

class SortParserTest extends TestCase
{
    public static function goodDataProvider(): array
    {
        return [
            ['/getter?sort=00', ['00' => SortInterface::ASC]],
            ['/getter?sort=0-0', ['0-0' => SortInterface::ASC]],
            ['/getter?sort=-asdf', ['asdf' => SortInterface::DESC]],
            ['/getter?sort=asdf', ['asdf' => SortInterface::ASC]],
            ['/getter?sort=asdf.asdf', ['asdf.asdf' => SortInterface::ASC]],
            ['/getter?sort=-asdf.asdf', ['asdf.asdf' => SortInterface::DESC]],
            ['/getter?sort=asdf-asdf', ['asdf-asdf' => SortInterface::ASC]],
            ['/getter?sort=-asdf.asdf-asdf', ['asdf.asdf-asdf' => SortInterface::DESC]],
            ['/getter?sort=-asdf_asdf-asdf.asdf', ['asdf_asdf-asdf.asdf' => SortInterface::DESC]]
        ];
    }

    /**
     * @dataProvider goodDataProvider
     */
    public function testParse($data, $expected)
    {
        $_SERVER['REQUEST_URI'] = $data;
        $request                = ServerRequestFactory::createFromGlobals();
        $parser                 = new SortParser();
        $result                 = $parser->parse($request);
        $this->assertEquals($expected, $result->getOrder());
    }

    public function testEmpty()
    {
        $_SERVER['REQUEST_URI'] = '';
        $request                = ServerRequestFactory::createFromGlobals();
        $parser                 = new SortParser();
        $result                 = $parser->parse($request);
        $this->assertNull($result);
    }

    public static function badDataProvider()
    {
        return [
            ['/getter?sort=a'],
            ['/getter?sort=*adsf'],
            ['/getter?sort=--'],
            ['/getter?sort=-'],
            ['/getter?sort=.'],
            ['/getter?sort=-.'],
            ['/getter?sort=asdf-'],
            ['/getter?sort=asdf.'],
            ['/getter?sort=-qwe-asdf*qwefasdf*+asdfqwef'],
            ['/getter?sort=*asdf,asdf,-asdf']
        ];
    }

    /**
     * @dataProvider badDataProvider
     */
    public function testBadRequest($data)
    {
        $_SERVER['REQUEST_URI'] = $data;
        $request                = ServerRequestFactory::createFromGlobals();
        $this->expectException(PatternDoesNotMatch::class);
        $parser = new SortParser();
        $parser->parse($request);
    }
}
