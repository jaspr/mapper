<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Request;

use JSONAPI\Mapper\Driver\RecommendedNamingStrategy;
use JSONAPI\Mapper\Driver\SchemaDriver;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Request\Body\BodyParser;
use JSONAPI\Mapper\Request\Fieldset\FieldsetInterface;
use JSONAPI\Mapper\Request\Fieldset\FieldsetParser;
use JSONAPI\Mapper\Request\Filtering\FilterInterface;
use JSONAPI\Mapper\Request\Filtering\QData\QuatrodotFilterParser;
use JSONAPI\Mapper\Request\Inclusion\InclusionInterface;
use JSONAPI\Mapper\Request\Inclusion\InclusionParser;
use JSONAPI\Mapper\Request\Pagination\PageStrategyParser;
use JSONAPI\Mapper\Request\Pagination\PaginationInterface;
use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\Parser;
use JSONAPI\Mapper\Request\Path\PathInterface;
use JSONAPI\Mapper\Request\Path\PathParser;
use JSONAPI\Mapper\Request\Sorting\SortInterface;
use JSONAPI\Mapper\Request\Sorting\SortParser;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use Slim\Psr7\Factory\ServerRequestFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class ParserTest extends TestCase
{
    private static MetadataRepository $mr;
    private static string $baseURL;

    public static function setUpBeforeClass(): void
    {
        self::$mr      = MetadataFactory::create(
            [__DIR__ . '/../Resources/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new SchemaDriver()
        );
        self::$baseURL = 'http://unit.test.org';
    }

    public function testGetFieldset()
    {
        $request = ServerRequestFactory::createFromGlobals();
        $parser  = Parser::createDefault(self::$baseURL, self::$mr);
        $up      = $parser->parse($request);
        $this->assertInstanceOf(FieldsetInterface::class, $up->getFieldset());
    }

    public function testGetFilter()
    {
        $request = ServerRequestFactory::createFromGlobals();
        $parser  = Parser::createDefault(self::$baseURL, self::$mr);
        $up      = $parser->parse($request);
        $this->assertInstanceOf(FilterInterface::class, $up->getFilter());
    }

    public function testGetSort()
    {
        $request = ServerRequestFactory::createFromGlobals();
        $parser  = Parser::createDefault(self::$baseURL, self::$mr);
        $up      = $parser->parse($request);
        $this->assertInstanceOf(SortInterface::class, $up->getSort());
    }

    public function testConstruct()
    {
        $request = ServerRequestFactory::createFromGlobals();
        $parser = new Parser(self::$baseURL);
        $this->assertInstanceOf(Parser::class, $parser);
        $up = $parser->parse($request);
        $this->assertInstanceOf(ParsedRequest::class, $up);
        $this->assertNull($up->getBody());
        $this->assertNull($up->getInclusion());
        $this->assertNull($up->getSort());
        $this->assertNull($up->getPath());
        $this->assertNull($up->getPagination());
    }

    public function testGetInclusion()
    {
        $request = ServerRequestFactory::createFromGlobals();
        $parser  = Parser::createDefault(self::$baseURL, self::$mr);
        $up      = $parser->parse($request);
        $this->assertInstanceOf(InclusionInterface::class, $up->getInclusion());
    }

    public function testGetPath()
    {
        $request = ServerRequestFactory::createFromGlobals();
        $parser  = Parser::createDefault(self::$baseURL, self::$mr);
        $up      = $parser->parse($request);
        $this->assertInstanceOf(PathInterface::class, $up->getPath());
    }

    public function testGetPagination()
    {
        $request = ServerRequestFactory::createFromGlobals();
        $parser  = Parser::createDefault(self::$baseURL, self::$mr);
        $up      = $parser->parse($request);
        $this->assertInstanceOf(PaginationInterface::class, $up->getPagination());
    }
}
