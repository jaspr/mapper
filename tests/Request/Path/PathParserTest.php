<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Request\Path;

use Fig\Http\Message\RequestMethodInterface;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Driver\KebabCaseURLNamingStrategy;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Request\Path\PathInterface;
use JSONAPI\Mapper\Request\Path\PathParser;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

/**
 * Class PathParserTest
 *
 * @package JSONAPI\Test\URI\Path
 */
class PathParserTest extends TestCase
{
    /**
     * @var MetadataRepository
     */
    private static MetadataRepository $mr;
    private static string $baseUrl;

    public static function setUpBeforeClass(): void
    {
        self::$mr      = MetadataFactory::create(
            [RESOURCES . '/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        self::$baseUrl = 'http://unit.test.org';
    }

    public function testGetId()
    {
        $test                   = '/getter/uuid/relationships/relation';
        $_SERVER['REQUEST_URI'] = $test;
        $request                = ServerRequestFactory::createFromGlobals();
        $parser                 = new PathParser(self::$mr, self::$baseUrl);
        $path                   = $parser->parse($request);
        $this->assertEquals('uuid', $path->getId());
    }

    public function testGetResourceType()
    {
        $test                   = '/getter/uuid/relationships/relation';
        $_SERVER['REQUEST_URI'] = $test;
        $request                = ServerRequestFactory::createFromGlobals();
        $parser                 = new PathParser(self::$mr, self::$baseUrl);
        $path                   = $parser->parse($request);
        $this->assertEquals('getter', $path->getResourceType());
    }

    public function testToString()
    {
        $test                   = 'getter/uuid/relationships/relation';
        $_SERVER['REQUEST_URI'] = $test;
        $request                = ServerRequestFactory::createFromGlobals();
        $parser                 = new PathParser(self::$mr, self::$baseUrl);
        $path                   = $parser->parse($request);
        $result                 = (string)$path;
        $this->assertIsString($result);
        $this->assertEquals($test, $result);
    }

    public function testGetPrimaryResourceType()
    {
        $test                   = '/getter/uuid';
        $_SERVER['REQUEST_URI'] = $test;
        $request                = ServerRequestFactory::createFromGlobals();
        $parser                 = new PathParser(self::$mr, self::$baseUrl);
        $path                   = $parser->parse($request);
        $this->assertEquals('getter', $path->getPrimaryResourceType());
    }

    public function testConstruct()
    {
        $parser = new PathParser(self::$mr, self::$baseUrl);
        $this->assertInstanceOf(PathParser::class, $parser);
    }

    public function testParse()
    {
        $test                   = '/api/getter/uuid';
        $_SERVER['REQUEST_URI'] = $test;
        $request                = ServerRequestFactory::createFromGlobals();
        $parser                 = new PathParser(self::$mr, self::$baseUrl . '/api');
        $path                   = $parser->parse($request);
        $this->assertInstanceOf(PathInterface::class, $path);
        $this->assertEquals('getter', $path->getResourceType());

        $test                   = '/getter/uuid';
        $_SERVER['REQUEST_URI'] = $test;
        $request                = ServerRequestFactory::createFromGlobals();
        $path                   = $parser->parse($request);
        $this->assertEquals('getter', $path->getResourceType());
    }

    public function testProxyUrl()
    {
        $data                   = '/resources/getter';
        $_SERVER['REQUEST_URI'] = $data;
        $request                = ServerRequestFactory::createFromGlobals();
        $parser                 = new PathParser(self::$mr, self::$baseUrl . '/resources');
        $path                   = $parser->parse($request);
        $this->assertTrue($path->isCollection());
        $this->assertEquals('getter', $path->getResourceType());

        $data                   = '/resources/getter/some-uuid';
        $_SERVER['REQUEST_URI'] = $data;
        $request                = ServerRequestFactory::createFromGlobals();
        $path                   = $parser->parse($request);
        $this->assertEquals('getter', $path->getResourceType());
        $this->assertEquals('some-uuid', $path->getId());
    }

    public function testIsRelationship()
    {
        $test                   = '/getter/uuid/relationships/relation';
        $_SERVER['REQUEST_URI'] = $test;
        $request                = ServerRequestFactory::createFromGlobals();
        $parser                 = new PathParser(self::$mr, self::$baseUrl);
        $path                   = $parser->parse($request);
        $this->assertTrue($path->isRelationship());
    }

    public function testIsCollection()
    {
        $test                   = '/getter';
        $_SERVER['REQUEST_URI'] = $test;
        $request                = ServerRequestFactory::createFromGlobals();
        $parser                 = new PathParser(self::$mr, self::$baseUrl);
        $path                   = $parser->parse($request);
        $this->assertTrue($path->isCollection());
    }

    public function testGetRelationshipName()
    {
        $test                   = '/getter/uuid/relationships/relation';
        $_SERVER['REQUEST_URI'] = $test;
        $request                = ServerRequestFactory::createFromGlobals();
        $parser                 = new PathParser(self::$mr, self::$baseUrl);
        $path                   = $parser->parse($request);
        $this->assertEquals('relation', $path->getRelationshipName());
    }

    public function testIssue44()
    {
        $test                   = '/getter/uu.id/relationships/relation';
        $_SERVER['REQUEST_URI'] = $test;
        $request                = ServerRequestFactory::createFromGlobals();
        $parser                 = new PathParser(self::$mr, self::$baseUrl);
        $path                   = $parser->parse($request);
        $this->assertEquals('uu.id', $path->getId());
    }

    public function testGetRelationshipNameWithKebabCaseNamingStrategy()
    {
        $test                   = '/getter/uuid/relationships/doctrine-collection';
        $_SERVER['REQUEST_URI'] = $test;
        $request                = ServerRequestFactory::createFromGlobals();
        $parser                 = new PathParser(self::$mr, self::$baseUrl, new KebabCaseURLNamingStrategy());
        $path                   = $parser->parse($request);
        $this->assertEquals('doctrineCollection', $path->getRelationshipName());
    }
}
