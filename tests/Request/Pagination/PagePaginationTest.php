<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Request\Pagination;

use JSONAPI\Mapper\Request\Pagination\NumberSizeResult;
use JSONAPI\Mapper\Request\Pagination\PageStrategyParser;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;

class PagePaginationTest extends TestCase
{
    public static function goodData()
    {
        return [
            ['/getter?page[number]=6&page[size]=20'],
            ['/getter?page[number]=1'],
            ['/getter?page[size]=25'],
            ['']
        ];
    }

    public static function badData()
    {
        return [
            ['/getter?page[number]=string&page[size]=null']
        ];
    }

    /**
     * @dataProvider goodData
     */
    public function testParseSuccess($data)
    {
        $_SERVER['REQUEST_URI'] = $data;
        $request                = ServerRequestFactory::createFromGlobals();
        $pagination             = new PageStrategyParser();
        $result                 = $pagination->parse($request);
        $this->assertEquals(26, $result->getNumber() + $result->getSize());
    }

    /**
     * @dataProvider badData
     */
    public function testParseFailure($data)
    {
        $_SERVER['REQUEST_URI'] = $data;
        $request                = ServerRequestFactory::createFromGlobals();
        $pagination             = new PageStrategyParser();
        $result                 = $pagination->parse($request);
        $this->assertEquals(26, $result->getNumber() + $result->getSize());
    }

    public function testConstruct()
    {
        $pagination = new PageStrategyParser();
        $this->assertInstanceOf(PageStrategyParser::class, $pagination);
        return $pagination;
    }

    public function testPagination()
    {
        $_SERVER['REQUEST_URI'] = '/getter?page[size]=25&page[number]=2';
        $request                = ServerRequestFactory::createFromGlobals();
        $parser                 = new PageStrategyParser();
        $result                 = $parser->parse($request);
        $this->assertNull($result->last());
        $result->setTotal(100);
        $next = $result->next();
        $this->assertInstanceOf(NumberSizeResult::class, $next);
        $this->assertEquals(25, $next->getSize());
        $this->assertEquals(3, $next->getNumber());
        $prev = $result->prev();
        $this->assertInstanceOf(NumberSizeResult::class, $prev);
        $this->assertEquals(25, $prev->getSize());
        $this->assertEquals(1, $prev->getNumber());
        $first = $result->first();
        $this->assertInstanceOf(NumberSizeResult::class, $first);
        $this->assertEquals(25, $first->getSize());
        $this->assertEquals(1, $first->getNumber());
        $last = $result->last();
        $this->assertInstanceOf(NumberSizeResult::class, $last);
        $this->assertEquals(25, $last->getSize());
        $this->assertEquals(4, $last->getNumber());
    }
}
