<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Request\Pagination;

use JSONAPI\Mapper\Request\Pagination\LimitOffsetResult;
use JSONAPI\Mapper\Request\Pagination\OffsetStrategyParser;
use JSONAPI\Mapper\Request\Pagination\PaginationInterface;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;

class LimitOffsetPaginationTest extends TestCase
{
    public static function goodData()
    {
        return [
            ['/getter?page[limit]=25&page[offset]=0'],
            ['/getter?page[limit]=25'],
            ['/getter?page[offset]=0'],
            ['/getter']
        ];
    }

    public static function badData()
    {
        return [
            ['/getter?page[limit]=asdf&page[offset]=null']
        ];
    }

    /**
     * @dataProvider goodData
     */
    public function testParseSuccess($data)
    {
        $_SERVER['REQUEST_URI'] = $data;
        $request = ServerRequestFactory::createFromGlobals();
        $parser = new OffsetStrategyParser();
        $result = $parser->parse($request);
        $this->assertInstanceOf(LimitOffsetResult::class, $result);
        $this->assertEquals(25, $result->getLimit());
        $this->assertEquals(0, $result->getOffset());
    }

    /**
     * @dataProvider badData
     */
    public function testParseFailure($data)
    {
        $this->expectException(\UnexpectedValueException::class);
        $parser = new OffsetStrategyParser();
        $_SERVER['REQUEST_URI'] = $data;
        $request = ServerRequestFactory::createFromGlobals();
        $parser->parse($request);
    }

    public function testConstruct()
    {
        $this->assertInstanceOf(OffsetStrategyParser::class, new OffsetStrategyParser());
        $this->assertInstanceOf(OffsetStrategyParser::class, new OffsetStrategyParser(100, 200));
        $this->expectException(\TypeError::class);
        $this->assertInstanceOf(OffsetStrategyParser::class, new OffsetStrategyParser('string', 1.23));
    }

    public function testPagination()
    {
        $_SERVER['REQUEST_URI'] = '/getter?page[limit]=25&page[offset]=50';
        $request = ServerRequestFactory::createFromGlobals();
        $parser = new OffsetStrategyParser();
        $result = $parser->parse($request);
        $this->assertNull($result->last());
        $result->setTotal(100);
        $next = $result->next();
        $this->assertInstanceOf(LimitOffsetResult::class, $next);
        $this->assertEquals(25, $next->getLimit());
        $this->assertEquals(75, $next->getOffset());
        $prev = $result->prev();
        $this->assertInstanceOf(LimitOffsetResult::class, $prev);
        $this->assertEquals(25, $prev->getLimit());
        $this->assertEquals(25, $prev->getOffset());
        $first = $result->first();
        $this->assertInstanceOf(LimitOffsetResult::class, $first);
        $this->assertEquals(25, $first->getLimit());
        $this->assertEquals(0, $first->getOffset());
        $last = $result->last();
        $this->assertInstanceOf(LimitOffsetResult::class, $last);
        $this->assertEquals(25, $last->getLimit());
        $this->assertEquals(75, $last->getOffset());
    }
}
