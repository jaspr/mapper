<?php

/**
 * Created by tomas
 * 10.07.2022 14:59
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Request;

use JSONAPI\Mapper\Document\Link;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Driver\KebabCaseURLNamingStrategy;
use JSONAPI\Mapper\Encoding\LinkFactory;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Request\Parser;
use JSONAPI\Mapper\Test\Resources\Valid\GettersExample;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class LinkFactoryTest extends TestCase
{
    /**
     * @var MetadataRepository mr
     */
    private static MetadataRepository $mr;
    /**
     * @var string url
     */
    private static string $baseURL;

    public static function setUpBeforeClass(): void
    {
        self::$mr            = MetadataFactory::create(
            [RESOURCES . '/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        self::$baseURL       = 'http://unit.test.org';
    }

    public function testConstruct(): LinkFactory
    {
        $instance = new LinkFactory(self::$mr);
        $this->assertInstanceOf(LinkFactory::class, $instance);
        return $instance;
    }

    /**
     * @depends testConstruct
     */
    public function testGetDocumentLinks(LinkFactory $factory)
    {
        $request = ServerRequestFactory::createFromGlobals();
        $up      = (Parser::createDefault(self::$baseURL, self::$mr))->parse($request);
        $links   = $factory->getDocumentLinks($up);
        $this->assertArrayHasKey(Link::SELF, $links);
        $this->assertEquals(
            "getter/uuid?include=collection&fields%5Bresource%5D=publicProperty,privateProperty,relations",
            $links[Link::SELF]
        );
    }

    /**
     * @depends testConstruct
     */
    public function testGetResourceLinksByType(LinkFactory $factory)
    {
        $links = $factory->getResourceLinksByType('getter');
        $this->assertArrayHasKey(Link::SELF, $links);
        $this->assertArrayHasKey(Link::COLLECTION, $links);
        $this->assertEquals("getter/{id}", $links[Link::SELF]);
        $this->assertEquals('getter', $links[Link::COLLECTION]);
    }

    /**
     * @depends testConstruct
     */
    public function testGetRelationshipLinksByType(LinkFactory $factory)
    {
        $links = $factory->getRelationshipLinksByType('getter', 'relation');
        $this->assertArrayHasKey(Link::SELF, $links);
        $this->assertArrayHasKey(Link::RELATED, $links);
        $this->assertEquals("getter/{id}/relationships/relation", $links[Link::SELF]);
        $this->assertEquals("getter/{id}/relation", $links[Link::RELATED]);
    }


    /**
     * @depends testConstruct
     */
    public function testGetResourceLinksByClass(LinkFactory $factory)
    {
        $links = $factory->getResourceLinksByClass(GettersExample::class);
        $this->assertArrayHasKey(Link::SELF, $links);
        $this->assertArrayHasKey(Link::COLLECTION, $links);
        $this->assertEquals("getter/{id}", $links[Link::SELF]);
        $this->assertEquals("getter", $links[Link::COLLECTION]);
    }

    /**
     * @depends testConstruct
     */
    public function test(LinkFactory $factory)
    {
        $links = $factory->getRelationshipLinksByClass(GettersExample::class, 'doctrineCollection');
        $this->assertArrayHasKey(Link::SELF, $links);
        $this->assertArrayHasKey(Link::RELATED, $links);
        $this->assertEquals("getter/{id}/relationships/doctrineCollection", $links[Link::SELF]);
        $this->assertEquals("getter/{id}/doctrineCollection", $links[Link::RELATED]);
    }

    public function testKebabCaseStrategy()
    {
        $lf = new LinkFactory(self::$mr, new KebabCaseURLNamingStrategy());
        $links = $lf->getRelationshipLinksByClass(GettersExample::class, 'doctrineCollection');
        $this->assertArrayHasKey(Link::SELF, $links);
        $this->assertArrayHasKey(Link::RELATED, $links);
        $this->assertEquals("getter/{id}/relationships/doctrine-collection", $links[Link::SELF]);
        $this->assertEquals("getter/{id}/doctrine-collection", $links[Link::RELATED]);
    }
}
