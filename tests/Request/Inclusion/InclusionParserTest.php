<?php

/**
 * Created by tomas
 * at 29.11.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Request\Inclusion;

use JSONAPI\Mapper\Request\Inclusion\InclusionParser;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;

class InclusionParserTest extends TestCase
{
    public function testParse()
    {
        $data = '/getter?include=product,product.unit,product.category';
        $_SERVER['REQUEST_URI'] = $data;
        $request = ServerRequestFactory::createFromGlobals();
        $parser = new InclusionParser();
        $result = $parser->parse($request);
        $this->assertIsObject($result);
        $this->assertTrue($result->hasInclusions());
        $this->assertTrue($result->contains('product'));
        $this->assertCount(1, $result->getInclusions());
        foreach ($result->getInclusions() as $inclusion) {
            $this->assertEquals('product', $inclusion->getRelationName());
            $this->assertTrue($inclusion->hasInclusions());
            $this->assertCount(2, $inclusion->getInclusions());
            $this->assertArrayHasKey('unit', $inclusion->getInclusions());
            $this->assertArrayHasKey('category', $inclusion->getInclusions());
            $unit = $inclusion->getInclusions()['unit'];
            $category = $inclusion->getInclusions()['category'];
            $this->assertEquals('unit', $unit->getRelationName());
            $this->assertEquals('category', $category->getRelationName());
        }
        $tree = $result->toTree();
        $this->assertEquals(['product' => ['unit' => [], 'category' => []]], $tree);
    }
}
