import type {Resource, ToOneRelationship, ToManyRelationship, Index} from "@jaspr/client-js";

export interface BuiltInExample extends Resource {
    type: 'builtin';
    attributes: {
        dateTime: Date;
        enum: 'bar';
        assoc: { [key: string]: any }[];
    }
}

export interface DummyRelation extends Resource {
    type: 'relation';
    attributes: {
        property: string;
    }
    relationships: {
        example: ToOneRelationship<ThirdLevel>;
    }
}

export interface GettersExample extends Resource {
    type: 'getter';
    attributes: {
        stringProperty: string;
        intProperty: number;
        doubleProperty: number;
        arrayProperty: number[];
        boolProperty: boolean;
        dtoProperty: { [key: string]: any };
        dateProperty: Date;
    }
    relationships: {
        relation: ToOneRelationship<DummyRelation>;
        collection: ToManyRelationship<DummyRelation>;
        doctrineCollection: ToManyRelationship<DummyRelation>;
    }
}

export interface LinkageExample extends Resource {
    type: 'linkage';
    relationships: {
        collectionProperty: ToManyRelationship<DummyRelation>;
    }
}

export interface MetaExample extends Resource {
    type: 'meta';
    relationships: {
        relation: ToOneRelationship<DummyRelation>;
    }
}

export interface NoOperations extends Resource {
    type: 'noop';
    attributes: {
        stringProperty: string;
        intProperty: number;
        doubleProperty: number;
        arrayProperty: number[];
        boolProperty: boolean;
        dtoProperty: { [key: string]: any };
        dateProperty: Date;
    }
    relationships: {
        relation: ToOneRelationship<DummyRelation>;
        collection: ToManyRelationship<DummyRelation>;
    }
}

export interface PropsExample extends Resource {
    type: 'prop';
    attributes: {
        stringProperty: string;
        intProperty: number;
        arrayProperty: number[];
        boolProperty: boolean;
        dtoProperty: { [key: string]: any };
    }
    relationships: {
        relation: ToOneRelationship<DummyRelation>;
        collection: ToManyRelationship<DummyRelation>;
    }
}

export interface ThirdLevel extends Resource {
    type: 'third';
    attributes: {
        property: string;
    }
}

export default interface ResourceIndex extends Index {
    builtin: BuiltInExample,
    relation: DummyRelation,
    getter: GettersExample,
    linkage: LinkageExample,
    meta: MetaExample,
    noop: NoOperations,
    prop: PropsExample,
    third: ThirdLevel,
}
