<?php

/**
 * Created by Lasicka <lasicka@logio.cz>
 * at 30.10.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test;

use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\TypeScriptInterfaceBuilder;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class TypeScriptInterfaceBuilderTest extends TestCase
{
    public function testBuild()
    {
        $mr = MetadataFactory::create(
            [RESOURCES . '/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        $builder = new TypeScriptInterfaceBuilder($mr);
        $content = $builder->build();
        file_put_contents('tests/index.ts', $content);
        $this->assertNotEmpty($content);
        // cannot compare file because order of exported interfaces can vary, thus this test must count with it
//        $this->assert(__DIR__ . '/index.ts', $content);
    }
}
