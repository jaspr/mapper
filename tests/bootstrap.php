<?php

declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';

const RESOURCES =  __DIR__ . '/Resources';

$_SERVER["REQUEST_URI"] = "/getter/uuid?filter=stringProperty eq 'value'&include=collection&fields[resource]=publicProperty,privateProperty,relations&sort=-publicProperty,privateProperty&page[offset]=10&page[limit]=20";
$_SERVER["HTTP_HOST"] = "unit.test.org";
$_SERVER["REQUEST_SCHEME"] = "https";
$_SERVER["REQUEST_METHOD"] = "GET";
