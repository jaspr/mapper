<?php

/**
 * Created by tomas
 * at 11.07.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Metadata;

use JSONAPI\Mapper\Metadata\Operation;
use PHPUnit\Framework\TestCase;

class OperationTest extends TestCase
{
    public function testDiff()
    {
        $operation = [];
        $excluded = [Operation::CREATE, Operation::DELETE];
        $result = Operation::diff($operation, $excluded);
        $this->assertEquals([], $result);
    }
}
