<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Middleware;

use Fig\Http\Message\RequestMethodInterface;
use Fig\Http\Message\StatusCodeInterface;
use JSONAPI\Mapper\Compose\Builder;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Encoding\EncoderFactory;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Middleware\PsrJsonApiMiddleware;
use JSONAPI\Mapper\Request\Parser;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Factory\ServerRequestFactory;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Psr7\Stream;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

/**
 * Class PsrJsonApiMiddlewareTest
 *
 * @package JSONAPI\Test\Middleware
 */
class PsrJsonApiMiddlewareTest extends TestCase implements RequestHandlerInterface
{
    private static PsrJsonApiMiddleware $mw;

    public static function setUpBeforeClass(): void
    {
        $rf = new ResponseFactory();
        $sf = new StreamFactory();
        $mr = MetadataFactory::create(
            [__DIR__ . '/../Resources/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        $rp = Parser::createDefault('https://unit.test.org', $mr);
        self::$mw = new PsrJsonApiMiddleware($rf, $sf, $rp, EncoderFactory::createDefaultEncoder($mr));
    }

    public function testUnsupportedMediaType()
    {
        $rf = new ServerRequestFactory();
        $request = $rf
            ->createServerRequest(RequestMethodInterface::METHOD_POST, 'https://unit.test.org/getter')
            ->withBody(new Stream(fopen(__DIR__ . '/request.json', 'r')));
        $response = self::$mw->process($request, $this);
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals(Document::getMediaType(), $response->getHeader('Content-Type')[0]);
        $this->assertEquals(StatusCodeInterface::STATUS_UNSUPPORTED_MEDIA_TYPE, $response->getStatusCode());
    }

    public function testGoodPost()
    {
        $rf = new ServerRequestFactory();
        $request = $rf->createServerRequest(RequestMethodInterface::METHOD_POST, 'https://unit.test.org/getter')
            ->withAddedHeader('Content-Type', Document::getMediaType())
            ->withBody(new Stream(fopen(__DIR__ . '/request.json', 'r')));
        $response = self::$mw->process($request, $this);
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals(Document::getMediaType(), $response->getHeader('Content-Type')[0]);
        $this->assertEquals(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
    }

    public function testBadPost()
    {
        $rf = new ServerRequestFactory();
        $request = $rf->createServerRequest(RequestMethodInterface::METHOD_POST, 'https://unit.test.org/getter')
            ->withAddedHeader('Content-Type', Document::getMediaType())
            ->withBody(new Stream(fopen(__DIR__ . '/requestWithBadTypes.json', 'r')));
        $response = self::$mw->process($request, $this);
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals(Document::getMediaType(), $response->getHeader('Content-Type')[0]);
        $this->assertEquals(StatusCodeInterface::STATUS_BAD_REQUEST, $response->getStatusCode());
    }

    public function testGoodGet()
    {
        $rf = new ServerRequestFactory();
        $request = $rf->createServerRequest(RequestMethodInterface::METHOD_GET, 'https://unit.test.org/getter');
        $response = self::$mw->process($request, $this);
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals(Document::getMediaType(), $response->getHeader('Content-Type')[0]);
        $this->assertEquals(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
    }

    public function testBadGet()
    {
        $rf = new ServerRequestFactory();
        $request = $rf->createServerRequest(
            RequestMethodInterface::METHOD_GET,
            'https://unit.test.org/non-existing-resource'
        );
        $response = self::$mw->process($request, $this);
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals(Document::getMediaType(), $response->getHeader('Content-Type')[0]);
        $this->assertEquals(StatusCodeInterface::STATUS_BAD_REQUEST, $response->getStatusCode());
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if (
            in_array(
                $request->getMethod(),
                [
                    RequestMethodInterface::METHOD_POST,
                    RequestMethodInterface::METHOD_PATCH,
                    RequestMethodInterface::METHOD_DELETE
                ]
            )
        ) {
            $this->assertInstanceOf(Document::class, $request->getParsedBody());
            $builder = $request->getAttribute(PsrJsonApiMiddleware::BUILDER);
            $this->assertInstanceOf(Builder::class, $builder);
        }
        $factory = new ResponseFactory();
        return $factory->createResponse()->withBody(new Stream(fopen(__DIR__ . '/response.json', 'r')));
    }
}
