<?php

/**
 * Created by tomas
 * at 11.05.2024
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Encoding\Processor;

use JSONAPI\Mapper\Document\Members;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Encoding\EncoderFactory;
use JSONAPI\Mapper\Encoding\Processor\HideLinksIfNoOperationIsSupported;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Test\Resources\Valid\NoOperations;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class HideLinksIfNoOperationIsSupportedTest extends TestCase
{
    private static MetadataRepository $mr;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$mr = MetadataFactory::create(
            [__DIR__ . '/../../Resources/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
    }


    public function testProcessResource()
    {
        $encoder = EncoderFactory::createDefaultEncoder(self::$mr);
        $processor = new HideLinksIfNoOperationIsSupported(self::$mr);
        $object = new NoOperations('id');
        $resource = $encoder->encode($object);
        $resource->getLinks()->create(Members::SELF, 'https://foo.bar');
        $this->assertTrue($resource->getLinks()->find(Members::SELF) !== null);
        $processor->processResource($resource, $object);
        $this->assertTrue($resource->getLinks()->find(Members::SELF) === null);
    }

    public function testProcessRelationship()
    {
        $encoder = EncoderFactory::createDefaultEncoder(self::$mr);
        $processor = new HideLinksIfNoOperationIsSupported(self::$mr);
        $object = new NoOperations('id');
        $resource = $encoder->encode($object);
        $resource->getRelationship('relation')->getLinks()->create(Members::SELF, 'https://foo.bar');
        $resource->getRelationship('relation')->getLinks()->create(Members::RELATED, 'https://foo.bar');
        $this->assertTrue($resource->getRelationship('relation')->getLinks()->find(Members::SELF) !== null);
        $this->assertTrue($resource->getRelationship('relation')->getLinks()->find(Members::RELATED) !== null);
        $processor->processRelationship($resource->getRelationship('relation'), $object);
        $this->assertTrue($resource->getRelationship('relation')->getLinks()->find(Members::SELF) === null);
        $this->assertTrue($resource->getRelationship('relation')->getLinks()->find(Members::RELATED) === null);

        $resource->getRelationship('collection')->getLinks()->create(Members::SELF, 'https://foo.bar');
        $resource->getRelationship('collection')->getLinks()->create(Members::RELATED, 'https://foo.bar');
        $resource->getRelationship('collection')->getLinks()->create(Members::FIRST, 'https://foo.bar');
        $resource->getRelationship('collection')->getLinks()->create(Members::PREV, 'https://foo.bar');
        $resource->getRelationship('collection')->getLinks()->create(Members::NEXT, 'https://foo.bar');
        $resource->getRelationship('collection')->getLinks()->create(Members::LAST, 'https://foo.bar');
        $this->assertTrue($resource->getRelationship('collection')->getLinks()->find(Members::SELF) !== null);
        $this->assertTrue($resource->getRelationship('collection')->getLinks()->find(Members::RELATED) !== null);
        $this->assertTrue($resource->getRelationship('collection')->getLinks()->find(Members::FIRST) !== null);
        $this->assertTrue($resource->getRelationship('collection')->getLinks()->find(Members::PREV) !== null);
        $this->assertTrue($resource->getRelationship('collection')->getLinks()->find(Members::NEXT) !== null);
        $this->assertTrue($resource->getRelationship('collection')->getLinks()->find(Members::LAST) !== null);
        $processor->processRelationship($resource->getRelationship('collection'), $object);
        $this->assertTrue($resource->getRelationship('collection')->getLinks()->find(Members::SELF) === null);
        $this->assertTrue($resource->getRelationship('collection')->getLinks()->find(Members::RELATED) === null);
        $this->assertTrue($resource->getRelationship('collection')->getLinks()->find(Members::FIRST) !== null);
        $this->assertTrue($resource->getRelationship('collection')->getLinks()->find(Members::PREV) !== null);
        $this->assertTrue($resource->getRelationship('collection')->getLinks()->find(Members::NEXT) !== null);
        $this->assertTrue($resource->getRelationship('collection')->getLinks()->find(Members::LAST) !== null);
    }
}
