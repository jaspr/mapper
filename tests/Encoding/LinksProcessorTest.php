<?php

/**
 * Created by tomas
 * at 26.05.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Encoding;

use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\Id;
use JSONAPI\Mapper\Document\Link;
use JSONAPI\Mapper\Document\Relationship;
use JSONAPI\Mapper\Document\ResourceCollection;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\ResourceObjectIdentifier;
use JSONAPI\Mapper\Document\Type;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Encoding\LinkFactory;
use JSONAPI\Mapper\Encoding\Processor\LinksProcessor;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Request\Parser;
use JSONAPI\Mapper\Test\Resources\Valid\GettersExample;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class LinksProcessorTest extends TestCase
{
    private static MetadataRepository $mr;
    private static Parser $p;

    public static function setUpBeforeClass(): void
    {
        self::$mr = MetadataFactory::create(
            [__DIR__ . '/../Resources/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        self::$p  = Parser::createDefault('http://test.unit.org', self::$mr);
    }

    /**
     * @depends testConstruct
     */
    public function testProcessRelationship(LinksProcessor $processor)
    {
        $object       = new GettersExample('uuid');
        $resource     = new ResourceObject(new Type('getter'), new Id('uuid'));
        $relationship = new Relationship('relation', $resource);
        $resource->addRelationship($relationship);
        $processor->processRelationship($relationship, $object);
        $links = $relationship->getLinks();
        $this->assertFalse($links->isEmpty());
        $this->assertInstanceOf(Link::class, $links->find(Link::SELF));
        $this->assertInstanceOf(Link::class, $links->find(Link::RELATED));
        $relationship = new Relationship('relation', $resource);
        $relationship->setData(
            new ResourceCollection([
                new ResourceObjectIdentifier(new Type('relation'), new Id('relation1')),
                new ResourceObjectIdentifier(new Type('relation'), new Id('relation2')),
                new ResourceObjectIdentifier(new Type('relation'), new Id('relation3')),
                new ResourceObjectIdentifier(new Type('relation'), new Id('relation4')),
                new ResourceObjectIdentifier(new Type('relation'), new Id('relation5'))
            ]),
            2
        );
        $links = $relationship->getLinks();
        $processor->processRelationship($relationship, $object);
        $this->assertFalse($links->isEmpty());
        $this->assertInstanceOf(Link::class, $links->find(Link::SELF));
        $this->assertInstanceOf(Link::class, $links->find(Link::RELATED));
        $this->assertInstanceOf(Link::class, $links->find(Link::FIRST));
        $this->assertInstanceOf(Link::class, $links->find(Link::NEXT));
        $this->assertInstanceOf(Link::class, $links->find(Link::LAST));
    }

    /**
     * @depends testConstruct
     */
    public function testProcessResource(LinksProcessor $processor)
    {
        $object   = new GettersExample('uuid');
        $resource = new ResourceObject(new Type('getter'), new Id('uuid'));
        $processor->processResource($resource, $object);
        $links = $resource->getLinks();
        $this->assertInstanceOf(Link::class, $links->find(Link::SELF));
    }

    public function testConstruct()
    {
        $_SERVER["REQUEST_URI"] = "/getter/uuid?filter=stringProperty eq 'value'&include=collection&fields[resource]=publicProperty,privateProperty,relations&sort=-publicProperty,privateProperty&page[offset]=10&page[limit]=20";
        $request                = ServerRequestFactory::createFromGlobals();
        $req                    = self::$p->parse($request);
        $lp                     = new LinksProcessor(self::$mr);
        $lp->setRequest($req);
        $this->assertInstanceOf(LinksProcessor::class, $lp);
        return $lp;
    }

    /**
     * @depends testConstruct
     */
    public function testProcessDocument(LinksProcessor $processor)
    {
        $data     = new GettersExample('uuid');
        $document = new Document();
        $processor->processDocument($document, $data);
        $links = $document->getLinks();
        $this->assertInstanceOf(Link::class, $links->find(Link::SELF));


        $_SERVER["REQUEST_URI"] = "/getter?page[limit]=1&page[offset]=1";
        $request                = ServerRequestFactory::createFromGlobals();
        $req                    = self::$p->parse($request);
        $processor              = new LinksProcessor(self::$mr);
        $processor->setRequest($req);
        $data                   = new ResourceCollection([
            new ResourceObject(new Type('getter'), new Id('1')),
            new ResourceObject(new Type('getter'), new Id('2')),
            new ResourceObject(new Type('getter'), new Id('3'))
        ]);
        $req->getPagination()->setTotal(3);
        $processor->processDocument($document, $data);
        $links = $document->getLinks();
        $this->assertInstanceOf(Link::class, $links->find(Link::SELF));
        $this->assertInstanceOf(Link::class, $links->find(Link::FIRST));
        $this->assertInstanceOf(Link::class, $links->find(Link::NEXT));
        $this->assertInstanceOf(Link::class, $links->find(Link::PREV));
        $this->assertInstanceOf(Link::class, $links->find(Link::LAST));
    }
}
