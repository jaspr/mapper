<?php

/**
 * Created by Lasicka <lasicka@logio.cz>
 * at 09.08.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Encoding;

use Fig\Http\Message\RequestMethodInterface;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Encoding\EncoderFactory;
use JSONAPI\Mapper\Encoding\RequestDependentProcessor;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Request\Parser;
use JSONAPI\Mapper\Test\Resources\Valid\GettersExample;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class EncoderFactoryTest extends TestCase
{
    private static MetadataRepository $metadata;

    public static function setUpBeforeClass(): void
    {
        self::$metadata = MetadataFactory::create(
            [__DIR__ . '/../Resources/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
    }

    public function testCreateDefaultEncoder()
    {
        $encoder = EncoderFactory::createDefaultEncoder(self::$metadata);
        $this->assertCount(4, $encoder->getProcessors());
        $data = new GettersExample('uuid');
        $this->assertJsonStringEqualsJsonFile(
            __DIR__ . '/testCreateDefaultEncoderResult.json',
            json_encode($encoder->compose($data))
        );
    }

    public function testCreateRequestDependentEncoder()
    {
        $rf = new ServerRequestFactory();
        $r = $rf->createServerRequest(RequestMethodInterface::METHOD_GET, 'https://unit.test.org/getter');
        $pr = Parser::createDefault('https://unit.test.org', self::$metadata);
        $request = $pr->parse($r);
        $encoder = EncoderFactory::createRequestDependentEncoder(self::$metadata);
        $this->assertCount(7, $encoder->getProcessors());
        foreach ($encoder->getProcessors() as $processor) {
            if (is_a($processor, RequestDependentProcessor::class)) {
                $processor->setRequest($request);
            }
        }
        $data = new GettersExample('uuid');
        $this->assertJsonStringEqualsJsonFile(
            __DIR__ . '/testCreateRequestDependentEncoderResult.json',
            json_encode($encoder->compose($data))
        );
    }
}
