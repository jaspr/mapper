<?php

/**
 * Created by tomas
 * at 20.03.2021 22:20
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Encoding;

use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\Meta;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\ResourceObjectIdentifier;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Encoding\EncoderFactory;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Test\Resources\Valid\BuiltInExample;
use JSONAPI\Mapper\Test\Resources\Valid\DtoValue;
use JSONAPI\Mapper\Test\Resources\Valid\DummyRelation;
use JSONAPI\Mapper\Test\Resources\Valid\GettersExample;
use JSONAPI\Mapper\Test\Resources\Valid\MetaExample;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class EncoderTest extends TestCase
{
    private static MetadataRepository $metadata;

    public static function setUpBeforeClass(): void
    {
        self::$metadata = MetadataFactory::create(
            [__DIR__ . '/../Resources/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
    }

    public function testIdentify()
    {
        $encoder = EncoderFactory::createDefaultEncoder(self::$metadata);
        $object = new GettersExample('id');
        $result = $encoder->identify($object);
        $this->assertInstanceOf(ResourceObjectIdentifier::class, $result);
        $this->assertEquals('id', $result->getId());
        $this->assertEquals('getter', $result->getType());
    }

    public function testEncode()
    {
        $encoder = EncoderFactory::createDefaultEncoder(self::$metadata);
        $object = new GettersExample('id');
        $result = $encoder->encode($object);
        $this->assertInstanceOf(ResourceObject::class, $result);
        $this->assertEquals('id', $result->getId());
        $this->assertEquals('getter', $result->getType());
        foreach ($result->getAttributesData() as $key => $value) {
            switch ($key) {
                case 'stringProperty':
                    $this->assertEquals('string value', $value);
                    break;
                case 'boolProperty':
                    $this->assertTrue($value);
                    break;
                case 'intProperty':
                    $this->assertEquals(1, $value);
                    break;
                case 'doubleProperty':
                    $this->assertEquals(.1, $value);
                    break;
                case 'arrayProperty':
                    $this->assertEquals([1, 2, 3], $value);
                    break;
                case 'dateProperty':
                    $this->assertEquals('2022-01-01T00:00:00+02:00', $value);
                    break;
                case 'dtoProperty':
                    $this->assertInstanceOf(DtoValue::class, $value);
                    break;
            }
        }
    }

    public function testRelationshipMetaEncode()
    {
        $encoder = EncoderFactory::createDefaultEncoder(self::$metadata);
        $object = new MetaExample('meta');
        $result = $encoder->encode($object);
        $this->assertInstanceOf(ResourceObject::class, $result);
        $this->assertInstanceOf(Meta::class, $result->getMeta());
        $this->assertEquals(MetaExample::class, $result->getMeta()->getProperty('for'));
        $this->assertEquals(DummyRelation::class, $result->getRelationship('relation')->getMeta()->getProperty('for'));
    }

    public function testCompose()
    {
        $encoder = EncoderFactory::createDefaultEncoder(self::$metadata);
        $data = new GettersExample('id');
        $result = $encoder->compose($data);
        $this->assertInstanceOf(Document::class, $result);
    }

    public function testEnum()
    {
        $encoder = EncoderFactory::createDefaultEncoder(self::$metadata);
        $data = new BuiltInExample();
        $result = $encoder->encode($data);
        $this->assertInstanceOf(ResourceObject::class, $result);
        $this->assertEquals('bar', $result->getAttribute('enum')->getData());
    }
}
