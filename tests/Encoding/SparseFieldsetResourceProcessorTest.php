<?php

/**
 * Created by tomas
 * at 26.05.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Encoding;

use JSONAPI\Mapper\Document\Attribute;
use JSONAPI\Mapper\Document\Id;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\Type;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Encoding\Processor\SparseFieldsetResourceProcessor;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Request\Parser;
use JSONAPI\Mapper\Test\Resources\Valid\GettersExample;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class SparseFieldsetResourceProcessorTest extends TestCase
{
    /**
     * @depends testConstruct
     */
    public function testProcessResource(SparseFieldsetResourceProcessor $processor)
    {
        $object   = new GettersExample('uuid');
        $resource = new ResourceObject(new Type('getter'), new Id('uuid'));
        $resource->addAttribute(new Attribute('field1', 'foo'));
        $resource->addAttribute(new Attribute('field2', 'bar'));
        $this->assertCount(2, $resource->getAttributes());
        $processor->processResource($resource, $object);
        $attributes = $resource->getAttributes();
        $this->assertCount(1, $attributes);
    }

    public function testConstruct()
    {
        $_SERVER["REQUEST_URI"] = "/getter/uuid?fields[getter]=field1";
        $request                = ServerRequestFactory::createFromGlobals();
        $mr                     = MetadataFactory::create(
            [__DIR__ . '/../Resources/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        $p                      = Parser::createDefault('http://test.unit.org', $mr);
        $req                    = $p->parse($request);
        $processor              = new \JSONAPI\Mapper\Encoding\Processor\SparseFieldsetResourceProcessor();
        $processor->setRequest($req);
        $this->assertInstanceOf(\JSONAPI\Mapper\Encoding\Processor\SparseFieldsetResourceProcessor::class, $processor);
        return $processor;
    }
}
