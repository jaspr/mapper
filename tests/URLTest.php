<?php

/**
 * Created by tomas
 * at 30.10.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test;

use JSONAPI\Mapper\URL;
use PHPUnit\Framework\TestCase;

class URLTest extends TestCase
{
    public function testBaseWithPathWithoutTrailingSlashWithRelativePath()
    {
        $url = URL::create('http://localhost/api/v1');
        $url->path('example/1');
        $this->assertEquals('http://localhost/api/example/1', (string)$url);
    }

    public function testBaseWithPathWithTrailingSlashWithRelativePath()
    {
        $url = URL::create('http://localhost/api/v1/');
        $url->path('example/1');
        $this->assertEquals('http://localhost/api/v1/example/1', (string)$url);
    }

    public function testBaseWithoutPathWithoutTrailingSlashWithRelativePath()
    {
        $url = URL::create('http://localhost');
        $url->path('example/1');
        $this->assertEquals('http://localhost/example/1', (string)$url);
    }

    public function testBaseWithoutPathWithoutTrailingSlashWithAbsolutePath()
    {
        $url = URL::create('http://localhost');
        $url->path('/example/1');
        $this->assertEquals('http://localhost/example/1', (string)$url);
    }

    public function testBaseWithoutPathWithTrailingSlashWithAbsolutePath()
    {
        $url = URL::create('http://localhost/');
        $url->path('/example/1');
        $this->assertEquals('http://localhost/example/1', (string)$url);
    }

    public function testBaseWithoutPathWithTrailingSlashWithRelativePath()
    {
        $url = URL::create('http://localhost/');
        $url->path('example/1');
        $this->assertEquals('http://localhost/example/1', (string)$url);
    }

    public function testEmptyBaseWithAbsolutePath()
    {
        $url = URL::create('');
        $url->path('/example/1');
        $this->assertEquals('/example/1', (string)$url);
    }

    public function testEmptyBaseWithRelativePath()
    {
        $url = URL::create('');
        $url->path('example/1');
        $this->assertEquals('example/1', (string)$url);
    }

    public function testSubstitution()
    {
        $url = URL::create('http://localhost/api/{version}/');
        $url->path('example/{id}')
            ->replace('version', 'v1')
            ->replace('id', '1');
        $this->assertEquals('http://localhost/api/v1/example/1', (string)$url);
    }

    public function testWithParam()
    {
        $url = URL::create('http://localhost/?param=value&foo=bar');
        $url->query(['test' => 'some value']);
        $this->assertEquals('http://localhost/?param=value&foo=bar&test=some%20value', (string)$url);
    }
}
