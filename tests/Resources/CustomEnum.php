<?php

/**
 * Created by Lasicka <lasicka@logio.cz>
 * at 21.06.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Resources;

enum CustomEnum: string
{
    case FOO = 'bar';
}
