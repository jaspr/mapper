<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Resources\Invalid;

use JSONAPI\Mapper\Annotation as API;

/**
 * Class BadAnnotationPlacement
 *
 * @package JSONAPI\Test\exceptions
 * @API\Resource("test")
 */
#[API\Resource("test")]
class BadAnnotationPlacement
{
    private $property;

    #[API\Id]
    public function getId(): string
    {
        return 'id';
    }
    /**
     * @return mixed
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param $property
     */
    #[API\Attribute]
    public function setProperty(
        $property
    ): void {
        $this->property = $property;
    }
}
