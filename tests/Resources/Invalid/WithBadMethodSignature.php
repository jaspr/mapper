<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Resources\Invalid;

use JSONAPI\Mapper\Annotation as API;
use JSONAPI\Mapper\Test\Resources\Valid\DummyRelation;
use JSONAPI\Mapper\Test\Resources\Valid\GettersExample;

/**
 * Class WithBadMethodSignature
 *
 * @package JSONAPI\Test\Resources\Invalid
 */
#[API\Resource("bad-signature")]
class WithBadMethodSignature
{
    private $property;

    #[API\Attribute]
    public GettersExample|DummyRelation $union;

    /**
     * @return mixed
     */
    #[API\Attribute]
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param mixed $property
     * @param       $anotherArgument
     */
    public function setProperty($property, $anotherArgument): void
    {
        $this->property = $property;
    }
}
