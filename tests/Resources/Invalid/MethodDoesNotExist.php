<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Resources\Invalid;

use JSONAPI\Mapper\Annotation as API;

/**
 * Class MethodDoesNotExist
 *
 * @package invalid
 */
#[API\Resource("method-not-exist")]
#[API\Meta("nonExistingGetter")]
class MethodDoesNotExist
{
}
