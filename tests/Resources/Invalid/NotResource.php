<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Resources\Invalid;

/**
 * Class NotResource
 *
 * @package JSONAPI\Test\exceptions
 */
class NotResource
{
}
