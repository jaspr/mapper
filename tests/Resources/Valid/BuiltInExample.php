<?php

/**
 * Created by Lasicka <lasicka@logio.cz>
 * at 21.06.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Resources\Valid;

use JSONAPI\Mapper\Annotation as API;
use JSONAPI\Mapper\Test\Resources\CustomEnum;

#[API\Resource("builtin")]
class BuiltInExample
{
    #[API\Id]
    public string $id;
    #[API\Attribute]
    public \DateTimeInterface $dateTime;
    #[API\Attribute]
    public CustomEnum $enum;
    #[API\Attribute(of: 'object')]
    public array $assoc;
    public function __construct()
    {
        $this->id = 'test';
        $this->dateTime = new \DateTimeImmutable();
        $this->enum = CustomEnum::FOO;
        $this->assoc = [
            'foo' => 'bar'
        ];
    }
}
