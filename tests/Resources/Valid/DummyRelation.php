<?php

/**
 * Created by IntelliJ IDEA.
 * User: tomas
 * Date: 24.04.2019
 * Time: 12:51
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Resources\Valid;

use Fig\Http\Message\RequestMethodInterface;
use JSONAPI\Mapper\Annotation as API;
use JSONAPI\Mapper\Metadata\Attribute;
use JSONAPI\Mapper\Metadata\Id;
use JSONAPI\Mapper\Metadata\Operation;
use JSONAPI\Mapper\Metadata\Relationship;
use JSONAPI\Mapper\Schema\Resource;
use JSONAPI\Mapper\Schema\ResourceSchema;

/**
 * Class DummyRelation
 *
 * @package JSONAPI\Test
 */
#[API\Resource("relation", operations: [
    Operation::READ,
    Operation::LIST
])]
class DummyRelation implements Resource
{
    /**
     * @var string
     */
    #[API\Id]
    public string $id;

    /**
     * @var string|null
     */
    #[API\Attribute]
    public ?string $property = null;

    /**
     * @var ThirdLevel
     */
    #[API\Relationship(ThirdLevel::class, operations: [Operation::READ])]
    public ThirdLevel $example;

    public function __construct(string $id)
    {
        $this->id = $id;
        $this->example = new ThirdLevel('ex1');
    }

    public static function getSchema(): ResourceSchema
    {
        return new ResourceSchema(
            __CLASS__,
            Id::createByProperty('id'),
            'relation',
            [Attribute::createByProperty('property')],
            [Relationship::createByProperty('example', PropsExample::class)]
        );
    }
}
