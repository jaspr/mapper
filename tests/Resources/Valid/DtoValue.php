<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Resources\Valid;

use JSONAPI\Mapper\Document\Convertible;

/**
 * Class DtoValue
 *
 * @package JSONAPI\Test\Resources\Valid
 */
class DtoValue implements Convertible
{
    /**
     * @var string
     */
    private string $word = 'string-value';

    /**
     * @var int
     */
    private int $number = 1234;

    /**
     * @var bool
     */
    private bool $boolean = true;

    /**
     * @return string
     */
    public function getStringProperty(): string
    {
        return $this->word;
    }

    /**
     * @param string $stringProperty
     */
    public function setStringProperty(string $stringProperty): void
    {
        $this->word = $stringProperty;
    }

    /**
     * @return int
     */
    public function getIntProperty(): int
    {
        return $this->number;
    }

    /**
     * @param int $intProperty
     */
    public function setIntProperty(int $intProperty): void
    {
        $this->number = $intProperty;
    }

    /**
     * @return bool
     */
    public function isBoolProperty(): bool
    {
        return $this->boolean;
    }

    /**
     * @param bool $boolProperty
     */
    public function setBoolProperty(bool $boolProperty): void
    {
        $this->boolean = $boolProperty;
    }

    /**
     * @param array $json
     *
     * @return static
     */
    public static function jsonDeserialize($json): self
    {
        $json = (array)$json;
        $self = new static();
        $self->setStringProperty($json['word']);
        $self->setIntProperty($json['number']);
        $self->setBoolProperty($json['boolean']);
        return $self;
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return array data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): array
    {
        return [
            'word' => $this->getStringProperty(),
            'number'    => $this->getIntProperty(),
            'boolean'   => $this->isBoolProperty()
        ];
    }
}
