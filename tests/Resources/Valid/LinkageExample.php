<?php

/**
 * Created by uzivatel
 * at 20.06.2022 14:25
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Resources\Valid;

use JSONAPI\Mapper\Annotation\Id;
use JSONAPI\Mapper\Annotation\Relationship;
use JSONAPI\Mapper\Annotation\Resource;

/**
 * Class LinkageExample
 *
 * @package JSONAPI\Mapper\Test\Resources\Valid
 */
#[Resource('linkage')]
class LinkageExample
{
    #[Id]
    public string $id;

    #[Relationship(target: DummyRelation::class)]
    public array $collectionProperty;
}
