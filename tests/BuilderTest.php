<?php

/**
 * Created by tomas
 * at 29.05.2023
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test;

use JSONAPI\Mapper\Compose\Builder;
use JSONAPI\Mapper\Document\DefaultNamespace;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Encoding\EncoderFactory;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Request\ParsedRequest;
use JSONAPI\Mapper\Request\Parser;
use JSONAPI\Mapper\Test\Resources\Valid\GettersExample;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class BuilderTest extends TestCase
{
    public function testBuild()
    {
        $_SERVER["REQUEST_URI"] = "/getter";
        $request                = ServerRequestFactory::createFromGlobals();
        $mr                     = MetadataFactory::create(
            [__DIR__ . '/Resources/Valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        $p                      = Parser::createDefault('http://test.unit.org', $mr);
        //Builder
        $req     = $p->parse($request);
        $encoder = EncoderFactory::createDefaultEncoder($mr);
        $builder = new Builder($req, $encoder);
        $this->assertInstanceOf(Builder::class, $builder);
        // Getters
        $this->assertInstanceOf(Encoder::class, $builder->getEncoder());
        $this->assertInstanceOf(ParsedRequest::class, $builder->getRequest());
        // Setters
        $builder->setTotal(10);
        $pagination = $req->getPagination();
        $refP        = new \ReflectionClass($pagination);
        $total      = $refP->getProperty('totalItems')->getValue($pagination);
        $this->assertEquals(10, $total);

        $data = [new GettersExample('uuid')];
        $builder->setData($data);
        $refB = new \ReflectionClass($builder);
        $innerData = $refB->getProperty('data')->getValue($builder);
        $this->assertCount(1, $innerData);
        $doc = $builder->build();
        $this->assertInstanceOf(Document::class, $doc);
    }
}
